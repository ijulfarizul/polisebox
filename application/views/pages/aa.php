<?php $this->load->view('includes/header'); ?>
<!-- BEGIN PAGE CONTENT-->

<div class="row">
    <div class="col-md-12 form-horizontal">
        <div class="form-group">
            <label class="col-md-3 control-label">Fullname</label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Fullname" id="fullname">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">IC Number</label>
            <div class="col-md-9">
                <input type="text" class="form-control input-large" placeholder="IC Number" id="nric">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">CP ID Number</label>
            <div class="col-md-9">
                <input type="text" class="form-control input-large" placeholder="CP ID Number" id="cp_id_number">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Mobile Number</label>
            <div class="col-md-9">
                <input type="text" class="form-control input-large" placeholder="Mobile Number" id="mobile_number">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Country</label>
            <div class="col-md-9">
            
                <select class="form-control input-medium" id="country">
                            <option value="-1">Please select country</option>
                            <?php foreach($this->far_location->list_all_country() as $ca => $cb){ ?>
                            <option value="<?php echo $cb['lc_id']; ?>"><?php echo $cb['lc_name']; ?></option>
                            <?php } ?>
                        </select>
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        <div class="form-group">
                    <label class="col-md-3 control-label">State</label>
                    <div class="col-md-9">
                        <select class="form-control input-medium" id="state">
                            <option value="-1">Select State</option>
                            
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
        <div class="form-group">
                    <label class="col-md-3 control-label">City</label>
                    <div class="col-md-9">
                        <select class="form-control input-medium" id="city">
                            <option value="-1">Select City</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
        <div class="form-group">
                    <label class="col-md-3 control-label">Area</label>
                    <div class="col-md-9">
                        <select multiple="multiple" class="form-control input-medium" id="area">
                            <option value="-1">Select Area</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
        <div class="form-actions">
            <div class="row">
				<div class="col-md-offset-3 col-md-9">
				    <button type="button" onclick="javascript: btn_click_submit();" class="btn green">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- END PAGE CONTENT-->
<?php $this->load->view('includes/footer'); ?>