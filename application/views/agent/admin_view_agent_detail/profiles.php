<?php $this->load->view('includes/header'); ?>
<?php $this->load->view('agent/admin_view_agent_detail/navbar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/global/plugins/multi.js/multi.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/css/intlTelInput.css">
<style>
.iti-flag {background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/img/flags.png");}

        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
          .iti-flag {background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/img/flags@2x.png");}
        }
        
        .intl-tel-input {
            width: 100% !important;
        }

</style>

<div class="row">
    <div class="col-md-12 form-horizontal">
        <div class="form-group">
            <label class="col-md-3 control-label">Fullname</label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Fullname" id="fullname" value="<?php echo $user_profile['fullname']; ?>">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">IC Number</label>
            <div class="col-md-9">
                <input type="text" class="form-control input-large" placeholder="IC Number" id="nric" value="<?php echo $user_profile['nric']; ?>">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Registration URL</label>
            <div class="col-md-9">
                <div class="input-inline input-xlarge">
				    <div class="input-group">
				        <span class="input-group-addon">
                            <?php echo base_url(); ?>
				        </span>
				        <input type="text" class="form-control" id="url_alias" value="<?php echo $user_detail['url_alias']; ?>" />
                        <span class="input-group-btn">
				            <a id="genpassword" href="<?php echo base_url(); ?><?php echo $user_detail['url_alias']; ?>" target="_blank" class="btn btn-success" type="button"><i class="fa fa-external-link-square"></i></a>
				        </span>
				    </div>
                    
				</div>
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Mobile Number</label>
            <div class="col-md-9">
                <input type="text" class="form-control input-large" placeholder="Mobile Number" id="mobile_number">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Country</label>
            <div class="col-md-9">
            
                <select class="form-control input-medium" id="country">
                            <option value="-1">Please select country</option>
                            <?php foreach($this->far_location->list_all_country() as $ca => $cb){ ?>
                            <option value="<?php echo $cb['lc_id']; ?>"><?php echo $cb['lc_name']; ?></option>
                            <?php } ?>
                        </select>
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        <div class="form-group">
                    <label class="col-md-3 control-label">State</label>
                    <div class="col-md-9">
                        <select class="form-control input-medium" id="state">
                            <option value="-1">Select State</option>
                            
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
        <div class="form-group">
                    <label class="col-md-3 control-label">City</label>
                    <div class="col-md-9">
                        <select class="form-control input-medium" id="city">
                            <option value="-1">Select City</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
        <div class="form-group">
                    <label class="col-md-3 control-label">Area</label>
                    <div class="col-md-9">
                        <select multiple="multiple" class="form-control input-medium" id="area">
                            <option value="-1">Select Area</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
        <div class="form-actions">
            <div class="row">
				<div class="col-md-offset-3 col-md-9">
				    <!--
                    <button type="button" onclick="javascript: btn_click_submit();" class="btn green">Submit</button>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>



<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/multi.js/multi.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/js/intlTelInput.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/js/utils.js"></script>

<script type="text/javascript">
$(function(){
    blockUI();
    
    
    
    $("#mobile_number").intlTelInput({
        onlyCountries: [
            "my",
            "sg",
            "id",
            "th",
            "bn",
            "hk",
            "tw",
            "mo"
        ],
        preferredCountries: [
            "my",
            "id"
        ],
        separateDialCode: true
    });
    $("#mobile_number").intlTelInput("setNumber", '<?php echo $user_profile['phonemobile']; ?>');
    
    //autoset country to malaysia
    $("#country").val(<?php echo $user_profile['cp_lc_id']; ?>);
    onchange_country();
    
    $("#country").on("change", function(){
        onchange_country();
    });
    
    //onchange_state();
    $("#state").on("change", function(){
        onchange_state();
    });
    
    //onchange_city();
    $("#city").on("change", function(){
        onchange_city();
    });
    
    
})
</script>

<script type="text/javascript">
function btn_click_submit(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    var fullname = $("#fullname").val();
    var nric = $("#nric").val();
    var areas = $("#area").val();
    var cp_id_number = $("#cp_id_number").val();
    var phonemobile = $("#mobile_number").intlTelInput("getNumber");

    
    Metronic.blockUI({
        boxed: true,
        message: 'Sending data to server...'
    });

    $.ajax({
        url: '<?php echo base_url(); ?>cp/ajax_admin_edit_volunteer',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                uacc_id: '<?php echo $user_detail['uacc_id']; ?>',
                fullname: fullname,
                nric: nric,
                cp_id_number: cp_id_number,
                phonemobile: phonemobile,
                areas: areas
            } 
        },
        success: function(data){
            Metronic.unblockUI();
            if(data.status == "success"){
                
                swal({
                    title: "Good job!",
                    text: "Volunteer has been updated",
                    type: "success",
                    closeOnConfirm: true
                })
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        if(j){
                            sweetAlert("Oops...", j, "error");
                        } else {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                        }
                        
                    }
                });
            }
            
            
        }
    })

    

}
</script>

<script type="text/javascript">
function onchange_country(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    var country = $("#country").val();
    if(country == "-1"){
        $("#country").closest('.form-group').addClass('has-error'); 
        $("#country").closest('.form-group').find('.error_message').text("Please select country").show(); return false
    }
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/list_state_by_country",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                lc_id: country
            }
        },
        success: function(data){
            if(data.status == "success"){
                var list_state = data.list_state;
                var htmlOption = "<option value='-1'>Please select state</option>"
                $.each(list_state, function(i,j){
                    htmlOption += "<option value='"+j.ls_id+"'>"+j.ls_name+"</option>"
                });
                $("#state").html(htmlOption).val(<?php echo $user_profile['cp_ls_id'] ?>);
                onchange_state();
            }else{
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        }
    })
}

function onchange_state(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    var state = $("#state").val();
    if(state == "-1"){
        $("#state").closest('.form-group').addClass('has-error'); 
        $("#state").closest('.form-group').find('.error_message').text("Please select state").show(); return false
    }
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/list_city_by_state",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                ls_id: state
            }
        },
        success: function(data){
            if(data.status == "success"){
                var list_city = data.list_city;
                var htmlOption = "<option value='-1'>Please select city</option>"
                $.each(list_city, function(i,j){
                    htmlOption += "<option value='"+j.lct_id+"'>"+j.lct_name+"</option>"
                });
                $("#city").html(htmlOption).val(<?php echo $user_profile['cp_lct_id'] ?>);
                onchange_city();
            }else{
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        }
    })
}

function onchange_city(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    var city = $("#city").val();
    if(city == "-1"){
        $("#city").closest('.form-group').addClass('has-error'); 
        $("#city").closest('.form-group').find('.error_message').text("Please select city").show(); return false
    }
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/list_area_by_city",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                lct_id: city
            }
        },
        success: function(data){
            if(data.status == "success"){
                var list_area = data.list_area;
                var selected_area = [];
                <?php $arr = explode(",",$user_profile['cp_area_id']); foreach($arr as $a => $b){ ?>
                selected_area.push('<?php echo $b; ?>');
                <?php } ?>
                
                console.log(selected_area);
                
                var htmlOption = ""
                $.each(list_area, function(i,j){
                    
                    if ($.inArray(j.area_id, selected_area)!='-1') {
                        htmlOption += "<option value='"+j.area_id+"' selected='selected'>"+j.area_name+"</option>"
                    } else {
                        htmlOption += "<option value='"+j.area_id+"'>"+j.area_name+"</option>"
                    }
                    
                    
                });
                $("#area").html(htmlOption);
                
                var select = document.getElementById('area');
                multi( select, {
                    non_selected_header: 'Choose area',
                    selected_header: 'Selected area'
                });
                $.unblockUI();
                
            }else{
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        }
    })
}
</script>