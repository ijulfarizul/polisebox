<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Far_wallet {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    /**
     * List All Wallet in Database
     * @return array
     */
    function list_all_wallet_in_db(){
        $names = array();
        $database_name = $this->CI->db->database;
        $query = $this->CI->db->query("SELECT table_name FROM information_schema.tables where table_schema='".$database_name."'");
        $rows = $query->result_array();
        foreach($rows as $a => $b){
            if(substr($b['table_name'], 0, 11) == 'far_wallet_'){
                $names[] = $b['table_name'];
            }
        }
        
        return $names;
    }
    
    /**
     * List all available wallet as defined in meta table
     * @return array
     */
    function list_all_available_wallet(){
        $output = array();
        $available_wallets = array();
        $available_wallets = $this->CI->far_meta->get_values('list_available_wallet', 'single_array');
        foreach($available_wallets as $a => $wallet_table_name){
            $insert_arr['wallet_table_name'] = $wallet_table_name;
            $insert_arr['wallet_name'] = $this->get_wallet_name($wallet_table_name);
            $output[] = $insert_arr;
        }
        return $output;
    }
    
    /**
     * Get wallet name
     * @param string $wallet_table_name Wallet table name
     * @return string If no name specify in meta table, will return nice wallet table name. eg far_wallet_alpha become Alpha Wallet.
     */
    function get_wallet_name($wallet_table_name){
        $meta_tag_name = 'wallet_name_'.$wallet_table_name;
        $wallet_name = $this->CI->far_meta->get_value($meta_tag_name);
        
        if(strlen($wallet_name) == 0){
            $x = explode('_', $wallet_table_name);
            $y = $x[2].' '.$x[1];
            $wallet_name = ucfirst($y);
        }
        
        return $wallet_name;
    }
    
    /**
     * Get total amount for a single wallet
     * @param int $uacc_id User Account ID
     * @param string $wallet_table_name Wallet table name
     * @param null $status If no wallet status specify, will default to 'success'
     * @return int Total sum wallet amount
     */
    function total_single_wallet($uacc_id, $wallet_table_name, $status = 'success'){
        $query = $this->CI->db->query('SELECT COALESCE(SUM(amount), 0) as total_amount FROM '.$wallet_table_name.' WHERE receiver="'.$uacc_id.'" AND status="'.$status.'" LIMIT 1');
        return $query->row()->total_amount;
    }
    
    /**
     * Get total amount for all wallet
     * @param int $uacc_id User Account ID
     * @param array $wallet_table_name Wallet table name. Must be in array format
     * @param null $status If no wallet status specify, will default to 'success'
     * @return int Total sum multiple wallet amount
     */
    function total_amount_multiple_wallet($uacc_id, $wallet_table_names, $status = 'success'){
        $total_amount_for_multiple_wallet = 0;
        foreach($wallet_table_names as $a => $b){
            $query = $this->CI->db->query('SELECT COALESCE(SUM(amount), 0) as total_amount FROM '.$b.' WHERE receiver="'.$uacc_id.'" AND status="'.$status.'" LIMIT 1');
            $amount = $query->row()->total_amount;
            $total_amount_for_multiple_wallet = $total_amount_for_multiple_wallet+$amount;
        }

        return $total_amount_for_multiple_wallet;
    }
    
    
    /**
     * Get statement
     * @param int $uacc_id User Account ID
     * @param int $month Month of statement. Put 00 to select all month
     * @param int $year Year of statement. Put 00 to select all year
     * @param string $wallet_table_name Wallet table name.
     * @param null $status If no wallet status specify, will default to 'success'
     */
    function monthly_statement_single($uacc_id, $month, $year, $wallet_table_name, $status = 'success', $sql_limit_start, $sql_limit_length){
        if($month == '00'){
            $sql_month = "";
        }else{
            $sql_month = "AND MONTH(create_dttm)='".$month."'";
        }
        
        if($year == '00'){
            $sql_year = "";
        }else{
            $sql_year = "AND YEAR(create_dttm)='".$year."'";
        }
        
        if(($sql_limit_start > -1) && ($sql_limit_length > 0)){
            $sql_limit = "LIMIT ".$sql_limit_start.", ".$sql_limit_length;
        }else{
            $sql_limit = "";
        }
        

        
        $credit_query = $this->CI->db->query("SELECT * FROM ".$wallet_table_name."
        
         WHERE (receiver='".$uacc_id."' AND amount >= '0') 
            ".$sql_month."
            ".$sql_year."
            AND status='".$status."'
            ORDER BY create_dttm,id DESC ".$sql_limit);
        $credit_rows = $credit_query->result_array();
        
        $debit_query = $this->CI->db->query("SELECT * FROM ".$wallet_table_name." WHERE (receiver='".$uacc_id."' AND amount < '0') 
            ".$sql_month."
            ".$sql_year."
            AND status='".$status."'
            ORDER BY create_dttm,id DESC ".$sql_limit);
        $debit_rows = $debit_query->result_array();
        
        $arr_merge = array_merge($credit_rows, $debit_rows);

        usort($arr_merge, array('Far_wallet','dateSort'));
        return $arr_merge;
    }
    
    function total_balance_id_range_single($uacc_id, $end_id, $wallet_table_name, $status = 'success'){
        $query = $this->CI->db->query('SELECT COALESCE(SUM(amount), 0) as total_amount FROM '.$wallet_table_name.' WHERE receiver="'.$uacc_id.'" AND status="'.$status.'" AND id BETWEEN "0" AND "'.$end_id.'" LIMIT 1');
        return $query->row()->total_amount;
    }
    
    
}


?>