<?php $this->load->view('includes/header'); ?>
<?php $this->load->view('shop/member_shop_detail/navbar'); ?>

<?php
$whatsapp_split = $this->far_helper->split_msisdn_and_prefix($shop_detail['whatsapp_mobile_number']);
$whatsapp_prefix = $whatsapp_split['prefix'];
$whatsapp_phonenumber = $whatsapp_split['phone_number'];

$callme_split = $this->far_helper->split_msisdn_and_prefix($shop_detail['call_mobile_number']);
$callme_prefix = $callme_split['prefix'];
$callme_phonenumber = $callme_split['phone_number'];

$smsme_split = $this->far_helper->split_msisdn_and_prefix($shop_detail['sms_mobile_number']);
$smsme_prefix = $smsme_split['prefix'];
$smsme_phonenumber = $smsme_split['phone_number'];
?>

<style>
.product_description_text {
    white-space: pre-wrap;
}
.picture-container-square {
    position: relative;
    cursor: pointer;
    text-align: center;
}


.picture-square {
    /*width: 106px;
    height: 106px;*/
    width: 100%;
    padding-top: 100%;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
    border-radius: 10px;
    margin: 5px auto;
    overflow: hidden;
    transition: all 0.2s;
    -webkit-transition: all 0.2s;
    background: url(https://www.usahawan.me/img/logo/logo-usahawan-white-bg-252x252.png) no-repeat;
    -webkit-background-size: cover !important;
    background-size: cover !important;
    
}

.picture-square input[type="file"] {
    cursor: pointer;
    display: block;
    height: 100%;
    left: 0;
    opacity: 0 !important;
    position: absolute;
    top: 0;
    width: 100%;
}

.picture-square:hover {
    border-color: #05a5a9;
}

.input-group-addon.blue {
    background-color: #025490;
    border: 1px solid #025490;
    color: #ffffff;
    border-radius: 4px;
    border-bottom-right-radius: 0px;
    border-top-right-radius: 0px;
}
</style>

<div class="row form-horizontal">
	<div class="col-md-12">
		<div class="form-group">
			<label class="col-md-3 control-label">URL</label>
			<div class="col-md-9">
				<div class="input-inline input-xlarge">
					<div class="input-group">
						<span class="input-group-addon">www.usahawan.me/</span>
						<input type="text" class="form-control" placeholder="URL" value="<?php echo $shop_detail['product_url_alias']; ?>" id="product_url_alias">
					</div>
				</div>
				<span class="help-block error_message" style="display: none;"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Product Name</label>
			<div class="col-md-9">
				<input type="text" class="form-control input-large" value="<?php echo htmlspecialchars($shop_detail['product_name']); ?>" id="product_name"> <span class="help-block error_message" style="display: none;"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Product Description</label>
			<div class="col-md-9">
				<textarea id="product_description" class="form-control"><?php echo $shop_detail['product_description']; ?>
				</textarea>
				<span class="help-block error_message" style="display: none;"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Price (RM)</label>
			<div class="col-md-9">
				<input type="text" class="form-control input-small" value="<?php echo $shop_detail['product_price']; ?>" id="product_price"> <span class="help-block error_message" style="display: none;"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">&nbsp;</label>
			<div class="col-md-9">
				<?php
                                                        
                                                        //echo '<pre>'; print_r($meta); echo '</pre>';
                                                        //product_image_additional_1
                                                        $image_picture_1 = $shop_detail['product_image_main'];
                                                        $thumb_picture_upload_1 = $meta['thumb_picture_upload_1']['value'];
                                                        if(strlen($thumb_picture_upload_1) < 5){
                                                            $thumb_picture_upload_1 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_2 = $meta['product_image_additional_1']['value'];
                                                        $thumb_picture_upload_2 = $meta['thumb_picture_upload_2']['value'];
                                                        if(strlen($thumb_picture_upload_2) < 5){
                                                            $thumb_picture_upload_2 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_3 = $meta['product_image_additional_2']['value'];
                                                        $thumb_picture_upload_3 = $meta['thumb_picture_upload_3']['value'];
                                                        if(strlen($thumb_picture_upload_3) < 5){
                                                            $thumb_picture_upload_3 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_4 = $meta['product_image_additional_3']['value'];
                                                        $thumb_picture_upload_4 = $meta['thumb_picture_upload_4']['value'];
                                                        if(strlen($thumb_picture_upload_4) < 5){
                                                            $thumb_picture_upload_4 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_5 = $meta['product_image_additional_4']['value'];
                                                        $thumb_picture_upload_5 = $meta['thumb_picture_upload_5']['value'];
                                                        if(strlen($thumb_picture_upload_5) < 5){
                                                            $thumb_picture_upload_5 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_6 = $meta['product_image_additional_4']['value'];
                                                        $thumb_picture_upload_6 = $meta['thumb_picture_upload_5']['value'];
                                                        if(strlen($thumb_picture_upload_6) < 5){
                                                            $thumb_picture_upload_6 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_6 = $meta['product_image_additional_5']['value'];
                                                        $thumb_picture_upload_6 = $meta['thumb_picture_upload_6']['value'];
                                                        if(strlen($thumb_picture_upload_6) < 5){
                                                            $thumb_picture_upload_6 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_7 = $meta['product_image_additional_6']['value'];
                                                        $thumb_picture_upload_7 = $meta['thumb_picture_upload_7']['value'];
                                                        if(strlen($thumb_picture_upload_7) < 5){
                                                            $thumb_picture_upload_7 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_8 = $meta['product_image_additional_7']['value'];
                                                        $thumb_picture_upload_8 = $meta['thumb_picture_upload_8']['value'];
                                                        if(strlen($thumb_picture_upload_8) < 5){
                                                            $thumb_picture_upload_8 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_9 = $meta['product_image_additional_8']['value'];
                                                        $thumb_picture_upload_9 = $meta['thumb_picture_upload_9']['value'];
                                                        if(strlen($thumb_picture_upload_9) < 5){
                                                            $thumb_picture_upload_9 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                                                        $image_picture_10 = $meta['product_image_additional_9']['value'];
                                                        $thumb_picture_upload_10 = $meta['thumb_picture_upload_10']['value'];
                                                        if(strlen($thumb_picture_upload_10) < 5){
                                                            $thumb_picture_upload_10 = "https://cpanel.usahawan.me/assets/global/img/usahawan/logo-transparent-252x252.png";
                                                        }
                                                        
                ?>
			<div class="row">
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_1; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-1" accept="image/*">
							<input type="hidden" id="dominant_color">
							<input type="hidden" id="picture_upload_1" class="picture-upload-input-hidden" value="<?php echo $image_picture_1; ?>" name="fileupload"> <input type="hidden" id="thumb_picture_upload_1" class="thumb_picture" value="<?php echo $thumb_picture_upload_1; ?>">
						</div>
						<h6>Pilih Gambar Utama</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_2; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-2" accept="image/*">
							<input type="hidden" id="picture_upload_2" class="picture-upload-input-hidden" value="<?php echo $image_picture_2; ?>"> <input type="hidden" id="thumb_picture_upload_2" class="thumb_picture" value="<?php echo $thumb_picture_upload_2; ?>">
						</div>
						<h6>Pilih Gambar</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_3; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-3" accept="image/*">
							<input type="hidden" id="picture_upload_3" class="picture-upload-input-hidden" value="<?php echo $image_picture_3; ?>"> <input type="hidden" id="thumb_picture_upload_3" class="thumb_picture" value="<?php echo $thumb_picture_upload_3; ?>">
						</div>
						<h6>Pilih Gambar</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_4; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-4" accept="image/*">
							<input type="hidden" id="picture_upload_4" class="picture-upload-input-hidden" value="<?php echo $image_picture_4; ?>"> <input type="hidden" id="thumb_picture_upload_4" class="thumb_picture" value="<?php echo $thumb_picture_upload_4; ?>">
						</div>
						<h6>Pilih Gambar</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_5; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-5" accept="image/*">
							<input type="hidden" id="picture_upload_5" class="picture-upload-input-hidden" value="<?php echo $image_picture_5; ?>"> <input type="hidden" id="thumb_picture_upload_5" class="thumb_picture" value="<?php echo $thumb_picture_upload_5; ?>">
						</div>
						<h6>Pilih Gambar</h6>
					</div>
				</div>
				<?php if($logged_in['is_paid_account'] == "yes"){ ?>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_6; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-6" accept="image/*">
							<input type="hidden" id="picture_upload_6" class="picture-upload-input-hidden" value="<?php echo $image_picture_6; ?>"> <input type="hidden" id="thumb_picture_upload_6" class="thumb_picture" value="<?php echo $thumb_picture_upload_6; ?>">
						</div>
						<h6>Pilih Gambar</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_7; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-7" accept="image/*">
							<input type="hidden" id="picture_upload_7" class="picture-upload-input-hidden" value="<?php echo $image_picture_7; ?>"> <input type="hidden" id="thumb_picture_upload_7" class="thumb_picture" value="<?php echo $thumb_picture_upload_7; ?>">
						</div>
						<h6>Pilih Gambar</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_8; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-8" accept="image/*">
							<input type="hidden" id="picture_upload_8" class="picture-upload-input-hidden" value="<?php echo $image_picture_8; ?>"> <input type="hidden" id="thumb_picture_upload_8" class="thumb_picture" value="<?php echo $thumb_picture_upload_8; ?>">
						</div>
						<h6>Pilih Gambar</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_9; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-9" accept="image/*">
							<input type="hidden" id="picture_upload_9" class="picture-upload-input-hidden" value="<?php echo $image_picture_9; ?>"> <input type="hidden" id="thumb_picture_upload_9" class="thumb_picture" value="<?php echo $thumb_picture_upload_9; ?>">
						</div>
						<h6>Pilih Gambar</h6>
					</div>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-3">
					<div class="picture-container-square">
						<div class="picture-square" style="background: url(<?php echo $thumb_picture_upload_10; ?>
							) no-repeat;"> <input type="file" id="wizard-picture-upload-10" accept="image/*">
							<input type="hidden" id="picture_upload_10" class="picture-upload-input-hidden" value="<?php echo $image_picture_10; ?>"> <input type="hidden" id="thumb_picture_upload_10" class="thumb_picture" value="<?php echo $thumb_picture_upload_10; ?>">
						</div>
						<h6>Pilih Gambar</h6>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label">&nbsp;</label>
		<div class="col-md-9">
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-whatsapp"></i>Wassap (dot) Me
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-3 control-label">Whatsapp Phone Number</label>
								<div class="col-md-9">
									<div class="input-group input-xlarge">
										<span class="input-group-btn input-xsmall">
										<select class="form-control" id="whatsapp_prefix">
											<option value="6010" <?php if($whatsapp_prefix=='6010'){ echo "selected='selected' ;"; } ?>>010</option>
											<option value="6011" <?php if($whatsapp_prefix=='6011'){ echo "selected='selected' ;"; } ?>>011</option>
											<option value="6012" <?php if($whatsapp_prefix=='6012'){ echo "selected='selected' ;"; } ?>>012</option>
											<option value="6013" <?php if($whatsapp_prefix=='6013'){ echo "selected='selected' ;"; } ?>>013</option>
											<option value="6014" <?php if($whatsapp_prefix=='6014'){ echo "selected='selected' ;"; } ?>>014</option>
											<option value="6015" <?php if($whatsapp_prefix=='6015'){ echo "selected='selected' ;"; } ?>>015</option>
											<option value="6016" <?php if($whatsapp_prefix=='6016'){ echo "selected='selected' ;"; } ?>>016</option>
											<option value="6017" <?php if($whatsapp_prefix=='6017'){ echo "selected='selected' ;"; } ?>>017</option>
											<option value="6018" <?php if($whatsapp_prefix=='6018'){ echo "selected='selected' ;"; } ?>>018</option>
											<option value="6019" <?php if($whatsapp_prefix=='6019'){ echo "selected='selected' ;"; } ?>>019</option>
										</select>
										</span>
										<input type="text" class="form-control" placeholder="Mobile Phone" id="whatsapp_number" value="<?php echo $whatsapp_phonenumber; ?>">
									</div>
									<span class="help-block error_message" style="display: none;"></span>
								</div>
							</div>
							<?php
                                                                        //$wassapme_text = $meta['wassapme_text']['value'];
                                                                        $wassapme_text = $shop_detail['wassapme_text'];
                                                                        if(strlen($wassapme_text) == 0){
                                                                            $wassapme_text = "Hai, Saya berminat untuk mengetahui lebih lanjut berkenaan ".$shop_detail['product_name'];
                                                                        }
                                                                        ?>
							<div class="form-group">
								<label class="col-md-3 control-label">Whatsapp Message</label>
								<div class="col-md-9">
									<input class="form-control" id="wassapme_text" value="<?php echo $wassapme_text; ?>" /> <span class="help-block error_message" style="display: none;"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label">&nbsp;</label>
		<div class="col-md-9">
			<div class="portlet box red" style="border: 1px solid #3b5998;">
				<div class="portlet-title" style="background-color: #3b5998">
					<div class="caption">
						<i class="fa fa-phone"></i>Call Me
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-3 control-label">Call Me Phone Number</label>
								<div class="col-md-9">
									<div class="input-group input-xlarge">
										<span class="input-group-btn input-xsmall">
										<select class="form-control" id="callme_prefix">
											<option value="6010" <?php if($callme_prefix=='6010'){ echo "selected='selected' ;"; } ?>>010</option>
											<option value="6011" <?php if($callme_prefix=='6011'){ echo "selected='selected' ;"; } ?>>011</option>
											<option value="6012" <?php if($callme_prefix=='6012'){ echo "selected='selected' ;"; } ?>>012</option>
											<option value="6013" <?php if($callme_prefix=='6013'){ echo "selected='selected' ;"; } ?>>013</option>
											<option value="6014" <?php if($callme_prefix=='6014'){ echo "selected='selected' ;"; } ?>>014</option>
											<option value="6015" <?php if($callme_prefix=='6015'){ echo "selected='selected' ;"; } ?>>015</option>
											<option value="6016" <?php if($callme_prefix=='6016'){ echo "selected='selected' ;"; } ?>>016</option>
											<option value="6017" <?php if($callme_prefix=='6017'){ echo "selected='selected' ;"; } ?>>017</option>
											<option value="6018" <?php if($callme_prefix=='6018'){ echo "selected='selected' ;"; } ?>>018</option>
											<option value="6019" <?php if($callme_prefix=='6019'){ echo "selected='selected' ;"; } ?>>019</option>
										</select>
										</span>
										<input type="text" class="form-control" placeholder="Mobile Phone" id="callme_number" value="<?php echo $callme_phonenumber; ?>">
									</div>
									<span class="help-block error_message" style="display: none;"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label">&nbsp;</label>
		<div class="col-md-9">
			<div class="portlet box red" style="border: 1px solid #f4731c;">
				<div class="portlet-title" style="background-color: #f4731c">
					<div class="caption">
						<i class="fa fa-commenting-o"></i>SMS Me
					</div>
				</div>
				<div class="portlet-body">
					<div class="form-group">
						<label class="col-md-3 control-label">SMS Me Phone Number</label>
						<div class="col-md-9">
							<div class="input-group input-xlarge">
								<span class="input-group-btn input-xsmall">
								<select class="form-control" id="smsme_prefix">
									<option value="6010" <?php if($smsme_prefix=='6010'){ echo "selected='selected' ;"; } ?>>010</option>
									<option value="6011" <?php if($smsme_prefix=='6011'){ echo "selected='selected' ;"; } ?>>011</option>
									<option value="6012" <?php if($smsme_prefix=='6012'){ echo "selected='selected' ;"; } ?>>012</option>
									<option value="6013" <?php if($smsme_prefix=='6013'){ echo "selected='selected' ;"; } ?>>013</option>
									<option value="6014" <?php if($smsme_prefix=='6014'){ echo "selected='selected' ;"; } ?>>014</option>
									<option value="6015" <?php if($smsme_prefix=='6015'){ echo "selected='selected' ;"; } ?>>015</option>
									<option value="6016" <?php if($smsme_prefix=='6016'){ echo "selected='selected' ;"; } ?>>016</option>
									<option value="6017" <?php if($smsme_prefix=='6017'){ echo "selected='selected' ;"; } ?>>017</option>
									<option value="6018" <?php if($smsme_prefix=='6018'){ echo "selected='selected' ;"; } ?>>018</option>
									<option value="6019" <?php if($smsme_prefix=='6019'){ echo "selected='selected' ;"; } ?>>019</option>
								</select>
								</span>
								<input type="text" class="form-control" placeholder="Mobile Phone" id="smsme_number" value="<?php echo $smsme_phonenumber; ?>">
							</div>
							<span class="help-block error_message" style="display: none;"></span>
						</div>
					</div>
					<?php
                                                                        $smsme_text = $shop_detail['smsme_text'];
                                                                        if(strlen($smsme_text) == 0){
                                                                            $smsme_text = "Hai, Saya berminat untuk mengetahui lebih lanjut berkenaan ".$shop_detail['product_name'];
                                                                        }
                                                                        ?>
					<div class="form-group">
						<label class="col-md-3 control-label">SMS Message</label>
						<div class="col-md-9">
							<input class="form-control" id="smsme_text" value="<?php echo $smsme_text; ?>" /> <span class="help-block error_message" style="display: none;"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-3 control-label">&nbsp;</label>
		<div class="col-md-9">
			<button type="button" onclick="javascript: btn_click_submit();" class="btn green">Submit <i class="fa fa-arrow-right"></i></button>
		</div>
	</div>
</div>
</div>

<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/autosize/dist/autosize.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jQuery-File-Upload-9.18.0/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jQuery-File-Upload-9.18.0/js/jquery.iframe-transport.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jQuery-File-Upload-9.18.0/js/jquery.fileupload.js"></script>
<script type="text/javascript">
function btn_click_submit(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    //check product_url_alias
    var product_url_alias = $("#product_url_alias").val();
    if (product_url_alias === false){ 
        $("#product_url_alias").closest('.form-group').addClass('has-error'); 
        $("#product_url_alias").closest('.form-group').find('.error_message').text("URL cannot be blank").show(); return false
    }
    if (product_url_alias === "") {
        $("#product_url_alias").closest('.form-group').addClass('has-error'); 
        $("#product_url_alias").closest('.form-group').find('.error_message').text("URL cannot be blank").show(); return false
    }
    
    //check product_name
    var product_name = $("#product_name").val();
    if (product_name === false){ 
        $("#product_name").closest('.form-group').addClass('has-error'); 
        $("#product_name").closest('.form-group').find('.error_message').text("Product Name cannot be blank").show(); return false
    }
    if (product_name === "") {
        $("#product_name").closest('.form-group').addClass('has-error'); 
        $("#product_name").closest('.form-group').find('.error_message').text("Product Name cannot be blank").show(); return false
    }
    
    //check price
    var product_price = $("#product_price").val();
    if (product_price === false){ 
        $("#product_price").closest('.form-group').addClass('has-error'); 
        $("#product_price").closest('.form-group').find('.error_message').text("Product Price cannot be blank").show(); return false
    }
    if (product_price === "") {
        $("#product_price").closest('.form-group').addClass('has-error'); 
        $("#product_price").closest('.form-group').find('.error_message').text("Product Price cannot be blank").show(); return false
    }
    
    //check product_description
    var product_description = $("#product_description").val();
    if (product_description === false){ 
        $("#product_description").closest('.form-group').addClass('has-error'); 
        $("#product_description").closest('.form-group').find('.error_message').text("Product Description cannot be blank").show(); return false
    }
    if (product_description === "") {
        $("#product_description").closest('.form-group').addClass('has-error'); 
        $("#product_description").closest('.form-group').find('.error_message').text("Product Description cannot be blank").show(); return false
    }
    
    //check picture_upload_1
    var picture_upload_1 = $("#picture_upload_1").val();
    if (picture_upload_1 === false){ 
        $("#picture_upload_1").closest('.form-group').addClass('has-error'); 
        $("#picture_upload_1").closest('.form-group').find('.error_message').text("Please upload main picture").show(); return false
    }
    if (picture_upload_1 === "") {
        $("#picture_upload_1").closest('.form-group').addClass('has-error'); 
        $("#picture_upload_1").closest('.form-group').find('.error_message').text("Please upload main picture").show(); return false
    }
    
    var picture_upload_2 = $("#picture_upload_2").val();
    var picture_upload_3 = $("#picture_upload_3").val();
    var picture_upload_4 = $("#picture_upload_4").val();
    var picture_upload_5 = $("#picture_upload_5").val();
    
    var thumb_picture_upload_1 = $("#thumb_picture_upload_1").val();
    var thumb_picture_upload_2 = $("#thumb_picture_upload_2").val();
    var thumb_picture_upload_3 = $("#thumb_picture_upload_3").val();
    var thumb_picture_upload_4 = $("#thumb_picture_upload_4").val();
    var thumb_picture_upload_5 = $("#thumb_picture_upload_5").val();
    
    
    <?php if($logged_in['is_paid_account'] == "yes"){ ?>
    var picture_upload_6 = $("#picture_upload_6").val();
    var picture_upload_7 = $("#picture_upload_7").val();
    var picture_upload_8 = $("#picture_upload_8").val();
    var picture_upload_9 = $("#picture_upload_9").val();
    var picture_upload_10 = $("#picture_upload_10").val();
    
    var thumb_picture_upload_6 = $("#thumb_picture_upload_6").val();
    var thumb_picture_upload_7 = $("#thumb_picture_upload_7").val();
    var thumb_picture_upload_8 = $("#thumb_picture_upload_8").val();
    var thumb_picture_upload_9 = $("#thumb_picture_upload_9").val();
    var thumb_picture_upload_10 = $("#thumb_picture_upload_10").val();
    <?php } ?>
    
    //check whatsapp_number
    var whatsapp_number = $("#whatsapp_number").val();
    if (whatsapp_number === false){ 
        $("#whatsapp_number").closest('.form-group').addClass('has-error'); 
        $("#whatsapp_number").closest('.form-group').find('.error_message').text("Whatsapp Phone Number cannot be blank").show(); return false
    }
    if (whatsapp_number === "") {
        $("#whatsapp_number").closest('.form-group').addClass('has-error'); 
        $("#whatsapp_number").closest('.form-group').find('.error_message').text("Whatsapp Phone Number cannot be blank").show(); return false
    }
    
    //check wassapme_text
    var wassapme_text = $("#wassapme_text").val();
    if (wassapme_text === false){ 
        $("#wassapme_text").closest('.form-group').addClass('has-error'); 
        $("#wassapme_text").closest('.form-group').find('.error_message').text("Whatsapp Message cannot be blank").show(); return false
    }
    if (wassapme_text === "") {
        $("#wassapme_text").closest('.form-group').addClass('has-error'); 
        $("#wassapme_text").closest('.form-group').find('.error_message').text("Whatsapp Message cannot be blank").show(); return false
    }
    
    var whatsapp_phone_number = $("#whatsapp_prefix").val()+whatsapp_number;
    var callme_phone_number = $("#callme_prefix").val()+$("#callme_number").val();
    var smsme_phone_number = $("#smsme_prefix").val()+$("#smsme_number").val();
    
    
    if(smsme_phone_number.length > 5){
        //check smsme_text
        var smsme_text = $("#smsme_text").val();
        if (smsme_text === false){ 
            $("#smsme_text").closest('.form-group').addClass('has-error'); 
            $("#smsme_text").closest('.form-group').find('.error_message').text("SMS Message cannot be blank").show(); return false
        }
        if (smsme_text === "") {
            $("#smsme_text").closest('.form-group').addClass('has-error'); 
            $("#smsme_text").closest('.form-group').find('.error_message').text("SMS Message cannot be blank").show(); return false
        }
    }
    
    
    blockUI()
    
    $.ajax({
        url: '<?php echo base_url(); ?>shop/ajax_member_edit_shop',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                fu_id: "<?php echo  $shop_detail['fu_id']; ?>",
                product_url_alias: product_url_alias, 
                product_name: product_name,
                product_description: product_description,
                product_price: product_price,
                picture_upload_1: picture_upload_1,
                picture_upload_2: picture_upload_2,
                picture_upload_3: picture_upload_3,
                picture_upload_4: picture_upload_4,
                picture_upload_5: picture_upload_5,
                
                thumb_picture_upload_1: thumb_picture_upload_1,
                thumb_picture_upload_2: thumb_picture_upload_2,
                thumb_picture_upload_3: thumb_picture_upload_3,
                thumb_picture_upload_4: thumb_picture_upload_4,
                thumb_picture_upload_5: thumb_picture_upload_5,
                
                <?php if($logged_in['is_paid_account'] == "yes"){ ?>
                picture_upload_6: picture_upload_6,
                picture_upload_7: picture_upload_7,
                picture_upload_8: picture_upload_8,
                picture_upload_9: picture_upload_9,
                picture_upload_10: picture_upload_10,
                
                thumb_picture_upload_6: thumb_picture_upload_6,
                thumb_picture_upload_7: thumb_picture_upload_7,
                thumb_picture_upload_8: thumb_picture_upload_8,
                thumb_picture_upload_9: thumb_picture_upload_9,
                thumb_picture_upload_10: thumb_picture_upload_10,
                <?php } ?>
                
                whatsapp_phone_number: whatsapp_phone_number,
                wassapme_text: wassapme_text,
                
                callme_phone_number: callme_phone_number,
                smsme_phone_number: smsme_phone_number,
                smsme_text: smsme_text
                
                
            } 
        },
        success: function(data){
            Metronic.unblockUI();
            if(data.status == "success"){
                
                swal({
                    title: "Success!",
                    text: "Shop saved successfully",
                    type: "success",
                    closeOnConfirm: true
                },
                function(){
                    //window.location = "https://retail.linkodes.com/campaign/ra_view_campaign_detail/?page=branch_list&autoopen=yes&TRUFHFIR8237ERDUERJERWE5ERT89SEDRF9SER5ERT8ERWTEWQ354RWEQ5RWER8WERT54ERWR58Q45="+data.campaign_id;
                });
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
            
            
        }
    })

    
}
</script>

<script type="text/javascript">
$(function () {
    $('#wizard-picture-upload-1,#wizard-picture-upload-2,#wizard-picture-upload-3,#wizard-picture-upload-4,#wizard-picture-upload-5,#wizard-picture-upload-6,#wizard-picture-upload-7,#wizard-picture-upload-8,#wizard-picture-upload-9,#wizard-picture-upload-10').fileupload({
        //url: "https://www.usahawan.me/js/jQuery-File-Upload-9.18.0/server/php/index.php",
        //url: "https://cpanel.usahawan.me/api/upload_image",
        url: "<?php echo base_url(); ?>proxy_upload_image.php",
        type: "POST",
        dataType: 'json',
        formData: {
            upload_key: "<?php echo $upload_key; ?>"
        },
        add: function (e, data) {
            blockUI();
            
            //data.context = $('<p/>').text('Uploading...').appendTo(document.body);
            data.submit();
        },
        done: function (e, data) {
            var result = data.result.files[0];
            var thumbnailUrl = result.thumbnailUrl;
            var pictureUrl = result.url;
            var el = e.target;
            
            
            
            //$("#wizard-picture-upload-1").closest('div.picture-square').children('img').attr('src', pictureUrl)
            a = "url('"+thumbnailUrl+"') no-repeat";
            var $picture_square = $(el).closest('div.picture-square');
            $picture_square.css('background', a)
                .css('background-size', 'cover');
            
            $picture_square.children('.picture-upload-input-hidden').val(pictureUrl);
            
            //thumbnail
            $picture_square.children('.thumb_picture').val(thumbnailUrl);
            
            //remove red error
            $picture_square.removeClass('picture_square_error');
            
            var el_id = $(el).attr("id");
            if(el_id == 'wizard-picture-upload-1'){
                //get_dominant_color(thumbnailUrl);
            }
            
            
            //process preview
            $.unblockUI();
        },
        start: function(e, data){
            blockUI();
        }
    });
});
$(function(){
    autosize($('#product_description'));
    
    autosize($('#og_description'));
})
</script>