<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class User extends REST_Controller
{
  public $post_data;

  function __construct(){
    parent::__construct();
    // Load Helper
    $this->load->helper('api_response_helper');
    $this->load->helper('common_helper');
    $this->load->helper('jwt_helper');

    // Load Library
    $this->load->library('public_transformer');
    $this->load->library('form_validation');

    // Load Model
    $this->load->model('api/common_model');
    $this->load->model('api/public/user_model');

    // Retrieve $_POST data
    $this->post_data = post_data_handler();

    // API KEY guard, check for valid api-key
    apiKeyGuard();
  }

  /**
  * @api {post} /public/user User Details/Profile 
  * @apiName UserDetails
  * @apiGroup User
  *
  * @apiHeader {String} api-key API Key, sha1 of "cpapp_ios_<app_version>" / "cpapp_android_<app_version>"
  * @apiHeader {String} Authorization login token example. "Bearer <login_token>"
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <user object>
  *   }
  * }
  */
  public function index_get() {
    $res = [];

    $auth_user = apiAuthGuard();

    $res = $this->public_transformer->transform($auth_user, 'user', true);

    api_response($res);
  }

  /**
  * @api {post} /public/user/login User Login 
  * @apiName UserLogin
  * @apiGroup User
  *
  * @apiParam {String} nric IC number
  * @apiParam {String} password Password
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <user object>
  *   }
  * }
  */
  public function login_post() {
    $res = [];

    $this->form_validation->set_data($this->post_data);
    $this->form_validation->set_rules('nric', 'nric', 'required');
    $this->form_validation->set_rules('password', 'password', 'required');

    $this->post_data['REMOTE_ADDR'] = $this->input->ip_address();

    if ($this->form_validation->run() == FALSE) {
      api_response($error_data = [], false, array_values($this->form_validation->error_array())[0]);
    }

    $user = $this->user_model->login($this->post_data['nric'], $this->post_data['password']);

    $res_data = $user;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res = $this->public_transformer->transform($user, 'user', true);

    api_response($res);
  }

  /**
  * @api {post} /public/user/change_password Change password
  * @apiName ChangePassword
  * @apiGroup User
  *
  * @apiHeader {String} api-key API Key, sha1 of "cpapp_ios_<app_version>" / "cpapp_android_<app_version>"
  * @apiHeader {String} Authorization login token example. "Bearer <login_token>"
  *
  * @apiParam {String} current_password Current Password
  * @apiParam {String} new_password New Password
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      "updated": true
  *   }
  * }
  */
  public function change_password_post() {
    $res = [];

    $auth_user = apiAuthGuard();

    $this->form_validation->set_data($this->post_data);
    $this->form_validation->set_rules('current_password', 'current_password', 'required');
    $this->form_validation->set_rules('new_password', 'new_password', 'required');
    $this->form_validation->set_rules('confirm_new_password', 'confirm_new_password', 'required|matches[new_password]');

    $this->post_data['REMOTE_ADDR'] = $this->input->ip_address();

    if ($this->form_validation->run() == FALSE) {
      api_response($error_data = [], false, array_values($this->form_validation->error_array())[0]);
    }

    $change_password = $this->user_model->changePassword($this->post_data, $auth_user['uacc_id']);

    $res_data = $change_password;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res = $res_data;

    api_response($res);
  }

  /**
  * @api {post} /public/user/register Register user
  * @apiName UserRegister
  * @apiGroup User
  *
  * @apiParam {String} fullname Full name
  * @apiParam {String} nric IC number
  * @apiParam {String} password Password
  * @apiParam {String} confirm_password Confirm Password
  * @apiParam {String} mobile Mobile number
  * @apiParam {String} email Email Address
  * @apiParam {String} address_line_1 Address line 1
  * @apiParam {String} [address_line_2] Address line 2
  * @apiParam {String} address_city City name
  * @apiParam {String} address_state_id State id
  * @apiParam {String} address_postcode Postcode
  * @apiParam {String} address_country_id Country id
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <user object>
  *   }
  * }
  */
  public function register_post() {
    $res = [];

    $this->form_validation->set_data($this->post_data);
    $this->form_validation->set_rules('fullname', 'fullname', 'required');
    $this->form_validation->set_rules('nric', 'nric', 'required|is_unique[user_accounts.uacc_username]');
    $this->form_validation->set_rules('password', 'password', 'required');
    $this->form_validation->set_rules('confirm_password', 'confirm_password', 'required|matches[password]');
    $this->form_validation->set_rules('mobile', 'mobile', 'required');
    $this->form_validation->set_rules('email', 'email', 'required');
    $this->form_validation->set_rules('address_line_1', 'address_line_1', 'required');
    $this->form_validation->set_rules('address_city', 'address_city', 'required');
    $this->form_validation->set_rules('address_state_id', 'address_state_id', 'required');
    $this->form_validation->set_rules('address_postcode', 'address_postcode', 'required');
    $this->form_validation->set_rules('address_country_id', 'address_country_id', 'required');

    $this->post_data['REMOTE_ADDR'] = $this->input->ip_address();

    if ($this->form_validation->run() == FALSE) {
      api_response($error_data = [], false, array_values($this->form_validation->error_array())[0]);
    }

    $user = $this->user_model->register($this->post_data);

    $res_data = $user;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res = $this->public_transformer->transform($user, 'user', true);

    api_response($res);
  }

}