<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announcement_model extends CI_Model {
  public $flexi_auth_config;

  function __construct(){
    parent::__construct();
    

    $this->config->load('flexi_auth', TRUE);
    $this->flexi_auth_config = $this->config->item('flexi_auth');
  }

  public function getAnnouncement( $conf = [] ) {
    $ares = [];

    $this->db->select('*');
    $this->db->from('announcement_detail');

    if(isset($conf['where'])) {
      $this->db->where($conf['where']);
    }

    if(isset($conf['limit'])) {
      $this->db->limit($conf['limit'], $conf['offset']);
    }

    $this->db->order_by('announcement_id', 'desc');


    $query = $this->db->get();
    if($query->num_rows()) {
      $ares = $query->result_array();
      
      if(isset($conf['single']) && $conf['single']) {
        $ares = $ares[0];
      }
    }

    return $ares;
  }

}


