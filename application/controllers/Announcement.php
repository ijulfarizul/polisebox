<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announcement extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function admin_list_all_announcement(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->view('announcement/admin/admin_list_all_announcement', $this->data);
    }
    
    function ajax_admin_list_all_announcement(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');
        $uacc_created_by = $this->user['uacc_id'];
        
        $this->datatables->select('anc.announcement_id');
        $this->datatables->select('anc.title');
        $this->datatables->select('anc.message');
        $this->datatables->select('anc.create_dttm');
        $this->datatables->from('announcement_detail anc');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function admin_create_announcement(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_announcement');
        
        
        $this->load->view('announcement/admin/admin_create_announcement', $this->data);
    }
    
    function ajax_admin_create_announcement(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_announcement');
        $this->load->library('far_helper');
        $error = array();
        $output = array();
        
        //check title
        if(strlen($postdata['title']) <= 5){
            $error['title'] = "Title must be at least 6 characters";
        }
        
        if(strlen($postdata['message']) <= 8){
            $error['message'] = "Message must be at least 8 characters";
        }
        
        if(count($error) == 0){
            
            //insert announcement
            $announcement_id = $this->far_announcement->insert_announcement_detail();
            $this->far_announcement->update_announcement_detail('announcement_id', $announcement_id, 'title', $postdata['title']);
            $this->far_announcement->update_announcement_detail('announcement_id', $announcement_id, 'message', $postdata['message']);
            
            $output['status'] = 'success';
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    

    
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */