<?php $this->load->view('includes/header'); ?>
<style>
.tab-pane {
    min-height: 200px;
}
</style>
<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gear"></i>Wallet Settings
							</div>
							<div class="actions">
								<a href="javascript:;" class="btn default btn-sm">
								<i class="fa fa-plus"></i> Add New Wallet </a>
								
							</div>
						</div>
						<div class="portlet-body">
							
							<div class="tabbable tabbable-tabdrop">
								<ul class="nav nav-tabs">
                                    <?php foreach($list_all_wallet_in_db as $a => $b){ ?>
									<li class="<?php if($a == 0){ echo 'active'; } ?>">
										<a href="#tab_settings_<?php echo $b; ?>" data-toggle="tab">#<?php echo $a+1; ?> Wallet</a>
									</li>
                                    <?php } ?>
								</ul>
								<div class="tab-content">
                                    <?php foreach($list_all_wallet_in_db as $a => $b){ ?>
									<div class="tab-pane <?php if($a == 0){ echo 'active'; } ?>" id="tab_settings_<?php echo $b; ?>">
										
									</div>
                                    <?php } ?>
									<div class="tab-pane" id="tab_settings_bravo">
										
									</div>
									
								</div>
							</div>
							
						</div>
					</div>

<div id="modal_admin_add_new_wallet" class="modal fade container" tabindex="-1" data-width="760">
    <div class="modal-header">
        <h4 class="modal-title">Create New Wallet</h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" role="form" id="form_wallet_create_new">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Wallet Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter wallet name" id="new_wallet_name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Description</label>
                    <div class="col-md-9">
                        <textarea class="form-control" rows="3" id="new_wallet_description"></textarea>
                    </div>
                </div>
            </div>
            
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn blue">Close</button>
        <button type="button" class="btn green" onclick="javascript: confirm_submit_create_new_group();">Submit</button>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>

<script type="text/javascript">
$(function(){
    /*
    Metronic.blockUI({
        boxed: true,
        message: 'Loading page. This will take some time...'
    });
    */
    
    //load_tab_settings_general();
    //load_tab_settings_usergroup();
    
    /*
    $(document).ajaxStop(function () {
        console.log('all ajax finish')
        Metronic.unblockUI();
    });
    */
});

function load_tab_settings_usergroup(){
    $("#tab_settings_usergroup").load('<?php echo base_url(); ?>settings/admin_ajax_load_tab_settings_usergroup', function(){
        console.log('ok - tab_settings_usergroup loaded');
    })
}

function load_tab_settings_general(){
    $("#tab_settings_general").load('<?php echo base_url(); ?>settings/admin_ajax_general', function(){
        console.log('ok - tab_settings_general loaded');
    })
}
</script>