<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Far_chicken {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
        
    }
    
    
    function count_all_contact(){
        $query = $this->CI->db->query("SELECT * FROM app_user_profile");
        return $query->num_rows();
    }
    
    function list_all_contact($limit, $start){
        $query = $this->CI->db->query("SELECT * FROM app_user_profile ORDER BY profile_id DESC LIMIT ". $start .", ".$limit);
        $rows = $query->result_array();
        return $rows;
    }
    
    function member_count_all_contact($uacc_id){
        $query = $this->CI->db->query("SELECT * FROM app_user_contact WHERE uacc_id='".$uacc_id."'");
        return $query->num_rows();
    }
    
    function member_list_all_contact($limit, $start, $uacc_id){
        $query = $this->CI->db->query("SELECT * FROM app_user_contact WHERE uacc_id='".$uacc_id."' ORDER BY create_dttm DESC LIMIT ". $start .", ".$limit);
        $rows = $query->result_array();
        return $rows;
    }
    
    function count_total_downline($uacc_id){
        $query = $this->CI->db->query("SELECT uacc_id FROM user_accounts WHERE FIND_IN_SET('".$uacc_id."', list_upline)");
        return $query->num_rows();
    }
    
    function count_total_free_user($uacc_id){
        $query = $this->CI->db->query("SELECT uacc_id FROM user_accounts WHERE uacc_activated_by_purchase='0' AND FIND_IN_SET('".$uacc_id."', list_upline)");
        return $query->num_rows();
    }
    
    function count_total_paid_member($uacc_id){
        $query = $this->CI->db->query("SELECT uacc_id FROM user_accounts WHERE uacc_activated_by_purchase='1' AND FIND_IN_SET('".$uacc_id."', list_upline)");
        return $query->num_rows();
    }
    
    function get_order($column, $value)	
	{		
		$query = $this->CI->db->query('SELECT * FROM btc_orders WHERE '.$column.'="'.$value.'"');		
		return $query->row_array();			
	}
    
    function update_order($key, $key_value, $column, $value){
        $data = array(
            $column => $value
            );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('btc_orders', $data); 
    }
    
    function delete_order($invoice_code){
        //app_user_profile
        $this->CI->db->where('invoice_code', $invoice_code);
        $this->CI->db->delete('btc_orders');
    }
    
    function delete_app_user_profile($uacc_id){
        //app_user_profile
        $this->CI->db->where('uacc_id', $uacc_id);
        $this->CI->db->delete('app_user_profile');
    }
    
    function delete_app_card($uacc_id){
        //app_card
        $this->CI->db->where('uacc_id', $uacc_id);
        $this->CI->db->delete('app_cards');
    }
    function delete_app_user_contact($uacc_id){
        //app_user_contact
        $this->CI->db->where('uacc_id', $uacc_id);
        $this->CI->db->delete('app_user_contact');
    }
    
    function delete_app_user_contact_id($uacc_id){
        //app_user_contact
        $this->CI->db->where('contact_id', $uacc_id);
        $this->CI->db->delete('app_user_contact');
    }
    
    function delete_app_user_contact_by_api($uacc_id, $contact_id){
        
        $this->CI->db->where('contact_id', $contact_id);
        $this->CI->db->where('uacc_id', $uacc_id);
        $this->CI->db->delete('app_user_contact');
    }
    
    function delete_user($uacc_id){
        //app_user_contact
        $this->CI->db->where('uacc_id', $uacc_id);
        $this->CI->db->delete('user_accounts');
    }
    
    function delete_user_profile($uacc_id){
        //app_user_contact
        $this->CI->db->where('upro_uacc_fk', $uacc_id);
        $this->CI->db->delete('demo_user_profiles');
    }
    
    function delete_app_namecard($uacc_id){
        $this->CI->db->where('uacc_id', $uacc_id);
        $this->CI->db->delete('app_namecard');
    }
    
    function delete_app_namecard_by_namecard_id($namecard_id){
        $this->CI->db->where('id', $namecard_id);
        $this->CI->db->delete('app_namecard');
    }
    
    function get_namecards($column, $value){
        $query = $this->CI->db->query('SELECT * FROM app_namecard WHERE '.$column.'="'.$value.'" ORDER BY id DESC');		
		return $query->result_array();	
    }
    
    function get_btc_unilevel($column, $value){
        $query = $this->CI->db->query('SELECT * FROM btc_unilevel_products WHERE '.$column.'="'.$value.'"');		
		return $query->row_array();		
    }
    
    function count_invoice_complete_dttm_and_status($date_start, $date_end, $status){
        $query = $this->CI->db->query('SELECT count(uacc_id) as count_total FROM btc_orders WHERE status="'.$status.'" AND DATE(complete_dttm) BETWEEN "'.$date_start.'" AND "'.$date_end.'"');	
        return $query->row()->count_total;
    }
    
    function list_invoice_by_complete_dttm_and_status($date_start, $date_end, $status){
        $query = $this->CI->db->query('SELECT * FROM btc_orders WHERE status="'.$status.'" AND DATE(complete_dttm) BETWEEN "'.$date_start.'" AND "'.$date_end.'"');	
		return $query->result_array();
    }
    
    function list_invoice_by_create_dttm_and_status($date_start, $date_end, $status){
        $query = $this->CI->db->query('SELECT * FROM btc_orders WHERE status="'.$status.'" AND DATE(create_dttm) BETWEEN "'.$date_start.'" AND "'.$date_end.'"');	
		return $query->result_array();
    }
    
    function list_invoice_by_create_dttm($date_start, $date_end){
        $query = $this->CI->db->query('SELECT * FROM btc_orders WHERE DATE(create_dttm) BETWEEN "'.$date_start.'" AND "'.$date_end.'"');	
		return $query->result_array();
    }
    
    function get_uplines($uacc_id, $count){
        $this->CI->load->library('far_users');
        $upline_arr = array();
        $user = $this->CI->far_users->get_user('uacc_id', $uacc_id);
        //$upline_arr[] = $user['uacc_upline_id'];
        
        
        
        if($count < 1){
            echo 'error count upline';
            exit();
        }
        $upline_id = $user['uacc_upline_id'];
        for($i=1; $i <= $count; $i++){
            
            $upline = $this->CI->far_users->get_user('uacc_id', $upline_id);
            $upline_arr[] = $upline['uacc_id'];
            $upline_id = $upline['uacc_upline_id'];
            
        }
        
        return $upline_arr;
    }
    
    function list_all_potential_income_for_user($rcxid){
        $list_potential_income = array();
        $query = $this->CI->db->query("SELECT * FROM far_wallet WHERE rcxid='".$rcxid."' AND status='potential_income' AND type='unilevel_bonus'");
        $rows = $query->result_array();
        return $rows;
    }
    
    function list_all_potential_income_expired($expired_dttm){
        $list_potential_income = array();
        $query = $this->CI->db->query("SELECT * FROM far_wallet WHERE status='potential_income' AND expired_dttm <= '".$expired_dttm."'");
        $rows = $query->result_array();
        return $rows;
    }
    
    function update_wallet_status_potential_income($wallet_id, $update_status, $update_dttm){
        $update_dttm = date('Y-m-d H:i:s');
        $query = $this->CI->db->query('UPDATE far_wallet SET status="'.$update_status.'", update_dttm="'.$update_dttm.'" WHERE id="'.$wallet_id.'" AND status="potential_income"');
    }
    
    function update_all_potential_income_for_user($rcxid, $status, $update_dttm){
        $query = $this->CI->db->query('UPDATE far_wallet SET status="'.$status.'", update_dttm="'.$update_dttm.'" WHERE rcxid="'.$rcxid.'" AND status="potential_income"');
    }
    
    function list_all_user_for_admin(){
        $query = $this->CI->db->query("SELECT *,u.uacc_id as u_uacc_id FROM user_accounts u LEFT JOIN app_user_profile app ON u.uacc_id=app.uacc_id ORDER BY u.uacc_id DESC");
        $rows = $query->result_array();
        return $rows;
    }
    
    function get_upline_id($uacc_id){
        $query = $this->CI->db->query("SELECT 
            uacc_id,
            uacc_username,
            uacc_upline_id,
            uacc_activated_by_purchase,
            uacc_email,
            is_leader,
            leader_bonus
        FROM user_accounts WHERE uacc_id='".$uacc_id."'");
        $rows = $query->row_array();
        return $rows;
    }
    
    function get_first_leader($uacc_id){
        $list_upline = array();

        $user_first = $this->get_upline_id($uacc_id);
        $upline_first = $this->get_upline_id($user_first['uacc_upline_id']);
        
        $list_upline[] = array(
            'uacc_id'   => $upline_first['uacc_id'],
            'is_leader' => $upline_first['is_leader']
        );
        
        $level = 0; 
        $switch = 'on';
        $level_limit = 12;
        $t_uacc_id = $upline_first['uacc_upline_id'];
        do {
            
            //list the upline
            $upline = $this->get_upline_id($t_uacc_id);
            
            $list_upline[] = array(
                'uacc_id'   => $upline['uacc_id'],
                'is_leader' => $upline['is_leader']
            );
            
            if($upline['is_leader'] == '1'){
                $switch = 'off';
            }else{
                
                $level++;
            }
            
            $t_uacc_id = $upline['uacc_upline_id'];
            
        } while ($switch == 'on');
        
        
        return $list_upline;
    }
    
    
    function get_all_upline_compress_by_uacc_activated_purchase($uacc_id){
        
        $list_upline = array();

        $user_first = $this->get_upline_id($uacc_id);
        $upline_first = $this->get_upline_id($user_first['uacc_upline_id']);
        if($upline_first['uacc_activated_by_purchase'] == '1'){
            $list_upline[] = $upline_first['uacc_id'];
            $level_limit = 11;
        }else{
            $level_limit = 12;
        }
        
        
        
        $level = 0; 
        $switch = 'on';
        //$level_limit = 12;
        $t_uacc_id = $upline_first['uacc_upline_id'];
        do {
            
            //list the upline
            $upline = $this->get_upline_id($t_uacc_id);
            
            //$list_upline[] = $upline;
            
            if($level == $level_limit){
                $switch = 'off';
            }else{
                
                if($upline['uacc_activated_by_purchase'] == '1'){
                    $list_upline[] = $upline['uacc_id'];
                    $level++;
                }else{
                    //$list_upline[] = $upline['uacc_id'];
                }
            }
            
            $t_uacc_id = $upline['uacc_upline_id'];
            
        } while ($switch == 'on');
        
        /*
        for($level = 1; $level <= 12; $level++){
            
            
            
            $upline = array(
                'level' => $level
            );
            
            $list_upline[] = $upline;
        }
        */

        
        
        return $list_upline;
    }
    
    function get_all_upline($uacc_id){
        
        $list_upline = array();

        $user_first = $this->get_upline_id($uacc_id);
        //$list_upline[] = $user_first['uacc_upline_id'];

        $level = 0; 
        $switch = 'on';
        $level_limit = 12;
        $t_uacc_id = $user_first['uacc_upline_id'];
        do {
            
            
            
            //list the upline
            $upline = $this->get_upline_id($t_uacc_id);
            
            $list_upline[] = $upline['uacc_id'];
            $level++;
            
            if($level == $level_limit){
                $switch = 'off';
            }
            
            $t_uacc_id = $upline['uacc_upline_id'];
            
        } while ($switch == 'on');
        
        return $list_upline;
    }
    
    function generateNewPassword($length = 8) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    function deduct_count_total_downline_for_each_upline($uacc_id){
        
        $list_all_upline = $this->get_all_upline($uacc_id);
        
        foreach($list_all_upline as $a => $b){
            $sql = "UPDATE user_accounts SET count_total_downline = count_total_downline-1 WHERE uacc_id='".$b."'";
            $query = $this->CI->db->query($sql);
        }
    }
    
    function create_leader_schedule($data){
        
        
        $this->CI->db->insert('far_leader_schedule', $data); 
        $last_insert_id = $this->CI->db->insert_id();
        
        return $last_insert_id;
    }
    
    function list_all_leader(){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE is_leader='1' ORDER BY uacc_id DESC");
        return $query->result_array();
    }
    
    function count_leader_register_user($uacc_id, $year, $month){
        
        $query = $this->CI->db->query("SELECT COUNT(uacc_id) as count_total FROM far_leader_schedule WHERE uacc_id='".$uacc_id."' AND MONTH(created_dttm)='".$month."' AND YEAR(created_dttm)='".$year."'");
        return $query->row()->count_total;
    }
    
}

?>