<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/typeahead/typeahead.css">
<style>
.control-text {
    font-size: 14px;
    font-weight: normal;
    margin-top: 7px;
}
</style>

<div class="row">
    <div class="col-md-12 form-horizontal">
        <div class="form-body">
            
            
            <ul class="nav nav-tabs">
                <li class="active">
				    <a href="#tab_newuser" data-toggle="tab">
				        Update Profile
                    </a>
				</li>
				
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="tab_newuser">
                    <h3 class="block">User details</h3>
				    <div class="form-group">
                        <label class="col-md-3 control-label">Fullname</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Enter Fullname" id="fullname" value="<?php echo ucwords(strtolower($user_profile['fullname'])); ?>" />
                            <span class="help-block error_message" style="display: none;"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">IC Number</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Enter IC Number" value="<?php echo ucwords(strtolower($user_profile['nric'])); ?>" id="nric" />
                            <span class="help-block error_message" style="display: none;"></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Enter Email" value="<?php echo strtolower($user_profile['email']); ?>" id="email" />
                            <span class="help-block error_message" style="display: none;"></span>
                        </div>
                    </div>
				</div>
				
            </div>

            
            
            <h3 class="block">Bank Details</h3>
            <div class="form-group">
                <label class="col-md-3 control-label">Bank Name</label>
                <div class="col-md-9">
                    <select class="form-control" id="bn_id">
                        <?php foreach($this->far_helper->list_all_bank() as $a => $b){ ?>
                        <option <?php if($user_profile['bank_id'] == $b['bn_id']){ echo "selected"; } ?> value="<?php echo $b['bn_id']; ?>"><?php echo $b['bn_name']; ?></option>
                        <?php } ?>
                    </select>
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Account Holder</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="Enter Holder Name" value="<?php echo ucwords(strtolower($user_profile['bank_account_holder'])); ?>" id="bank_account_holder" />
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Account Number</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="Enter Account Number" value="<?php echo ucwords(strtolower($user_profile['bank_account_number'])); ?>" id="bank_account_number" />
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            
            <h3 class="block">Shipping details</h3>
            <div class="form-group">
                <label class="col-md-3 control-label">Address 1</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="Enter Address 1" value="<?php echo ucwords(strtolower($user_profile['address_1'])); ?>" id="address_1" />
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Address 2</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="Enter Address 2" value="<?php echo ucwords(strtolower($user_profile['address_2'])); ?>" id="address_2" />
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Postcode</label>
                <div class="col-md-9">
                    <input type="text" class="form-control input-small" placeholder="Enter Postcode" value="<?php echo ucwords(strtolower($user_profile['postcode'])); ?>" id="postcode" />
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">City</label>
                <div class="col-md-9">
                    <input type="text" class="form-control input-medium" placeholder="Enter City" value="<?php echo ucwords(strtolower($user_profile['city'])); ?>" id="city" />
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Country</label>
                <div class="col-md-9">
                    <select class="form-control input-medium" id="country">
                        <option value="1">Malaysia</option>
                    </select>
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">State</label>
                <div class="col-md-9">
                    <select class="form-control input-medium" id="state">
                        <option>Select State</option>
                        <?php foreach($this->far_location->list_state(1) as $a => $b){ ?>
                        <option <?php if($b['ls_id'] == $user_profile['ls_id']){ echo 'selected'; } ?> value="<?php echo $b['ls_id']; ?>"><?php echo $b['ls_name']; ?></option>
                        <?php } ?>
                    </select>
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            
        </div>
        <div class="form-actions">
            <div class="row">
				<div class="col-md-offset-3 col-md-9">
				    <button type="button" onclick="javascript: btn_click_submit_save_profile();" class="btn green">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
<br />
<br />
<br />
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/components-dropdowns.js"></script>




<script type="text/javascript">
$(function(){
    ComponentsDropdowns.init();
    
    
});

function btn_click_submit_save_profile(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    //check fullname
        var fullname = $("#fullname").val();
        if (fullname === false){ 
            $("#fullname").closest('.form-group').addClass('has-error'); 
            $("#fullname").closest('.form-group').find('.error_message').text("Fullname cannot blank").show(); return false
        }
        if (fullname === "") {
            $("#fullname").closest('.form-group').addClass('has-error'); 
            $("#fullname").closest('.form-group').find('.error_message').text("Fullname cannot blank").show(); return false
        }
        
        
        //check email
        var email = $("#email").val();
        if (email === false){ 
            $("#email").closest('.form-group').addClass('has-error');
            $("#email").closest('.form-group').find('.error_message').text("Email cannot blank").show(); return false
        }
        if (email === "") {
            $("#email").closest('.form-group').addClass('has-error');
            $("#email").closest('.form-group').find('.error_message').text("Email cannot blank").show(); return false
        }
    
    var nric = $("#nric").val();
    
    
    //check address 1
    var address_1 = $("#address_1").val();
    if (address_1 === false){ 
        $("#address_1").closest('.form-group').addClass('has-error');
        $("#address_1").closest('.form-group').find('.error_message').text("Address 1 cannot blank").show(); return false
    }
    if (address_1 === "") {
        $("#address_1").closest('.form-group').addClass('has-error');
        $("#address_1").closest('.form-group').find('.error_message').text("Address 1 cannot blank").show(); return false
    }
    
    var address_2 = $("#address_2").val();
    
    //check postcode
    var postcode = $("#postcode").val();
    if (postcode === false){ 
        $("#postcode").closest('.form-group').addClass('has-error');
        $("#postcode").closest('.form-group').find('.error_message').text("Postcode cannot blank").show(); return false
    }
    if (postcode === "") {
        $("#postcode").closest('.form-group').addClass('has-error');
        $("#postcode").closest('.form-group').find('.error_message').text("Postcode cannot blank").show(); return false
    }
    
    //check city
    var city = $("#city").val();
    if (city === false){ 
        $("#city").closest('.form-group').addClass('has-error');
        $("#city").closest('.form-group').find('.error_message').text("City cannot blank").show(); return false
    }
    if (city === "") {
        $("#city").closest('.form-group').addClass('has-error');
        $("#city").closest('.form-group').find('.error_message').text("City cannot blank").show(); return false
    }
    
    //check state
    var state = $("#state").val();
    if (state === false){ 
        $("#state").closest('.form-group').addClass('has-error');
        $("#state").closest('.form-group').find('.error_message').text("State cannot blank").show(); return false
    }
    if (state === "Select State") {
        $("#state").closest('.form-group').addClass('has-error');
        $("#state").closest('.form-group').find('.error_message').text("State cannot blank").show(); return false
    }
    
    var country = $("#country").val();
    
    var bn_id = $("#bn_id").val();
    var bank_account_holder = $("#bank_account_holder").val();
    var bank_account_number = $("#bank_account_number").val();
    
    Metronic.blockUI({
        boxed: true,
        message: 'Sending data to server...'
    });
    
    $.ajax({
        url: '<?php echo base_url(); ?>profile/ajax_update_profile',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                fullname: fullname, 
                nric: nric,
                email: email,
                address_1: address_1,
                address_2: address_2,
                postcode: postcode,
                city: city,
                state: state,
                country: country,
                bn_id: bn_id,
                bank_account_holder: bank_account_holder,
                bank_account_number: bank_account_number
                
            } 
        },
        success: function(data){
            Metronic.unblockUI();
            if(data.status == "success"){
                
                swal({
                    title: "Good job!",
                    text: "Your profile has been updated!",
                    type: "success",
                    closeOnConfirm: true
                },
                function(){
                    //window.location = "<?php echo base_url(); ?>order/view/"+data.invoice_number;
                });
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
            
            
        }
    })
}

</script>
<!--
<script type="text/javascript" src="<?php echo base_url(); ?>demo_new_order.js?<?php echo time(); ?>"></script>
-->