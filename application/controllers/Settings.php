<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends MY_Controller {
    private $user;
    //private $error = array();
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function admin_general(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        $this->load->view('settings/admin_general', $this->data);
    }
    
    function admin_ajax_general(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        
        $this->load->view('settings/admin_ajax_general', $this->data);
    }
    
    function ci_admin_ajax_general(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        
        if(strlen($postdata['title']) < 5){
            $this->error(array(
                'text' => 'Title must be more than 5 character',
                'element_id' => 'settings_general_title'
            ));
        }
        
        if(strlen($postdata['footer_text']) < 5){
            $this->error(array(
                'text' => 'Footer Text must be more than 5 character',
                'element_id' => 'settings_general_footer_text'
            ));
        }
        
        if(count($this->error) == 0){
            //update title
            $this->far_meta->update_value('title', $postdata['title']);
            $this->far_meta->update_value('footer_text', $postdata['footer_text']);
            $output['status'] = 'success';
        }else{
            $output['status'] = 'error';
            $output['error_msg'] = $this->error;
        }
        
        echo json_encode($output);
        
    }
    
    function admin_ajax_load_tab_settings_usergroup(){
        
        $sql_select = 'ugrp_id, ugrp_name, ugrp_desc, ugrp_admin';
        $list_user_group = $this->flexi_auth->get_groups($sql_select)->result_array();
        
        $this->data['list_user_group'] = $list_user_group;
        $this->load->view('settings/admin_ajax_load_tab_settings_usergroup', $this->data);
    }
    
    function admin_ajax_create_new_group(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        
        
    }
    
    function ajax_admin_change_user_password(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        
        $postdata = $this->input->post('postdata');
        
        
        
        if(strlen($postdata['new_password']) <= 5){
            $this->error(array(
                'text' => 'New Password must be more that 5 character',
                'element_id' => 'input_new_password'
            ));
        }
        
        
        if(count($this->error) == 0){
            
            //change user password
            if($this->flexi_auth->change_password($postdata['uacc_id'], $postdata['current'], $postdata['new_password'])){
                $this->far_users->update_user('uacc_id', $postdata['uacc_id'], 'uacc_raw_password', $postdata['new_password']);
                $output['status'] = 'success';
            }else{
                $output['status'] = 'error';
                
            }
            
            
            
        }else{
            $output['status'] = 'error';
            $output['error_msg'] = $this->error;
        }
        
        echo json_encode($output);
    }
    
    function admin_wallet(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        
        //list all wallet
        $list_all_wallet_in_db = $this->far_wallet->list_all_wallet_in_db();
        $this->data['list_all_wallet_in_db'] = $list_all_wallet_in_db;
        
        $this->load->view('settings/admin_wallet', $this->data);
    }
    
    /**
     * Police station
     */
    function admin_list_police_station(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        
        $list_state = $this->far_location->list_state(163);
        $this->data['list_state'] = $list_state;
        
        $this->load->view('settings/admin_list_police_station', $this->data); 
    }
    
    function ajax_admin_list_police_station(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');

        
        $this->datatables->select('ps.ps_id');
        $this->datatables->select('ps.ps_name');
        $this->datatables->select('ps.address');
        $this->datatables->select('ps.postcode');
        $this->datatables->select('ls.ls_name');
        $this->datatables->select('lct.lct_name');
        $this->datatables->select('area.area_name');
        $this->datatables->select('ps.tel_landline');
        $this->datatables->select('ps.email');
        $this->datatables->from('police_station ps');
        $this->datatables->join('location_state ls', 'ps.ls_id=ls.ls_id', 'left');
        $this->datatables->join('location_city lct', 'ps.lct_id=lct.lct_id', 'left');
        $this->datatables->join('location_area area', 'ps.area_id=area.area_id', 'left');
        
        //$this->datatables->order_by('cc_id desc');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function ajax_admin_delete_police_station(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_police_station');
        $error = array();
        $output = array();
        
        if(count($error) == 0){
            
            //delete from database
            $this->far_police_station->delete_police_station($postdata['ps_id']);
            
            $output['status'] = 'success';
        }else{
            
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
    function ajax_admin_add_new_police_station(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_police_station');
        $error = array();
        $output = array();
        
        //school name
        if(strlen($postdata['ps_name']) <= 3){
            $error['ps_name'] = "Police Station name must be more than 3 character";
        }
    
        if(count($error) == 0){
            
            //insert police station
            $data_insert = array(
                'ps_name' => $postdata['ps_name']
            );
            $ps_id = $this->far_police_station->insert_police_station($data_insert);
            
            //update
            $this->far_police_station->update_police_station('ps_id', $ps_id, 'address', $postdata['address']);
            $this->far_police_station->update_police_station('ps_id', $ps_id, 'postcode', $postdata['postcode']);
            $this->far_police_station->update_police_station('ps_id', $ps_id, 'ls_id', $postdata['ls_id']);
            $this->far_police_station->update_police_station('ps_id', $ps_id, 'lct_id', $postdata['lct_id']);
            $this->far_police_station->update_police_station('ps_id', $ps_id, 'area_id', $postdata['area_id']);
            $this->far_police_station->update_police_station('ps_id', $ps_id, 'tel_landline', $postdata['tel_landline']);
            $this->far_police_station->update_police_station('ps_id', $ps_id, 'email', $postdata['email']);
            $this->far_police_station->update_police_station('ps_id', $ps_id, 'latitude', $postdata['latitude']);
            $this->far_police_station->update_police_station('ps_id', $ps_id, 'longitude', $postdata['longitude']);
            
            $output['status'] = 'success';
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    function ajax_admin_get_police_station(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_police_station');
        
        $police_station_detail = $this->far_police_station->get_police_station('ps_id', $postdata['ps_id']);
        echo json_encode($police_station_detail);
    }
    
    function ajax_admin_edit_police_station(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_police_station');
        $error = array();
        $output = array();
        
        //school name
        if(strlen($postdata['ps_name']) <= 3){
            $error['edit_ps_name'] = "Police Station name must be more than 3 character";
        }
    
        if(count($error) == 0){
            
            //update
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'ps_name', $postdata['ps_name']);
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'address', $postdata['address']);
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'postcode', $postdata['postcode']);
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'ls_id', $postdata['ls_id']);
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'lct_id', $postdata['lct_id']);
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'area_id', $postdata['area_id']);
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'tel_landline', $postdata['tel_landline']);
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'email', $postdata['email']);
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'latitude', $postdata['latitude']);
            $this->far_police_station->update_police_station('ps_id', $postdata['ps_id'], 'longitude', $postdata['longitude']);
            
            $output['status'] = 'success';
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    /**
     * App Version
     */
    function admin_app_version(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        $this->load->library('far_meta');
        
        $version_android = $this->far_meta->get_value('app_version_android');
        $this->data['version_android'] = $version_android;
        
        $version_ios = $this->far_meta->get_value('app_version_ios');
        $this->data['version_ios'] = $version_ios;
        
        $publish_store_status_android = $this->far_meta->get_value('publish_store_status_android');
        $this->data['publish_store_status_android'] = $publish_store_status_android;
        
        $publish_store_status_ios = $this->far_meta->get_value('publish_store_status_ios');
        $this->data['publish_store_status_ios'] = $publish_store_status_ios;
        
        $this->load->view('settings/admin_app_version', $this->data);
    }
    
    function admin_update_app_version(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_meta');
        $error = array();
        $output = array();
        
        $this->far_meta->update_value('app_version_android', $postdata['version_android']);
        $this->far_meta->update_value('app_version_ios', $postdata['version_ios']);
        
        $this->far_meta->update_value('publish_store_status_android', $postdata['publish_store_status_android']);
        $this->far_meta->update_value('publish_store_status_ios', $postdata['publish_store_status_ios']);
        
        $output['status'] = 'success';
        
        echo json_encode($output);
    }
    
    /**
     * Location
     */
    function admin_list_all_country(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        $this->load->view('location/admin_list_all_country', $this->data);
    }
    
    function ajax_admin_list_all_country(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');
        
        $this->datatables->select('lc.lc_id');
        $this->datatables->select('lc.lc_code');
        $this->datatables->select('lc.lc_name');
        $this->datatables->from('location_country lc');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function admin_list_all_state(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        $this->load->view('location/admin_list_all_state', $this->data);
    }
    
    function ajax_admin_list_all_state(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');
        
        $this->datatables->select('ls.ls_id');
        $this->datatables->select('ls.ls_name');
        $this->datatables->select('lc.lc_name');
        $this->datatables->from('location_state ls');
        $this->datatables->join('location_country lc', 'ls.lc_id=lc.lc_id', 'left');
        //$this->datatables->from('location_state ls');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function admin_list_all_city(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        $this->load->view('location/admin_list_all_city', $this->data);
    }
    
    function ajax_admin_list_all_city(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');
        
        $this->datatables->select('lct.lct_id');
        $this->datatables->select('lct.lct_name');
        $this->datatables->select('ls.ls_name');
        $this->datatables->select('lc.lc_name');
        $this->datatables->from('location_city lct');
        $this->datatables->join('location_state ls', 'ls.ls_id=lct.ls_id', 'left');
        $this->datatables->join('location_country lc', 'ls.lc_id=lc.lc_id', 'left');
        //$this->datatables->from('location_state ls');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function admin_list_all_area(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        $this->load->view('location/admin_list_all_area', $this->data);
    }
    
    function ajax_admin_list_all_area(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');
        
        $this->datatables->select('area.area_id');
        $this->datatables->select('area.area_name');
        $this->datatables->select('lct.lct_name');
        $this->datatables->select('ls.ls_name');
        $this->datatables->select('lc.lc_name');
        $this->datatables->from('location_area area');
        $this->datatables->join('location_city lct', 'area.lct_id=lct.lct_id', 'left');
        $this->datatables->join('location_state ls', 'ls.ls_id=lct.ls_id', 'left');
        $this->datatables->join('location_country lc', 'ls.lc_id=lc.lc_id', 'left');
        //$this->datatables->from('location_state ls');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function ajax_admin_delete_area(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $error = array();
        $output = array();
        
        if(count($error) == 0){
            
            //delete from database
            $this->far_location->delete_area($postdata['area_id']);
            
            $output['status'] = 'success';
        }else{
            
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
    function ajax_add_new_area(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $error = array();
        $output = array();
        
        //school name
        if(strlen($postdata['area']) < 5){
            $error['area'] = "Location name must be more than 5 character";
        }
    
        if(count($error) == 0){
            
            //insert school
            $data_location = array(
                'area_name' => $postdata['area'],
                'lct_id' => $postdata['lct_id']
            );
            $sc_id = $this->far_location->insert_area($data_location);
            
            
            $output['status'] = 'success';
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    function ajax_admin_get_area(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        
        $area_detail = $this->far_location->get_area('area_id', $postdata['area_id']);
        echo json_encode($area_detail);
    }
    
    function ajax_add_new_city(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $error = array();
        $output = array();
        
        //school name
        if(strlen($postdata['city']) < 3){
            $error['city'] = "City name must be more than 3 character";
        }
    
        if(count($error) == 0){
            
            //insert state
            $data_city = array(
                'ls_id' => $postdata['ls_id'],
                'lct_name' => $postdata['city']
            );
            $sc_id = $this->far_location->insert_city($data_city);
            
            
            $output['status'] = 'success';
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    function ajax_add_new_state(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $error = array();
        $output = array();
        
        //school name
        if(strlen($postdata['state']) < 5){
            $error['state'] = "State name must be more than 5 character";
        }
    
        if(count($error) == 0){
            
            //insert state
            $data_state = array(
                'lc_id' => $postdata['lc_id'],
                'ls_name' => $postdata['state']
            );
            $sc_id = $this->far_location->insert_state($data_state);
            
            
            $output['status'] = 'success';
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    function ajax_admin_delete_state(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $error = array();
        $output = array();

        if(count($error) == 0){
            
            //delete from database
            $this->far_location->delete_state($postdata['ls_id']);
            
            
            $output['status'] = 'success';
        }else{
            $output['message_single'] = current($error);
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
    function ajax_admin_delete_city(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $error = array();
        $output = array();

        if(count($error) == 0){
            
            //delete from database
            $this->far_location->delete_city($postdata['lct_id']);
            
            
            $output['status'] = 'success';
        }else{
            $output['message_single'] = current($error);
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
    function list_state_by_country(){
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $output = array();
        $list_state = $this->far_location->list_state($postdata['lc_id']);
        
        $output['status'] = "success";
        $output['list_state'] = $list_state;
        
        echo json_encode($output);
        
    }
    
    function list_city_by_state(){
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $output = array();
        $list_city = $this->far_location->list_city_by_state($postdata['ls_id']);
        
        $output['status'] = "success";
        $output['list_city'] = $list_city;
        
        echo json_encode($output);
    }
    
    function list_area_by_city(){
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $output = array();
        $list_area = $this->far_location->list_area_by_city($postdata['lct_id']);
        
        $output['status'] = "success";
        $output['list_area'] = $list_area;
        
        echo json_encode($output);
    }
    
    
    /**
     * OneSignal push notification
     */
    function admin_push_notification(){
        
        $meta_onesignal_app_id = $this->far_meta->get_value('onesignal_app_id');
        $this->data['meta_onesignal_app_id'] = $meta_onesignal_app_id;
        
        $meta_onesignal_api_key = $this->far_meta->get_value('onesignal_api_key');
        $this->data['meta_onesignal_api_key'] = $meta_onesignal_api_key;
        
        $this->load->view('settings/admin_push_notification', $this->data);
    }
    
    function admin_update_onesignal_configuration(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_meta');
        $error = array();
        $output = array();
        
        if(count($error) == 0){
            
            if(isset($postdata['onesignal_app_id'])){
                $this->far_meta->update_value('onesignal_app_id', $postdata['onesignal_app_id']);
            }
            
            if(isset($postdata['onesignal_api_key'])){
                $this->far_meta->update_value('onesignal_api_key', $postdata['onesignal_api_key']);
            }
            
            
            
            $output['status'] = 'success';
        }else{
            
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */