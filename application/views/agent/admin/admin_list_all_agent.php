<?php $this->load->view('includes/header'); ?>
<a href="<?php echo base_url(); ?>agent/admin_add_new_agent" class="btn blue">
    <i class="fa fa-plus"></i> Add New Agent
</a>
<br />
<br />
<div class="table-container">
    <table id="example" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Registration Date</th>
                <th>Fullname</th>
                <th>Total QR Code</th>
                <th>Actions<sup>(s)</sup></th>
            </tr>
        </thead>                             
    </table>
</div>
<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>
<script type="text/javascript">
$(function(){
    
    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>agent/datatable_admin_list_all_agent",
            "type": "POST"
        },
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "columns": [
            { "data": "uacc_date_added" },
            { "data": "fullname" },
            { "data": "total_count_qrcode" },
            { "data": "uacc_id", "render": function ( data, type, row ) {
                var html = "";
                html += '<a href="<?php echo base_url(); ?>agent/admin_view_agent_detail/?uacc_id='+row.uacc_id+'" class="btn btn-xs green"><i class="fa fa-search"></i> View</a>';
                html += '<a href="javascript: void(0);" onclick="javascript: btn_login_company(\''+row.uacc_id+'\');" class="btn btn-xs blue"><i class="fa fa-sign-in"></i> Login as Agent</a>';
                return html;
            } },
        ],
        "order": [0, 'desc']
    } );

})
</script>
<script type="text/javascript">
function btn_login_company(uacc_id){
    swal({
      title: "Are you sure?",
      text: "You will gain full access to this account",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, sure!",
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    function(){
        $.ajax({
            url: "<?php echo base_url(); ?>users/admin_login_as_a_user",
            type: "POST",
            dataType: "json",
            data: {
                postdata: {
                    uacc_id: uacc_id
                }
            },
            success: function(data){
                if(data.status == 'success'){
                    swal({
                        title: "Success",
                        text: "Redirecting you to User Account",
                        type: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        closeOnConfirm: false
                    });
                    
                    window.location = '<?php echo base_url(); ?>auth_admin/dashboard';
                }else{
                    var single_error_msg = data.single_error_msg;
                    sweetAlert("Oops...", single_error_msg+"! Please contact admin", "error");
                }
            }
        })
    });
}
</script>