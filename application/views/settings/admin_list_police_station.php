<?php $this->load->view('includes/header'); ?>
<div class="clearfix" style="margin-bottom: 20px;">
    <a href="javascript: void(0);" onclick="javascript: modal_add_new_police_station();" class="btn green">
        Add New Police Station <i class="fa fa-plus"></i>
    </a>
</div>
<div class="table-container">
    <table id="example" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>#ID</th>
                <th>Name</th>
                <th>Address</th>
                <th>Postcode</th>
                <th>State</th>
                <th>City</th>
                <th>Area</th>
                <th>Tel</th>
                <th>Email</th>
                <th>Actions<sup>(s)</sup></th>
            </tr>
        </thead>                             
    </table>
</div>
<div id="modal_add_new_police_station" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add new police station</h4>
    </div>
    <div class="modal-body" id="modal_body_add_new_category">
        <div class="row">
            <div class="col-md-12 form-horizontal">
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter Police Station Name" id="ps_name">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Address</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter Police Station Address" id="address">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Postcode</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Postcode" id="postcode">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">State</label>
                    <div class="col-md-9">
                        <select class="form-control" id="state">
                            <?php foreach($list_state as $a => $b){ ?>
                            <option value="<?php echo $b['ls_id']; ?>"><?php echo $b['ls_name']; ?></option>
                            <?php } ?>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">City</label>
                    <div class="col-md-9">
                        <select class="form-control" id="lct_id">
                            <option value="-1">Please select city</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Area</label>
                    <div class="col-md-9">
                        <select class="form-control" id="area_id">
                            <option value="-1">Please select area</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Telephone</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Telephone" id="tel_landline">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Email</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Email" id="email">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Latitude</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Latitude" id="latitude">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Longitude</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Longitude" id="longitude">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn red pull-left" data-dismiss="modal">Close</button>
        <button type="button" onclick="javascript: btn_add_new_police_station();" class="btn green pull-right">Submit <i class="fa fa-arrow-right"></i></button>
    </div>
</div>

<div id="modal_edit_police_station" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit police station</h4>
    </div>
    <div class="modal-body" id="modal_body_edit_category">
        <div class="row">
            <div class="col-md-12 form-horizontal">
                
                <div class="form-group">
                    <label class="col-md-3 control-label">Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter Police Station Name" id="edit_ps_name">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Address</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter Police Station Address" id="edit_address">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Postcode</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Postcode" id="edit_postcode">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">State</label>
                    <div class="col-md-9">
                        <select class="form-control" id="edit_state">
                            <?php foreach($list_state as $a => $b){ ?>
                            <option value="<?php echo $b['ls_id']; ?>"><?php echo $b['ls_name']; ?></option>
                            <?php } ?>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">City</label>
                    <div class="col-md-9">
                        <select class="form-control" id="edit_lct_id">
                            <option value="-1">Please select city</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Area</label>
                    <div class="col-md-9">
                        <select class="form-control" id="edit_area_id">
                            <option value="-1">Please select area</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Telephone</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Telephone" id="edit_tel_landline">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Email</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Email" id="edit_email">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Latitude</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Latitude" id="edit_latitude">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Longitude</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Longitude" id="edit_longitude">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" id="edit_ps_id" />
        <button type="button" class="btn red pull-left" data-dismiss="modal">Close</button>
        <button type="button" onclick="javascript: btn_edit_police_station();" class="btn green pull-right">Submit <i class="fa fa-arrow-right"></i></button>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>
<script type="text/javascript">
$(function(){
    //onchange_state();
    $("#state,#edit_state").on("change", function(){
        var selected = $(this).val();
        var element_id = $(this).attr("id");
        onchange_state(element_id, selected);
    }); 
    
    $("#lct_id,#edit_lct_id").on("change", function(){
        var selected = $(this).val();
        var element_id = $(this).attr("id");
        onchange_city(element_id, selected);
    }); 
});
</script>

<script type="text/javascript">
function onchange_city(element_id, city){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    if(city == "-1"){
        $("#city").closest('.form-group').addClass('has-error'); 
        $("#city").closest('.form-group').find('.error_message').text("Please select state").show(); return false
    }
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/list_area_by_city",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                lct_id: city
            }
        },
        success: function(data){
            if(data.status == "success"){
                var list_area = data.list_area;
                var htmlOption = "<option value='-1'>Please select area</option>"
                $.each(list_area, function(i,j){
                    htmlOption += "<option value='"+j.area_id+"'>"+j.area_name+"</option>"
                });
                if(element_id == 'lct_id'){
                    $("#area_id").html(htmlOption);
                } else if (element_id == 'edit_lct_id'){
                    $("#edit_area_id").html(htmlOption);
                }
                
                //check if default value set
                var el_data, $elm;
                if(element_id == 'lct_id'){
                    el_data = $("#area_id").data();
                    $elm = $("#area_id");
                } else if (element_id == 'edit_lct_id'){
                    el_data = $("#edit_area_id").data();
                    $elm = $("#edit_area_id");
                }
                
                if(el_data.default_value){
                    $elm.val(el_data.default_value);
                    
                }
                
            }else{
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        }
    })
}
</script>

<script type="text/javascript">
function onchange_state(element_id, state){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    if(state == "-1"){
        $("#state").closest('.form-group').addClass('has-error'); 
        $("#state").closest('.form-group').find('.error_message').text("Please select state").show(); return false
    }
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/list_city_by_state",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                ls_id: state
            }
        },
        success: function(data){
            if(data.status == "success"){
                var list_city = data.list_city;
                var htmlOption = "<option value='-1'>Please select city</option>"
                $.each(list_city, function(i,j){
                    htmlOption += "<option value='"+j.lct_id+"'>"+j.lct_name+"</option>"
                });
                if(element_id == 'state'){
                    $("#lct_id").html(htmlOption);
                } else if (element_id == 'edit_state'){
                    $("#edit_lct_id").html(htmlOption);
                }
                
                //check if default value set
                var el_data, $elm;
                if(element_id == 'state'){
                    el_data = $("#lct_id").data();
                    $elm = $("#lct_id");
                } else if (element_id == 'edit_state'){
                    el_data = $("#edit_lct_id").data();
                    $elm = $("#edit_lct_id");
                }
                
                if(el_data.default_value){
                    $elm.val(el_data.default_value);
                    $elm.trigger("change");
                }
                
            }else{
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        }
    })
}
</script>

<script type="text/javascript">
function quick_edit(ps_id){
    
    Metronic.blockUI({
        boxed: true,
        message: 'Loading data from server...'
    });
    
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/ajax_admin_get_police_station",
        type: "POST",
        dataType: "JSON",
        data: {
            postdata: {
                ps_id: ps_id
            }
        },
        success: function(data){
            
            Metronic.unblockUI();
            $("#edit_ps_id").val(data.ps_id);
            $("#edit_ps_name").val(data.ps_name);
            $("#edit_address").val(data.address);
            $("#edit_postcode").val(data.postcode);
            $("#edit_state").val(data.ls_id).trigger("change");
            $("#edit_lct_id").data('default_value', data.lct_id);
            $("#edit_area_id").data('default_value', data.area_id);
            $("#edit_tel_landline").val(data.tel_landline);
            $("#edit_email").val(data.email);
            $("#edit_latitude").val(data.latitude);
            $("#edit_longitude").val(data.longitude);
            $("#modal_edit_police_station").modal("show");
        }
    })
}
</script>
<script type="text/javascript">
function modal_add_new_police_station(){
    $("#modal_add_new_police_station").modal("show");
    onchange_state('state', $("#state").val());
    
}
</script>
<script type="text/javascript">
var datatable_el;
$(function(){
    
    $('#modal_add_new_police_station').on('shown.bs.modal', function() {
        $("#ps_name").focus();
    });
    
    datatable_el = $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>settings/ajax_admin_list_police_station",
            "type": "POST"
        },
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "columns": [
            { "data": "ps_id" },
            { "data": "ps_name" },
            { "data": "address" },
            { "data": "postcode" },
            { "data": "ls_name" },
            { "data": "lct_name" },
            { "data": "area_name" },
            { "data": "tel_landline" },
            { "data": "email" },
            { "data": "ps_id", "render": function ( data, type, row ) {
                redited = '';
                //redited += '<a href="javascript: void(0);" onclick="javascript: quick_edit_section1(\''+row.el_id+'\');" class="btn btn-xs green"><i class="fa fa-pencil"></i></a>';
                redited += '<a href="javascript: void(0);" onclick="javascript: quick_delete(\''+row.ps_id+'\');" class="btn btn-xs red"><i class="fa fa-times"></i></a>'
                redited += '<a href="javascript: void(0);" onclick="javascript: quick_edit(\''+row.ps_id+'\');" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>'
                return redited
            } },
        ],
        "order": [0, 'desc']
    } );

})
</script>

<script type="text/javascript">
function quick_delete(ps_id){
    swal({
        title: "Are you sure?",
        text: "This action cannot be undone",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
    function(){
        
        $.ajax({
            url: "<?php echo base_url(); ?>settings/ajax_admin_delete_police_station",
            type: "POST",
            dataType: "json",
            data: {
                postdata: {
                    ps_id: ps_id
                }
            },
            success: function(data){
                if(data.status == "success"){
                    datatable_el.ajax.reload(function(){
                        swal("Deleted!", "Police Station has been deleted successfully", "success");
                    });
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        })
        
        
    });
}
</script>


<script type="text/javascript">
function btn_add_new_police_station(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    
    //check category name
    var ps_name = $("#ps_name").val();
    if (ps_name === false){ 
        $("#ps_name").closest('.form-group').addClass('has-error');
        $("#ps_name").closest('.form-group').find('.error_message').text("Name cannot blank").show(); return false
    }
    if (ps_name === "") {
        $("#ps_name").closest('.form-group').addClass('has-error');
        $("#ps_name").closest('.form-group').find('.error_message').text("Name cannot blank").show(); return false
    }
    
    var address = $("#address").val();
    var postcode = $("#postcode").val();
    var state = $("#state").val();
    var tel_landline = $("#tel_landline").val();
    var email = $("#email").val();
    var latitude = $("#latitude").val();
    var longitude = $("#longitude").val();
    var lct_id = $("#lct_id").val();
    var area_id = $("#area_id").val();
    
    Metronic.blockUI({
        target: "#modal_add_new_police_station",
        boxed: true,
        message: 'Sending data to server...'
    });
    
    $.ajax({
        url: '<?php echo base_url(); ?>settings/ajax_admin_add_new_police_station',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                ps_name: ps_name,
                address: address,
                postcode: postcode,
                ls_id: state,
                lct_id: lct_id,
                area_id: area_id,
                tel_landline: tel_landline,
                email: email,
                latitude: latitude,
                longitude: longitude
            } 
        },
        success: function(data){
            Metronic.unblockUI("#modal_add_new_police_station");
            if(data.status == "success"){
                
                swal({
                    title: "Success!",
                    text: "New police station mode has been added",
                    type: "success",
                    closeOnConfirm: true
                },
                function(){
                    //$("#modal_add_new_state").modal("hide");
                    Metronic.blockUI({
                        target: "#modal_add_new_police_station",
                        boxed: true,
                        message: 'Reloading data...'
                    });
                    datatable_el.ajax.reload(function(){
                        Metronic.unblockUI("#modal_add_new_police_station");
                        $("#modal_add_new_police_station").modal("hide");
                        $("#ps_name").val("");
                        $("#address").val("");
                        $("#postcode").val("");
                        $("#state").val("");
                        $("#tel_landline").val("");
                        $("#email").val("");
                        $("#latitude").val("");
                        $("#longitude").val("");
                    });
                    //window.location = "<?php echo base_url(); ?>order/view/"+data.invoice_number;
                });
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
            
            
        }
    })
}
</script>

<script type="text/javascript">
function btn_edit_police_station(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    //check category name
    var edit_ps_name = $("#edit_ps_name").val();
    if (edit_ps_name === false){ 
        $("#edit_ps_name").closest('.form-group').addClass('has-error');
        $("#edit_ps_name").closest('.form-group').find('.error_message').text("Category name cannot blank").show(); return false
    }
    if (edit_ps_name === "") {
        $("#edit_ps_name").closest('.form-group').addClass('has-error');
        $("#edit_ps_name").closest('.form-group').find('.error_message').text("Category name cannot blank").show(); return false
    }
    
    var ps_id = $("#edit_ps_id").val();
    var address = $("#edit_address").val();
    var postcode = $("#edit_postcode").val();
    var state = $("#edit_state").val();
    var tel_landline = $("#edit_tel_landline").val();
    var email = $("#edit_email").val();
    var latitude = $("#edit_latitude").val();
    var longitude = $("#edit_longitude").val();
    var lct_id = $("#edit_lct_id").val();
    var area_id = $("#edit_area_id").val();
    
    Metronic.blockUI({
        target: "#modal_edit_police_station",
        boxed: true,
        message: 'Saving data to server...'
    });
    
    $.ajax({
        url: '<?php echo base_url(); ?>settings/ajax_admin_edit_police_station',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                ps_name: edit_ps_name,
                ps_id: ps_id,
                address: address,
                postcode: postcode,
                ls_id: state,
                lct_id: lct_id,
                area_id: area_id,
                tel_landline: tel_landline,
                email: email,
                latitude: latitude,
                longitude: longitude
            } 
        },
        success: function(data){
            Metronic.unblockUI("#modal_edit_police_station");
            if(data.status == "success"){
                
                swal({
                    title: "Success!",
                    text: "Police Station successfully updated",
                    type: "success",
                    closeOnConfirm: true
                },
                function(){
                    //$("#modal_add_new_state").modal("hide");
                    Metronic.blockUI({
                        target: "#modal_edit_police_station",
                        boxed: true,
                        message: 'Reloading data...'
                    });
                    datatable_el.ajax.reload(function(){
                        Metronic.unblockUI("#modal_edit_police_station");
                        $("#modal_edit_police_station").modal("hide");
                        $("#edit_ps_name").val("");
                        $("#edit_address").val("");
                        $("#edit_postcode").val("");
                        $("#edit_state").val("");
                        $("#edit_tel_landline").val("");
                        $("#edit_email").val("");
                        $("#edit_latitude").val("");
                        $("#edit_longitude").val("");
                    });
                    //window.location = "<?php echo base_url(); ?>order/view/"+data.invoice_number;
                });
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
            
            
        }
    })
}
</script>