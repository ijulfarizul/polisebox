<?php $this->load->view('includes/header'); ?>
<?php
$user_group = $this->far_users->get_group($logged_in['ugrp_id']);

        if(strlen($user_profile['profilepic_url']) > 4){
            $url_of_picture = $user_profile['profilepic_url'];
        }else{
            $url_of_picture = base_url()."assets/global/img/intelmx/male_default_user.png";
        }
?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>


<style>
.profile-img-container {
    position: relative;
    display: inline-block; /* added */
    overflow: hidden; /* added */
}
.profile-img-container img:hover {
    opacity: 0.5
}
/*  Image Hover effect */
.profile-img-container img {
    opacity: 1;
   transition: opacity .25s ease-in-out;
   -moz-transition: opacity .25s ease-in-out;
   -webkit-transition: opacity .25s ease-in-out;
}

.profile-img-container:hover img {
    opacity: 0.5
}
.profile-img-container:hover a {
    opacity: 1; /* added */
    top: 0; /* added */
    z-index: 500;
}
/* added */
.profile-img-container:hover a span {
    top: 50%;
    position: absolute;
    left: 0;
    right: 0;
    transform: translateY(-50%);
}
/* added */
.profile-img-container a {
    display: block;
    position: absolute;
    top: -100%;
    opacity: 0;
    left: 0;
    bottom: 0;
    right: 0;
    text-align: center;
    color: inherit;
}

</style>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN PAGE CONTENT-->
<div class="row margin-top-20">
				<div class="col-md-12">
                    <?php //echo '<pre>'; print_r($logged_in); echo '</pre>'; ?>
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic profile-img-container">
								<img src="<?php echo $url_of_picture; ?>" class="img-responsive" alt="">
                                <a href="javascript: void(0);" ><span class="fa fa-upload fa-5x"></span></a>
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div class="profile-usertitle-name">
									 <?php echo ucwords(strtolower($user_profile['fullname'])); ?>
								</div>
								<div class="profile-usertitle-job">
									 <?php echo $user_group['ugrp_name']; ?>
								</div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
							
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav">
									<li class="active">
										<a href="<?php echo base_url(); ?>profile/account_settings">
										<i class="icon-settings"></i>
										Account Settings </a>
									</li>
									
									
								</ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
						<!-- PORTLET MAIN -->
						
						<!-- END PORTLET MAIN -->
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
										</div>
										<ul class="nav nav-tabs">
											
											
											<li class="active">
												<a href="#tab_change_password" data-toggle="tab">Change Password</a>
											</li>
											<li>
												<a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
											</li>
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											
											<!-- END PERSONAL INFO TAB -->
											
											<!-- CHANGE PASSWORD TAB -->
											<div class="tab-pane active" id="tab_change_password">
												<form action="#">
													<div class="form-group">
														<label class="control-label">Current Password</label>
														<input type="password" class="form-control" id="chpass_current" />
													</div>
													<div class="form-group">
														<label class="control-label">New Password</label>
														<input type="password" class="form-control" id="chpass_new" />
													</div>
													<div class="form-group">
														<label class="control-label">Re-type New Password</label>
														<input type="password" class="form-control" id="chpass_retype_new" />
													</div>
													<div class="margin-top-10">
														<a href="javascript:;" class="btn green-haze" onclick="javascript: confirm_change_password();">
														Change Password </a>
														
													</div>
												</form>
											</div>
											<!-- END CHANGE PASSWORD TAB -->
											<!-- PRIVACY SETTINGS TAB -->
											<div class="tab-pane" id="tab_1_4">
												<form action="#">
													<table class="table table-light table-hover">
													<tr>
														<td>
															 Receive monthly promotion email?
														</td>
														<td>
															<label class="uniform-inline">
															<input type="radio" name="optionsRadios1" value="option1"/>
															Yes </label>
															<label class="uniform-inline">
															<input type="radio" name="optionsRadios1" value="option2" checked/>
															No </label>
														</td>
													</tr>
													<tr>
														<td>
															 Hide my email from others ?
														</td>
														<td>
															<label class="uniform-inline">
															<input type="checkbox" value=""/> Yes </label>
														</td>
													</tr>
													
													
													</table>
													<!--end profile-settings-->
													<div class="margin-top-10">
														<a href="javascript:;" class="btn green-haze">
														Save Changes </a>
														<a href="javascript:;" class="btn default">
														Cancel </a>
													</div>
												</form>
											</div>
											<!-- END PRIVACY SETTINGS TAB -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
   
<div id="modal_upload_profile_picture" class="modal fade container" tabindex="-1" data-width="760">
    <div class="modal-header">
        <h4 class="modal-title">Upload Profile Picture</h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" role="form" id="form_settings_general">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Group Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter group name" id="new_group_name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Description</label>
                    <div class="col-md-9">
                        <textarea class="form-control" rows="3" id="new_group_description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-9">
                        <label> <input type="checkbox" value="yes" id="new_is_admin" /> Allowed to Member / Admin Area </label>
                    </div>
                </div>
            </div>
            
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn blue">Close</button>
        <button type="button" class="btn green" onclick="javascript: confirm_submit_create_new_group();">Submit</button>
    </div>
</div>
<!-- END PAGE CONTENT-->
<?php $this->load->view('includes/footer'); ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/profile.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
    Profile.init(); // init page demo
});

function open_modal_upload_profile_picture(){
    $("#modal_upload_profile_picture").modal('show');
}

function confirm_save_profile(){
    var postdata = {
        profile_firstname: $("#profile_firstname").val(),
        profile_lastname : $("#profile_lastname").val()
    }
    swal({   
        title: "Are you sure ?",      
        type: "warning",   
        showCancelButton: true,   
        closeOnConfirm: false,
        closeOnCancel: false,   
        showLoaderOnConfirm: true,
        confirmButtonText: "Confirm !",
    }, function(isConfirm){   
        if(isConfirm){
            
            $.ajax({
                url: '<?php echo base_url(); ?>profile/ci_member_save_profile',
                type: 'post',
                dataType: 'json',
                data: { postdata },
                success: function(data){
                    $('.form-group').removeClass('has-error');
                    $('.form-group .help-inline').text('');
                    var status = data.status;
                    if(status == 'success'){
                        //reload frame
                        //load_tab_settings_general();
                        
                        setTimeout(function(){     
                            swal("Success!", "Operation success!", "success")
                        }, 1500);
                    }else{
                        
                        var error_msg = data.error_msg;
                        $(error_msg).each(function(i, j){
                            var element_id = '#'+j.element_id;
                            var element = $(element_id);
                            
                            //clear all error
                            element.closest('.form-group').addClass('has-error');
                            
                            //check if help-inline exists
                            var help_inline_exists = element.siblings('.help-inline').length;
                            if(help_inline_exists == 0){
                                element.parent().append('<span class="help-inline">Error !</span>');
                            }
                            element.siblings('.help-inline').text(j.text);
                        });
                        
                        swal("Error!", "There some problem when submitting the data.", "error")
                    }
                }
            })
            
            
        }else{
            swal({   
                type: "error",   
                title: "Cancelled",   
                text: "Actions has been canceled.",   
                timer: 1500,   
                showConfirmButton: false 
            });
            
        }
        
    });
}

function confirm_change_password(){
    var current = $("#chpass_current").val();
    var new_password = $("#chpass_new").val();
    var retype_password = $("#chpass_retype_new").val();
    
    var postdata = {
        current: current,
        new_password : new_password,
        retype_password : retype_password
    }
    swal({   
        title: "Are you sure ?",      
        type: "warning",   
        showCancelButton: true,   
        closeOnConfirm: false,
        closeOnCancel: false,   
        showLoaderOnConfirm: true,
        confirmButtonText: "Confirm !",
    }, function(isConfirm){   
        if(isConfirm){
            
            $.ajax({
                url: '<?php echo base_url(); ?>profile/ci_member_change_password',
                type: 'post',
                dataType: 'json',
                data: { postdata },
                success: function(data){
                    $('.form-group').removeClass('has-error');
                    $('.form-group .help-inline').text('');
                    var status = data.status;
                    if(status == 'success'){
                        //reload frame
                        //load_tab_settings_general();
                        
                        setTimeout(function(){     
                            swal("Success!", "Operation success!", "success")
                        }, 1500);
                    }else{
                        
                        var error_msg = data.error_msg;
                        $(error_msg).each(function(i, j){
                            var element_id = '#'+j.element_id;
                            var element = $(element_id);
                            
                            //clear all error
                            element.closest('.form-group').addClass('has-error');
                            
                            //check if help-inline exists
                            var help_inline_exists = element.siblings('.help-inline').length;
                            if(help_inline_exists == 0){
                                element.parent().append('<span class="help-inline">Error !</span>');
                            }
                            element.siblings('.help-inline').text(j.text);
                        });
                        
                        swal("Error!", "There some problem when submitting the data.", "error")
                    }
                }
            })
            
            
        }else{
            swal({   
                type: "error",   
                title: "Cancelled",   
                text: "Actions has been canceled.",   
                timer: 1500,   
                showConfirmButton: false 
            });
            
        }
        
    });
    
    
}


</script>