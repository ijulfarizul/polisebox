<?php $this->load->view('includes/header'); ?>

<?php
$CI =& get_instance();
$post_info = $this->input->post('search');

//$this->load->library('far_products');

//$CI->load->library('far_users');

?>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>List All User
                </div>
            </div>
            <div class="portlet-body" id="bonus_report_body">
                <form action="<?php echo base_url(); ?>users/admin_list_all_user" method="post" class="horizontal-form" id="searchform">
                    <input type="hidden" name="search_user" value="search_user" />
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Enter Search Query ( Fullname / Email / Phone )</label>
                                    <input type="text" id="fullName" name="search[search_all]" class="form-control" value="<?php echo $post_info['search_all'];?>" />
                                </div>
                            </div>
                        
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">Membership</label>
                                    <div>
                                        <select class="form-control select2me" name="search[membership]" id="select_group_id">
                                            <option <?php if($post_info['membership'] == 'all'){ echo 'selected'; } ?> value="all">All</option>
                                            <?php foreach($list_all_user_group as $a => $b){ ?>
                                            <option <?php if($post_info['membership'] == $b['ugrp_id']){ echo 'selected'; } ?> value="<?php echo $b['ugrp_id']; ?>"><?php echo $b['ugrp_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="form-actions right" style="text-align: right;">
                        <button type="submit" value="search_term" name="search_term" class="btn blue"><i class="fa fa-check"></i> Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php if(is_array($search_return)){ ?>
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">              
                    <i class="fa fa-cogs"></i>User information          
                </div>
            </div>
        
            <div class="portlet-body flip-scroll">
                <table class="table table-striped table-bordered table-hover" id="datatable_user">
                    <thead>
                        <tr>
                        
                        <th>ID</th>
                        
                        <th>Username</th>
                        
                        <th>Fullname</th>
                        
                        <th>Email</th>
                        
                        <th>Membership</th>
                        
                        <th>Downline<sup>total</sup></th>
                        <th>Action</th>
                        
                        </tr>
                    
                    </thead>
                
                
                    <tbody>
                    <?php //echo '<pre>'; print_r($search_return); echo '</pre>'; ?>
                    <?php foreach($search_return as $key => $val){ ?>
                    
                    <?php 
                    $user_group = $this->far_users->get_user_group($val['uacc_id']);
                    ?>
                        <tr>
                        
                            <td><?php echo $val['uacc_id']; ?></td>
                        
                            <td><?php echo $val['uacc_username']; ?></td>
                        
                            <td><?php echo $val['firstname'].' '.$val['lastname']; ?></td>
                        
                            <td><?php echo $val['uacc_email'] ?></td>
                        
                        <td>
                        <?php
                        echo $user_group['ugrp_name'];
                        ?>
                        </td>
                        
                        
                        <td>
                        <?php echo $val['count_total_downline']; ?>
                        </td>
                        <td  class="hidden-print td_<?php echo $val['uacc_id']; ?>" ref="<?php echo $val['uacc_id']; ?>">
                        <a href="<?php echo base_url(); ?>users/admin_view_contact_detail/<?php echo $val['uacc_id']; ?>" class="btn green btn-xs"><i class="fa fa-search"></i></a>
                        
                        
                        
                        
                        
                        </td>
                        </tr>
                    
                    <?php } ?>
                    
                    
                    
                    
                    
                    
                    
                    </tbody>
                
                </table>
            
            
            
            
            
            </div>
        
        </div>
    
    </div>

<?php }else{ ?>



<?php } ?>





</div>

<div id="confirm-delete" class="modal fade modal-scroll" tabindex="-1">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
<h4 class="modal-title">Confirm delete</h4>
</div>
<div class="modal-body" id="modal_body_confitm_delete">
You are about to delete user from system. This action cannot be undone. Click confirm to delete this user.<br />
User ID : <span id="confirm_delete_user_id"></span>
</div>
<div class="modal-footer">

<button type="button" class="btn red" onclick="javascript: confirm_delete(this);" id="confirm_delete_btn">Confirm</button>
<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
</div>
</div>



<?php $this->load->view('includes/footer'); ?>


<script type="text/javascript">

$(function(){

$("#searchform").submit(function(){
var fullname = $("#fullName").val();

if(fullname.length == 0){
$("#fullName").blur();
$("#accno").blur();
Apprise('Please put input search', {
buttons: {
confirm: {
action: function() { 
Apprise('close');
}, // Callback function
className: null, // Custom class name(s)
id: 'confirm', // Element ID
text: 'Ok', // Button text
}
},
input: false, // input dialog
override: true,
});
return false;
}

});


$('#datatable_user').DataTable({
"order": [[ 0, "desc" ]],
"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
"iDisplayLength": 50,
"oLanguage": {
"sSearch": "Filter records:"
}
});

$("input[aria-controls=datatable_user]").removeClass('input-small').addClass('input-large')

});

function confirm_delete(){
var uacc_id = $("#confirm_delete_btn").data('uacc_id');

Metronic.blockUI({
target: "#confirm-delete",
boxed: true,
message: 'Deleting user. This will take a loooooong time. Please wait...'
});

$.ajax({
url: '<?php echo base_url(); ?>reports/delete_user',
dataType: 'json',
data: { uacc_id: uacc_id },
type: 'post',
success: function(data){
Metronic.unblockUI("#confirm-delete");

var status = data.status;
if(status == 'success'){
$('.td_'+uacc_id).parent().hide();
$('#confirm-delete').modal('hide');
}else{
alert('error. please contact admin');
location.reload();
}
}
});
}

function delete_user(uacc_id){
var $modal = $('#confirm-delete');
// create the backdrop and wait for next modal to be triggered
$('body').modalmanager('loading');
$modal.modal();
$("#confirm_delete_btn").data('uacc_id', uacc_id);
$("#confirm_delete_user_id").text(uacc_id)
/*
setTimeout(function(){
$("#modal_body_confitm_delete").load('<?php echo base_url(); ?>invitation/ajax_view/'+reward_code, '', function(){

$("#qrcode_invitation_code_text").text(reward_code);
});
}, 1000);
*/

}




</script>



