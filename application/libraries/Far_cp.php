<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Far_cp {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    /**
     * Get User Account
     */
    /**
    function get_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    */
    /**
    function list_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->result_array();
    }
    */
    
    /**
     * Insert / Create profile
     * @param array $data 
     */
    /**
    function insert_profile($data){
        $this->CI->db->insert('user_profiles', $data);
        return $this->CI->db->insert_id();
    }
    */
    /**
     * Update
     * @param string $key Database column name
     * @param string $key_value Database column value(s)
     * @param string $column Column name to update
     * @param string $value New value for specified column
     */
    /**
    function update_user($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('user_accounts', $data); 
    }
    */
    
    function delete_cp_volunteer($uacc_id){
        //delete profiles
        $this->CI->db->where('uacc_id', $uacc_id);
        $this->CI->db->delete('user_profiles');
        
        //delete user
        $this->CI->db->where('uacc_id', $uacc_id);
        $this->CI->db->delete('user_accounts');
    }
    
    function insert_log_report($data = array()){
        $data['create_dttm'] = date("Y-m-d H:i:s");
        $this->CI->db->insert('cp_log_report', $data);
        return $this->CI->db->insert_id();
    }
    
    function get_log_report($column, $value){
        $query = $this->CI->db->query("SELECT * FROM cp_log_report WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    
    function insert_log_photo($data = array()){
        $data['create_dttm'] = date("Y-m-d H:i:s");
        $this->CI->db->insert('cp_log_photo', $data);
        return $this->CI->db->insert_id();
    }
    
    function update_log_photo($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('cp_log_photo', $data); 
    }
    
    
}


?>