<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Far_menu {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    function list_parent(){
        $query = $this->CI->db->query("SELECT * FROM far_menu WHERE parent_id='0' ORDER BY sort ASC");
        $row = $query->result_array();
        return $row;
    }
    
    function get_menu($menu_id){
        $query = $this->CI->db->query("SELECT * FROM far_menu WHERE id='".$menu_id."'");
        $row = $query->row_array();
        return $row;
    }
    
    function update_menu($menu_id, $column, $value){
        $data = array(
            $column => $value
        );
        $this->CI->db->where('id', $menu_id);
        $this->CI->db->update('far_menu', $data); 
    }
    
    public function list_all_menu(){
        $query = $this->CI->db->query("SELECT * FROM far_menu WHERE parent_id='0' ORDER BY sort ASC");
        foreach ($query->result_array() as $row){
            $children = array();
            $query2 = $this->CI->db->query("SELECT * FROM far_menu WHERE parent_id='".$row['id']."'");
            foreach ($query2->result_array() as $row2){
                $children2 = array();
                $query3 = $this->CI->db->query("SELECT * FROM far_menu WHERE parent_id='".$row2['id']."'");
                foreach ($query3->result_array() as $row3){
                    
                    $children2[] = $row3;
                }
                $row2['children'] = $children2;
                $children[] = $row2;
            }
            $row['children'] = $children;
            $output[] = $row;
        }
        
        return $output;
    }
    
    public function list_menu_by_group($group_id = 'all'){
        $query = $this->CI->db->query('SELECT * FROM far_menu WHERE parent_id="0" AND FIND_IN_SET("'.$group_id.'",group_id) ORDER BY sort ASC');
        foreach ($query->result_array() as $row){
            
            
            $query2 = $this->CI->db->query("SELECT * FROM far_menu WHERE parent_id='".$row['id']."' AND FIND_IN_SET('".$group_id."',group_id) ORDER BY sort ASC");
            foreach ($query2->result_array() as $row2){
                $row['children'][] = $row2;
            }
            
            
            
            $output[] = $row;
        }
        return $output;
    }
    
    public function page_title(){
        $url_class = $this->CI->router->fetch_class();
        $url_method = $this->CI->router->fetch_method();
        
        $query = $this->CI->db->query("SELECT page_title, page_title_small FROM far_menu WHERE controller='".$url_class."' AND function='".$url_method."%' LIMIT 1");
        
        if($query->num_rows() == 0){
            $segs = $this->CI->uri->segment_array();
            $url_method2 = $url_method."/".end($segs);
            $query = $this->CI->db->query("SELECT page_title, page_title_small FROM far_menu WHERE controller='".$url_class."' AND function='".$url_method2."' LIMIT 1");
        }
        
        
        
        $rows = $query->row();
        return $rows;
    }
    
    public function breadcrumb(){
        $url_class = $this->CI->router->fetch_class();
        $url_method = $this->CI->router->fetch_method();
        
        $query = $this->CI->db->query("SELECT parent_id,name,link,controller,function FROM far_menu WHERE controller='".$url_class."' AND function='".$url_method."' LIMIT 1");
        if($query->num_rows() == 0){
            $segs = $this->CI->uri->segment_array();
            $url_method2 = $url_method."/".end($segs);
            $query = $this->CI->db->query("SELECT parent_id,name,link,controller,function FROM far_menu WHERE controller='".$url_class."' AND function='".$url_method2."' LIMIT 1");
        }
        $breadcrumb[] = array('name' => $query->row()->name, 'class' => $query->row()->controller, 'method' => $query->row()->function, 'link' => $query->row()->link);
        
        $query2 = $this->CI->db->query("SELECT parent_id,name,link,controller,function FROM far_menu WHERE id='".$query->row()->parent_id."' LIMIT 1");
        $breadcrumb[] = array('name' => $query2->row()->name, 'class' => $query2->row()->controller, 'method' => $query2->row()->function, 'link' => $query2->row()->link);
        
        $reverse_breadcrumb = array_reverse($breadcrumb);
        
        if(strlen($reverse_breadcrumb[0]['name']) == 0){
            unset($reverse_breadcrumb[0]);
        }
        
        $final_breadcrumb = array_values($reverse_breadcrumb);
        
        return $final_breadcrumb;
    }
    
    public function list_all_group(){
        $query = $this->CI->db->query("SELECT * FROM user_groups");
        return $query->result_array();
    }
    
    function get_menu_by_class_method_group($class, $method, $ugrp_id){
        $query = $this->CI->db->query("SELECT * FROM far_menu WHERE controller='".$class."' AND function='".$method."' AND FIND_IN_SET('".$ugrp_id."',group_id)");
        $row = $query->row_array();
        return $row;
    }
    
    function get_menu_by_class_method($class, $method){
        $query = $this->CI->db->query("SELECT * FROM far_menu WHERE controller='".$class."' AND function='".$method."'");
        $row = $query->row_array();
        return $row;
    }
    
}


?>