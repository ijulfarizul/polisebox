<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.4/css/select.dataTables.min.css" />
<style>
table.dataTable.select tbody tr,
table.dataTable thead th:first-child {
  cursor: pointer;
}
</style>
<a href="<?php echo base_url(); ?>qrcodes/admin_generate_new_qrcode" class="btn blue">
    <i class="fa fa-plus"></i> Generate New Qr Code
</a>
<br />
<br />
<div class="table-container">
    <table id="example" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>#ID</th>
                <th><input name="select_all" value="1" type="checkbox"></th>
                <th>Serial Number</th>
                <th>Security Number</th>
                <th>User detail</th>
                <th>Actions<sup>(s)</sup></th>
            </tr>
        </thead>                             
    </table>
    
    <!--
    <div class="btn-group btn-group-solid">
        <button type="button" class="btn green activated_by_row_count"><i class="fa fa-cogs"></i> Transfer to agent (<span class="text_total_selected">0</span>)</button>
        <button type="button" class="btn red activated_by_row_count"><i class="fa fa-times"></i> Delete selected (<span class="text_total_selected">0</span>)</button>
    </div>
    -->
    
</div>
<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>
<script type="text/javascript">
var table;
var rows_selected = [];
$(function(){
    
    table = $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>qrcodes/datatable_admin_list_all_qrcode",
            "type": "POST"
        },
        deferRender: true,
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "columns": [
            { "data": "qrcode_id" },
            { "data": "qrcode_id" },
            { "data": "serial_number" },
            { "data": "security_number" },
            { "data": "fullname", "render": function ( data, type, row ) {
                var html = "";
                if(row.uacc_id > 0){
                    html += row.fullname+'<br>';
                    html += row.nric;
                }
                
                return html;
            } },
            { "data": "qrcode_id", "render": function ( data, type, row ) {
                var html = "";
                html += '<a href="<?php echo base_url(); ?>order/view/'+row.od_invoice_number+'" class="btn btn-xs green"><i class="fa fa-search"></i> View</a>';
                if(!row.uacc_id){
                    html += '<a href="javascript: void(0);" onclick="javascript: quick_delete(\''+row.qrcode_id+'\');" class="btn btn-xs red"><i class="fa fa-times"></i></a>';
                }
                
                
                return html;
            } },
        ],
        "order": [0, 'desc'],
        "columnDefs": [
            { 
                targets: 1, 
                orderable: false,
                searchable: false,
                width: '1%',
                className: 'dt-body-center',
                'render': function (data, type, full, meta){
                     return '<input type="checkbox" class="row_checkbox" data-qrcode_id="'+full.qrcode_id+'">';
                 }
            }
        ],
        "drawCallback": function( settings ) {
            var test = $("input[type=checkbox]:not(.toggle, .md-check, .md-radiobtn, .make-switch, .icheck)");
            if (test.size() > 0) {
                test.each(function() {
                    if ($(this).parents(".checker").size() === 0) {
                        $(this).show();
                        $(this).uniform();
                    }
                });
            }
            reselect();
        },
    } );
    
    
    // Handle click on checkbox
   $('#example tbody').on('click', 'input[type="checkbox"]', function(e){
      var $row = $(this).closest('tr');

      // Get row data
      var data = table.row($row).data();
        //console.log(data);
      // Get row ID
      var rowId = data.qrcode_id;

      // Determine whether row ID is in the list of selected row IDs 
      var index = $.inArray(rowId, rows_selected);

      // If checkbox is checked and row ID is not in list of selected row IDs
      if(this.checked && index === -1){
         rows_selected.push(rowId);

      // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
      } else if (!this.checked && index !== -1){
         rows_selected.splice(index, 1);
      }

      if(this.checked){
         $row.addClass('selected');
      } else {
         $row.removeClass('selected');
      }

      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle click on table cells with checkboxes
   $('#example').on('click', 'tbody td, thead th:first-child', function(e){
      $(this).parent().find('input[type="checkbox"]').trigger('click');
   });

   // Handle click on "Select all" control
   $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
      if(this.checked){
         $('#example tbody input[type="checkbox"]:not(:checked)').trigger('click');
      } else {
         $('#example tbody input[type="checkbox"]:checked').trigger('click');
      }

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle table draw event
   table.on('draw', function(){
      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);
   });
    
   // Handle form submission event 
   $('#frm-example').on('submit', function(e){
      var form = this;

      // Iterate over all selected checkboxes
      $.each(rows_selected, function(index, rowId){
         // Create a hidden element 
         $(form).append(
             $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'id[]')
                .val(rowId)
         );
      });

      // FOR DEMONSTRATION ONLY     
      
      // Output form data to a console     
      $('#example-console').text($(form).serialize());
      console.log("Form submission", $(form).serialize());
       
      // Remove added elements
      $('input[name="id\[\]"]', form).remove();
       
      // Prevent actual form submission
      e.preventDefault();
   });


});

function reselect(){
    if(rows_selected.length === 0){
        
    }else{
        $(rows_selected).each(function(i, j){
            $(".row_checkbox[data-qrcode_id='"+j+"']").trigger("click")
        })
    }
}

function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
   
   //update total selected
   $(".text_total_selected").text(rows_selected.length);
   if(rows_selected.length === 0){
    $(".activated_by_row_count").addClass('disabled');
   }else{
    $(".activated_by_row_count").removeClass('disabled');
   }
}

</script>

<script type="text/javascript">
function quick_delete(qrcode_id){
    swal({
        title: "Are you sure?",
        text: "This action cannot be undone",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
    function(){
        
        $.ajax({
            url: "<?php echo base_url(); ?>qrcodes/ajax_admin_delete_qrcode",
            type: "POST",
            dataType: "json",
            data: {
                postdata: {
                    qrcode_id: qrcode_id
                }
            },
            success: function(data){
                if(data.status == "success"){
                    table.ajax.reload(function(){
                        swal("Deleted!", "QR Code has been deleted successfully", "success");
                    });
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        })
        
        
    });
}
</script>

<script type="text/javascript">
function transfer_to_agent(){
    
    
}
</script>

