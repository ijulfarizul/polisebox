<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agent extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function admin_list_all_agent(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->view('agent/admin/admin_list_all_agent', $this->data);
    }
    
    function datatable_admin_list_all_agent(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');
        
        $this->datatables->select('u.uacc_id');
        $this->datatables->select('u.uacc_username');
        $this->datatables->select('u.uacc_date_added');
        $this->datatables->select('p.fullname');
        
        //count total assigned qrcode
        $this->datatables->select('(SELECT COUNT(qrcode_id) as total_count_qrcode FROM qrcode_detail qra WHERE qra.agent_uacc_id=u.uacc_id) AS total_count_qrcode');
        
        $this->datatables->from('user_accounts u');
        $this->datatables->join('user_profiles p', 'u.uacc_id=p.uacc_id', 'left');
        $this->datatables->where('u.uacc_group_fk', '5');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function admin_add_new_agent(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        
        $list_all_country = $this->far_location->list_all_country();
        $this->data['list_all_country'] = $list_all_country;
        
        $this->load->view('agent/admin/admin_add_new_agent', $this->data);
    }
    
    function ajax_admin_add_new_agent(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $this->load->library('far_helper');
        $error = array();
        $output = array();
        
        $list_areas = array_filter($postdata['areas']);
        
        //check fullname
        if(strlen($postdata['fullname']) <= 5){
            $error['fullname'] = "Fullname must be at least 6 characters";
        }
        
        if(strlen($postdata['nric']) <= 8){
            $error['nric'] = "NRIC must be at least 8 characters";
        }
        
        if (!preg_match('/^[0-9]+$/', $postdata['nric'])) {
            $error['nric'] = "NRIC must be number only";
        }
        
        if(count($list_areas) == 0){
            $error['area'] = "Please select area";
        }
        
        //check url alias no space
        if (!preg_match('/^[a-z0-9]+$/', $postdata['url_alias'])) {
            $error['url_alias'] = "Registration URL incorrect. Only small letter and number are allowed";
        }
        if(strlen($postdata['url_alias']) <= 5){
            $error['url_alias'] = "Registration URL must be at least 6 characters";
        }
        
        
        
        if(count($error) == 0){
            
            //create user
            $username = $postdata['url_alias'];
            $email = $username.'@agent.polisebox.com';
            $password = $this->far_helper->generateRandomString(6);
            $user_data = array();
            $group_id = 5;
            $activate = TRUE;
            $user_id = $this->flexi_auth->insert_user($email, $username, $password, $user_data, $group_id, $activate);
            if($user_id){
                $this->far_users->update_user('uacc_id', $user_id, 'uacc_raw_password', $password);
                $this->far_users->update_user('uacc_id', $user_id, 'url_alias', $postdata['url_alias']);
                
                //update profile
                $this->far_users->insert_profile(array('uacc_id' => $user_id));
                $this->far_users->update_profile('uacc_id', $user_id, 'fullname', $postdata['fullname']);
                $this->far_users->update_profile('uacc_id', $user_id, 'nric', $postdata['nric']);
                $this->far_users->update_profile('uacc_id', $user_id, 'phonemobile', $postdata['phonemobile']);
                
                //cp_lc_id
                //get city by area
                $area_detail = $this->far_location->get_area('area_id', $list_areas[0]);
                $this->far_users->update_profile('uacc_id', $user_id, 'cp_lc_id', $area_detail['lc_id']);
                $this->far_users->update_profile('uacc_id', $user_id, 'cp_ls_id', $area_detail['ls_id']);
                $this->far_users->update_profile('uacc_id', $user_id, 'cp_lct_id', $area_detail['lct_id']);
                //implode area
                $implode_area = implode(',', $list_areas);
                $this->far_users->update_profile('uacc_id', $user_id, 'cp_area_id', $implode_area);
                
                $output['redirect_url'] = base_url().'agent/admin_view_agent_detail/?uacc_id='.$user_id;
                $output['status'] = 'success';
            }else{
                $error['user_creation'] = "User creation error. Please try again using different input";
                $output['message_single'] = current($error);
                $output['errors'] = $error;
                $output['status'] = 'error';
            }
            
            

            
            
            
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
    function admin_view_agent_detail(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        $page = $this->input->get('page');
        $uacc_id = $this->input->get('uacc_id');
        
        $this->data['page'] = $page;
        
        //user_detail
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        $this->data['user_detail'] = $user_detail;
        
        $user_profile = $this->far_users->get_profile('uacc_id', $uacc_id);
        $this->data['user_profile'] = $user_profile;
        
        
        if($page == 'facebook_post'){
        
        }elseif($page == 'log_report'){
            
            $this->load->view('agent/admin_view_agent_detail/log_report', $this->data);
        }else{
            
            $list_all_country = $this->far_location->list_all_country();
            $this->data['list_all_country'] = $list_all_country;
            
            $this->load->view('agent/admin_view_agent_detail/profiles', $this->data);
        }
        
    }
    
    function ajax_search_user_by_username_to_transfer_qrcode(){
        $this->far_auth->allowed_group('3,4', $this->user['ugrp_id']);
        $search_term = $this->input->post('search_email_affiliator');
        $output = array();
        $error = array();
        
        if(strlen($search_term) <= 3){
            $error['search_email_affiliator'] = "Carian perlulah lebih dari 3 huruf";
        }
        
        
        if(count($error) == 0){
            //search by username, email, phone number or name
            $query = $this->db->query("
                SELECT 
                    u.uacc_id,
                    u.uacc_username,
                    p.fullname,
                    p.phonemobile,
                    p.email,
                    p.profilepic_url
                FROM user_accounts u
                LEFT JOIN user_profiles p
                ON u.uacc_id=p.uacc_id
                WHERE 
                    u.uacc_group_fk = '5' AND
                    (
                    u.uacc_username LIKE '%".$search_term."%' OR
                    p.fullname LIKE '%".$search_term."%' OR
                    p.phonemobile LIKE '%".$search_term."%' OR
                    p.email LIKE '%".$search_term."%'
                    )
                ORDER BY RAND()
                LIMIT 3
            ");
            
            $rows = $query->result_array();
            $numrow = $query->num_rows();
            
            if($numrow == 0){
                $error['search_email_affiliator'] = "Tiada carian dijumpai. Sila cuba dengan perkataan lain";
                $output['errors'] = $error;
                $output['error_message_single'] = current($error);
                $output['status'] = 'error';
            }else{
                $list_user = $this->shuffleThis($rows);
                $output['list_user'] = $rows;
                $output['status'] = 'success';
            }
            
            
            
        }else{
            $output['errors'] = $error;
            $output['error_message_single'] = current($error);
            $output['status'] = 'error';
        }
        
        
        echo json_encode($output);
    }
    
    function ajax_member_transfer_qrcode_to_member(){
        $this->far_auth->allowed_group('3,4', $this->user['ugrp_id']);
        $this->load->library('far_qrcode');
        $this->load->library('far_helper');
        //$this->load->library('far_sms');
        //$this->load->library('far_telegram');
        
        $postdata = $this->input->post('postdata');
        $error = array();
        $output = array();
        
        
        
        //check if all pin are belong to user
        /*
        foreach($postdata['list_pinnumber'] as $a => $b){
            $pind = $this->far_qrcode->get_qrcode('serial_number', $b);
            if($pind['agent_uacc_id'] != $this->user['uacc_id']){
                $error[$b] = "PIN not belong to this user";
            }
        }
        */
        
        //check if receiver exists
        $receiver_detail = $this->far_users->get_user('uacc_id', $postdata['receiver_uacc_id']);
        if(count($receiver_detail) == 0){
            $error['receiver'] = "Receiver not exists";
        }
        
        
        
        if(count($error) == 0){
            
            $receiver_profile = $this->far_users->get_profile('uacc_id', $receiver_detail['uacc_id']);
            
            //transfer all pin
            $list_transaction_pin = array();
            foreach($postdata['list_pinnumber'] as $a => $b){
                $this->far_qrcode->transfer_qrcode_ownership($receiver_detail['uacc_id'], $b);
                $list_transaction_pin[] = $b;
            }
            
            $total_pin_transfered = count($postdata['list_pinnumber']);
            
            /*
            //sender profile
            $sender_profile = $this->far_users->get_profile('uacc_id', $this->user['uacc_id']);
            $sender_fullname = $sender_profile['fullname'];
            
            //send sms to receiver
            $receiver_fullname = $receiver_profile['fullname'];
            $receiver_msisdn = $this->far_helper->fix_msisdn($receiver_profile['phonemobile']);
            if(strlen($receiver_msisdn) > 6){
                //send sms
                //$receiver_message = "Usahawan.me - Anda menerima ".$total_pin_transfered." PIN daripada ".$sender_fullname.". Tunggu apa lagi? Kasi Gegarla Boss!!!";
                $receiver_message = "Pindahan PIN telah berjaya! Anda menerima ".$total_pin_transfered." PIN daripada ".ucwords(strtolower($sender_fullname));
                $this->far_sms->send_sms($receiver_msisdn, $receiver_message, 'transfer_pin_receiver', $receiver_detail['uacc_id']);
            }
            
            $receiver_telegram_message = "You receive <b>".$total_pin_transfered."</b> PIN from ".ucwords(strtolower($sender_fullname)).PHP_EOL.PHP_EOL;
            $receiver_telegram_message .= "Received PIN List:".PHP_EOL;
            //send list of pin to receiver
            $receiver_list_pin = "Senarai PIN diterima:".PHP_EOL.PHP_EOL;
            foreach($list_transaction_pin as $c => $d){
                $receiver_list_pin .= $d.PHP_EOL;
                $receiver_telegram_message .= $d.PHP_EOL;
            }
            
            //send telegram to receiver
            $this->far_telegram->send_telegram_queue($receiver_detail['uacc_id'], $receiver_telegram_message);
            */
        
            $output['status'] = "success";
        }else{
            
            $output['status'] = "error";
            $output['errors'] = $error;
            $output['error_message_single'] = current($error);
        }
        
        echo json_encode($output);
    }
    

    function shuffleThis($list) { 
        if (!is_array($list)) return $list; 
    
        $keys = array_keys($list); 
        shuffle($keys); 
        $random = array(); 
        foreach ($keys as $key) { 
            $random[$key] = $list[$key]; // CHANGE HERE that preserves the keys
        }
        return $random; 
    }
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */