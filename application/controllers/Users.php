<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function admin_list_all_user(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        if ($this->input->post('search_term')){
			$this->data['search_return'] = $this->ci_admin_list_all_user($this->input->post('search'));
		}
		$a = $this->session->all_userdata();
        //echo '<pre>'; print_r($a); echo '</pre>'; exit();
		//$this->data['list_users'] = $this->far_users->list_all_users();
        $this->data['list_all_user_group'] = $this->far_users->list_all_user_group();
        $this->load->view('users/admin_list_all_user', $this->data);
    }
    
    public function ci_admin_list_all_user($search_array){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        $s = $search_array;
        $output = array();
        $where_array = array();
		
		if(strlen($s['search_all']) > 0){ 
		  $name_arr = array();
		  $name_arr = explode(' ', $s['search_all']);
          foreach($name_arr as $a => $b){
            $where_array[] = '(p.firstname LIKE "%'.$b.'%" OR p.lastname LIKE "%'.$b.'%" OR u.uacc_username LIKE "%'.$b.'%" OR u.uacc_email LIKE "%'.$b.'%")';
          }
        }
        
        if(strlen($s['membership']) > 0){
            if($s['membership'] == 'all'){
                $where_array[] = 'u.uacc_id IS NOT NULL'; 
            }else{
                $where_array[] = 'u.uacc_group_fk="'.$s['membership'].'"'; 
            }
        }
        
        if(count($where_array) != 0){


            $where = implode(' AND ', $where_array);

            $sql = "SELECT *,u.uacc_id as uacc_id FROM user_accounts u LEFT OUTER JOIN user_profiles p ON u.uacc_id=p.uacc_id WHERE ".$where;
            $query = $this->db->query($sql);
            $output = $query->result_array();
            $total_users = $query->num_rows();
        }

        $this->data["list_all_member"] = $output;
        return $output;

    }
    
    function admin_view_user_detail(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        $page = $this->input->get('page');
        $uacc_id = $this->input->get('uacc_id');
        
        $this->data['page'] = $page;
        
        //user_detail
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        $this->data['user_detail'] = $user_detail;
        
        $user_profile = $this->far_users->get_profile('uacc_id', $uacc_id);
        $this->data['user_profile'] = $user_profile;
        
        
        if($page == 'facebook_post'){
        
        }elseif($page == 'log_report'){
            
            $this->load->view('agent/admin_view_agent_detail/log_report', $this->data);
        }else{
            
            $list_all_country = $this->far_location->list_all_country();
            $this->data['list_all_country'] = $list_all_country;
            
            $this->load->view('users/admin_view_user_detail/profiles', $this->data);
        }
        
    }
    
    function admin_load_tab_profile(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        $error = array();
        $uacc_id = $this->input->post('uacc_id');
        
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        if(count($user_detail) == 0){
            $error['uacc_id'] = 'User not exists';
        }
        
        if(count($error) == 0){
            
            $user_profile = $this->far_users->get_profile('uacc_id', $uacc_id);
            
            $this->data['user_profile'] = $user_profile;
            $this->data['uacc_id'] = $uacc_id;
            $this->load->view('users/admin_load_tab_profile', $this->data);
        }else{
            
        }
    }
    
    function admin_load_tab_uplines(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        $error = array();
        $uacc_id = $this->input->post('uacc_id');
        
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        if(count($user_detail) == 0){
            $error['uacc_id'] = 'User not exists';
        }
        
        if(count($error) == 0){
            
            $user_profile = $this->far_users->get_profile('uacc_id', $uacc_id);
            
            //get upline detail
            $upline_detail = $this->far_users->get_user('uacc_id', $user_detail['uacc_upline_id']);
            $upline_profile = $this->far_users->get_profile('uacc_id', $user_detail['uacc_upline_id']);
            
            $this->data['upline_detail'] = $upline_detail;
            $this->data['upline_profile'] = $upline_profile;
            
            $this->data['user_profile'] = $user_profile;
            $this->data['uacc_id'] = $uacc_id;
            $this->load->view('users/admin_load_tab_uplines', $this->data);
        }else{
            
        }
    }
    
    function admin_ajax_create_profile(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        $error = array();
        $output = array();
        $uacc_id = $this->input->post('uacc_id');
        
        //check profile already exists
        $user_profile = $this->far_users->get_profile('uacc_id', $uacc_id);
        if(count($user_profile) > 0){
            $error['uacc_id'] = 'Profile already created';
        }
        
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        if(count($user_detail) == 0){
            $error['uacc_id'] = 'User not exists';
        }
        
        if(count($error) == 0){
            $profile_id = $this->far_users->insert_profile(array('uacc_id' => $uacc_id));
            $output['profile_id'] = $profile_id;
            $output['status'] = 'success';
        }else{
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
    
    function ajax_modal_list_all_users(){
        $this->far_auth->allowed_group('2,3', $this->user['ugrp_id']);
        $error = array();
        $output = array();
        $uacc_id = $this->input->post('uacc_id');
        
        $list_all_user = $this->far_users->list_all_users();
        
        $this->data['list_all_user'] = $list_all_user;
        $this->load->view('users/ajax_modal_list_all_users', $this->data);
    }
    
    
    function admin_search_user(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        
        if ($this->input->post('search_user')){
			$this->data['search_return'] = $this->process_search_users($this->input->post());
		}
        
        $this->data['message'] = $this->session->flashdata('message');
        
        $this->load->view('users/admin/admin_search_user', $this->data);
    }
    
    function process_search_users($search_array){
        
        $postdata = $this->input->post();
        $s = $search_array;
        
        //build query
        if($s['search_fullname']){ $where_array[] = 'p.fullname LIKE "%'.$s['search_fullname'].'%"'; }
        //search_product_description
        if($s['search_serial_number']){ $where_array[] = 'qr.serial_number LIKE "%'.$s['search_serial_number'].'%"'; }
        //search_product_url_alias
        if($s['search_product_url_alias']){ $where_array[] = 'product_url_alias LIKE "%'.$s['search_product_url_alias'].'%"'; }
        //search_website_without_owner 
        if($s['search_website_without_owner'] == "yes"){ $where_array_exact[] = 'uacc_id IS NULL'; }
        
        
        $where_exact = '';
        if(count($where_array_exact) > 0){
            $where_exact = implode(' AND ', $where_array_exact);
        }
        
        if((count($where_array) != 0) || (count($where_array_exact) != 0)){
            $where = implode(' OR ', $where_array);
            
            if((count($where_array) == 0) && (count($where_array_exact) > 0)){
                $sql = "SELECT * FROM user_profiles p LEFT JOIN qrcode_detail qr ON p.uacc_id=qr.uacc_id WHERE ".$where_exact;
            }else{
                if(count($where_array_exact) > 0){
                    $sql = "SELECT * FROM user_profiles p LEFT JOIN qrcode_detail qr ON p.uacc_id=qr.uacc_id WHERE ".$where.' AND '.$where_exact;
                }else{
                    $sql = "SELECT * FROM user_profiles p LEFT JOIN qrcode_detail qr ON p.uacc_id=qr.uacc_id WHERE ".$where;
                }
                
            }
            
            $query = $this->db->query($sql);
            $list_website = $query->result_array();
            
            
        }else{
            
        }
        
        
        return $list_website;
    }
    
    function admin_login_as_a_user(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $error = array();
        $output = array();
        
        //is agent exists
        $user_detail = $this->far_users->get_user('uacc_id', $postdata['uacc_id']);
        if(count($user_detail) == 0){
            $error['uacc_id'] = "User not exists";
        }
        
        if(count($error) == 0){
            
            //logout as an admin
            //$this->flexi_auth->logout(FALSE);
            
            //unset
            unset($_SESSION["auypek"]);
            
            //set admin session
            $session_data = array(
                'al_username' => $this->user['uacc_username'],
                'al_userid' => $this->user['uacc_id'],
                'al_sec_key' => md5($this->user['uacc_id'].'@h56fy')
            );
            $session_serialize = serialize($session_data);
            $_SESSION["auypek"] = $session_serialize;
            
            
            $a = $this->flexi_auth->login($user_detail['uacc_username'], $user_detail['uacc_raw_password'], FALSE);
            
            
            $output['status'] = 'success';
            $output['login_status'] = $a;
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    function admin_login_revert(){
        $this->far_auth->allowed_group('3,4,5,6', $this->user['ugrp_id']);
        $sec = $this->input->get('sec');
        $uacc_id = $this->input->get('i');
        $md5_uacc_id = md5($uacc_id.'@h56fy');
        
        $ex = explode("tt2m3", $sec);
        $sec_key = $ex[1];

        
        if($sec_key == $md5_uacc_id){
            //user detail
            $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
            $a = $this->flexi_auth->login($user_detail['uacc_username'], $user_detail['uacc_raw_password'], FALSE);
            unset($_SESSION["auypek"]);
            redirect('auth_admin/dashboard');
        }else{
            echo '<h1>Authentication error. please logion again</h1>';
        }
    }
    
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */