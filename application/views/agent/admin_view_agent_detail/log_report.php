<?php $this->load->view('includes/header'); ?>
<?php $this->load->view('cp/admin_view_cp_detail/navbar'); ?>

<div class="row">
    <div class="col-md-12">
        <div class="table-container">
            <table id="example" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Log Date</th>
                        <th>Qr Code</th>
                        <th>Actions<sup>(s)</sup></th>
                    </tr>
                </thead>                             
            </table>
        </div>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>

<script type="text/javascript">
$(function(){
    
    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>cp/ajax_admin_view_cp_detail_log_report",
            "type": "POST",
            "data": {
                "postdata": {
                    "cp_uacc_id": "<?php echo $user_detail['uacc_id']; ?>"
                }
            }
        },
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "columns": [
            { "data": "create_dttm" },
            { "data": "serial_number" },
            { "data": "log_id", "render": function ( data, type, row ) {
                redited = '<a href="<?php echo base_url(); ?>order/view/'+row.od_invoice_number+'" class="btn btn-xs green"><i class="fa fa-search"></i> View</a>';
                return redited
            } },
        ],
        "order": [0, 'desc']
    } );

})
</script>