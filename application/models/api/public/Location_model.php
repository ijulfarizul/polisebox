<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location_model extends CI_Model {
  public $flexi_auth_config;

  function __construct(){
    parent::__construct();
    

    $this->config->load('flexi_auth', TRUE);
    $this->flexi_auth_config = $this->config->item('flexi_auth');
  }

  public function searchPoliceStation( $post_data = [] ) {
    $ares = [];

    if($post_data) {
      $lat = $post_data['latitude'];
      $lng = $post_data['longitude'];
      $radius = (isset($post_data['radius'])) ? $post_data['radius'] : 10;

      $this->db->select('*, ( 6371 * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) ) AS distance');
      $this->db->from('police_station');
      $this->db->having('distance < '.$radius);
      $this->db->join('location_state', 'police_station.ls_id = location_state.ls_id', 'left');
      $this->db->join('location_city', 'police_station.lct_id = location_city.lct_id', 'left');
      $this->db->join('location_area', 'police_station.area_id = location_area.area_id', 'left');
      $this->db->order_by('distance');
      $query = $this->db->get();

      if($query->num_rows()) {
        $ares =  $query->result_array();
      }
    }

    return $ares;
  }

  public function getArea( $conf = [], $single = false ) {
    $ares = [];

    $this->db->select('*');
    $this->db->from('location_area');
    $this->db->join('location_city', 'location_area.lct_id = location_city.lct_id', 'left');

    if(isset($conf['where'])) {
      $this->db->where($conf['where']);
    }

    $query = $this->db->get();
    if($query->num_rows()) {
      $ares = $query->result_array();

      if($single) {
        $ares = $ares[0];
      }
    }

    return $ares;
  }

  public function getCity( $conf = [], $single = false ) {
    $ares = [];

    $this->db->select('*');
    $this->db->from('location_city');
    $this->db->join('location_state', 'location_state.ls_id = location_city.ls_id', 'left');

    if(isset($conf['where'])) {
      $this->db->where($conf['where']);
    }

    $query = $this->db->get();
    if($query->num_rows()) {
      $ares = $query->result_array();
      
      if($single) {
        $ares = $ares[0];
      }
    }

    return $ares;
  }

  public function getState( $conf = [], $single = false ) {
    $ares = [];

    $this->db->select('*');
    $this->db->from('location_state');
    $this->db->join('location_country', 'location_country.lc_id = location_state.lc_id', 'left');

    if(isset($conf['where'])) {
      $this->db->where($conf['where']);
    }

    $query = $this->db->get();
    if($query->num_rows()) {
      $ares = $query->result_array();
      
      if($single) {
        $ares = $ares[0];
      }
    }

    return $ares;
  }

  public function getCountry( $conf = [], $single = false ) {
    $ares = [];

    $this->db->select('*');
    $this->db->from('location_country');

    if(isset($conf['where'])) {
      $this->db->where($conf['where']);
    }

    $query = $this->db->get();
    if($query->num_rows()) {
      $ares = $query->result_array();
      
      if($single) {
        $ares = $ares[0];
      }
    }

    return $ares;
  }

}


