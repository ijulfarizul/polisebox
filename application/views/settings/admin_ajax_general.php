<form class="form-horizontal" role="form" id="form_settings_general">
    <div class="form-body">
        <div class="form-group">
            <label class="col-md-3 control-label">Header Logo URL</label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Enter Title" value="<?php echo $this->far_meta->get_value('header_logo_url'); ?>" id="settings_general_header_logo_url">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Title</label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Enter Title" value="<?php echo $this->far_meta->get_value('title'); ?>" id="settings_general_title">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Footer Text</label>
            <div class="col-md-9">
                <input type="text" class="form-control" value="<?php echo $this->far_meta->get_value('footer_text'); ?>" id="settings_general_footer_text" />
                <span class="help-inline"> This will appear on the footer. HTML allowed </span>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                <button type="button" class="btn green" onclick="javascript: confirm_save_general_settings();">Submit</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
(function ( $ ) {
 
    $.fn.INajaxsubmit = function( options ) {
 
        // This is the easiest way to have default options.
        var settings = $.extend({
            // These are the defaults.
            dataType: 'json',
            type: 'post',
            confirmBeforeSubmit: {
                title: "Are you sure ?",      
                type: "warning",   
                showCancelButton: true,   
                closeOnConfirm: false,
                closeOnCancel: false,   
                showLoaderOnConfirm: true,
                confirmButtonText: "Confirm !"
            }
        }, options );
        
        var error = {}
        
        //check settings
        if(!settings.url){
            error.url = 'No URL';
        }
        
        //if confirmBeforeSubmit
        var confirmBeforeSubmit = settings.confirmBeforeSubmit;
        if($(confirmBeforeSubmit).length > 0){
            
        }
        
        if(error.length == 0){
            var a = settings;
            return a;
        }else{
            console.log(error);
            return false;
        }
        
 
    };
 
}( jQuery ));

function confirm_save_general_settings(){
    var postdata = {
        title: $("#settings_general_title").val(),
        footer_text : $("#settings_general_footer_text").val()
    }
    swal({   
        title: "Are you sure ?",      
        type: "warning",   
        showCancelButton: true,   
        closeOnConfirm: false,
        closeOnCancel: false,   
        showLoaderOnConfirm: true,
        confirmButtonText: "Confirm !",
    }, function(isConfirm){   
        if(isConfirm){
            
            $.ajax({
                url: '<?php echo base_url(); ?>settings/ci_admin_ajax_general',
                type: 'post',
                dataType: 'json',
                data: { postdata },
                success: function(data){
                    var status = data.status;
                    if(status == 'success'){
                        //reload frame
                        load_tab_settings_general();
                        
                        setTimeout(function(){     
                            swal("Success!", "Operation success!", "success")
                        }, 2000);
                    }else{
                        
                        $('.form-group').removeClass('has-error');
                        $('.form-group .help-inline').text('');
                        
                        var error_msg = data.error_msg;
                        $(error_msg).each(function(i, j){
                            var element_id = '#'+j.element_id;
                            var element = $(element_id);
                            
                            //clear all error
                            element.closest('.form-group').addClass('has-error');
                            
                            //check if help-inline exists
                            var help_inline_exists = element.siblings('.help-inline').length;
                            if(help_inline_exists == 0){
                                element.parent().append('<span class="help-inline">Error !</span>');
                            }
                            element.siblings('.help-inline').text(j.text);
                        });
                        
                        swal("Error!", "There some problem when submitting the data.", "error")
                    }
                }
            })
            
            
        }else{
            swal({   
                type: "error",   
                title: "Cancelled",   
                text: "Actions has been canceled.",   
                timer: 2000,   
                showConfirmButton: false 
            });
            
        }
        
    });
}
</script>