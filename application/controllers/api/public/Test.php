<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Test extends REST_Controller
{

  function __construct(){
    parent::__construct();
    $this->load->helper('api_response_helper');
    $this->load->helper('common_helper');
    $this->load->helper('jwt_helper');

  }

  public function index_get() {
    echo API_VERSION;
    echo "ok";
  }


}