<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }

    function overview(){
        $this->data['user_profile'] = $this->far_users->get_profile('uacc_id', $this->user['uacc_id']);
        $this->load->view('profile/overview.php', $this->data);
    }
    
    function account_settings(){
        
        $user_profile = $this->far_users->get_profile('uacc_id', $this->user['uacc_id']);
        $this->data['user_profile'] = $user_profile;
        
        $this->load->view('profile/account_settings.php', $this->data);
    }
    
    function ci_member_save_profile(){
        $postdata = $this->input->post('postdata');
        
        if(strlen($postdata['profile_firstname']) < 3){
            $this->error(array(
                'text' => 'Firstname must be more than 3 character',
                'element_id' => 'profile_firstname'
            ));
        }
        
        if(strlen($postdata['profile_lastname']) < 3){
            $this->error(array(
                'text' => 'Lastname must be more than 3 character',
                'element_id' => 'profile_lastname'
            ));
        }
        
        if(count($this->error) == 0){
            
            //update
            $this->far_users->update_profile('uacc_id', $this->user['uacc_id'], 'firstname', $postdata['profile_firstname']);
            $this->far_users->update_profile('uacc_id', $this->user['uacc_id'], 'lastname', $postdata['profile_lastname']);
            
            $output['status'] = 'success';
        }else{
            $output['status'] = 'error';
            $output['error_msg'] = $this->error;
        }
        
        echo json_encode($output);
    }
    
    function ci_member_change_password(){
        $postdata = $this->input->post('postdata');
        
        
        if($postdata['current'] != $this->user['uacc_raw_password']){
            $this->error(array(
                'text' => 'Password not match',
                'element_id' => 'chpass_current'
            ));
        }
        
        if(strlen($postdata['new_password']) <= 5){
            $this->error(array(
                'text' => 'New Password must be more that 5 character',
                'element_id' => 'chpass_new'
            ));
        }
        
        if($postdata['retype_password'] != $postdata['new_password']){
            $this->error(array(
                'text' => 'New password not match',
                'element_id' => 'chpass_retype_new'
            ));
        }
        
        if(count($this->error) == 0){
            
            //change user password
            if($this->flexi_auth->change_password($this->user['uacc_username'], $postdata['current'], $postdata['new_password'])){
                $this->far_users->update_user('uacc_id', $this->user['uacc_id'], 'uacc_raw_password', $postdata['new_password']);
                $output['status'] = 'success';
            }else{
                $output['status'] = 'error';
                
            }
            
            
            
        }else{
            $output['status'] = 'error';
            $output['error_msg'] = $this->error;
        }
        
        echo json_encode($output);
    }
    
    function my_profile(){
        $this->far_auth->allowed_group('3,4,5', $this->user['ugrp_id']);
        $this->load->library('far_location');
        
        $user_profile = $this->far_users->get_profile('uacc_id', $this->user['uacc_id']);
        $this->data['user_profile'] = $user_profile;
        $this->data['uacc_id'] = $uacc_id;
        
        $this->load->view('profile/my_profile', $this->data);
    }
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */