<?php $this->load->view('includes/header'); ?>
<style>
.tab-pane {
    min-height: 200px;
}
</style>
<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gear"></i>System Settings
							</div>
							
						</div>
						<div class="portlet-body">
							
							<div class="tabbable tabbable-tabdrop">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#tab_settings_general" data-toggle="tab">General</a>
									</li>
									<li>
										<a href="#tab_settings_usergroup" data-toggle="tab">User Group</a>
									</li>
									
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_settings_general">
										
									</div>
									<div class="tab-pane" id="tab_settings_usergroup">
										
									</div>
									
								</div>
							</div>
							
						</div>
					</div>

<?php $this->load->view('includes/footer'); ?>

<script type="text/javascript">
$(function(){
    
    Metronic.blockUI({
        boxed: true,
        message: 'Loading page. This will take some time...'
    });
    
    load_tab_settings_general();
    load_tab_settings_usergroup();
    
    
    $(document).ajaxStop(function () {
        console.log('all ajax finish')
        Metronic.unblockUI();
    });
});

function load_tab_settings_usergroup(){
    $("#tab_settings_usergroup").load('<?php echo base_url(); ?>settings/admin_ajax_load_tab_settings_usergroup', function(){
        console.log('ok - tab_settings_usergroup loaded');
    })
}

function load_tab_settings_general(){
    $("#tab_settings_general").load('<?php echo base_url(); ?>settings/admin_ajax_general', function(){
        console.log('ok - tab_settings_general loaded');
    })
}
</script>