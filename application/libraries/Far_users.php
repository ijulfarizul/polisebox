<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//$this->CI->db->escape_str()
class Far_users {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    /**
     * Get User Account
     */
    function get_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$this->CI->db->escape_str($value)."'");
        return $query->row_array();
    }
    
    function get_user($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$this->CI->db->escape_str($value)."'");
        return $query->row_array();
    }
    
    function get_user_by_column($column, $value, $column_name){
        $query = $this->CI->db->query("SELECT ".$column_name." FROM user_accounts WHERE ".$column."='".$this->CI->db->escape_str($value)."'");
        $row = $query->row_array();
        return $row[$column_name];
    }
    
    function list_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$this->CI->db->escape_str($value)."'");
        return $query->result_array();
    }
    
    /**
     * Get Profile
     */
    function get_profile($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_profiles WHERE ".$column."='".$this->CI->db->escape_str($value)."'");
        return $query->row_array();
    }
    
    function get_profile_by_column($column, $value, $column_name){
        $query = $this->CI->db->query("SELECT ".$column_name." FROM user_profiles WHERE ".$column."='".$this->CI->db->escape_str($value)."'");
        $row = $query->row_array();
        return $row[$column_name];
    }
    
    function insert_profile($data){
        $this->CI->db->insert('user_profiles', $data); 
    }
    
    function update_profile($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $this->CI->db->escape_str($key_value));
        $this->CI->db->update('user_profiles', $data); 
    }
    
    /**
     * Get Group Detail
     */
    function get_group($ugrp_id){
        $query = $this->CI->db->query("SELECT * FROM user_groups WHERE ugrp_id='".$ugrp_id."'");
        return $query->row_array();
    }
    
    function get_user_group($uacc_id){
        $query = $this->CI->db->query("SELECT ugrp_id,ugrp_name,ugrp_desc,ugrp_admin FROM user_accounts u LEFT JOIN user_groups g ON u.uacc_group_fk=g.ugrp_id WHERE u.uacc_id='".$uacc_id."'");
        return $query->row_array();
    }
    
    function update_user($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('user_accounts', $data); 
    }
    
    function list_all_user_group(){
        $query = $this->CI->db->query("SELECT * FROM user_groups");
        return $query->result_array();
    }
    
    /**
     * List All user accounts and profile
     * @param array $exclude_array_group_id Group ID to exclude from the list. If no specified, will show all group
     * @param array $exclude_array_user_id User Accounts ID (uacc_id) to exclude from the list. If no specified, will show all group
     *@return array
     */
    function list_all_users(){
        
        $query = $this->CI->db->query("
        SELECT 
            u.uacc_id,u.uacc_group_fk,u.uacc_username,p.firstname,p.lastname,u.uacc_email
        FROM user_accounts u LEFT JOIN user_profiles p 
        ON u.uacc_id=p.uacc_id");
        return $query->result_array();
        
        
        
    }
    
    
    
}


?>