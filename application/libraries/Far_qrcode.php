<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Far_qrcode {
    private $CI;
    private $qrcode_location;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
        
        $this->qrcode_location = FCPATH . 'assets' . DIRECTORY_SEPARATOR . 'qrcode' . DIRECTORY_SEPARATOR;
    }
    
    /**
     * Get User Account
     */
    /**
    function get_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    */
    /**
    function list_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->result_array();
    }
    */
    
    /**
     * Insert / Create profile
     * @param array $data 
     */
    /**
    function insert_profile($data){
        $this->CI->db->insert('user_profiles', $data);
        return $this->CI->db->insert_id();
    }
    */
    /**
     * Update
     * @param string $key Database column name
     * @param string $key_value Database column value(s)
     * @param string $column Column name to update
     * @param string $value New value for specified column
     */
    /**
    function update_user($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('user_accounts', $data); 
    }
    */
    
    function get_qrcode($column, $value){
        $query = $this->CI->db->query("SELECT * FROM qrcode_detail WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    
    function generate_qrcode($quantity = 1){
        for($i = 1; $i <= $quantity; $i++){
            $qrcode_id = $this->insert_qrcode_detail();
            //generate code
            $first_phase = 10000000+$qrcode_id;
            $random_number = $this->CI->far_helper->generateRandomnumber(20);
            $serial_number = $first_phase.'A'.$random_number;
            $security_number = $this->CI->far_helper->generateRandomnumber(3).$qrcode_id;
            
            //make qrcode
            $qrcode_data = array(
                'save_location' => './assets/qrcode/',
                'contents' => $serial_number,
                'filename' => $serial_number
            );
            $make = $this->make_qrcode($qrcode_data);
            
            $this->update_qrcode_detail('qrcode_id', $qrcode_id, 'serial_number', $serial_number);
            $this->update_qrcode_detail('qrcode_id', $qrcode_id, 'security_number', $security_number);
            $this->update_qrcode_detail('qrcode_id', $qrcode_id, 'qrcode_filename', $make['qrcode_filename']);
            $this->update_qrcode_detail('qrcode_id', $qrcode_id, 'qrcode_url', $make['qrcode_url']);
            
        }
        
        
        
    }
    
    function update_qrcode_detail($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('qrcode_detail', $data); 
    }
    
    function insert_qrcode_detail($data = array()){
        $data['create_dttm'] = date("Y-m-d H:i:s");
        $this->CI->db->insert('qrcode_detail', $data);
        return $this->CI->db->insert_id();
    }
    
    private function make_qrcode($qrcode_data = array()){
        require_once APPPATH.'third_party/phpqrcode/qrlib.php';
        if(!$qrcode_data['save_location']){
            $qrcode_data['save_location'] = './assets/qrcode/';
        }
        
        $codeContents = $qrcode_data['contents'];
        $fileName = $qrcode_data['filename'].'.png';
        $pngAbsoluteFilePath = $qrcode_data['save_location'].$fileName;
        
        // generating 
        if (!file_exists($pngAbsoluteFilePath)) { 
            QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_H); 
            $output['creation_type'] = 'new';
        } else { 
            $output['creation_type'] = 'nothing_file_already_generated';
        }
        $qrcode_url = base_url().'assets/qrcode/'.$fileName;
        
        $output['status'] = 'success';
        $output['qrcode_filename'] = $fileName;
        $output['qrcode_url'] = $qrcode_url;
        
        return $output;
    }
    
    function transfer_qrcode_ownership($agent_uacc_id, $serial_number){
        $update_data = array(
            'agent_uacc_id' => $agent_uacc_id,
        );

        $this->CI->db->where('serial_number', $serial_number);
        $this->CI->db->update('qrcode_detail', $update_data);
    }
    
    function delete_qrcode($qrcode_id){
        //get qrcode detail
        $qrcode_detail = $this->get_qrcode('qrcode_id', $qrcode_id);
        
        $fullpath = $this->qrcode_location . $qrcode_detail['qrcode_filename'];
        //delete qrcode from folder
        unlink($fullpath);
        
        //delete from database
        $this->CI->db->where('qrcode_id', $qrcode_id);
        $this->CI->db->delete('qrcode_detail');
    }
    
}


?>