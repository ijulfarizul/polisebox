<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function user(){
        
        $url_alias = $this->uri->segment(3);
        if(strlen($url_alias) >= 3){
            $referral_detail = $this->far_users->get_user('url_alias', $url_alias);
            if(count($referral_detail) > 3){
                
                $upline_uacc_id = $referral_detail['uacc_id'];
            }else{
                $upline_uacc_id = $this->session->userdata('upline_uacc_id');
            }
            
            

            if(!$upline_uacc_id){
                $this->session->set_userdata('upline_uacc_id', $upline_uacc_id);
            }
        }
        
        
        $this->data['upline_uacc_id'] = $upline_uacc_id;
        
        //get referral
        $referral_profile = $this->far_users->get_profile('uacc_id', $upline_uacc_id);
        $this->data['referral_profile'] = $referral_profile;
        
        
        if($upline_uacc_id < 1){
            redirect('register/user/cops');
        }
        
        $this->load->view('register/user', $this->data);
    }
    
    function ajax_create_user(){
        $postdata = $this->input->post('postdata');
        $this->load->library('far_qrcode');
        //$this->load->library('far_sms');
        $error = array();
        $output = array();
        
        $post_nric = $postdata['nric'];
        
        //check username
        if(strlen($postdata['nric']) <= 8){
            $error['nric'] = "NRIC or Passport must be more than 8 character";
        }
        
        if(!preg_match('/^[A-Z0-9]{4,20}$/m', $post_nric)){
            $error['nric'] = "Only uppercase and numerals. Must not contain spaces or symbols (eg: $! @ #% {^ &)";
        }
        
        //check fullname
        if(strlen($postdata['fullname']) <= 3){
            $error['fullname'] = "Fullname must be more than 3 character";
        }
        
        //is valid email
        if(!$this->far_helper->isValidEmail($postdata['email'])){
            $error['email'] = "Email format is not valid";
        }
        
        //check if nric passport already registered
        $user_detail = $this->far_users->get_user('uacc_username', $post_nric);
        if(count($user_detail) > 3){
            $error['nric'] = "NRIC / Passport already registered";
        }
        
        //check serial number
        if(strlen($postdata['serial_number']) < 4){
            $error['serial_number'] = "Serial number error";
        }
        
        
        $registrar_detail = $this->far_users->get_user('uacc_id', $postdata['upline_uacc_id']);
        
        //check qrcode must be exists
        $qrcode_detail = $this->far_qrcode->get_qrcode('serial_number', $postdata['serial_number']);
        if(count($qrcode_detail) == 0){
            $error['serial_number'] = "Serial number not exists. Please try again";
        }else{
            
            //check serial number must be belong to registrar
            if($qrcode_detail['agent_uacc_id'] != $postdata['upline_uacc_id']){
                $error['serial_number'] = "Serial number not belong to this agent. Please try using different serial number";
            }
            
        }
        
        
        if(count($error) == 0){
            
            //register user and get uacc_id
            $username = $post_nric;
            $email_username = $this->far_helper->generateRandomnumber(10).'@member.policebox.tk';
            $email = $postdata['email'];
            $password = 'default_'.$this->far_helper->generateRandomnumber(6);
            $user_data = array();
            $group_id = 4;
            $activate = TRUE;
            $user_id = $this->flexi_auth->insert_user($email_username, $username, $password, $user_data, $group_id, $activate);
            if(!$user_id){
                $error['user_registration'] = "There are some error while registering your account. Please try again with different value";
                
                $output['message_single'] = current($error);
                $output['errors'] = $error;
                $output['status'] = 'error';
            }else{
                
                $default_profile_picture = base_url()."assets/global/img/polisebox/logo_252x252_blue_background.png";
                $this->far_users->update_user('uacc_id', $user_id, 'uacc_raw_password', $password);
                $this->far_users->update_user('uacc_id', $user_id, 'uacc_upline_id', $postdata['upline_uacc_id']);
                //$this->far_users->regenerate_upline_list($user_id);
                
                //update OTP
                $one_time_password = $this->far_helper->generateRandomnumber(6);
                $this->far_users->update_user('uacc_id', $user_id, 'one_time_password', $one_time_password);
                
                //insert profile
                $this->far_users->insert_profile(array('uacc_id' => $user_id));
                $this->far_users->update_profile('uacc_id', $user_id, 'fullname', $postdata['fullname']);
                $this->far_users->update_profile('uacc_id', $user_id, 'phonemobile', $postdata['nric']);
                $this->far_users->update_profile('uacc_id', $user_id, 'email', $email);
                $this->far_users->update_profile('uacc_id', $user_id, 'profilepic_url', $default_profile_picture);
                $this->far_users->update_profile('uacc_id', $user_id, 'phonemobile', $postdata['phonemobile']);
                

                //created by uacc id
                $this->far_users->update_profile('uacc_id', $user_id, 'setup_account', 0);
                $this->far_users->update_user('uacc_id', $user_id, 'oauth_provider', 'web_form');
                
                //update qrcode
                $this->far_qrcode->update_qrcode_detail('serial_number', $postdata['serial_number'], 'uacc_id', $user_id);
                
                //send sms OTP
                $this->sms_send_otp_registration($user_id);
                
                //$affiliate_default_url = $user_id.$this->far_helper->generateRandomnumber();
                //$this->far_users->update_user('uacc_id', $user_id, 'affiliate_default_url', $affiliate_default_url);
                $this->far_users->update_user('uacc_id', $user_id, 'url_alias', $username);
                
                //$telegram_auth_code = $this->far_helper->generateRandomString(10);
                //$this->far_users->update_user('uacc_id', $user_id, 'telegram_auth_code', $telegram_auth_code);
                
                $sms_user_message = "Tahniah ".ucwords(strtolower($postdata['register_fullname']))."! Pendaftaran berjaya. Login di https://www.usahawan.me".PHP_EOL.PHP_EOL;
                $sms_user_message .= "Username: ".$username.PHP_EOL;
                $sms_user_message .= "Password: ".$password.PHP_EOL;
                $sms_user_msisdn = $this->far_helper->fix_msisdn($postdata['phonemobile']);
                //$this->far_sms->send_sms_queue($sms_user_msisdn, $sms_user_message, 'account_registration_success', $user_id);
                
                //send sms telegram and whatsapp to user
                $sms_user_wt = "Usahawan.Me".PHP_EOL;
                $sms_user_wt .= "Connect to Telegram by visiting:".PHP_EOL;
                $sms_user_wt .= "https://www.usahawan.me/i/t/J8L9B59KA6".PHP_EOL.PHP_EOL;
                
                $sms_user_wt .= "Join Whatsapp Group:".PHP_EOL;
                $sms_user_wt .= "https://www.usahawan.me/i/w/";
                //$this->far_sms->send_sms_queue($sms_user_msisdn, $sms_user_wt, 'sms_whatsapp_telegram_connect', $user_id);
                
                //send sms to upline
                //$this->send_telegram_or_sms_to_upline_saying_registration_success($postdata['upline_uacc_id'], $user_id);
                
                //send telegram to admins
                //$this->send_telegram_to_admins_new_user_register($user_id);
                
                if(!$this->user['uacc_id']){
                    $check_user_login = $this->flexi_auth->login($username, $password, TRUE);
                }
                
                
                $output['secure_key'] = md5($user_id.'_key');
                $output['uacc_id'] = $user_id;
                $output['status'] = 'success';
            }
            
            
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    function success(){
        $uacc_id = $this->input->get('u');
        $secure_key = $this->input->get('secure_key');
        $md5_key = md5($uacc_id.'_key');
        
        if($secure_key != $md5_key){
            $this->data['secure_key'] = "invalid";
        }else{
            $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
            $user_profile = $this->far_users->get_profile('uacc_id', $user_detail['uacc_id']);
            
            if(strlen($user_detail['telegram_auth_code']) < 3){
                $telegram_auth_code = $this->far_helper->generateRandomString(10);
                $this->far_users->update_user('uacc_id', $user_detail['uacc_id'], 'telegram_auth_code', $telegram_auth_code);
                $user_detail['telegram_auth_code'] = $telegram_auth_code;
            }
            
            $this->data['user_detail'] = $user_detail;
            $this->data['user_profile'] = $user_profile;
            
            $this->data['secure_key'] = "valid";
        }
        
        
        
        
        
        $this->load->view('register/success', $this->data);
    }
    
    function camera_scan_qrcode(){
        
        $this->load->view('register/camera_scan_qrcode', $this->data);
    }
    
    function send_telegram_or_sms_to_upline_saying_registration_success($uacc_id, $downline_uacc_id){
        $this->load->library('far_telegram');
        $this->load->library('far_sms');
        
        $downline_detail = $this->far_users->get_user('uacc_id', $downline_uacc_id);
        $downline_profile = $this->far_users->get_profile('uacc_id', $downline_detail['uacc_id']);
        
        for($i = 1; $i <= 20; $i++){
            //check if first upline
            $colum_name = "upline_level_".$i;
            $upline_uacc_id = $downline_detail[$colum_name];
            
            $upline_profile = $this->far_users->get_profile('uacc_id', $upline_uacc_id);
            
            if($upline_uacc_id > 1){
                if($i == 1){
                    //first upline
                    $telegram_detail = $this->far_telegram->get_telegram_user('uacc_id', $upline_uacc_id);
                    if(count($telegram_detail) > 0){
                        //send telegram to direct sponsor
                        $tg_message_direct_sponsor = "<b>New User Registration</b>".PHP_EOL.PHP_EOL;
                        $tg_message_direct_sponsor .= "Username:".PHP_EOL;
                        $tg_message_direct_sponsor .= $downline_detail['uacc_username'].PHP_EOL.PHP_EOL;
                        
                        $tg_message_direct_sponsor .= "Fullname:".PHP_EOL;
                        $tg_message_direct_sponsor .= $downline_profile['fullname'].PHP_EOL.PHP_EOL;
                        
                        $tg_message_direct_sponsor .= "Email:".PHP_EOL;
                        $tg_message_direct_sponsor .= $downline_profile['email'].PHP_EOL.PHP_EOL;
                        
                        $tg_message_direct_sponsor .= "Phone:".PHP_EOL;
                        $tg_message_direct_sponsor .= $downline_profile['phonemobile'].PHP_EOL.PHP_EOL;
                        
                        $tg_message_direct_sponsor .= "Sincerely,".PHP_EOL;
                        $tg_message_direct_sponsor .= "The Usahawan (dot) Me Team";
                        
                        $this->far_telegram->send_telegram_queue($telegram_detail['uacc_id'], $tg_message_direct_sponsor);
                    }else{
                        //send sms to upline
                        $sms_user_message = "Usahawan.me".PHP_EOL.PHP_EOL;
                        $sms_user_message .= "New user registration".PHP_EOL;
                        $sms_user_message .= "Username: ".$downline_detail['uacc_username'].PHP_EOL.PHP_EOL;
                        $sms_user_message .= "Fullname: ".$downline_profile['fullname'];
                        $sms_user_msisdn = $this->far_helper->fix_msisdn($upline_profile['phonemobile']);
                        $this->far_sms->send_sms_queue($sms_user_msisdn, $sms_user_message, 'account_registration_success', $user_id);
                    }
                }else{
                    //level 2 and up
                    $telegram_detail = $this->far_telegram->get_telegram_user('uacc_id', $upline_uacc_id);
                    if(count($telegram_detail) > 0){
                        //send telegram to direct sponsor
                        $tg_message_direct_sponsor = "<b>New User Registration</b>".PHP_EOL.PHP_EOL;
                        $tg_message_direct_sponsor .= "Username:".PHP_EOL;
                        $tg_message_direct_sponsor .= $downline_detail['uacc_username'].PHP_EOL.PHP_EOL;
                        
                        $tg_message_direct_sponsor .= "Fullname:".PHP_EOL;
                        $tg_message_direct_sponsor .= $downline_profile['fullname'].PHP_EOL.PHP_EOL;
                        
                        $tg_message_direct_sponsor .= "Email:".PHP_EOL;
                        $tg_message_direct_sponsor .= $downline_profile['email'].PHP_EOL.PHP_EOL;
                        
                        $tg_message_direct_sponsor .= "Phone:".PHP_EOL;
                        $tg_message_direct_sponsor .= $downline_profile['phonemobile'].PHP_EOL.PHP_EOL;
                        
                        $tg_message_direct_sponsor .= "Sincerely,".PHP_EOL;
                        $tg_message_direct_sponsor .= "The Usahawan (dot) Me Team";
                        
                        $this->far_telegram->send_telegram_queue($telegram_detail['uacc_id'], $tg_message_direct_sponsor);
                    }else{
                        //send sms to upline
                        $sms_user_message = "Usahawan.me".PHP_EOL.PHP_EOL;
                        $sms_user_message .= "New user registration".PHP_EOL;
                        $sms_user_message .= "Username: ".$downline_detail['uacc_username'].PHP_EOL;
                        $sms_user_message .= "Fullname: ".$downline_profile['fullname'];
                        $sms_user_msisdn = $this->far_helper->fix_msisdn($upline_profile['phonemobile']);
                        $this->far_sms->send_sms_queue($sms_user_msisdn, $sms_user_message, 'account_registration_success', $user_id);
                    }
                    
                    
                    
                }
                
                
                
                
            }
            
            
            
        }
        
        
        
        
        
        //send to others upline
        
    }
    

    function send_telegram_to_admins_new_user_register($uacc_id){
        $this->load->library('far_telegram');
        
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        $user_profile = $this->far_users->get_profile('uacc_id', $uacc_id);
        
        $upline_detail = $this->far_users->get_user('uacc_id', $user_detail['uacc_upline_id']);
        $upline_profile = $this->far_users->get_profile('uacc_id', $user_detail['uacc_upline_id']);
        
        
        $tg_message_to_admin = "<b>New User Registration</b>".PHP_EOL.PHP_EOL;
        $tg_message_to_admin .= "Username:".PHP_EOL;
        $tg_message_to_admin .= $user_detail['uacc_username'].PHP_EOL.PHP_EOL;
        
        $tg_message_to_admin .= "Fullname:".PHP_EOL;
        $tg_message_to_admin .= $user_profile['fullname'].PHP_EOL.PHP_EOL;
        
        $tg_message_to_admin .= "Email:".PHP_EOL;
        $tg_message_to_admin .= $user_profile['email'].PHP_EOL.PHP_EOL;
        
        $tg_message_to_admin .= "Phone:".PHP_EOL;
        $tg_message_to_admin .= $user_profile['phonemobile'].PHP_EOL.PHP_EOL;
        
        $tg_message_to_admin .= "Upline:".PHP_EOL;
        $tg_message_to_admin .= $upline_profile['fullname'].'( '.$upline_detail['uacc_username'].' )'.PHP_EOL.PHP_EOL;
        
        $tg_message_to_admin .= "Sincerely,".PHP_EOL;
        $tg_message_to_admin .= "The Usahawan (dot) Me Team";
        $this->far_telegram->send_telegram_to_admin($tg_message_to_admin);
    }
    
    private function sms_send_otp_registration($uacc_id){
        $this->load->library('far_sms');
        
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        $user_profile = $this->far_users->get_profile('uacc_id', $uacc_id);
        
        $message = "CP - Your OTP is ".$user_detail['one_time_password'];
        $this->far_sms->send_sms($user_profile['phonemobile'], $message, "bulksms", $uacc_id);
    }
    
    function testingsms(){
        $this->load->library('curl');
        
        $user_detail = $this->far_users->get_user('uacc_id', $user_id);
        $user_id = '0';

        $phoneX = str_replace("-","",$user_detail['hp_number']);
        $phone = str_replace(" ","",$phoneX);
        
        if($phone[0] == '0'){
            $phone = '6'.$phone;
        }
        
        $phone = '60177973943';

        $message = 'Gi mana?';
        //$sms_status = $this->curl->simple_get('http://api.gosms.com.my/eapi/sms.aspx?company=apostrophe&user=edenads&password=ZWRlbjEzMTM%3D&gateway=I&mode=BUK&type=TX&hp='.$mobile_number.'&mesg='.urlencode($message).'&charge=0&maskid=digi&convert=0');
        $gosms_url = 'http://api.gosms.com.my/eapi/sms.aspx?company='.$this->far_meta->get_value('gosms_company').'&user='.$this->far_meta->get_value('gosms_username').'&password='.$this->far_meta->get_value('gosms_password').'&gateway=L&mode=BUK&type=TX&hp='.$phone.'&mesg='.urlencode($message).'&charge=0&maskid=digi&convert=0';
        
        echo $gosms_url; exit();
        
        $sms_status = $this->curl->simple_get($gosms_url);
        
        
        
        $data = array(
            'uacc_id' => $user_id,
            'msisdn' => $phone,
            'message' => $message,
            'sent_dttm' => date('Y-m-d H:i:s'),
            'bulksms_status' => $sms_status,
            'message_type' => 'test_sms'
        );
        $this->far_sms->insert_smsout($data);
        
        return $sms_status;
    }
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */