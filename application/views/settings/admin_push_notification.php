<?php $this->load->view('includes/header'); ?>
<div class="row">
    <div class="col-md-12 form-horizontal">
        <div class="form-body">
            
            
            <ul class="nav nav-tabs">
                <li class="active">
				    <a href="#onesignal" data-toggle="tab"> OneSignal </a>
				</li>
				<li>
				    <a href="#text_message" data-toggle="tab"> Message </a>
				</li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="onesignal">
				    <h3 class="block">OneSignal Configuration</h3>
                    <div class="form-group">
                        <label class="col-md-3 control-label">OneSignal App ID</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Enter APP ID" value="<?php echo $meta_onesignal_app_id; ?>" id="meta_onesignal_app_id" />
                            <span class="help-block error_message" style="display: none;"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">OneSignal API Key</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Enter API Key" value="<?php echo $meta_onesignal_api_key; ?>" id="meta_onesignal_api_key" />
                            <span class="help-block error_message" style="display: none;"></span>
                        </div>
                    </div>
                    <div class="row">
        				<div class="col-md-offset-3 col-md-9">
        				    <button type="button" onclick="javascript: btn_onesignal_config_click_submit();" class="btn green">Submit</button>
                        </div>
                    </div>
				</div>
                <div class="tab-pane fade" id="text_message">
				    <p>Under construction</p>
				</div>
            </div>
            
            
            
            
            
            
        </div>
        
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript">
function btn_onesignal_config_click_submit(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    //check meta_onesignal_app_id
    var meta_onesignal_app_id = $("#meta_onesignal_app_id").val();
    if (meta_onesignal_app_id === false){ 
        $("#meta_onesignal_app_id").closest('.form-group').addClass('has-error'); 
        $("#meta_onesignal_app_id").closest('.form-group').find('.error_message').text("OneSignal App ID cannot blank").show(); return false
    }
    if (meta_onesignal_app_id === "") {
        $("#meta_onesignal_app_id").closest('.form-group').addClass('has-error'); 
        $("#meta_onesignal_app_id").closest('.form-group').find('.error_message').text("OneSignal App ID cannot blank").show(); return false
    }
    
    //check meta_onesignal_api_key
    var meta_onesignal_api_key = $("#meta_onesignal_api_key").val();
    if (meta_onesignal_api_key === false){ 
        $("#meta_onesignal_api_key").closest('.form-group').addClass('has-error'); 
        $("#meta_onesignal_api_key").closest('.form-group').find('.error_message').text("OneSignal API Key cannot blank").show(); return false
    }
    if (meta_onesignal_api_key === "") {
        $("#meta_onesignal_api_key").closest('.form-group').addClass('has-error'); 
        $("#meta_onesignal_api_key").closest('.form-group').find('.error_message').text("OneSignal API Key cannot blank").show(); return false
    }
    
    Metronic.blockUI({
        boxed: true,
        message: 'Sending data to server...'
    });

    $.ajax({
        url: '<?php echo base_url(); ?>settings/admin_update_onesignal_configuration',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                onesignal_app_id: meta_onesignal_app_id, 
                onesignal_api_key: meta_onesignal_api_key
            } 
        },
        success: function(data){
            Metronic.unblockUI();
            if(data.status == "success"){
                
                swal({
                    title: "Good job!",
                    text: "OneSignal Configuration has been updated",
                    type: "success",
                    closeOnConfirm: true
                })
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
            
            
        }
    })

    

}
</script>