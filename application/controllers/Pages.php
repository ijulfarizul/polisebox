<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function change_site_lang(){
        $this->load->library('user_agent');
        
        $new_site_lang = $this->input->get('new_site_lang');
        $this->session->set_userdata('site_lang', $new_site_lang);
        
        if(strlen($this->agent->referrer()) > 3){
            redirect($this->agent->referrer());
        }else{
            redirect(base_url());
        }
    }
    
    function mainpage(){
        $this->load->library('far_translation');
        
        $lang = $this->far_translation->list_idiom($this->session->site_lang);
        $this->data['lang'] = $lang;
        
        //url alias
        $url_alias = $this->input->get('url_alias');
        if(strlen($url_alias) >= 3){
            $sponsor_detail = $this->far_users->get_user('url_alias', $url_alias);
            if(count($sponsor_detail) > 3){
                $referral_uacc_id = $sponsor_detail['uacc_id'];
            }else{
                $referral_uacc_id = 1;
            }
        }else{
            $sponsor_detail = $this->far_users->get_user('url_alias', 'cops');
            $referral_uacc_id = $sponsor_detail['uacc_id'];
        }
        
        $this->data['referral_uacc_id'] = $referral_uacc_id;
        $this->data['sponsor_detail'] = $sponsor_detail;
        
        
        $this->load->view('pages/mainpage', $this->data);
    }
    
    

    
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */