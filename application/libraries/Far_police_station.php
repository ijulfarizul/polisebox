<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Far_police_station {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    /**
     * Get User Account
     */
    /**
    function get_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    */
    /**
    function list_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->result_array();
    }
    */
    
    /**
     * Insert / Create profile
     * @param array $data 
     */
    /**
    function insert_profile($data){
        $this->CI->db->insert('user_profiles', $data);
        return $this->CI->db->insert_id();
    }
    */
    /**
     * Update
     * @param string $key Database column name
     * @param string $key_value Database column value(s)
     * @param string $column Column name to update
     * @param string $value New value for specified column
     */
    /**
    function update_user($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('user_accounts', $data); 
    }
    */
    function insert_police_station($data){
        $this->CI->db->insert('police_station', $data);
        return $this->CI->db->insert_id();
    }
    
    function delete_police_station($ps_id){
        //delete police station
        $this->CI->db->where('ps_id', $ps_id);
        $this->CI->db->delete('police_station');
    }
    function get_police_station($column, $value){
        $query = $this->CI->db->query("SELECT * FROM police_station WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    function update_police_station($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('police_station', $data); 
    }
    
}


?>