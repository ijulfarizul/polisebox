<?php $this->load->view('includes/header'); ?>
<!-- BEGIN PAGE CONTENT-->
<div class="row form-horizontal">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-md-3 control-label">Quantity</label>
            <div class="col-md-9">
                <input type="text" class="form-control input-large" placeholder="Quantity" id="quantity">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        <div class="form-group">
        	<label class="col-md-3 control-label">&nbsp;</label>
        	<div class="col-md-9">
        		<button class="btn green" onclick="javascript: btn_click_submit();">Submit <i class="fa fa-arrow-right"></i></button>
                <span class="help-block error_message" style="display: none;"></span>
        	</div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript">
function btn_click_submit(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    var quantity = $("#quantity").val();
    
    blockUI();
    $.ajax({
        url: "<?php echo base_url(); ?>qrcodes/ajax_admin_generate_qrcode",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                quantity: quantity
            }
        },
        success: function(data){
            $.unblockUI();
            if(data.status == "success"){
                
                swal({
                    title: "Success!",
                    text: "QR Code has been generated",
                    type: "success",
                    closeOnConfirm: true
                },
                function(){
                    window.location = "<?php echo base_url(); ?>qrcodes/admin_list_all_qrcode";
                });
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
        }
    })
}
</script>