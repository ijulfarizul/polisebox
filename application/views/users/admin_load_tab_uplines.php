<div class="tabbable">
	<ul class="nav nav-tabs">
		<li class="active">
		<a href="#tab_upline_unilevel" data-toggle="tab">
		Unilevel Upline<sup>s</sup> </a>
		</li>
		<li>
		<a href="#tab_upline_compress" data-toggle="tab">
		Compress Upline<sup>s</sup> </a>
		</li>
		<li>
		<a href="#tab_upline_change" data-toggle="tab">
		Change Upline</a>
		</li>
	</ul>
	<div class="tab-content no-space">
		<div class="tab-pane active" id="tab_upline_unilevel">
        
		</div>
		<div class="tab-pane" id="tab_upline_compress">
			
		</div>
		<div class="tab-pane" id="tab_upline_change">
			<form action="#" method="post" id="form_tab_change_upline">
                <div class="form-body">
                    
                    <div class="form-group">
                        <label class="col-md-2 control-label">&nbsp;</label>
                        <div class="col-md-10">
                            <h3 class="form-section">Current Upline</h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Username: </label>
                        <div class="col-md-10">
                            <input type="text" readonly="" class="form-control" name="user_detail[uacc_username]" value="<?php echo $upline_detail['uacc_username']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Fullname: </label>
                        <div class="col-md-10">
                            <input type="text" readonly="" class="form-control" name="user_profiles[lastname]" value="<?php echo $upline_profile['firstname'].' '.$upline_profile['lastname']; ?>">
                        </div>
                    </div>
                
                    <hr />
                    <div class="form-group">
                        <label class="col-md-2 control-label">&nbsp;</label>
                        <div class="col-md-10">
                            <h3 class="form-section">New Upline</h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">&nbsp;</label>
                        <div class="col-md-10">
                            <div class="input-group">
                                <input type="text" readonly="" class="form-control" id="new_upline_text" value="" />
                                <input type="hidden" id="new_upline_uacc_id" />
                                <span class="input-group-btn">
                                    <button class="btn red" type="button" id="btn_click_open_modal_user">Click to select user</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">&nbsp;</label>
                        <div class="col-md-10">
                            <button type="button" class="btn green">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/intelmlm/far-modal-list-user.js"></script>
<script type="text/javascript">

 
// Usage example:
$("#btn_click_open_modal_user").FarModalListUser({
    url: '<?php echo base_url(); ?>users/ajax_modal_list_all_users',
    data: { uacc_id: '<?php echo $logged_in['uacc_id']; ?>' },
    selectUserClassBtn: '.select_user_btn',
    afterSelectUserHiddenInputId: "#new_upline_uacc_id"
});
</script>