<?php $this->load->view('includes/header'); 
$CI =& get_instance();
//$CI->load->library('far_hierarchy');
?> 
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Quicksand" />
<style>
.contact_profile {
    width: 37px !important;
    height: 37px !important;
    display: inline-block;
    background-size: contain !important;
}
</style>
<div class="row">
				<div class="col-md-12 form-horizontal form-row-seperated">
					
						<div class="portlet">
							
							<div class="portlet-body" id="body_frame" >
								<div class="tabbable">
									<ul class="nav nav-tabs">
										<li class="active">
											<a href="#tab_account" data-toggle="tab">
											Account </a>
										</li>
                                        <li>
											<a href="#tab_profile" data-toggle="tab">
											Profile </a>
										</li>
                                        <li>
											<a href="#tab_uplines" data-toggle="tab">
											Upline<sup>s</sup> </a>
										</li>
									</ul>
									<div class="tab-content no-space">
                                        
										<div class="tab-pane active" id="tab_account">
                                            <form action="<?php echo base_url(); ?>products/save/" method="post" id="form_general">
											<div class="form-body">
                                                <div class="form-group">
													<label class="col-md-2 control-label">Username: 
													</label>
													<div class="col-md-10">
														<input type="text" readonly="" class="form-control" name="user_detail[uacc_username]" value="<?php echo $user_detail['uacc_username']; ?>">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-2 control-label">Password: 
													</label>
													<div class="col-md-10">
                                                        <div class="input-group">
                											
                											<input type="text" readonly="" class="form-control" id="user_password" value="<?php echo $user_detail['uacc_raw_password']; ?>">
                                                            <span class="input-group-btn">
                											<button class="btn red" type="button" onclick="javascript: btn_click_to_change_password('<?php echo $user_detail['uacc_id']; ?>');">Click to change</button>
                											</span>
                										</div>
														
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-2 control-label">Email: 
													</label>
													<div class="col-md-10">
														<input type="text" readonly="" class="form-control" name="user_detail[uacc_email]" value="<?php echo $user_detail['uacc_email']; ?>">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-2 control-label">Last IP: 
													</label>
													<div class="col-md-10">
														<input type="text" readonly="" class="form-control" name="user_detail[uacc_ip_address]" value="<?php echo $user_detail['uacc_ip_address']; ?>">
													</div>
												</div>
                                                <div class="form-group">
													<label class="col-md-2 control-label">Phone Number: 
													</label>
													<div class="col-md-10">
														<input type="text" readonly="" class="form-control" name="user_detail[hp_number]" value="+<?php echo $user_detail['hp_number']; ?>">
													</div>
												</div>
											</div>
                                            </form>
										</div>
                                        
                                        <div class="tab-pane" id="tab_profile">
                                            <div id="frame_admin_load_tab_profile"></div>
										</div>
                                        
                                        <div class="tab-pane" id="tab_uplines">
                                            <div id="frame_admin_load_tab_uplines"></div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					
				</div>
			</div>
<div id="modal_click_to_change_password" class="modal fade modal-scroll" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Change password confirmation</h4>
    </div>
    <div class="modal-body" id="modal_body_click_to_change_password">
        <div id="modal_click_to_change_password_error_frame" style="display: none;">
            <div class="alert alert-danger">
                
            </div>
        </div>
        
        <div id="modal_click_to_change_password_success_frame" style="display: none;">
            <div class="alert alert-success">
                <strong>Success !!</strong> Password has been changed.
            </div>
        </div>
        
        
        <p>You are about to change user&#39;s password. This action cannot be undone. Click confirm to change.</p>
        
        
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <td>User ID :</td>
                    <td> <span id="successRegUsername"><?php echo $user_detail['uacc_id']; ?></span> </td>
                </tr>
                <tr>
                    <td>Username :</td>
                    <td> <span id="successRegUsername"><?php echo $user_detail['uacc_username']; ?></span> </td>
                </tr>
                <tr>
                    <td>Fullname :</td>
                    <td><?php echo $app_profile['firstname'].' '.$app_profile['lastname']; ?></td>
                </tr>
                <tr>
                    <td>Current Password :</td>
                    <td><?php echo $user_detail['uacc_raw_password']; ?></td>
                </tr>
                <tr>
                    <td>New Password :</td>
                    <td><input type="text" class="form-control input-sm" id="input_new_password" /></td>
                </tr>
            </tbody>
        </table>
        <div class="form-group">
        
            <div class="checkbox-list">
                
                <label>
                    <input type="checkbox" id="send_sms" name="send_sms" value="yes" checked=""> Send new password using sms to <?php echo $user_detail['hp_number']; ?>
                </label>
                <label>
                    <input type="checkbox" id="send_email" name="send_email" value="yes" checked=""> Send new password using email to <?php echo $user_detail['uacc_email']; ?>
                </label>
            </div>
        </div>
        
    </div>
    <div class="modal-footer">
        <button type="button" class="btn red" onclick="javascript: confirm_change_password('<?php echo $user_detail['uacc_id']; ?>');" id="confirm_change_password">Confirm</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
    </div>
</div>

<div id="modal_create_billplz" class="modal fade modal-scroll" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Create new Billplz bill</h4>
    </div>
    <div class="modal-body" id="modal_body_click_to_change_password">
        
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <td>API Key :</td>
                    <td> 
                    <input id="billplz[api_key]" type="text" class="form-control input-sm" value="<?php echo $this->far_meta->get_value('billplz_api_key'); ?>" />
                     </td>
                </tr>
                <tr>
                    <td>Collection ID :</td>
                    <td> 
                    <input name="billplz[collection_id]" id="billplz_collection_id" type="text" class="form-control input-sm" value="<?php echo $this->far_meta->get_value('billplz_collection_id'); ?>" />
                     </td>
                </tr>
                <tr>
                    <td>Email :</td>
                    <td> 
                        <input name="billplz[email]" id="billplz_email" type="text" class="form-control input-sm" value="<?php echo $user_detail['uacc_email']; ?>" />
                    </td>
                </tr>
                <tr>
                    <td>Name :</td>
                    <td>
                        <input name="billplz[name]" id="billplz_name" type="text" class="form-control input-sm" value="<?php echo $app_profile['firstname'].' '.$app_profile['lastname']; ?>" />
                    </td>
                </tr>
                <tr>
                    <td>Due at :</td>
                    <td>
                        <input name="billplz[due_at]" id="billplz_due_at" type="text" class="form-control input-sm" value="2020-1-1" />
                    </td>
                </tr>
                <tr>
                    <td>Amount :</td>
                    <td>
                        <?php
                        //get amount
                        $price = '100';
                        $rate = $this->far_meta->get_value('usd_myr_rate');
                        $price_in_myr = $price*$rate.'00';
                        ?>
                        <input name="billplz[amount]" id="billplz_amount" type="text" class="form-control input-sm" value="<?php echo sprintf('%.2f', $price_in_myr / 100); ?>" />
                    </td>
                </tr>
                <tr>
                    <td>Callback URL :</td>
                    <td>
                        <input name="billplz[callback_url]" id="billplz_callback_url" type="text" class="form-control input-sm" value="<?php echo $this->far_meta->get_value('billplz_callback_url'); ?>" />
                    </td>
                </tr>
            </tbody>
        </table>
        
        
    </div>
    <div class="modal-footer">
        <button type="button" class="btn red" onclick="javascript: confirm_create_billplz('<?php echo $user_detail['uacc_id']; ?>');" id="confirm_create_billplz">Confirm</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
    </div>
</div>

<div id="modal-modal_list_downline_and_upline" class="modal modal-scroll container fade" tabindex="-1" data-replace="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Select User</h4>
    </div>
    <div class="modal-body" id="modal-body-modal_list_downline_and_upline">
				
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    </div>
</div>

<div id="modal_confirm_change_upline" class="modal modal-scroll container fade bg-red" tabindex="-1" data-replace="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Confirm Action</h4>
    </div>
    <div class="modal-body" id="body_modal_confirm_change_upline">
        <div id="change_upline_return_message"></div>
        <div id="modal_body_only_confirm_change_upline">
            <div class="alert alert-danger bg-red">
                <strong>Last Warning!</strong> Confirm this action without any regrets
            </div>
        </div>
        
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
        <button type="button" class="btn red" onclick="javascript: confirm_change_upline('<?php echo $user_detail['uacc_id']; ?>');" id="confirm_change_upline" style="border: 1px solid #FFFFFF;">Confirm</button>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>


<script type="text/javascript">
$(function(){
    
    Metronic.blockUI({
        boxed: true,
        message: 'Loading page. This will take some time...'
    });
    
    
    admin_load_tab_profile();
    admin_load_tab_uplines();
    
    
    $(document).ajaxStop(function () {
      console.log('all ajax finish')
      Metronic.unblockUI();
    });
    
});

function admin_load_tab_profile(){
    $("#frame_admin_load_tab_profile").load('<?php echo base_url(); ?>users/admin_load_tab_profile', { uacc_id: '<?php echo $user_detail['uacc_id']; ?>' }, function(){
        console.log('Profile loaded');
    });
}

function admin_load_tab_uplines(){
    $("#frame_admin_load_tab_uplines").load('<?php echo base_url(); ?>users/admin_load_tab_uplines', { uacc_id: '<?php echo $user_detail['uacc_id']; ?>' }, function(){
        console.log('Uplines loaded');
    });
}





function btn_click_to_change_password(uacc_id){
    
    $('#input_new_password').alphanumeric();
    $("#modal_click_to_change_password").modal('show');
    
    Metronic.blockUI({
        target: '#modal_click_to_change_password',
        boxed: true,
        message: 'Loading...'
    });
    
    var random_password = randomPassword();
    $("#input_new_password").val(random_password);
    Metronic.unblockUI('#modal_click_to_change_password');
    
    
    
}

function randomPassword()
{
    var text = "";
    var possible = "ABCEFGHJKLMNPQRSTUVWXYZ3456789";

    for( var i=0; i < 6; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function confirm_change_password(uacc_id){
    var new_password = $("#input_new_password").val();
    var send_sms = 'no';
    var send_email = 'no';
    
    if ($('#send_sms').is(":checked")) {
        send_sms = 'yes';
    }
    
    if ($('#send_email').is(":checked")) {
        send_email = 'yes';
    }
    
    $("#modal_click_to_change_password_error_frame").children('div.alert').html('');
    $("#modal_click_to_change_password_error_frame").hide();
    
    $("#modal_click_to_change_password_success_frame").hide();
    
    Metronic.blockUI({
        target: '#modal_click_to_change_password',
        boxed: true,
        message: 'Updating...'
    });
    
    var postdata = { 
        send_sms: send_sms, send_email: send_email, uacc_id: uacc_id, new_password: new_password 
    }
    
    $.ajax({
        url: '<?php echo base_url(); ?>settings/ajax_admin_change_user_password',
        type: 'post',
        dataType: 'json',
        data: { postdata: postdata },
        success: function(data){
            
            if(data.status == 'success'){
                
                $("#modal_click_to_change_password_success_frame").show();
                setTimeout(function(){ 
                    $("#modal_click_to_change_password").modal('hide');
                }, 3000);
                
            }else if(data.status == 'error'){
                
                $.each(data.error_msg, function(i,j){
                    var p = $("#modal_click_to_change_password_error_frame").children('div.alert');
                    var text = j.text;
                    $('<p>'+text+'</p>').appendTo(p);
                    
                });
                
                $("#modal_click_to_change_password_error_frame").show();
                
            }
            
            $("#user_password").val(new_password);
            Metronic.unblockUI('#modal_click_to_change_password');
            
        }
    })
    
    console.log(send_sms);
}


function admin_view_downline(uacc_id){
    
    $("#ajax-modal-admin_view_downline").modal('show');
    
    $("#ajax-modal-admin_view_downline_modal_body").load('<?php echo base_url(); ?>hierarchy/admin_ajax_view_downline', { uacc_id: uacc_id }, function(){
        Metronic.unblockUI("#ajax-modal-admin_view_downline");
    });
    
}





function modal_select_user(){
    
    
    Metronic.blockUI({
        boxed: true,
        message: 'Loading...'
    });
    
    $("#modal-body-modal_list_downline_and_upline").load('<?php echo base_url().'wallet/member_ajax_search_user_transfer_3rd_party'; ?>', 
        { 
            universal_show_all_user: 'yes'
        }
    , function(){
        console.log('ok - search user');
        $("#modal-modal_list_downline_and_upline").modal('show');
        Metronic.unblockUI();
    })
}

function onclick_select_user(selected){
    var data = $(selected).data();
    var choose_fullname = data.uaccusername+' ( '+data.fullname+' )';
    $("#choose_fullname").val(choose_fullname);
    $("#choose_uacc_id").val(data.uaccid);
    $("#choose_uacc_username").val(data.uaccusername);
    
    $("#modal-modal_list_downline_and_upline").modal("hide");
}

function open_modal_confirm_change_upline(){
    var new_upline = $("#choose_uacc_id").val();
    
    if(new_upline.length == 0){
        Apprise("Please select upline");
        return false;
    }
    $("#modal_body_only_confirm_change_upline").show();
    $("#change_upline_return_message").html("");
    $("#modal_confirm_change_upline").addClass('bg-red');
    $("#confirm_change_upline").show();
    $("#modal_confirm_change_upline").modal('show');
}

function confirm_change_upline(uacc_id){
    var new_upline = $("#choose_uacc_id").val();
    
    if(new_upline.length == 0){
        Apprise("Please select upline");
        return false;
    }
    
    $("#change_upline_return_message").load('<?php echo base_url(); ?>reports/admin_change_upline', {
        uacc_id: uacc_id,
        new_upline: new_upline
    },function(){
        console.log('change upline task success');
    })
    
    console.log(uacc_id)
}




</script>
		