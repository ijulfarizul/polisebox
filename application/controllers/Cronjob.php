<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronjob extends MY_Controller {
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		$this->data = null;
	}
    
    function daily_backup_db(){
        $user = $this->db->username;
        $password = $this->db->password;
        $host = $this->db->hostname;
        $db_name = $this->db->database;
        $output_path = APPPATH."backup_db/";
        $filename = date('Y-m-d_H-i-s').'.sql';


        $exported_db_name = $output_path.$filename;
        
        exec('mysqldump --user='.$user.' --password='.$password.' --host='.$host.' '.$db_name.' > '.$exported_db_name);
        echo $filename." success";
    }
    

    
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */