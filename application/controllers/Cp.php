<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cp extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function admin_search_volunteer(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        if ($this->input->post('search_user')){
			$this->data['search_return'] = $this->process_search_volunteer($this->input->post());
		}
        
        $this->data['message'] = $this->session->flashdata('message');
        $this->load->view('cp/admin/admin_search_volunteer', $this->data);
    }
    
    function process_search_volunteer($search_array){
        
        $postdata = $this->input->post();
        $s = $search_array;
        
        
        
        //build query
        if($s['search_fullname']){ $where_array[] = 'p.fullname LIKE "%'.$s['search_fullname'].'%"'; }
        //search_product_description
        if($s['search_serial_number']){ $where_array[] = 'qr.serial_number LIKE "%'.$s['search_serial_number'].'%"'; }
        //search_product_url_alias
        if($s['search_product_url_alias']){ $where_array[] = 'product_url_alias LIKE "%'.$s['search_product_url_alias'].'%"'; }
        //search_website_without_owner 
        if($s['search_website_without_owner'] == "yes"){ $where_array_exact[] = 'uacc_id IS NULL'; }
        
        
        $where_exact = '';
        $where_array_exact[] = 'u.uacc_group_fk="6"';
        if(count($where_array_exact) > 0){
            $where_exact = implode(' AND ', $where_array_exact);
        }
        
        if((count($where_array) != 0) || (count($where_array_exact) != 0)){
            $where = implode(' OR ', $where_array);
            
            if((count($where_array) == 0) && (count($where_array_exact) > 0)){
                $sql = "SELECT * FROM user_profiles p LEFT JOIN user_accounts u ON p.uacc_id=u.uacc_id WHERE ".$where_exact;
            }else{
                if(count($where_array_exact) > 0){
                    $sql = "SELECT * FROM user_profiles p LEFT JOIN user_accounts u ON p.uacc_id=u.uacc_id WHERE ".$where.' AND '.$where_exact;
                }else{
                    $sql = "SELECT * FROM user_profiles p LEFT JOIN user_accounts u ON p.uacc_id=u.uacc_id WHERE ".$where;
                }
                
            }
            
            //echo $sql; exit();
            
            $query = $this->db->query($sql);
            $list_website = $query->result_array();
            
            
        }else{
            
        }
        
        
        return $list_website;
    }
    
    function admin_add_volunteer(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        
        $list_all_country = $this->far_location->list_all_country();
        $this->data['list_all_country'] = $list_all_country;
        
        $this->load->view('cp/admin/admin_add_volunteer', $this->data);
    }
    
    function ajax_admin_add_volunteer(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $this->load->library('far_helper');
        $error = array();
        $output = array();
        
        $list_areas = array_filter($postdata['areas']);
        
        //check fullname
        if(strlen($postdata['fullname']) <= 5){
            $error['fullname'] = "Fullname must be at least 6 characters";
        }
        
        if(strlen($postdata['nric']) <= 8){
            $error['nric'] = "NRIC must be at least 8 characters";
        }
        
        if (!preg_match('/^[0-9]+$/', $postdata['nric'])) {
            $error['nric'] = "NRIC must be number only";
        }
        
        if(count($list_areas) == 0){
            $error['area'] = "Please select area";
        }
        
        
        
        if(count($error) == 0){
            
            //create user
            $username = $postdata['cp_id_number'];
            $email = $username.'@cp.polisebox.com';
            $password = $this->far_helper->generateRandomString(6);
            $user_data = array();
            $group_id = 6;
            $activate = TRUE;
            $user_id = $this->flexi_auth->insert_user($email, $username, $password, $user_data, $group_id, $activate);
            if($user_id){
                $this->far_users->update_user('uacc_id', $user_id, 'uacc_raw_password', $password);
                
                //update profile
                $this->far_users->insert_profile(array('uacc_id' => $user_id));
                $this->far_users->update_profile('uacc_id', $user_id, 'fullname', $postdata['fullname']);
                $this->far_users->update_profile('uacc_id', $user_id, 'nric', $postdata['nric']);
                $this->far_users->update_profile('uacc_id', $user_id, 'cp_id_number', $postdata['cp_id_number']);
                $this->far_users->update_profile('uacc_id', $user_id, 'phonemobile', $postdata['phonemobile']);
                
                //cp_lc_id
                //get city by area
                $area_detail = $this->far_location->get_area('area_id', $list_areas[0]);
                $this->far_users->update_profile('uacc_id', $user_id, 'cp_lc_id', $area_detail['lc_id']);
                $this->far_users->update_profile('uacc_id', $user_id, 'cp_ls_id', $area_detail['ls_id']);
                $this->far_users->update_profile('uacc_id', $user_id, 'cp_lct_id', $area_detail['lct_id']);
                //implode area
                $implode_area = implode(',', $list_areas);
                $this->far_users->update_profile('uacc_id', $user_id, 'cp_area_id', $implode_area);
                
                $output['redirect_url'] = base_url().'cp/admin_view_cp_detail/?uacc_id='.$user_id;
                $output['status'] = 'success';
            }else{
                $error['user_creation'] = "User creation error. Please try again using different input";
                $output['message_single'] = current($error);
                $output['errors'] = $error;
                $output['status'] = 'error';
            }
            
            

            
            
            
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
    function ajax_admin_edit_volunteer(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_location');
        $this->load->library('far_helper');
        $error = array();
        $output = array();
        
        $list_areas = array_filter($postdata['areas']);
        
        //check fullname
        if(strlen($postdata['fullname']) <= 5){
            $error['fullname'] = "Fullname must be at least 6 characters";
        }
        
        if(strlen($postdata['nric']) <= 8){
            $error['nric'] = "NRIC must be at least 8 characters";
        }
        
        if (!preg_match('/^[0-9]+$/', $postdata['nric'])) {
            $error['nric'] = "NRIC must be number only";
        }
        
        if(count($list_areas) == 0){
            $error['area'] = "Please select area";
        }
        
        //check user detail
        $user_detail = $this->far_users->get_user('uacc_id', $postdata['uacc_id']);
        if(count($user_detail) == 0){
            $error['uacc_id'] = "User not exists";
        }
        
        if(count($error) == 0){
            
            
            $this->far_users->update_profile('uacc_id', $user_detail['uacc_id'], 'fullname', $postdata['fullname']);
            $this->far_users->update_profile('uacc_id', $user_detail['uacc_id'], 'nric', $postdata['nric']);
            $this->far_users->update_profile('uacc_id', $user_detail['uacc_id'], 'cp_id_number', $postdata['cp_id_number']);
            $this->far_users->update_profile('uacc_id', $user_detail['uacc_id'], 'phonemobile', $postdata['phonemobile']);
            
            //cp_lc_id
            //get city by area
            $area_detail = $this->far_location->get_area('area_id', $list_areas[0]);
            $this->far_users->update_profile('uacc_id', $user_detail['uacc_id'], 'cp_lc_id', $area_detail['lc_id']);
            $this->far_users->update_profile('uacc_id', $user_detail['uacc_id'], 'cp_ls_id', $area_detail['ls_id']);
            $this->far_users->update_profile('uacc_id', $user_detail['uacc_id'], 'cp_lct_id', $area_detail['lct_id']);
            //implode area
            $implode_area = implode(',', $list_areas);
            $this->far_users->update_profile('uacc_id', $user_detail['uacc_id'], 'cp_area_id', $implode_area);
            
            $output['status'] = 'success';
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
    function ajax_admin_delete_cp(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_cp');
        $error = array();
        $output = array();
        
        if(count($error) == 0){
            
            //delete from database
            $this->far_cp->delete_cp_volunteer($postdata['uacc_id']);
            
            $output['status'] = 'success';
        }else{
            
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    
    function admin_view_cp_detail(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_location');
        $page = $this->input->get('page');
        $uacc_id = $this->input->get('uacc_id');
        
        $this->data['page'] = $page;
        
        //user_detail
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        $this->data['user_detail'] = $user_detail;
        
        $user_profile = $this->far_users->get_profile('uacc_id', $uacc_id);
        $this->data['user_profile'] = $user_profile;
        
        
        if($page == 'facebook_post'){
        
        }elseif($page == 'log_report'){
            
            $this->load->view('cp/admin_view_cp_detail/log_report', $this->data);
        }else{
            
            $list_all_country = $this->far_location->list_all_country();
            $this->data['list_all_country'] = $list_all_country;
            
            $this->load->view('cp/admin_view_cp_detail/profiles', $this->data);
        }
        
    }
    
    function ajax_admin_view_cp_detail_log_report(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');
        $postdata = $this->input->post('postdata');
        
        $this->datatables->select('lr.log_id');
        $this->datatables->select('lr.cp_uacc_id');
        $this->datatables->select('lr.serial_number');
        $this->datatables->select('lr.create_dttm');
        $this->datatables->select('lp.photo_url');
        $this->datatables->from('cp_log_report lr');
        $this->datatables->join('cp_log_photo lp', 'lr.log_id=lp.log_id', 'left');
        
        $this->datatables->where('lr.cp_uacc_id="'.$postdata['cp_uacc_id'].'"');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function datatables(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->view('order/user_purchase_report', $this->data);
    }
    
    function ajax_datatables(){
        $this->far_auth->allowed_group('4,5', $this->user['ugrp_id']);
        $this->load->library('datatables');
        $uacc_created_by = $this->user['uacc_id'];
        
        $this->datatables->select('u.uacc_id');
        $this->datatables->select('u.uacc_username');
        $this->datatables->select('p.fullname');
        $this->datatables->select('od.od_invoice_number');
        $this->datatables->select('od.od_quantity');
        $this->datatables->select('od.od_status');
        $this->datatables->select('od.od_create_dttm');
        $this->datatables->select('pd.pd_name');
        $this->datatables->from('far_order od');
        $this->datatables->join('far_product pd', 'od.pd_id=pd.pd_id', 'left');
        $this->datatables->join('user_accounts u', 'od.uacc_id=u.uacc_id', 'left');
        $this->datatables->join('user_profiles p', 'u.uacc_id=p.uacc_id', 'left');
        
        $this->datatables->where('od.uacc_id="'.$uacc_created_by.'"');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    

    
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */