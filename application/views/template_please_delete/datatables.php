<?php $this->load->view('includes/header'); ?>
<a href="<?php echo base_url(); ?>order/admin_create_new_order" class="btn blue">
    <i class="fa fa-plus"></i> Create New Order
</a>
<br />
<br />
<div class="table-container">
    <table id="example" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Username</th>
                <th>Upline</th>
                <th>Stockist</th>
                <th>Package Name</th>
                <th>Quantity</th>
                <th>Order Date</th>
                <th>Status</th>
                <th>Actions<sup>(s)</sup></th>
            </tr>
        </thead>                             
    </table>
</div>
<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>
<script type="text/javascript">
$(function(){
    
    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>order/admin_ajax_list_all_order",
            "type": "POST"
        },
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "columns": [
            { "data": "uacc_username" },
            { "data": "upline_username" },
            { "data": "stockist_username" },
            { "data": "pd_name" },
            { "data": "od_quantity" },
            { "data": "od_create_dttm" },
            { "data": "od_status", "render": function ( data, type, row ) {
                var e_od_status;
                if(row.od_status == "ordered"){
                    e_od_status = '<span class="label label-primary">Ordered </span>';
                } else if(row.od_status == "completed") {
                    e_od_status = '<span class="label label-success">Completed </span>';
                }
                return e_od_status
            } },
            { "data": "fullname", "render": function ( data, type, row ) {
                redited = '<a href="<?php echo base_url(); ?>order/view/'+row.od_invoice_number+'" class="btn btn-xs green"><i class="fa fa-search"></i> View</a>';
                return redited
            } },
        ],
        "order": [5, 'desc']
    } );

})
</script>
<script type="text/javascript">
var htmlModalViewUser="";
htmlModalViewUser += "<div id=\"modal_view_user_ajax\" class=\"modal fade\" tabindex=\"-1\" data-width=\"760\">";
htmlModalViewUser += "<div class=\"modal-body\">";
htmlModalViewUser += "<div id=\"modal_body_view_user_ajax\"><\/div>";
htmlModalViewUser += "<\/div>";
htmlModalViewUser += "<div class=\"modal-footer\">";
htmlModalViewUser += "<button type=\"button\" class=\"btn red\" data-dismiss=\"modal\">Close<\/button>";
htmlModalViewUser += "<\/div>";
htmlModalViewUser += "<\/div>";

$(htmlModalViewUser).appendTo('body');


function open_modal_view_user(uacc_username){
    
    Metronic.blockUI({
        boxed: true,
        message: 'Loading..'
    });
    
    $("#modal_body_view_user_ajax").load('<?php echo base_url(); ?>users/ajax_modal_view_user',{
        postdata: {
            uacc_username: uacc_username
        }
    }, function(data){
        Metronic.unblockUI();
        $("#modal_view_user_ajax").modal("show");
    })
    
}
</script>