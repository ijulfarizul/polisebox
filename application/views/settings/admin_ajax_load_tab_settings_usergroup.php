<?php
//echo '<pre>'; print_r($list_user_group); echo '</pre>';
?>
<div class="row">
    <div class="col-md-12">
        <!--
<p>
            <a href="javascript:;" onclick="open_modal_add_new_group();" class="btn btn-sm green"> <i class="fa fa-plus"></i> Add New Group </a>
        </p>
-->
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                    Group ID
                    </th>
                    <th>
                    Name
                    </th>
                    <th>
                    Description
                    </th>
                    <th>
                    Allowed to Member / Admin Area
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($list_user_group as $a => $b){ ?>
                <tr>
                    <td>
                        <?php echo $b['ugrp_id']; ?>
                    </td>
                    <td>
                        <?php echo $b['ugrp_name']; ?>
                    </td>
                    <td>
                        <?php echo $b['ugrp_desc']; ?>
                    </td>
                    <td>
                        <?php echo $b['ugrp_admin']; ?>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div id="modal_admin_create_new_group" class="modal fade container" tabindex="-1" data-width="760">
    <div class="modal-header">
        <h4 class="modal-title">Create new User Group</h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" role="form" id="form_settings_general">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Group Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter group name" id="new_group_name" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Description</label>
                    <div class="col-md-9">
                        <textarea class="form-control" rows="3" id="new_group_description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-9">
                        <label> <input type="checkbox" value="yes" id="new_is_admin" /> Allowed to Member / Admin Area </label>
                    </div>
                </div>
            </div>
            
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn blue">Close</button>
        <button type="button" class="btn green" onclick="javascript: confirm_submit_create_new_group();">Submit</button>
    </div>
</div>

<script type="text/javascript">
$(function(){
    Metronic.init();
})
function open_modal_add_new_group(){
    $("#modal_admin_create_new_group").modal("show");
}

function confirm_submit_create_new_group(){
    var ugrp_name = $("#new_group_name").val();
    var ugrp_desc = $("#new_group_description").val();
    var ugrp_admin = $("#new_is_admin").val();
    
    Metronic.blockUI({
        target: '#modal_admin_create_new_group',
        boxed: true,
        message: 'Creating...'
    });
    
    $.ajax({
        url: '<?php echo base_url(); ?>settings/'
    })
}
</script>