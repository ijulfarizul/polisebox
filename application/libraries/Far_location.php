<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Far_location {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    /**
     * Get User Account
     */
    /**
    function get_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    */
    /**
    function list_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->result_array();
    }
    */
    
    /**
     * Insert / Create profile
     * @param array $data 
     */
    /**
    function insert_profile($data){
        $this->CI->db->insert('user_profiles', $data); 
    }
    */
    /**
     * Update
     * @param string $key Database column name
     * @param string $key_value Database column value(s)
     * @param string $column Column name to update
     * @param string $value New value for specified column
     */
    /**
    function update_user($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('user_accounts', $data); 
    }
    */
    
    function list_all_area(){
        
    }
    
    function list_state($lc_id){
        $query = $this->CI->db->query("SELECT * FROM location_state WHERE lc_id='".$lc_id."'");
        return $query->result_array();
    }
    
    function list_area_by_city($lct_id){
        $query = $this->CI->db->query("SELECT * FROM location_area WHERE lct_id='".$lct_id."'");
        return $query->result_array();
    }
    
    function list_city_by_state($ls_id){
        $query = $this->CI->db->query("SELECT * FROM location_city WHERE ls_id='".$ls_id."'");
        return $query->result_array();
    }
    
    function list_all_state(){
        $query = $this->CI->db->query("SELECT * FROM location_state");
        return $query->result_array();
    }
    
    function list_all_country(){
        $query = $this->CI->db->query("SELECT * FROM location_country ORDER BY lc_name");
        return $query->result_array();
    }
    
    function list_all_country_and_state(){
        $query = $this->CI->db->query("SELECT country.lc_id, country.lc_name,state.ls_id,state.ls_name FROM location_state state LEFT JOIN location_country country ON state.lc_id=country.lc_id");
        return $query->result_array();
    }
    
    function get_state($column, $value, $column_name = NULL){
        if($column_name){
            $query = $this->CI->db->query("SELECT ".$column_name." FROM location_state WHERE ".$column."='".$value."'");
            $row = $query->row_array();
            return $row[$column_name];
        }else{
            $query = $this->CI->db->query("SELECT * FROM location_state WHERE ".$column."='".$value."'");
        $row = $query->row_array();
            return $row;
        }
        return $query->row()->ls_name;
    }
    
    function delete_area($area_id){
        $this->CI->db->delete('location_area', array('area_id' => $area_id)); 
    }
    
    function insert_area($data){
        $this->CI->db->insert('location_area', $data); 
        return $this->CI->db->insert_id();
    }
    
    function insert_state($data){
        $this->CI->db->insert('location_state', $data); 
        return $this->CI->db->insert_id();
    }
    
    function delete_state($ls_id){
        $this->CI->db->delete('location_state', array('ls_id' => $ls_id)); 
    }
    
    function insert_city($data){
        $this->CI->db->insert('location_city', $data); 
        return $this->CI->db->insert_id();
    }
    
    function delete_city($lct_id){
        $this->CI->db->delete('location_city', array('lct_id' => $lct_id)); 
    }
    
    function get_city($column, $value){
        $query = $this->CI->db->query("SELECT * FROM location_city WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    
    function get_state2($column, $value){
        $query = $this->CI->db->query("SELECT * FROM location_state WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    
    
    
    function get_area($column, $value){
        $query = $this->CI->db->query("SELECT * FROM location_area WHERE ".$column."='".$value."'");
        $row = $query->row_array();
        if(count($row) > 0){
            //get city detail
            $city_detail = $this->get_city('lct_id', $row['lct_id']);
            $state_detail = $this->get_state2('ls_id', $city_detail['ls_id']);
            $country_detail = $this->get_country('lc_id', $state_detail['lc_id']);
            
            $row['lct_id'] = $row['lct_id'];
            $row['ls_id'] = $state_detail['ls_id'];
            $row['lc_id'] = $country_detail['lc_id'];
        }
        return $row;
    }
    
    function get_country($column, $value){
        $query = $this->CI->db->query("SELECT * FROM location_country WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    
}


?>