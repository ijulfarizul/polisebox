<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Translation extends MY_Controller {
    private $user;
    function __construct() {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();
        $this->load->library('far_translation');	

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
    }


    public function global_pass_to_view() {
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()) {
            $this->data['logged_in']['is_admin'] = true;
        }
    }


    function translator_list_all() {
        
        $list_all_meta = $this->far_translation->list_all_meta();
        $this->data['list_all_meta'] = $list_all_meta;

        $list_all_language = $this->far_translation->list_all_language();
        $this->data['list_all_language'] = $list_all_language;
        
        $this->load->view('translation/translator_list_all', $this->data);
    }


    function ajax_translator_list_all_meta_primary_secondary() {

        $primary_selected = $this->input->post('primary');

        $secondary_selected = $this->input->post('secondary');

        $list_all_meta = $this->far_translation->list_all_meta_primary_secondary($primary_selected,
            $secondary_selected);

        $this->data['list_all_meta'] = $list_all_meta;

        $this->data['primary'] = $primary_selected;

        $this->data['secondary'] = $secondary_selected;

        $this->load->view('translation/ajax_translator_list_all_meta_primary_secondary',
            $this->data);

    }


    function ajax_translator_add_meta_lang() {

        $post = $this->input->post('post');

        $output = array();

        $error = array();


        //check meta existing

        $meta = $this->far_translation->is_meta_exists($post['meta']);

        if($meta) {

            $error['meta'] = 'Meta already exists';

        }


        if(strlen($post['meta']) < 2) {

            $error['meta'] = 'Meta must be more than 2 character';

        }


        if(strlen($post['lang_primary_text']) < 2) {

            $error['lang_primary_text'] = 'Primary text must be more than 2 character';

        }


        if(strlen($post['lang_secondary_text']) < 2) {

            $error['lang_secondary_text'] = 'Secondary text must be more than 2 character';

        }


        if(count($error) == 0) {

            $primary_column_lang = 'lang_' . $post['lang_primary_name'];

            $secondary_column_lang = 'lang_' . $post['lang_secondary_name'];

            //insert translation

            //create meta

            $trans_id = $this->far_translation->create_meta($post['meta']);

            $this->far_translation->update_meta('trans_id', $trans_id, $primary_column_lang,
                $post['lang_primary_text']);

            $this->far_translation->update_meta('trans_id', $trans_id, $secondary_column_lang,
                $post['lang_secondary_text']);


            $output['status'] = 'success';

        } else {

            $output['status'] = 'error';

            $output['error_message'] = $error;

            $output['single_msg'] = current($error);

        }


        echo json_encode($output);

    }


    function ajax_translator_delete_meta() {

        $trans_id = $this->input->post('trans_id');

        $error = array();

        $output = array();


        if(count($error) == 0) {


            $this->far_translation->delete_meta($trans_id);


            $output['status'] = 'success';

        } else {


            $output['status'] = 'error';

        }


        echo json_encode($output);

    }


    function ajax_translator_update_meta() {

        $post = $this->input->post('post');

        $error = array();

        $output = array();


        //check meta existing

        $meta = $this->far_translation->is_meta_exists_by_id($post['trans_id']);

        if(!$meta) {

            $error['trans_id'] = 'Translation ID not exists';

        }


        if(count($error) == 0) {


            $primary_column_lang = 'lang_' . $post['lang_primary_name'];

            $secondary_column_lang = 'lang_' . $post['lang_secondary_name'];


            $this->far_translation->update_meta('trans_id', $post['trans_id'], $primary_column_lang,
                $post['primary_text']);

            $this->far_translation->update_meta('trans_id', $post['trans_id'], $secondary_column_lang,
                $post['secondary_text']);


            $output['trans_id'] = $post['trans_id'];

            $output['status'] = 'success';

        } else {

            $output['status'] = 'error';

        }


        echo json_encode($output);

    }
    
    

}

?>