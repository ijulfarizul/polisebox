<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Location extends REST_Controller
{
  public $post_data;

  function __construct(){
    parent::__construct();
    // Load Helper
    $this->load->helper('api_response_helper');
    $this->load->helper('common_helper');
    $this->load->helper('jwt_helper');

    // Load Library
    $this->load->library('public_transformer');
    $this->load->library('form_validation');

    // Load Model
    $this->load->model('api/common_model');
    $this->load->model('api/public/user_model');
    $this->load->model('api/public/location_model');

    // Retrieve $_POST data
    $this->post_data = post_data_handler();

    // API KEY guard, check for valid api-key
    apiKeyGuard();
  }

  /**
  * @api {post} /public/location/police_station Police station 
  * @apiName LocationPoliceStation
  * @apiGroup Location
  *
  * @apiHeader {String} api-key API Key, sha1 of "cpapp_ios_<app_version>" / "cpapp_android_<app_version>"
  * @apiHeader {String} Authorization login token example. "Bearer <login_token>"
  *
  * @apiParam {String} longitude Longitude
  * @apiParam {String} latitude Latitude
  * @apiParam {Integer} [radius] Search radius, default is 10 (km)
  *
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <police_station object>
  *   }
  * }
  */
  public function police_station_post() {
    $res = [];

    $auth_user = apiAuthGuard();

    $this->form_validation->set_data($this->post_data);
    $this->form_validation->set_rules('latitude', 'latitude', 'required');
    $this->form_validation->set_rules('longitude', 'longitude', 'required');

    if ($this->form_validation->run() == FALSE) {
      api_response($error_data = [], false, array_values($this->form_validation->error_array())[0]);
    }

    $police_data = $this->location_model->searchPoliceStation($this->post_data);

    $res_data = $police_data;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res['list'] = $this->public_transformer->transform($police_data, 'police_station');

    api_response($res);
  }

  /**
  * @api {get} /public/location/area/:area_id Location area list or detail
  * @apiName LocationArea
  * @apiGroup Location
  *
  * @apiHeader {String} api-key API Key, sha1 of "cpapp_ios_<app_version>" / "cpapp_android_<app_version>"
  * @apiHeader {String} Authorization login token example. "Bearer <login_token>"
  *
  * @apiParam {Integer} [:area_id] Area id for details
  *
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <location_area object>
  *   }
  * }
  */
  public function area_get( $area_id = null ) {
    $res = [];

    $auth_user = apiAuthGuard();

    $single = false;
    $conf = [];
    if(!empty($area_id)) {
      $conf['where'] = ['area_id' => $area_id];
      $single = true;
    }
    $location_data = $this->location_model->getArea($conf, $single);


    $res_data = $location_data;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res['list'] = $this->public_transformer->transform($location_data, 'location_area', $single);
    if($single) {
      $res = $res['list'];
    }

    api_response($res);
  }

  /**
  * @api {get} /public/location/city/:city_id Location city list or detail
  * @apiName LocationCity
  * @apiGroup Location
  *
  * @apiHeader {String} api-key API Key, sha1 of "cpapp_ios_<app_version>" / "cpapp_android_<app_version>"
  * @apiHeader {String} Authorization login token example. "Bearer <login_token>"
  *
  * @apiParam {Integer} [:city_id] City id for details
  *
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <location_city object>
  *   }
  * }
  */
  public function city_get( $city_id = null ) {
    $res = [];

    $auth_user = apiAuthGuard();

    $single = false;
    $conf = [];
    if(!empty($city_id)) {
      $conf['where'] = ['lct_id' => $city_id];
      $single = true;
    }
    $location_data = $this->location_model->getCity($conf, $single);


    $res_data = $location_data;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res['list'] = $this->public_transformer->transform($location_data, 'location_city', $single);
    if($single) {
      $res = $res['list'];
    }

    api_response($res);
  }

  /**
  * @api {get} /public/location/state/:state_id Location state list or detail
  * @apiName LocationState
  * @apiGroup Location
  *
  * @apiHeader {String} api-key API Key, sha1 of "cpapp_ios_<app_version>" / "cpapp_android_<app_version>"
  * @apiHeader {String} Authorization login token example. "Bearer <login_token>"
  *
  * @apiParam {Integer} [:state_id] State id for details
  *
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <location_state object>
  *   }
  * }
  */
  public function state_get( $state_id = null ) {
    $res = [];

    $auth_user = apiAuthGuard();

    $single = false;
    $conf = [];
    if(!empty($state_id)) {
      $conf['where'] = ['ls_id' => $state_id];
      $single = true;
    }
    $location_data = $this->location_model->getState($conf, $single);


    $res_data = $location_data;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res['list'] = $this->public_transformer->transform($location_data, 'location_state', $single);
    if($single) {
      $res = $res['list'];
    }

    api_response($res);
  }

  /**
  * @api {get} /public/location/country/:country_id Location country list or detail
  * @apiName LocationCountry
  * @apiGroup Location
  *
  * @apiHeader {String} api-key API Key, sha1 of "cpapp_ios_<app_version>" / "cpapp_android_<app_version>"
  * @apiHeader {String} Authorization login token example. "Bearer <login_token>"
  *
  * @apiParam {Integer} [:country_id] State id for details
  *
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <location_country object>
  *   }
  * }
  */
  public function country_get( $country_id = null ) {
    $res = [];

    $auth_user = apiAuthGuard();

    $single = false;
    $conf = [];
    if(!empty($country_id)) {
      $conf['where'] = ['lc_id' => $country_id];
      $single = true;
    }
    $location_data = $this->location_model->getCountry($conf, $single);


    $res_data = $location_data;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res['list'] = $this->public_transformer->transform($location_data, 'location_country', $single);
    if($single) {
      $res = $res['list'];
    }

    api_response($res);
  }



}