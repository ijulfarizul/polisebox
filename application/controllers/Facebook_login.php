<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Facebook Login
 * Reuqired file
 * 1 - Library : Facebook.php
 * Third Party
 * 1 - Facebook library
 * Config : Facebook.php
 * 
 */
class Facebook_login extends MY_Controller {
    private $user;
    function __construct()
    {
        parent::__construct();

		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2)
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE,
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE,
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			);
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}

		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded!
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;

		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');

		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;

        $this->load->library('facebook');
		$this->load->helper('url');


	}

    function redirect_uri(){
        echo '<pre>'; print_r($_REQUEST); echo '</pre>';
    }

    public function index()
	{
		$this->load->view('examples/start');
	}

    public function web_login()
	{
    $this->load->helper('cookie');
    $this->load->library('far_keyword');

		$data['user'] = array();

    $creator_key = $this->input->cookie('cp_creator_key', TRUE);
    setcookie('cp_creator_key', $creator_key, time() + (86400 * 30), '/','.wassap.me');

		// Check if user is logged in
		if ($this->facebook->is_authenticated()){
			// User logged in, get user details
			$user = $this->facebook->request('get', '/me?fields=id,name,email');
			if (!isset($user['error'])){
				$data['user'] = $user;

                 $facebook_id = $user['id'];

                //check already save the id in database or not
                $hf_profile = $this->facebook->get_hf_profile('hf_facebook_id', $facebook_id);

                if(count($hf_profile) == 0){
                    //add into hf profile
                    $hf_profile = array(
                        'hf_facebook_id' => $facebook_id,
                        'hf_name' => $user['name'],
                        'hf_email' => $user['email']
                    );

                    $this->db->insert('hauth_profile_facebook', $hf_profile);
                }

                $facebook_email = $user['email'];
                $facebook_username = $facebook_email;
                $facebook_name = $user['name'];


                //check if already registered in user_accounts
                $user_detail = $this->far_users->get_user('uacc_username', $facebook_username);
                if(count($user_detail) == 0){
                    //register user
                    //register this user
                    $username = $facebook_username;
                    $email = $facebook_email;
                    $password = 'fb_123456789';
                    $user_data = array();
                    $group_id = 5;
                    $activate = TRUE;

                    $user_id = $this->flexi_auth->insert_user($email, $username, $password, $user_data, $group_id, $activate);
                    //if success
                    if($user_id){
                        $this->far_users->update_user('uacc_id', $user_id, 'uacc_raw_password', $password);

                        //insert profile
                        $this->far_agent->insert_profile(array('uacc_id' => $user_id));
                        $this->far_agent->update_profile('uacc_id', $user_id, 'agency_uacc_id', 999999);
                        $this->far_agent->update_profile('uacc_id', $user_id, 'agent_name', ($facebook_name ? $facebook_name : $facebook_email));
                        $this->far_agent->update_profile('uacc_id', $user_id, 'agent_status', 'active');
                        $this->far_agent->update_profile('uacc_id', $user_id, 'email', $facebook_email);

                        //profile pic
                        $fb_profilepic_url = "https://graph.facebook.com/v2.2/".$facebook_id."/picture?width=500&height=500";
                        $this->far_agent->update_profile('uacc_id', $user_id, 'profilepic_url', $fb_profilepic_url);

                        //created by uacc id
                        $this->far_agent->update_profile('uacc_id', $user_id, 'created_by_uacc_id', '-1');
                        $this->far_agent->update_profile('uacc_id', $user_id, 'setup_account', 0);
                        $this->far_users->update_user('uacc_id', $user_id, 'oauth_provider', 'facebook');
                        //auto login and redirect
                        $auto_login = $this->flexi_auth->login($username, $password, TRUE);
                        $this->far_keyword->sync_temporary_keyword($user_id, $creator_key);
                        if($auto_login){
                            redirect("auth_admin/dashboard");
                        }else{
                            echo "Error with auto login, but user successfully authenticate with facebook";
                        }

                    }else{
                        echo "Error with facebook login";
                    }
                }else{

                    $auto_login = $this->flexi_auth->login($user_detail['uacc_username'], $user_detail['uacc_raw_password'], TRUE);
                    $this->far_keyword->sync_temporary_keyword($user_detail['uacc_id'], $creator_key);
                    if($auto_login){
                        redirect("auth_admin/dashboard");
                    }else{
                        echo "Error with auto login, but user successfully authenticate with facebook";
                    }
                }

            }

		}else{

          //direct login
          redirect($this->facebook->login_url());

		}

		// display view
		$this->load->view('examples/web', $data);
	}






}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */
