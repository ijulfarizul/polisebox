<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin/pages/css/profile-old.css"/>
<style>
.profilepic_square {
    background-size: cover !important;
}
</style>
<div class="row form-horizontal">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-md-3 control-label">Receiver</label>
            <div class="col-md-9">
                <div class="input-group">
                    <div class="input-icon">
                        <i class="fa fa-user fa-fw"></i>
                        <input id="register_direct_sponsor_fullname" class="form-control" disabled="" readonly="" type="text" name="password" placeholder="Click to choose receiver">
                        <input type="hidden" id="register_direct_sponsor_uacc_id" value="" />
                    </div>
                    <span class="input-group-btn">
                        <button id="btn_open_modal_choose_direct_sponsor" class="btn blue-madison" type="button" onclick="javascript: btn_open_modal_choose_direct_sponsor();"> Click to choose <i class="fa fa-arrow-right fa-fw"></i></button>
                    </span>
                </div>
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        	<label class="col-md-3 control-label">Choose Qr Code</label>
        	<div class="col-md-9">
        		<div class="table-container">
                    <table id="example" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><input name="select_all" value="1" id="example-select-all" type="checkbox" onclick="javascript: click_select_all();" /></th>
                                <th>QR Code</th>
                                <th>Actions<sup>(s)</sup></th>
                            </tr>
                        </thead>                             
                    </table>
                </div>
                <span class="help-block error_message" style="display: none;"></span>
        	</div>
        </div>
        
    <div class="form-group">
        	<label class="col-md-3 control-label">&nbsp;</label>
        	<div class="col-md-9">
        		<button type="button" onclick="javascript: btn_click_confirm_transfer();" class="btn green">Transfer PIN <i class="fa fa-arrow-right"></i></button>
        	</div>
        </div>
</div>
<style>
.image-media-choose {
    width: 102px;
} 
.bigicn-only {
    padding: 35px 0px !important;
}
</style>
<div id="modal_choose_direct_sponsor" class="modal container fade" role="dialog">
	<div class="modal-content form-horizontal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Search receiver</h4>
		</div>
		<div class="modal-body">
            <div class="form-group">
            	<label class="col-md-3 control-label">Name / Email / Phone number</label>
            	<div class="col-md-9">
            		<div class="input-group">
                        <div class="input-icon">
                            <i class="fa fa-user fa-fw"></i>

                            <input id="search_email_affiliator" class="form-control" type="text" name="password" placeholder="Type your search here" />
                        </div>
                        <span class="input-group-btn">
                            <button id="genpassword" class="btn blue-madison" type="button" onclick="javascript: btn_ajax_search_affiliator();"> Search <i class="fa fa-search"></i></button>
                        </span>
                        
                    </div>
                    <span class="help-block error_message" style="display: none;"></span>
            	</div>
            </div>
            
            <hr />
            <div class="row blur" id="frame_search_result" style="min-height: 150px;">
                <div class="col-md-8 col-md-offset-2">
                    
                    <div id="frame_search_result_body">
                        
                        <!--
                        <div class="row portfolio-block">
                        	<div class="col-md-8" style="padding-left: 0px;">
                        		<div class="portfolio-text">
                        			<img src="https://www.usahawan.me/img/logo/logo-usahawan-white-bg-252x252.png" alt="" class="image-media-choose">
                        			<div class="portfolio-text-info">
                        				<h4>Usahawan 1</h4>
                        				<p>
                        					No. Telefon: 60137974467 <br>
                        					Email: 60137974467@vip.usahawan.me
                        				</p>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="col-md-4" style="padding-right: 0px; padding-left: 0px;">
                        		<div class="portfolio-btn">
                        			<a href="javascript:;" class="btn bigicn-only"><span> Pilih </span></a>
                        		</div>
                        	</div>
                        </div>
                        
                        <div class="row portfolio-block">
                        	<div class="col-md-8" style="padding-left: 0px;">
                        		<div class="portfolio-text">
                        			<img src="https://www.usahawan.me/img/logo/logo-usahawan-white-bg-252x252.png" alt="" class="image-media-choose">
                        			<div class="portfolio-text-info">
                        				<h4>Usahawan 1</h4>
                        				<p>
                        					No. Telefon: 60137974467 <br>
                        					Email: 60137974467@vip.usahawan.me
                        				</p>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="col-md-4" style="padding-right: 0px; padding-left: 0px;">
                        		<div class="portfolio-btn">
                        			<a href="javascript:;" class="btn bigicn-only"><span> Pilih </span></a>
                        		</div>
                        	</div>
                        </div>
                        
                        <div class="row portfolio-block">
                        	<div class="col-md-8" style="padding-left: 0px;">
                        		<div class="portfolio-text">
                        			<img src="https://www.usahawan.me/img/logo/logo-usahawan-white-bg-252x252.png" alt="" class="image-media-choose">
                        			<div class="portfolio-text-info">
                        				<h4>Usahawan 1</h4>
                        				<p>
                        					No. Telefon: 60137974467 <br>
                        					Email: 60137974467@vip.usahawan.me
                        				</p>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="col-md-4" style="padding-right: 0px; padding-left: 0px;">
                        		<div class="portfolio-btn">
                        			<a href="javascript:;" class="btn bigicn-only"><span> Pilih </span></a>
                        		</div>
                        	</div>
                        </div>
                        -->
                        
                        
                    </div>
                </div>
           	</div>
            
		</div>
		<div class="modal-footer">
            <input type="hidden" id="hidden_target_id" />
            <input type="hidden" id="hidden_uacc_id" />
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <!--
            <button type="button" class="btn green pull-right disabled" onclick="javascript: btn_choose_user_upline('register_direct_sponsor');" id="btn_choose_user_upline_confirm">Select <i class="fa fa-arrow-right"></i></button>
            -->
		</div>
	</div>
</div>

<!--
<div id="modal_choose_direct_sponsor" class="modal container fade" role="dialog">
	<div class="modal-content form-horizontal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Cari penerima PIN</h4>
		</div>
		<div class="modal-body">
            <div class="form-group">
            	<label class="col-md-3 control-label">Username</label>
            	<div class="col-md-9">
            		<div class="input-group">
                        <div class="input-icon">
                            <i class="fa fa-user fa-fw"></i>

                            <input id="search_email_affiliator" class="form-control" type="text" name="password" placeholder="Search email address here" />

                            <input id="search_username_affiliator" class="form-control" type="text" name="search_username_affiliator" placeholder="Search username" />
                        </div>
                        <span class="input-group-btn">
                            <button id="genpassword" class="btn blue-madison" type="button" onclick="javascript: btn_ajax_search_affiliator();"> Cari <i class="fa fa-search"></i></button>
                        </span>
                        
                    </div>
                    <span class="help-block error_message" style="display: none;"></span>
            	</div>
            </div>
            
            <hr />
            <div class="row blur" id="frame_search_result">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="picture-container">
     			        <div class="picture picture-square profilepic_square" id="search_affiliate_profilepic" style="height: 150px; background-repeat: no-repeat; background-size: cover;">
                                    
             			</div>
              		</div>
                            
                            
                    	</div>
                    	<div class="col-sm-6">
                    		<div class="form-group">
                    			<label id="search_fullname">Fullname here</label>
                            </div>
                            <div class="form-group">
                    			<label id="search_email">email@here.com</label>
                            </div>
                    		<div class="form-group">
                            	<label id="search_phonenumber">+60111111111</label>
                            </div>
                    	</div>
                    	
                    </div>
		</div>
		<div class="modal-footer">
            <input type="hidden" id="hidden_target_id" />
            <input type="hidden" id="hidden_uacc_id" />
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn green pull-right disabled" onclick="javascript: btn_choose_user_upline('register_direct_sponsor');" id="btn_choose_user_upline_confirm">Select <i class="fa fa-arrow-right"></i></button>
		</div>
	</div>
</div>
-->


<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>



<script type="text/javascript">
function btn_click_confirm_transfer(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    //check penerima
    var receiver_uacc_id = $("#hidden_uacc_id").val();
    if (receiver_uacc_id === false){ 
        $("#register_direct_sponsor_fullname").closest('.form-group').addClass('has-error'); 
        $("#register_direct_sponsor_fullname").closest('.form-group').find('.error_message').text("Please choose receiver").show(); return false
    }
    if (receiver_uacc_id === "") {
        $("#register_direct_sponsor_fullname").closest('.form-group').addClass('has-error'); 
        $("#register_direct_sponsor_fullname").closest('.form-group').find('.error_message').text("Please choose receiver").show(); return false
    }
    
    //
    var totalCheckboxes = $('input.select_checkbox:checkbox:checked').length;
    if(totalCheckboxes > 0){
        
    }else{
        $("#example").closest('.form-group').addClass('has-error'); 
        $("#example").closest('.form-group').find('.error_message').text("Please select atleast 1 QR Code").show(); return false
    }
    
    var fullname = $("#register_direct_sponsor_fullname").val();
    swal({
        title: "Are you sure",
        text: "Transfer "+totalCheckboxes+" QR Code to "+fullname,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
    function(){
        
        var list_pinnumber = [];
        $('input.select_checkbox:checkbox:checked').each(function(i){
            datapin = $(this).data();
            pinnumber = datapin.pinnumber;
            list_pinnumber.push(pinnumber)
            
        });
        
        $.ajax({
            url: '<?php echo base_url(); ?>agent/ajax_member_transfer_qrcode_to_member',
            type: 'POST',
            dataType: 'json',
            data: { 
                postdata: { 
                    receiver_uacc_id: receiver_uacc_id,
                    list_pinnumber: list_pinnumber
                } 
            },
            success: function(data){
                Metronic.unblockUI();
                if(data.status == "success"){
                    
                    
                    swal({
                        title: "Berjaya!",
                        text: totalCheckboxes+ " QR Code successfully transferred to "+fullname,
                        type: "success",
                        closeOnConfirm: true
                    },
                    function(){
                        window.location = "<?php echo base_url(); ?>qrcodes/admin_transfer_qrcode";
                    });
                }else{
                    var eell = data.errors;
                    $.each(eell, function(i,j){
                        var el_on_page = $("#"+i).length;
                        if (el_on_page){
                            $("#"+i).closest('.form-group').addClass('has-error');
                            $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                        } else {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                        }
                        console.log(i);
                        console.log(j)
                    })
                }
            }
        })
    });
}
</script>

<script type="text/javascript">
var datatable_el;
$(function(){
    
    datatable_el = $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>qrcodes/ajax_dt_admin_list_available_to_transfer_qrcodes",
            "type": "POST"
        },
        "lengthMenu": [[-1], ["All"]],
        "columns": [
            { "data": "qrcode_id" },
            { "data": "serial_number" },
            { "data": "qrcode_id", "render": function ( data, type, row ) {
                var html = "";
                
                return html;
            } },
        ],
        'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
            var pinnumber = full.serial_number;
            var activation_for_uacc_id = full.activation_for_uacc_id;
            var transfer_availability = 'no';
            if(activation_for_uacc_id == 'available'){
                transfer_availability = 'yes';
            }
            var html = "";
            html += '<input type="checkbox" name="id[]" class="select_checkbox" data-availabletotransfer="'+transfer_availability+'" onclick="javascript: click_checkbox();" data-pinnumber="'+pinnumber+'" value="' 
                + $('<div/>').text(data).html() + '">';
            return html;
         }
      }],
        "order": [1, 'desc'],
        "drawCallback": function(settings) {
           render_checkbox();
        }
    } );
    
    
    

});
</script>

<script type="text/javascript">
function btn_open_modal_choose_direct_sponsor(){
    $("#hidden_target_id").val('register_direct_sponsor')
    $('#modal_choose_direct_sponsor').modal({
        backdrop: 'static',
        keyboard: false
    });
    
}
function click_select_all(){
    $(".select_checkbox").each(function(i){
        var c = true;
        if(this.checked){
            c = false;
        }
        $(this).prop('checked', c);
    });
    $.uniform.update();
    
    var totalCheckboxes = $('input.select_checkbox:checkbox:checked').length;
    if(totalCheckboxes > 0){
        $(".transfer_btn").html("Transfer "+totalCheckboxes+" selected PIN <i class='fa fa-arrow-right'></i>");
    }else{
        $(".transfer_btn").html("Select PIN to transfer");
    }
    
}

function click_checkbox(){
    var totalCheckboxes = $('input.select_checkbox:checkbox:checked').length;
    if(totalCheckboxes > 0){
        $(".transfer_btn").html("Transfer "+totalCheckboxes+" selected PIN <i class='fa fa-arrow-right'></i>");
    }else{
        $(".transfer_btn").html("Select PIN to transfer");
    }
}
</script>

<script type="text/javascript">
function btn_ajax_search_affiliator(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    $("#frame_search_result").addClass('blur');
    $("#btn_choose_user_upline_confirm").addClass('disabled');
    
    
    //check search_email_affiliator
    var search_email_affiliator = $("#search_email_affiliator").val();
    if (search_email_affiliator === false){ 
        $("#search_email_affiliator").closest('.form-group').addClass('has-error'); 
        $("#search_email_affiliator").closest('.form-group').find('.error_message').text("Please fill in username").show(); return false
    }
    if (search_email_affiliator === "") {
        $("#search_email_affiliator").closest('.form-group').addClass('has-error'); 
        $("#search_email_affiliator").closest('.form-group').find('.error_message').text("Please fill in username").show(); return false
    }
    
    
    
    blockUI();
    $.ajax({
        //url: "<?php echo base_url(); ?>affiliate/ajax_search_user_by_email_to_become_affiliator",
        url: "<?php echo base_url(); ?>agent/ajax_search_user_by_username_to_transfer_qrcode",
        type: "POST",
        dataType: "JSON",
        data: {
            search_email_affiliator: search_email_affiliator
        },
        success: function(data){
            $.unblockUI();
            if(data.status == 'success'){
                var list_user = data.list_user;
                var htmlTable = "";
                $(list_user).each(function(i, j){
                    uacc_id = j.uacc_id;
                    username = j.uacc_username;
                    fullname = j.fullname;
                    phonemobile = j.phonemobile;
                    email = j.email;
                    profilepic_url = j.profilepic_url;
                    /*
                    htmlTable += "<div class='media'>";
                    htmlTable += '<a href="javascript:;" class="pull-left"><img alt="" src="'+profilepic_url+'" class="media-object"></a>';
                    htmlTable += '<div class="media-body">';
                    htmlTable += '<h4 class="media-heading">'+fullname+'</h4>';
                    htmlTable += '<p>No. Telefon: '+phonemobile+' <br>Email: '+email+'</p>';

                    htmlTable += "</div>";
                    htmlTable += "</div>";
                    */
                    
                    
                    htmlTable += "<div class=\"row portfolio-block\">";
                    htmlTable += "	<div class=\"col-md-8\" style='padding-left: 0px;'>";
                    htmlTable += "		<div class=\"portfolio-text\">";
                    htmlTable += "			<img src='"+profilepic_url+"' alt=\"\" class='image-media-choose'>";
                    htmlTable += "			<div class=\"portfolio-text-info\">";
                    htmlTable += "				<h4 class='sel_fullname'>"+fullname+"<\/h4>";
                    htmlTable += "				<p>";
                    htmlTable += 'Username: '+username+'<br>No. Telefon: '+phonemobile+' <br>Email: '+email+'';
                    htmlTable += "				<\/p>";
                    htmlTable += "			<\/div>";
                    htmlTable += "		<\/div>";
                    htmlTable += "	<\/div>";
                    htmlTable += "	<div class=\"col-md-4\" style='padding-right: 0px; padding-left: 0px;'>";
                    htmlTable += "		<div class=\"portfolio-btn\">";
                    htmlTable += "			<a data-fullname='"+fullname+"' data-uacc_id='"+uacc_id+"' href=\"javascript:;\" class=\"btn bigicn-only\" onclick='javascript: btn_select_user_receive_pin(this)'>";
                    htmlTable += "			<span>";
                    htmlTable += "			Select <\/span>";
                    htmlTable += "			<\/a>";
                    htmlTable += "		<\/div>";
                    htmlTable += "	<\/div>";
                    htmlTable += "<\/div>";


                    
                    
                });
                
                $("#frame_search_result_body").html(htmlTable);
                
                $("#frame_search_result").removeClass('blur');
                
                //$("#btn_choose_user_upline_confirm").removeClass('disabled');
            }else{
                //$("#search_email_affiliator").closest('.form-group').addClass('has-error'); 
                //$("#search_email_affiliator").closest('.form-group').find('.error_message').text("No user found. Please check email address").show(); return false
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
        }
    })
}

function btn_select_user_receive_pin(btn){
    var data = $(btn).data();
    var uacc_id = data.uacc_id;
    var fullname = data.fullname;
    
    $("#register_direct_sponsor_fullname").val(fullname);
    $("#register_direct_sponsor_uacc_id").val(uacc_id);
    
    $("#hidden_uacc_id").val(uacc_id);
    
    $("#modal_choose_direct_sponsor").modal("hide");
}

function btn_choose_user_upline(){
    var hidden_target_id = $("#hidden_target_id").val();
    var hidden_uacc_id = $("#hidden_uacc_id").val();
    var hidden_fullname = $("#search_fullname").text();
    $("#"+hidden_target_id+"_uacc_id").val(hidden_uacc_id);
    $("#"+hidden_target_id+"_fullname").val(hidden_fullname);
    
    $("#modal_choose_direct_sponsor").modal("hide");
    
}

function render_checkbox(){
    var test = $("input[type=checkbox]:not(.toggle, .md-check, .md-radiobtn, .make-switch, .icheck), input[type=radio]:not(.toggle, .md-check, .md-radiobtn, .star, .make-switch, .icheck)");
        if (test.size() > 0) {
            test.each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }
}
</script>