<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Dunlop One Page Multipurpose Business Template.">
        <meta name="author" content="CrackThemes">
       
        <title>Community Policing</title>
        
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
		
		<!-- Font Awesome CSS
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/font-awesome.min.css">
        -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/fontawesome-free-5.0.4/web-fonts-with-css/css/fontawesome-all.min.css">
        <!-- Elegant Font Icons CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/elegant-font-icons.css">
        <!-- Elegant Line Icons CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/elegant-line-icons.css">
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/bootstrap.min.css">
        <!-- animate CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/animate.min.css">
		<!-- Venobox CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/venobox/venobox.css">
		<!-- OWL-Carousel CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/owl.carousel.css">
        <!-- Menu CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/menu.css">
		<!-- Main CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/main.css">
		<!-- Responsive CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/website/css/responsive.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" />
        <script src="<?php echo base_url(); ?>assets/website/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        
<style>
.square {
  width: .7em;
  height: .7em;
  margin: .5em;
  display: inline-block;
}

/* Custom dropdown */
.custom-dropdown {
  position: relative;
  display: inline-block;
  vertical-align: middle;
  width: 170px;
}

.custom-dropdown select {
  background-color: #1abc9c;
  color: #fff;
  font-size: inherit;
  padding: 0 5px 0 10px;
  border: 0;
  margin: 0;
  border-radius: 3px;
  text-indent: 0.01px;
  text-overflow: '';
  -webkit-appearance: button; /* hide default arrow in chrome OSX */
}

.custom-dropdown::before,
.custom-dropdown::after {
  content: "";
  position: absolute;
  pointer-events: none;
}

.custom-dropdown::after { /*  Custom dropdown arrow */
  content: "\25BC";
  height: 1em;
  font-size: .625em;
  line-height: 1;
  right: 1.2em;
  top: 50%;
  margin-top: -.5em;
}

.custom-dropdown::before { /*  Custom dropdown arrow cover */
  width: 2em;
  right: 0;
  top: 0;
  bottom: 0;
  border-radius: 0 3px 3px 0;
}

.custom-dropdown select[disabled] {
  color: rgba(0,0,0,.3);
}

.custom-dropdown select[disabled]::after {
  color: rgba(0,0,0,.1);
}

.custom-dropdown::before {
  background-color: rgba(0,0,0,.15);
}

.custom-dropdown::after {
  color: rgba(0,0,0,.4);
}
#select_lang {
    width: 100% !important;
}


/* Redirecting */
@import url(https://fonts.googleapis.com/css?family=Open+Sans:300);
.body_redirect {
  background-color: #f1c40f !important;
  overflow: hidden !important;
}


.body {
  position: absolute;
  top: 50%;
  margin-left: -50px;
  left: 50%;
  animation: speeder .4s linear infinite;
}
.body > span {
  height: 5px;
  width: 35px;
  background: #000;
  position: absolute;
  top: -19px;
  left: 60px;
  border-radius: 2px 10px 1px 0;
}

.base span {
  position: absolute;
  width: 0;
  height: 0;
  border-top: 6px solid transparent;
  border-right: 100px solid #000;
  border-bottom: 6px solid transparent;
}
.base span:before {
  content: "";
  height: 22px;
  width: 22px;
  border-radius: 50%;
  background: #000;
  position: absolute;
  right: -110px;
  top: -16px;
}
.base span:after {
  content: "";
  position: absolute;
  width: 0;
  height: 0;
  border-top: 0 solid transparent;
  border-right: 55px solid #000;
  border-bottom: 16px solid transparent;
  top: -16px;
  right: -98px;
}

.face {
  position: absolute;
  height: 12px;
  width: 20px;
  background: #000;
  border-radius: 20px 20px 0 0;
  transform: rotate(-40deg);
  right: -125px;
  top: -15px;
}
.face:after {
  content: "";
  height: 12px;
  width: 12px;
  background: #000;
  right: 4px;
  top: 7px;
  position: absolute;
  transform: rotate(40deg);
  transform-origin: 50% 50%;
  border-radius: 0 0 0 2px;
}

.body > span > span:nth-child(1),
.body > span > span:nth-child(2),
.body > span > span:nth-child(3),
.body > span > span:nth-child(4) {
  width: 30px;
  height: 1px;
  background: #000;
  position: absolute;
  animation: fazer1 .2s linear infinite;
}

.body > span > span:nth-child(2) {
  top: 3px;
  animation: fazer2 .4s linear infinite;
}

.body > span > span:nth-child(3) {
  top: 1px;
  animation: fazer3 .4s linear infinite;
  animation-delay: -1s;
}

.body > span > span:nth-child(4) {
  top: 4px;
  animation: fazer4 1s linear infinite;
  animation-delay: -1s;
}

@keyframes fazer1 {
  0% {
    left: 0;
  }
  100% {
    left: -80px;
    opacity: 0;
  }
}
@keyframes fazer2 {
  0% {
    left: 0;
  }
  100% {
    left: -100px;
    opacity: 0;
  }
}
@keyframes fazer3 {
  0% {
    left: 0;
  }
  100% {
    left: -50px;
    opacity: 0;
  }
}
@keyframes fazer4 {
  0% {
    left: 0;
  }
  100% {
    left: -150px;
    opacity: 0;
  }
}
@keyframes speeder {
  0% {
    transform: translate(2px, 1px) rotate(0deg);
  }
  10% {
    transform: translate(-1px, -3px) rotate(-1deg);
  }
  20% {
    transform: translate(-2px, 0px) rotate(1deg);
  }
  30% {
    transform: translate(1px, 2px) rotate(0deg);
  }
  40% {
    transform: translate(1px, -1px) rotate(1deg);
  }
  50% {
    transform: translate(-1px, 3px) rotate(-1deg);
  }
  60% {
    transform: translate(-1px, 1px) rotate(0deg);
  }
  70% {
    transform: translate(3px, 1px) rotate(-1deg);
  }
  80% {
    transform: translate(-2px, -1px) rotate(1deg);
  }
  90% {
    transform: translate(2px, 1px) rotate(0deg);
  }
  100% {
    transform: translate(1px, -2px) rotate(-1deg);
  }
}
.longfazers {
  position: absolute;
  width: 100%;
  height: 100%;
}
.longfazers span {
  position: absolute;
  height: 2px;
  width: 20%;
  background: #000;
}
.longfazers span:nth-child(1) {
  top: 20%;
  animation: lf .6s linear infinite;
  animation-delay: -5s;
}
.longfazers span:nth-child(2) {
  top: 40%;
  animation: lf2 .8s linear infinite;
  animation-delay: -1s;
}
.longfazers span:nth-child(3) {
  top: 60%;
  animation: lf3 .6s linear infinite;
}
.longfazers span:nth-child(4) {
  top: 80%;
  animation: lf4 .5s linear infinite;
  animation-delay: -3s;
}

@keyframes lf {
  0% {
    left: 200%;
  }
  100% {
    left: -200%;
    opacity: 0;
  }
}
@keyframes lf2 {
  0% {
    left: 200%;
  }
  100% {
    left: -200%;
    opacity: 0;
  }
}
@keyframes lf3 {
  0% {
    left: 200%;
  }
  100% {
    left: -100%;
    opacity: 0;
  }
}
@keyframes lf4 {
  0% {
    left: 200%;
  }
  100% {
    left: -100%;
    opacity: 0;
  }
}

.run_redirect {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 9999;
    background: #49c4ff;
    height: 100%;
    width: 100%;
}

#redirecting h1 {
  position: absolute;
  font-family: 'Open Sans';
  font-weight: 600;
  font-size: 12px;
  text-transform: uppercase;
  left: 50%;
  top: 58%;
  margin-left: -20px;

}
</style>
        
    </head>
    <body data-spy="scroll" data-target="#navbar" data-offset="60">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <div id='preloader' >
            <div class='loader' >
                <img src="<?php echo base_url(); ?>assets/website/img/three-dots.svg" width="60" alt="">
            </div>
        </div><!-- Preloader -->
        
        <div id="redirecting" style="display: none;">
        <div class='body'>
          <span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </span>
          <div class='base'>
            <span></span>
            <div class='face'></div>
          </div>
        </div>
        <div class='longfazers'>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
            
            <h1 style="text-align: center;">Preparing registration page.<br />Redirecting in <span id="countdowntimer">10 </span> seconds</h1>
        </div>

        <header id="header" class="header_section">
            <div class="top_header">
                <div class="container">
                    <div class="col-sm-6 xs-padding">
                        <ul class="header_contact">
                            <li><a href="mailto:enquiry@cops.org.my"><i class="icon-envelope"></i> enquiry@cops.org.my</a></li>
                            <li><a href="call:+60320319999"><i class="icon-phone"></i> +603 - 2031 9999 </a></li>
                            <li>
                            
                                <span class="custom-dropdown big">
                                    <select id="select_lang">   
                                        <option value="en" <?php if($this->session->site_lang == 'en'){ echo "selected"; } ?>>English</option>
                                        <option value="ms" <?php if($this->session->site_lang == 'ms'){ echo "selected"; } ?>>Bahasa Malaysia</option>
                                    </select>
                                </span>
                            
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 xs-padding">
                        <ul class="header_social">
                            <li><a href="https://www.facebook.com/groups/selangorcops/" target="_blank"><i class="icon-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.top_header -->
            <nav id="navbar_wrap" class="navbar header">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="nav-btn navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="#" class="brand">
                            <img src="<?php echo base_url(); ?>assets/website/img/logo-dark.png?a=a" alt="Logo" class="logo_dark">
                        </a>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav nav-menu">
                            <li class="active"><a data-scroll href="#home"><?php echo $lang["MENU_HOME"]; ?> <span class="sr-only">(current)</span></a></li>
                            <li><a data-scroll href="#services"><?php echo $lang["MENU_SERVICES"]; ?></a></li>
                            <li><a data-scroll href="#testimonial"><?php echo $lang["MENU_TESTIMONI"]; ?></a></li>
                            <li><a data-scroll href="#blog"><?php echo $lang["MENU_EXAMPLES"]; ?></a></li>
                            <li><a data-scroll href="#contact"><?php echo $lang["MENU_CONTACT"]; ?></a></li>
                        </ul>
                        <div class="header_btn_group">
                            <a href="javascript: void(0);" onclick="javascript: redirecting_to_register();" class="header_btn"><?php echo $lang["MENU_START_NOW"]; ?></a>
                        </div>
                    </div><!--/.navbar-collapse -->
                </div>
            </nav><!-- Navigation Bar -->
        </header> <!-- Header -->

		<section id="home" class="slider_section header_height">
		    <ul id="main-slider" class="owl-carousel main_slider">
                <li class="slider_item" style="background-image: url(<?php echo base_url(); ?>assets/website/img/slider-1.jpg);">
                    <div class="container">
                        <div class="display-table">
                            <div class="table-cell">
                                <div class="slider_content">
                                    <h1 class="text-white">BUILD YOUR WEBSITE</h1>
                                    <p class="text-white">Lorem Ipsum is simply dummy text of the printing and typesetting <br> industry.Vivamus sodales varius sagittis.</p>
                                    <div class="btn_group">
                                        <a href="#" class="fs_btn"><?php echo $lang["GETSTARTED"]; ?></a>
                                        <a href="#" class="fs_btn">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="slider_item" style="background-image: url(<?php echo base_url(); ?>assets/website/img/slider-2.jpg);">
                    <div class="container">
                        <div class="display-table">
                            <div class="table-cell">
                                <div class="slider_content">
                                    <h1 class="text-white">BUILD YOUR WEBSITE</h1>
                                   <p class="text-white">Lorem Ipsum is simply dummy text of the printing and typesetting <br> industry.Vivamus sodales varius sagittis.</p>
                                    <div class="btn_group">
                                        <a href="#" class="fs_btn"><?php echo $lang["GETSTARTED"]; ?></a>
                                        <a href="#" class="fs_btn">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="slider_item" style="background-image: url(<?php echo base_url(); ?>assets/website/img/slider-3.jpg);">
                    <div class="container">
                        <div class="display-table">
                            <div class="table-cell">
                                <div class="slider_content">
                                    <h1 class="text-white">BUILD YOUR WEBSITE</h1>
                                    <p class="text-white">Lorem Ipsum is simply dummy text of the printing and typesetting <br> industry.Vivamus sodales varius sagittis.</p>
                                    <div class="btn_group">
                                        <a href="#" class="fs_btn"><?php echo $lang["GETSTARTED"]; ?></a>
                                        <a href="#" class="fs_btn">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
		</section><!-- /.slider_section -->
		
		<section id="about" class="about_section bd-bottom padding">
            <div class="container">
                <div class="col-sm-6 xs-padding">
                    <div class="about_content">
                        <h2>Who We Are</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the when an unknown printer took a galley of type scrambled it to make a type specimen book.</p>
                        <a href="#" class="fs_btn">Learn More</a>
                    </div> 
                </div>  
                <div class="col-sm-6">
                    <img src="<?php echo base_url(); ?>assets/website/img/about.png" alt="about-image">
                </div> 
            </div>    
        </section><!-- ./about_section -->
        
        <section id="services" class="service_section bg-grey bd-bottom padding">
			<div class="container">
				<div class="section_heading align-center mb-40">
					<h2>Our Business Services</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
				</div><!--/.section_heading -->
				<div class="service_lists">
                    <div class="col-sm-4 col-xs-6">
                        <div class="service_list">
                            <div class="service_content">
                                <i class="icon-laptop"></i>
                                <h4>Full Responsive</h4>
                                <p>Lorem Ipsum is simply dummy business the smart printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <div class="service_list">
                            <div class="service_content">
                                <i class="icon-linegraph"></i>
                                <h4>Clean Code</h4>
                                <p>Lorem Ipsum is simply dummy business the smart printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <div class="service_list">
                            <div class="service_content">
                                <i class="icon-grid"></i>
                                <h4>Pixel Perfect</h4>
                                <p>Lorem Ipsum is simply dummy business the smart printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <div class="service_list">
                            <div class="service_content">
                                <i class="icon-tools"></i>
                                <h4>Clean Design</h4>
                                <p>Lorem Ipsum is simply dummy business the smart printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <div class="service_list">
                            <div class="service_content">
                                <i class="icon-gears"></i>
                                <h4>Easy To Customize</h4>
                               <p>Lorem Ipsum is simply dummy business the smart printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <div class="service_list">
                            <div class="service_content">
                                <i class="icon-phone"></i>
                                <h4>24/7 Support</h4>
                                <p>Lorem Ipsum is simply dummy business the smart printing and typesetting industry.</p>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</section><!-- /.service_section -->
		
		<section class="counter_section padding">
        	<div class="container">
        		<ul class="counters">
        			<li class="col-md-3 col-xs-6 sm-padding">
        				<div class="counter_content">
        					<i class="icon-people"></i>
        					<h3 class="counter">267</h3>
        					<h4 class="text-white"><?php echo $lang["CLIENTS"]; ?></h4>
        				</div>
        			</li>
        			<li class="col-md-3 col-xs-6 sm-padding">
        				<div class="counter_content">
        					<i class="icon-globe"></i>
        					<h3 class="counter">1069</h3>
        					<h4 class="text-white">Website</h4>
        				</div>
        			</li>
        			<li class="col-md-3 col-xs-6 sm-padding">
        				<div class="counter_content">
        					<i class="icon-like"></i>
        					<h3 class="counter">2001</h3>
        					<h4 class="text-white"><?php echo $lang["VISITOR"]; ?></h4>
        				</div>
        			</li>
        			<li class="col-md-3 col-xs-6 sm-padding">
        				<div class="counter_content">
        					<i class="icon-laptop"></i>
        					<h3 class="counter">669</h3>
        					<h4 class="text-white">Website Lunch</h4>
        				</div>
        			</li>
        		</ul>
        	</div>
        </section> <!-- /.counter_section -->
		
		<!-- /.portfolio_section --> 
		
		<section id="testimonial" class="testimonial_section overlay">
			<div class="container">
				<div class="col-sm-8 col-sm-offset-2">
					<div id="testimonial_carousel" class="carousel slide testimonial" data-ride="carousel">
						<!-- Indicators -->
						<ul class="client_thumb carousel-indicators">
							<li data-target="#testimonial_carousel" data-slide-to="0" class="active"><img src="<?php echo base_url(); ?>assets/website/img/testimonial-1.jpg?a=a" alt="Client Thumb"></li>
							<li data-target="#testimonial_carousel" data-slide-to="1"><img src="<?php echo base_url(); ?>assets/website/img/testimonial-2.jpg?a=a" alt="Client Thumb"></li>
							<li data-target="#testimonial_carousel" data-slide-to="2"><img src="<?php echo base_url(); ?>assets/website/img/testimonial-3.jpg?a=a" alt="Client Thumb"></li>
						</ul>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<div class="testi_content">
									<h3>BOB SAUDAGAR</h3>
									<p>Usahawan dot Me Memang Best! Simple, mudah dan murah. Harga mampu milik. Pertama kali cuba dah jatuh cinta!</p>
								</div>
							</div>
							<div class="item">
								<div class="testi_content">
									<h3>HAFIZ KEDAH</h3>
									<p>Saya mula tertarik dengan Program Usahawan dot Me kerana menampilkan Produk digital mesra mobile</p>
								</div>
							</div>
							<div class="item">
								<div class="testi_content">
									<h3>HUSNA</h3>
									<p>Sekarang saya boleh bina mini web sendiri dengan mudah dan iklankannya dalam FB. Kami boleh. Anda juga boleh!</p>
								</div>
							</div>
						</div>

						<!-- Controls -->
						<div class="testi_control">
							<a class="carousel-control left_arrow" href="#testimonial_carousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
							<a class="carousel-control right_arrow" href="#testimonial_carousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</section><!-- /.testimonial_section -->
		
		<section id="pricing" class="pricing_section bg-grey bd-bottom padding">
           <div class="container">
               <div class="section_heading mb-40 align-center">
                   <h2><?php echo $lang["PLANS_AND_PRICING"]; ?></h2>
               </div><!-- /.section_heading -->
               <ul class="pricing_tables">
                    
                    <li class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4">
                        <div class="pricing_table mid">
                            <i class="icon-hotairballoon"></i>
                            <h3>Premium</h3>
                            <span class="price">RM120</span>
                            <ul>
                                <li>10 Website</li>
                                <li>100 <?php echo $lang["PRODUCT_IMAGE"]; ?></li>
                                <li>Free Backups</li>
                                <li>Unlimited Features</li>
                                <li>Life Time Support</li>
                            </ul>
                            <a href="javascript: void(0);" onclick="javascript: redirecting_to_register();" class="fs_btn"><?php echo $lang["MENU_START_NOW"]; ?></a>
                        </div>
                    </li>
                    
               </ul><!-- /.pricing_tables -->
           </div>
       </section><!-- /.pricing_section -->
       
       
       <style>
       .post_thumb {
        height: 232px;
        background-size: cover;
        background-position: center center;
        border-bottom: 5px solid #498fe2;
       }
       .product_description {
            word-wrap: break-word;
            height: 80px;
            padding: 0 10px 0 10px;
       }
       .blog_section .blog_posts .blog_post {
        border: 5px solid #498fe2;
            margin-bottom: 1rem;
            background-color: #498fe2;
            color: #FFFFFF !important;
            border-radius: 0.5rem;
       }
       .posted_by {
        padding: 0 10px 0 10px;
       }
       .blog_section .blog_posts .blog_post h3 {
        padding: 0 10px 0 10px;
       }
       .blog_section .blog_posts .blog_post .read_more {
        margin-left: 10px;
        color: #FFFFFF !important;
       }
       .blog_section .blog_posts .blog_post h3 a {
        color: #FFFFFF !important;
       }
       </style>
       <section id="blog" class="blog_section bd-bottom padding">
       
       
			<div class="container">
				<div class="section_heading mb-40 align-center">
					<h2>Mini Website Example</h2>
				</div>
                
                
                
				<ul class="blog_posts">
                    <?php foreach($list_website as $a => $b){ ?>
                    <?php
                    $shop_detail = $b['shop_detail'];
                    $list_images = $b['list_images'];
                    $user_profile = $b['user_profile'];
                    
                    $product_name = strlen($shop_detail['product_name']) > 20 ? substr($shop_detail['product_name'],0,20)."..." : $shop_detail['product_name'];
                    $product_description = strlen($shop_detail['product_description']) > 100 ? substr($shop_detail['product_description'],0,100)."..." : $shop_detail['product_description'];
                    //$user_profile = $this->far_users->get_profile('uacc_id', $shop_detail['uacc_id']);
                    ?>
                    
					<li class="col-md-4 col-xs-6 sm-padding">
						<div class="blog_post">
							<figure class="post_thumb" style="background-image: url(<?php echo $list_images[0]['image_url']; ?>);">
								
								<span class="date"><?php echo $this->far_date->convert_format($shop_detail['create_dttm'], 'F d, Y'); ?></span>
							</figure>
							<h3><a href="#"><?php echo $product_name; ?></a></h3>
							<div class="posted_by"><b>by:</b> <?php echo $user_profile['fullname']; ?></div>
							<p class="product_description"><?php echo $product_description; ?></p>
							<a href="<?php echo 'https://www.usahawan.me/'.$shop_detail['product_url_alias']; ?>" class="read_more">Read More</a>
						</div>
					</li>
                    <?php } ?>
				</ul>
			</div>
		</section><!-- /.blog_section -->
      
        <section id="contact" class="contact_section bg-grey bd-bottom padding">
            <div class="container">
                <div class="col-sm-6 xs-padding">
                    <div class="contact_info">
                        <i class="fa fa-phone"></i>
                        <h3><?php echo $lang["HAVEANYDOUBTS"]; ?></h3>
                        <h2><?php echo $lang["CALLUSNOW"]; ?></h2>
                        <p>+603 - 2031 9999</p>
                    </div>
                </div>
                <div class="col-sm-6 xs-padding">
                   <div class="section_heading">
                       <h2><?php echo $lang["CONTACTWITHUS"]; ?></h2>
                   </div>
                    <div class="contact_wrap">
                        <form action="#" method="post" id="ajax_form" class="form-horizontal contact_form">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Name" required>
                                </div>
                                <div class="col-sm-6">
                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <textarea id="message" name="message" cols="30" rows="5" class="form-control msg" placeholder="Message" required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 align-left">
                                    <button id="submit" class="fs_btn" type="submit"><?php echo $lang["SENDMESSAGE"]; ?></button>
                                </div>
                            </div>
                            <div id="form-messages" class="alert" role="alert"></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
       
       <div id="google_map"></div><!-- /#google_map -->
        
        <section class="widget_section padding">
            <div class="container">
                <div class="col-md-4 col-xs-6 sm-padding">
                    <div class="footer_content">
                        <img class="mb-15" src="<?php echo base_url(); ?>assets/website/img/logo-light.png?a=a" alt="Foxstar">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        <ul class="social_links"> 
                            <li><a href="https://www.facebook.com/groups/selangorcops/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-xs-6 sm-padding">
                    <div class="footer_content">
                        <h3><?php echo $lang["QUICKLINKS"]; ?></h3>
                        <ul class="links"> 
                            <li><a data-scroll href="#about"><?php echo $lang["ABOUTUS"]; ?></a></li>
                            <li><a data-scroll href="#contact"><?php echo $lang["MENU_CONTACT"]; ?></a></li>
                            <li><a  href="<?php echo base_url(); ?>privacypolicy.php"><?php echo $lang["PRIVACY_POLICY"]; ?></a></li>
                        </ul>
                    </div>    
                </div>
                <div class="col-md-4 col-xs-6 sm-padding">
                    <div class="footer_content"> 
                      <h3><?php echo $lang["FINDUS"]; ?></h3>
                        <p class="address">
                        	<i class="fa fa-home"></i> No.27-1, Jalan PU 7/4,<br />Taman Puchong Utama, <br>47140 Puchong,<br />Selangor Darul Ehsan.<br /><br />
                        	<i class="fa fa-envelope"></i> enquiry@cops.org.my<br>
                        	<i class="fa fa-globe"></i> <a href="#">www.cops.org.my</a>
                        </p>
                    </div>
                </div>
            </div>
        </section> <!-- /.widget_section -->

		<footer class="footer_section bg-dark">
			<div class="container">
				<div class="col-xs-6 xs-padding">
				    <div class="copyright">&copy; 2018 CO-OP <?php echo $lang["POWEREDBY"]; ?> Usahawan (dot) Me</div>
				</div>
				<div class="col-xs-6 xs-padding">
				    <ul class="footer_social">
				        <li><a href="https://www.facebook.com/groups/selangorcops/" target="_blank"><i class="social_facebook"></i></a></li>
				    </ul>
				</div>
			</div>
		</footer><!-- /.footer_section -->
        
		<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_carrot-up"></i></a>
	
		<!-- jQuery Lib -->
		<script src="<?php echo base_url(); ?>assets/website/js/vendor/jquery-1.12.4.min.js"></script>
		<!-- waypoints js -->
        <script src="<?php echo base_url(); ?>assets/website/js/vendor/jquery.waypoints.v2.0.3.min.js"></script>
		<!-- Bootstrap JS -->
		<script src="<?php echo base_url(); ?>assets/website/js/vendor/bootstrap.min.js"></script>
		<!-- OWL-Carousel JS -->
		<script src="<?php echo base_url(); ?>assets/website/js/vendor/owl.carousel.min.js"></script>
        <!-- Imagesloaded JS -->
        <script src="<?php echo base_url(); ?>assets/website/js/vendor/imagesloaded.pkgd.min.js"></script>
        <!-- Isotope JS -->
        <script src="<?php echo base_url(); ?>assets/website/js/vendor/jquery.isotope.v3.0.2.js"></script>
		<!-- Smooth Scroll JS -->
		<script src="<?php echo base_url(); ?>assets/website/js/vendor/smooth-scroll.min.js"></script>
		<!-- vendor JS -->
		<script src="<?php echo base_url(); ?>assets/website/js/vendor/venobox.min.js"></script>
		<!-- Counterup JS -->
		<script src="<?php echo base_url(); ?>assets/website/js/vendor/jquery.counterup.min.js"></script>
        <!-- Google Map JS -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsQaKC50IbTAoK5_-_HHb7taUUHwnusJo"></script>
		<!-- Contact JS -->
		<script src="<?php echo base_url(); ?>assets/website/js/contact.js"></script>
		<!-- Main JS -->
		<script src="<?php echo base_url(); ?>assets/website/js/main.js?time=<?php time(); ?>"></script>

<script>
$(function(){
    $("#select_lang").on('change', function(){
        onchange_language();
    })
});

function onchange_language(){
    $('body').removeClass('loaded');
    var new_site_lang = $("#select_lang").val();
    var url = "<?php echo base_url(); ?>pages/change_site_lang/?new_site_lang="+new_site_lang;
    window.location.href = url;
}

function redirecting_to_register(){
    var redirect_url = "https://www.usahawan.me/register/user/?referral=447&redirect=https%3A%2F%2Fwww.usahawan.me%2Fpages%2Fproduct_detail";
    
    $("body").addClass("body_redirect");
    $("#redirecting").show();
    $("#redirecting").addClass("run_redirect");
    
    var timeleft = 5;
    var downloadTimer = setInterval(function(){
    timeleft--;
    document.getElementById("countdowntimer").textContent = timeleft;
    
    if(timeleft <= 1){
        window.top.location.href = redirect_url;
    }
    
    if(timeleft <= 0)
        clearInterval(downloadTimer);
    },1000);
    
    
    
    
}
//window.top.location.href = redirect_url;
</script>

    </body>
</html>