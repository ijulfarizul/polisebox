<?php
$breadcrumb = $this->far_menu->page_title();

?>
<h3 class="page-title"> <?php echo $breadcrumb->page_title; ?> <small><?php echo $breadcrumb->page_title_small; ?></small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
                
                
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url(); ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
                    
                    <?php
                    $count_breadcrumb = count($this->far_menu->breadcrumb());
                    //echo '<pre>'; print_r($this->far_menu->breadcrumb()); echo '</pre>';
                    $last_breadcrumb = $count_breadcrumb-1;
                    foreach($this->far_menu->breadcrumb() as $key => $value){ ?>
                    <?php
                    if($value['link'] == 'ci_controller'){ $menulink = base_url().$value['class'].'/'.$value['method']; }
                    else{ $menulink = $value['link']; }
                    ?>
                        <?php if($last_breadcrumb != $key){ ?>
                        <li>
    						<a href="<?php echo $menulink; ?>"><?php echo $value[name]; ?></a>
                            
                            <i class="fa fa-angle-right"></i>
                            
    						
    					</li>
                        <?php }else{ ?>
                        <li>
    						<a style="text-decoration: none;" href="javascript: void();"><?php echo $value[name]; ?></a>
                            
    					</li>
                        <?php } ?>
                    <?php }
                    ?>
				</ul>
				<div class="page-toolbar">
                    <!--
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						Actions <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#">Action</a>
							</li>
							<li>
								<a href="#">Another action</a>
							</li>
							<li>
								<a href="#">Something else here</a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="#">Separated link</a>
							</li>
						</ul>
					</div>
                    -->
				</div>
			</div>