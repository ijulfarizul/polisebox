<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Far_date {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    function convert_format($dttm, $to_format){
        $date = new DateTime($dttm);
        $converted = $date->format($to_format);
        return $converted;
    }
    
    function list_all_month_between_two_dates($date_start, $date_end, $output_format = 'Y-m-d'){
        $start    = new DateTime($date_start);
        $start->modify('first day of this month');
        $end      = new DateTime($date_end);
        $end->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);
        
        $list_date = array();
        $output = array();
        foreach ($period as $dt) {
            $x = $dt->format("Y-m-d H:i:s");
            $list_date['output_format'] = $dt->format($output_format);
            $list_date['timestamp'] = strtotime($x);
            $list_date['standard'] = $x;
            $list_date['standard_month_year'] = $dt->format("Ym");
            $list_date['start_date_for_this_month'] = $dt->format("Y-m").'-01';
            $list_date['end_date_for_this_month'] = $dt->format("Y-m-t");
            $list_date['year'] = $dt->format("Y");
            $list_date['month'] = $dt->format("m");
            $output[] = $list_date;
        }
        
        return $output;
    }
    
    
}

?>