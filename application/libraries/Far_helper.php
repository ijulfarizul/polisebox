<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Far_helper {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    /**
     * Convert number to price format.
     */
    function convert_number_to_price_format($number){
        return number_format($number, 2, ".", "," );
    }
    
    function generateRandomnumber($length = 6){
        $result = '';
        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(1, 9);
        }
        return $result;
    }

    function generateRandomString($length = 10, $characters = NULL) {
        if(!$characters){
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function convert_positive_to_negative($positive_value){
        $num = -1 * abs($positive_value);
        return $num;
    }
    
    function convert_negative_to_positive($negative_value){
        return abs($negative_value);
    }
    
    /*
    Copyright (c) 2009, reusablecode.blogspot.com; some rights reserved.
    
    This work is licensed under the Creative Commons Attribution License. To view
    a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or
    send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, California
    94305, USA.
    */
 
    // Convert letters and numbers to corresponding NATO phonetic alphabet code words.
    function phonetic($char){
        $nato = array(
            "a" => "alfa", 
            "b" => "bravo", 
            "c" => "charlie", 
            "d" => "delta", 
            "e" => "echo", 
            "f" => "foxtrot", 
            "g" => "golf", 
            "h" => "hotel", 
            "i" => "india", 
            "j" => "juliett", 
            "k" => "kilo", 
            "l" => "lima", 
            "m" => "mike", 
            "n" => "november", 
            "o" => "oscar", 
            "p" => "papa", 
            "q" => "quebec", 
            "r" => "romeo", 
            "s" => "sierra", 
            "t" => "tango", 
            "u" => "uniform", 
            "v" => "victor", 
            "w" => "whisky", 
            "x" => "x-ray", 
            "y" => "yankee", 
            "z" => "zulu", 
            "0" => "zero", 
            "1" => "one", 
            "2" => "two", 
            "3" => "three", 
            "4" => "four", 
            "5" => "five", 
            "6" => "six", 
            "7" => "seven", 
            "8" => "eight", 
            "9" => "niner"
            );
 
        return $nato[strtolower($char)];
    }
    
    function list_all_bank(){
        $query = $this->CI->db->query("SELECT * FROM far_bank");
        return $query->result_array();
    }
    
    function fix_msisdn($phone_number){
        //remove non digit
        $phone_number = preg_replace('/[^0-9.]+/', '', $phone_number);
        
        if($phone_number[0] != 6){
            
            if($phone_number[0] == 0){
                $output = '6'.$phone_number;
            }else{
                $output = $phone_number;
            }
               
        }else{
            $output = $phone_number;
        }
        
        //filter count
        if(strlen($output) < 6){
            $output = "";
        }
        
        return $output;
    }
    
    function split_msisdn_and_prefix($msisdn){
        $output = array();
        $msisdn = $this->fix_msisdn($msisdn);
        $prefix = substr($msisdn, 0, 4);
        $phone_number = substr($msisdn, 4);
        
        $output['prefix'] = $prefix;
        $output['phone_number'] = $phone_number;
        return $output;
    }
    
    function replace_http_to_https($string){
        /*
        $string = strtolower($string);
        $url = preg_replace("/^http:/i", "https:", $string);
        */
        $url = str_replace( 'http://', 'https://', $string );
        return $url;
    }
    
    function isValidEmail($email) {
        return !!filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    
    
}


?>