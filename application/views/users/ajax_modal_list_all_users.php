<?php
//echo '<pre>'; print_r($list_sponsor_bonus); echo '</pre>';
?>

<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-hover" id="datatable_user_downline_and_upline">
		<thead>
		<tr>
			<th>
				#
			</th>
			<th>
				Action
			</th>
			<th>
				Username
			</th>
			<th>
				Fullname
			</th>
			<th>
				Email
			</th>
			
		</tr>
		</thead>
		<tbody>
		<?php 
        foreach($list_all_user as $key => $val){ ?>
		<tr>
			<td>
                <?php echo $key+1; ?>
			</td>
			<td>
				<a href="javascript: void(0);" data-fullname="<?php echo $val['firstname'].' '.$val['lastname']; ?>" data-uaccusername="<?php echo $val['uacc_username']; ?>" data-uaccid="<?php echo $val['uacc_id']; ?>" class="btn btn-xs green select_user_btn"> Select <i class="fa fa-arrow-right"></i>
				</a>
			</td>
			<td>
                <?php echo $val['uacc_username']; ?>
			</td>
			<td>
                <?php echo $val['firstname'].' '.$val['lastname']; ?>
			</td>
			<td>
                <?php echo $val['uacc_email'] ?>
			</td>
			
		</tr>
		<?php } ?>
		</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
function onclick_select_referral_for_agent(element){
    var data = $(element).data();
    
    var fullname = data.uaccusername+" ( "+data.fullname+" )";
    $("#input_referral_for_agent_text").val(fullname)
    $("#input_referral_for_agent").val(data.uaccid);
    $("#modal_select_referral").modal("hide");
}

$(function(){
    $('#datatable_user_downline_and_upline').DataTable({
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        "iDisplayLength": 10,
        "oLanguage": {
         "sSearch": "Search user:"
       }
    });
    
    $("input[aria-controls=datatable_user_downline_and_upline]").removeClass('input-small').addClass('input-large')
})
</script>