<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Registration Page</title>
<!-- CSS -->
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/register/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/register/assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/register/assets/css/form-elements.css?time=<?php echo time(); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/register/assets/css/style.css?time=<?php echo time(); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/css/intlTelInput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/sweetalert/sweetalert.css"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
<style>
.sweet-alert div {
    border-radius: 50% !important;
}
.sweet-alert {
    border-radius: 5px !important;
}
.sweet-alert button {
    border-radius: 5px !important;
}
.spinner {
  width: 50px;
  height: 50px;
  display: inline-block;
  padding: 0px;
  border-radius: 100% !important;
  border: 6px solid;
  border-top-color: #005a9c;
  border-bottom-color: #005a9c;
  border-left-color: rgba(0, 90, 156, 0.15);
  border-right-color: rgba(0, 90, 156, 0.15);
  -webkit-animation: spinner 0.8s ease-in-out infinite alternate;
  animation: spinner 0.8s ease-in-out infinite alternate;
}
@keyframes spinner {
  from {
    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}
@-webkit-keyframes spinner {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
  }
}
.iti-flag {background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/img/flags.png");}

@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
  .iti-flag {background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/img/flags@2x.png");}
}

.intl-tel-input {
    display: block;
}
.inner-bg {
    padding: 0px 0 80px 0;
}
.form-box {
    margin-top: 30px;
}
i.fa-usahawan  {
    content:"";
    background-image:url('<?php echo base_url(); ?>assets/global/img/polisebox/logo_100x100_transparent.png');
    width: 100px;
    height: 100px;
    display: inline-block;
    background-position:center;
    background-size:cover;
}

.input-icon > i {
    color: #ccc;
    display: block;
    position: absolute;
    margin: 16px 2px 4px 15px;
    z-index: 3;
    width: 16px;
    font-size: 16px;
    text-align: center;
}
.input-icon > .form-control {
    padding-left: 33px;
}
</style>
<!-- Favicon and touch icons -->
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/global/img/favicons3/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/global/img/favicons3/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/global/img/favicons3/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/global/img/favicons3/manifest.json">
<link rel="mask-icon" href="<?php echo base_url(); ?>assets/global/img/favicons3/safari-pinned-tab.svg" color="#035d8f">
<meta name="theme-color" content="#035d8f">
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Top content -->
<style>
#img1 {
    background-image: url(<?php echo $referral_profile['profilepic_url']; ?>);
}
.avatar {
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 50px;
    position: relative;
    width: 50px;
    border-radius: 6px;
    margin: 0px;
    border: 3px solid #dedede;
    float: left;
}
</style>
<div class="top-content">
	<div class="inner-bg">
		<div class="container">
			
			<div class="row">
				
				<div class="col-sm-12">
					<div class="form-box">
						<div class="form-top">
							<div class="form-top-left">
								<h3>Sign up now</h3>
								<p>
									Fill in the form below to get instant access:
								</p>
							</div>
							<div class="form-top-right" style="line-height: 10px;">
								<i class="fa fa-usahawan"></i>
							</div>
						</div>
						<div class="form-bottom">
                        
                        
							<form role="form" action="" method="post" class="registration-form">
                            
                                <?php if($upline_uacc_id > 0){ ?>
                                <div class="form-group">
									<div class="col-xs-12" style="padding: 0px 0px 0px 0px !important; margin-bottom: 10px;">
                                        <div class="pull-left" >
                                            <div class="avatar" id="img1">
                                            
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="pull-left" style="line-height: 20px; margin-left: 10px;">
                                                <span style="font-size: 12px;">Your referral is :</span>
                                                <br />
                                                <span class="fullname-footer" style="font-weight: bolder;"> <?php echo $referral_profile['fullname']; ?></span>
                                            </div>
                                    </div>
								</div>
                                <?php } ?>
                            
                            
								<div class="form-group">
									<label class="sr-only" for="form-last-name">Fullname</label>
									<input type="text" name="form-last-name" placeholder="Fullname" class="form-last-name form-control" id="fullname">
                                    <span class="help-block error_message" style="display: none;"></span>
								</div>
                                <div class="form-group">
									<label class="sr-only" for="form-last-name">Email</label>
									<input type="text" name="form-last-name" placeholder="Email" class="form-last-name form-control" id="email">
                                    <span class="help-block error_message" style="display: none;"></span>
								</div>
								<div class="form-group">
									<label class="sr-only" for="form-email">Phone Number</label>
									<input type="text" name="form-email" placeholder="Phone Number" class="form-email form-control" id="phonemobile">
                                    <span class="help-block error_message" style="display: none;"></span>
								</div>
								<div class="form-group">
									<label class="sr-only" for="form-email">IC Number / Passport</label>
									<input type="text" name="form-email" placeholder="IC Number / Passport" class="form-email form-control" id="nric">
                                    <span class="help-block error_message" style="display: none;"></span>
								</div>
                                
                                <div class="form-group">
									<label class="sr-only" for="form-email">Serial Number</label>
									<div class="input-group">
												<div class="input-icon">
													<i class="fa fa-lock fa-fw"></i>
													<input id="serial_number" class="form-control" type="text" name="password" placeholder="Serial Number">
												</div>
												<span class="input-group-btn">
												<button id="genpassword" class="btn btn-success" type="button" onclick="javascript: open_popup_scan_qrcode();"><i class="fa fa-camera"></i> Scan</button>
												</span>
											</div>
                                    <span class="help-block error_message" style="display: none;"></span>
								</div>
                                
								<button type="button" onclick="javascript: btn_submit_register();" class="btn">Register</button>
							</form>
						</div>
					</div>
                    <!--
                    <div class="social-login social-login-buttons">
                        <h3>...or signup with:</h3>
                   	    <a class="btn btn-link-1 btn-link-1-facebook" href="javascript: void(0);" onclick="javascript: signup_using_facebook();">
              		        <i class="fa fa-facebook"></i> Facebook
                       	</a>
                       	
                        <a class="btn btn-link-1 btn-link-1-google-plus" href="javascript: void(0);">
              		        <i class="fa fa-google-plus"></i> Google Plus
                       	</a>
                        
                   	</div>
                    -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer -->

<!-- Javascript -->
<script src="<?php echo base_url(); ?>assets/register/assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/register/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/register/assets/js/jquery.backstretch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/register/assets/js/scripts.js?as=a"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/js/intlTelInput.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/js/utils.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/jquery.popupwindow.js"></script>


<script src="//cdnjs.cloudflare.com/ajax/libs/chance/1.0.11/chance.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/keypress/2.1.4/keypress.min.js"></script>




<script type="text/javascript">
var listener = new window.keypress.Listener();
$(function(){
    listener.sequence_combo("a s d", function() {
        var fullname = chance.name();
        var usernameRegister = "test_"+fullname.replace(/\s/g,'');
        usernameRegister = usernameRegister.toLowerCase()+chance.integer({min: 1111, max: 9999});
        var emailRegister = usernameRegister+'@test.policebox.tk';
        $("#fullname").val(fullname);
        $("#email").val(emailRegister);
        $("#phonemobile").intlTelInput("setNumber", '+601'+chance.integer({min: 11111111, max: 99999999}));
        $("#nric").val(chance.integer({min: 600000000000, max: 900000000000}));

        
    }, true);
});
</script>
<script type="text/javascript">
$.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    return this;
  }
</script>
<script type="text/javascript">
$(function(){
    /*
        Fullscreen background
    */
    $.backstretch("<?php echo base_url(); ?>assets/global/img/polisebox/bg_640x960.png"); 
    
    //register
    $("#phonemobile").intlTelInput({
        onlyCountries: [
            "my",
            "sg",
            "id",
            "th",
            "bn"
        ],
        preferredCountries: [
            "my",
            "id"
        ],
        separateDialCode: true
    });
    
});
</script>
<script type="text/javascript">
function open_popup_scan_qrcode(){
    var window_name = "scan_qrcode";
    var url_gallery = '<?php echo base_url(); ?>register/camera_scan_qrcode';
    var popup_window = $.popupWindow(url_gallery, {
        width: 750,
        height: 689,
        name: window_name,
        onUnload: function(){
            
        },
        onbeforeunload: function(){
            
        }
    });
}
</script>

<script type="text/javascript">
function signup_using_facebook(){
    var window_name = "facebook_login";
    var url_gallery = '<?php echo base_url(); ?>facebook_login/web_login/?uacc_upline_id=<?php echo $upline_uacc_id; ?>';
    var popup_window = $.popupWindow(url_gallery, {
        width: 750,
        height: 689,
        name: window_name,
        onUnload: function(){
            check_is_user_loggedin();
        },
        onbeforeunload: function(){
            alert('loading');
        }
    });
}
function check_is_user_loggedin(){
    var jqxhr = $.getJSON("<?php echo base_url(); ?>login/is_user_loggedin", function(data) {
                if(data.logged_in == "yes"){
                    window.top.location.href = "<?php echo base_url(); ?>auth_admin/dashboard/";
                }
            })
            .done(function() {
                console.log( "second success" );
            })
            .fail(function() {
                console.log( "error" );
            })
            .always(function() {
                console.log( "complete" );
            });
    
}
</script>
<script type="text/javascript">
function btn_submit_register(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    var fullname = $("#fullname").val();
    var email = $("#email").val();
    var phonemobile = $("#phonemobile").intlTelInput("getNumber");
    var password = $("#password").val();
    var nric = $("#nric").val();
    var serial_number = $("#serial_number").val();
    
    blockUI();
    
    $.ajax({
        url: '<?php echo base_url(); ?>register/ajax_create_user',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                upline_uacc_id: '<?php echo $upline_uacc_id; ?>',
                fullname: fullname,
                email: email,
                phonemobile: phonemobile,
                password: password,
                nric: nric,
                serial_number: serial_number
            } 
        },
        success: function(data){
            
            if(data.status == "success"){
                
                window.location = "<?php echo base_url(); ?>register/success/?u="+data.uacc_id+'&secure_key='+data.secure_key;
                $.unblockUI();
            }else{
                $.unblockUI();
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        if(j){
                            sweetAlert("Oops...", j, "error");
                        }else{
                            sweetAlert("Oops...", "Something went wrong!", "error");
                        }
                        
                    }
                })
            }
            
            
        }
    })
}
</script>

<script type="text/javascript">
function blockUI() {
    $.blockUI({
        css: {
            backgroundColor: 'transparent',
            border: 'none'
        },
        message: '<div class="spinner"></div>',
        baseZ: 99999,
        overlayCSS: {
            backgroundColor: '#FFFFFF',
            opacity: 0.7,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}//end Blockui
</script>
<!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
</body>
</html>