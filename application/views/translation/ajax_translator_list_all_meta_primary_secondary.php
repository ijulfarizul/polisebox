<?php
$primary_lang = 'lang_'.$primary;
$secondary_lang = 'lang_'.$secondary;
?>
<input type="hidden" id="lang_editor_1_name" value="<?php echo $primary; ?>" />
<input type="hidden" id="lang_editor_2_name" value="<?php echo $secondary; ?>" />
<div class="row">
    <div class="col-md-12">
        <?php foreach($list_all_meta as $a => $b){ ?>
        <div class="panel panel_frame panel-default panel_trans_id_<?php echo $b['trans_id']; ?>">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a href="javascript: void(0);" class="btn btn-xs green copy_to_clipboard" data-clipboard-text="&lt;?php echo $lang[&quot;<?php echo $b['trans_meta']; ?>&quot;]; ?&gt;">
                        <i class="fa fa-clipboard"></i>
                    </a>
                    <a href="javascript: void(0);" class="btn btn-xs blue save_btn" onclick="javascript: save_meta(this, '<?php echo $b['trans_id']; ?>');">
                        <i class="fa fa-save"></i>
                    </a>
                    <a href="javascript: void(0);" class="btn btn-xs red delete_btn" onclick="javascript: open_modal_confirm_delete_meta('<?php echo $b['trans_id']; ?>');">
                        <i class="fa fa-close"></i>
                    </a>
                    <?php echo $b['trans_meta']; ?></h3>
            </div>
            <div class="panel-body">
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?php echo strtoupper($primary); ?></label>
                            <textarea class="form-control lang_editor lang_editor_1" rows="3" id="lang_editor_1"><?php echo $b[$primary_lang]; ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?php echo strtoupper($secondary); ?></label>
                            <textarea class="form-control lang_editor lang_editor_2" rows="3" id="lang_editor_2"><?php echo $b[$secondary_lang]; ?></textarea>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <?php } ?>
    </div>
</div>


<script type="text/javascript">
$(function(){
    $( ".lang_editor" ).focusin(function() {
        var panel = $(this).closest('.panel_frame');
        
        //reset all panel
        $(".panel_frame").removeClass('panel-primary').addClass('panel-default');
        
        //highlight focused panel
        $(panel).removeClass('panel-default').addClass('panel-primary');
    });
    
    $(".lang_editor").on('keyup', function(){
        var panel = $(this).closest('.panel_frame');
        //remove all class
        $(panel).removeClass('panel-default').removeClass('panel-primary').addClass('panel-danger');
    });
    
    var clipboard = new Clipboard('.copy_to_clipboard');
    clipboard.on('success', function(e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);
    
        e.clearSelection();
        
        $(e.trigger).pulsate({
            color: "#bf1c56",
            glow: true,
            repeat: false
        });
        
    });
    
});

function open_modal_confirm_delete_meta(trans_id){
    
    $("#input_hidden_trans_id").val(trans_id);
    $("#modal_delete_meta").modal('show');
}

function confirm_delete(){
    var trans_id = $("#input_hidden_trans_id").val();
    var lang_primary_name = $("#lang_editor_1_name").val();
    var lang_secondary_name = $("#lang_editor_2_name").val();
    $.ajax({
        url: '<?php echo base_url(); ?>translation/ajax_translator_delete_meta',
        type: 'post',
        dataType: 'json',
        data: { trans_id: trans_id },
        success: function(data){
            $("#modal_delete_meta").modal('hide');
            if(data.status == 'success'){
                load_meta_frame(lang_primary_name, lang_secondary_name);
            }else{
                alert('error');
            }
            
        }
    })
}

function save_meta(btn, trans_id){
    
    
    
    var panel = $(btn).closest('.panel_frame');
    var primary_text = $(panel).find('#lang_editor_1').get(0);
    var secondary_text = $(panel).find('#lang_editor_2').get(0);
    
    var lang_primary_name = $("#lang_editor_1_name").val();
    var lang_secondary_name = $("#lang_editor_2_name").val();
    
    
    Metronic.blockUI({
        target: panel,
        boxed: true,
        message: 'Saving...'
    });
    
    var data_to_post = {
        trans_id: trans_id,
        primary_text: $(primary_text).val(),
        secondary_text: $(secondary_text).val(),
        lang_primary_name: lang_primary_name,
        lang_secondary_name: lang_secondary_name
    }
    
    $.ajax({
        url: '<?php echo base_url(); ?>translation/ajax_translator_update_meta',
        type: 'post',
        dataType: 'json',
        data: { post: data_to_post },
        success: function(data){
            if(data.status == 'success'){
                var panel_trans_id = '.panel_trans_id_'+data.trans_id;
                $(panel_trans_id).removeClass('panel-danger').addClass('panel-default');
                Metronic.unblockUI(panel);
            }
        }
    })
}
</script>