/*
	Foxstar Theme Scripts
*/

(function($){ "use strict";
             
    $(window).on('load', function() {
        $('body').addClass('loaded');
    });

/*=========================================================================
    Sticky Header
=========================================================================*/ 
    $(function() {
        var header = $("#header"),
            yOffset = 0,
            headerHeight = $('.header_section').height(),
            triggerPoint = 10;
        $(window).on( 'scroll', function() {
            yOffset = $(window).scrollTop();

            if (yOffset >= triggerPoint) {
                header.addClass("fixed-top");
            } else {
                header.removeClass("fixed-top");
            }

        });
        $('.header_height').css( 'margin-top', headerHeight );
    });

/*=========================================================================
    Main Slider
=========================================================================*/
    var owlSlider = $('#main-slider');
    owlSlider.owlCarousel({
        items: 1,
        loop: true,
        smartSpeed: 500,
        autoplayTimeout: 3500,
        mouseDrag: false,
        touchDrag: false,
        autoplay: true,
        nav: true,
        navText: ['<i class="arrow_carrot-left"></i>', '<i class="arrow_carrot-right"></i>']
    });
    // Slider animation
    owlSlider.on('translate.owl.carousel', function () {
        $('.slider_content h1').removeClass('fadeInUp animated').hide();
        $('.slider_content p').removeClass('fadeInUp animated').hide();
        $('.slider_content .btn_group').removeClass('fadeInDown animated').hide();
    });

    owlSlider.on('translated.owl.carousel', function () {
        $('.owl-item.active .slider_content h1').addClass('fadeInUp animated').show();
        $('.owl-item.active .slider_content p').addClass('fadeInUp animated').show();
        $('.owl-item.active .slider_content .btn_group').addClass('fadeInDown animated').show();
    });

/*=========================================================================
    Isotope Active
=========================================================================*/
	$('.portfolio_items').imagesLoaded( function() {

		 // Add isotope click function
		$('.portfolio_filter li').on('click', function(){
	        $(".portfolio_filter li").removeClass("active");
	        $(this).addClass("active");
	 
	        var selector = $(this).attr('data-filter');
	        $(".portfolio_items").isotope({
	            filter: selector,
	            animationOptions: {
	                duration: 750,
	                easing: 'linear',
	                queue: false,
	            }
	        });
	        return false;
	    });

	    $(".portfolio_items").isotope({
	        itemSelector: '.single_item',
	        layoutMode: 'masonry',
	    });
	});
             
/*=========================================================================
	Counter Up Active
=========================================================================*/
	var counterSelector = $('.counter');
	counterSelector.counterUp({
		delay: 10,
		time: 1000
	});
             
        
		
/*=========================================================================
	Initialize smoothscroll plugin
=========================================================================*/
	smoothScroll.init({
		offset: 60
	});

/*=========================================================================
	Active venobox
=========================================================================*/
	var vbSelector = $('.img_popup');
	vbSelector.venobox({
		numeratio: true,
		infinigall: true
	}); 
				 
/*=========================================================================
	Scroll To Top
=========================================================================*/     
    $(window).on( 'scroll', function () {
        if ($(this).scrollTop() > 100) {
            $('#scroll-to-top').fadeIn();
        } else {
            $('#scroll-to-top').fadeOut();
        }
    });
 
/*=========================================================================
        Google Map Settings
=========================================================================*/
             
google.maps.event.addDomListener(window, 'load', init);

function init() {

	var mapOptions = {
		zoom: 17,
		center: new google.maps.LatLng(2.991952, 101.615373), 
		scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		scaleControl: false,
		draggable: false,
		styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#444444"},{"visibility":"on"}]}]
	};

	var mapElement = document.getElementById('google_map');

	var map = new google.maps.Map(mapElement, mapOptions);

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(2.991952, 101.615373),
		map: map,
		title: 'Location!'
	});
}

})(jQuery);
