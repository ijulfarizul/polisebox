<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ebox_model extends CI_Model {
  public $flexi_auth_config;

  function __construct(){
    parent::__construct();
    

    $this->config->load('flexi_auth', TRUE);
    $this->flexi_auth_config = $this->config->item('flexi_auth');
  }

  public function getEboxLogs( $conf = [] ) {
    $ares = [];

    $this->db->select('*');
    $this->db->from('cp_log_report');
    $this->db->join('user_profiles', 'cp_log_report.cp_uacc_id = user_profiles.uacc_id', 'left');

    if(isset($conf['single'])) {
      $this->db->join('cp_log_photo', 'cp_log_report.log_id = cp_log_photo.log_id', 'left');
    }

    if(isset($conf['where'])) {
      $this->db->where($conf['where']);
    }

    if(isset($conf['limit'])) {
      $this->db->limit($conf['limit'], $conf['offset']);
    }

    $this->db->order_by('cp_log_report.log_id', 'desc');


    $query = $this->db->get();
    if($query->num_rows()) {
      $ares = $query->result_array();
      
      if(isset($conf['single']) && $conf['single']) {
        $ares = $ares[0];
      }
    }

    return $ares;
  }

}


