<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function form(){
        
        $this->load->view('login/form', $this->data);
    }
    
    function ajax_login(){
        $postdata = $this->input->post('postdata');

        $error = array();
        $output = array();
        
        //check username
        if(strlen($postdata['username']) <= 3){
            $error['username'] = "Username must be more than 3 character";
        }
        
        //check username
        if(strlen($postdata['password']) <= 5){
            $error['password'] = "Password must be more than 5 character";
        }
        
        $auto_login = $this->flexi_auth->login($postdata['username'], $postdata['password'], TRUE);
        if(!$auto_login){
            $error['authentication'] = "Authentication error. Please check your username or password";
        }
        
        if(count($error) == 0){
            
            
            
            
            $output['status'] = 'success';
            
        }else{
            
            $output['message_single'] = current($error);
            $output['errors'] = $error;
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }

    function is_user_loggedin(){
        if($this->user['uacc_id'] > 0){
            $logged_in = "yes";
        }else{
            $logged_in = "no";
        }
        
        echo json_encode(array('logged_in' => $logged_in));
    }
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */