<?php $CI =& get_instance(); ?>

<div class="row">
					<div class="col-xs-12">
						<table class="table table-striped table-hover">
						<thead>
						<tr>
							<th>
								 #
							</th>
							<th>
								 Date
							</th>
							<th class="hidden-480">
								 Description
							</th>
							<th class="hidden-480">
								 In<sup>(<?php echo $currency; ?>)</sup>
							</th>
							<th class="hidden-480">
								 Out<sup>(<?php echo $currency; ?>)</sup>
							</th>
							<th>
								 Balance
							</th>
						</tr>
						</thead>
						<tbody>
						<?php if(count($list_statement) > 0){ ?>
                            <?php foreach($list_statement as $a => $b){ ?>
                                <tr>
        							<td>
        								 <?php echo $a+1; ?>
        							</td>
        							<td>
        								 <?php 
                                         $date = new DateTime($b['create_dttm']);
                                         echo $date->format('d F Y h:i:s A');
                                         ?>
        							</td>
        							<td class="hidden-480">
        								 <?php echo $b['remarks']; ?>
        							</td>
        							<td class="hidden-480">
        								 <?php 
                                            if($b['amount'] >= 0){  
        								    
                                            echo number_format($b['amount'], 2, ".", "," );
        								 } ?>
        							</td>
        							<td class="hidden-480">
        								 <?php if($b['amount'] < 0){  
        								    
                                            $positive = substr($b['amount'], 1);
                                            echo number_format($positive, 2, ".", "," );
        								 } ?>
        							</td>
        							<td>
        								 <?php
                                         echo $currency.' ';
                                         if($a == 0){
                                            //echo number_format($wallet_balance, 2, ".", "," );
                                         }else{
                                            $wallet_balance = $wallet_balance+($b['amount']);
                                            //echo number_format($wallet_balance, 2, ".", "," );
                                         }
                                         //echo '<pre>'; print_r($b); echo '</pre>';
                                         //$wallet_balance = $CI->far_wallet->total_balance_id_range($user_detail['uacc_username'], $b['id']);
                                         $wallet_balance = $CI->far_wallet->total_balance_id_range_single($user_detail['uacc_id'], $b['id'], $wallet_table_name);
                                         echo number_format($wallet_balance, 2, ".", "," );
                                         
                                         ?>
        							</td>
        						</tr>
                            <?php } ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="9" class="text-center"> No data available.</td>
                            </tr>
                        <?php } ?>
						
						
						
						</tbody>
						</table>
					</div>
				</div>