<?php if(count($user_profile) == 0){ ?>
    <div class="form-group">
        <label class="col-md-2 control-label">&nbsp;</label>
        <div class="col-md-10">
            <div style="padding-top: 10px;">
                <span class="label label-warning"> Profile not created. </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">&nbsp;</label>
        <div class="col-md-10">
            <button type="button" onclick="javascript: create_profile('<?php echo $uacc_id; ?>');" class="btn blue">Create Profile</button>
        </div>
    </div>
<script type="text/javascript">
function create_profile(uacc_id){
    Metronic.blockUI({
        boxed: true,
        message: 'Creating...'
    });
    
    $.ajax({
        url: '<?php echo base_url(); ?>users/admin_ajax_create_profile',
        type: 'POST',
        dataType: 'json',
        data: { uacc_id, uacc_id },
        success: function(data){
            Metronic.unblockUI();
            admin_load_tab_profile();
        }
    })
}
</script>
<?php exit(); } ?>

<form action="<?php echo base_url(); ?>products/save/" method="post" id="form_general">
    <div class="form-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Firstname: </label>
            <div class="col-md-10">
                <input type="text" readonly="" class="form-control" name="user_profiles[firstname]" value="<?php echo $user_profile['firstname']; ?>">
            </div>
        </div>
    </div>
    <div class="form-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Lastname: </label>
            <div class="col-md-10">
                <input type="text" readonly="" class="form-control" name="user_profiles[lastname]" value="<?php echo $user_profile['lastname']; ?>">
            </div>
        </div>
    </div>
</form>