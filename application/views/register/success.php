<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Community Policing Registration Page</title>
<!-- CSS -->
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/register/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/register/assets/css/form-elements.css?time=<?php echo time(); ?>">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/register/assets/css/style.css?time=<?php echo time(); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/css/intlTelInput.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/global/plugins/sweetalert/sweetalert.css"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
<style>
/***
Customized Bootstrap Alerts
***/
.alert {
  border-width: 1px;
}
.alert.alert-borderless {
  border: 0;
}

/***
Bootstrap Alerts
***/
.alert-success {
  background-color: #dff0d8;
  border-color: #d6e9c6;
  color: #3c763d;
}
.alert-success hr {
  border-top-color: #c9e2b3;
}
.alert-success .alert-link {
  color: #2b542c;
}

.alert-info {
  background-color: #d9edf7;
  border-color: #bce8f1;
  color: #31708f;
}
.alert-info hr {
  border-top-color: #a6e1ec;
}
.alert-info .alert-link {
  color: #245269;
}

.alert-warning {
  background-color: #fcf8e3;
  border-color: #faebcc;
  color: #8a6d3b;
}
.alert-warning hr {
  border-top-color: #f7e1b5;
}
.alert-warning .alert-link {
  color: #66512c;
}

.alert-danger {
  background-color: #f2dede;
  border-color: #ebccd1;
  color: #a94442;
}
.alert-danger hr {
  border-top-color: #e4b9c0;
}
.alert-danger .alert-link {
  color: #843534;
}
/***
Notes
***/
.note {
  margin: 0 0 20px 0;
  padding: 15px 30px 15px 15px;
  border-left: 5px solid #eee;
  -webkit-border-radius: 0 4px 4px 0;
  -moz-border-radius: 0 4px 4px 0;
  -ms-border-radius: 0 4px 4px 0;
  -o-border-radius: 0 4px 4px 0;
  border-radius: 0 4px 4px 0;
}
.note h1,
.note h2,
.note h3,
.note h4,
.note h5,
.note h6 {
  margin-top: 0;
}
.note h1 .close,
.note h2 .close,
.note h3 .close,
.note h4 .close,
.note h5 .close,
.note h6 .close {
  margin-right: -10px;
}
.note p {
  font-size: 13px;
}
.note p:last-child {
  margin-bottom: 0;
}
.note code,
.note .highlight {
  background-color: #fff;
}
.note.note-default {
  background-color: lightgray;
  border-color: #adadad;
  color: #333333;
}
.note.note-default.note-bordered {
  background-color: #c3c3c3;
  border-color: #a0a0a0;
}
.note.note-default.note-shadow {
  background-color: #c6c6c6;
  border-color: #a0a0a0;
  box-shadow: 5px 5px rgba(162, 162, 162, 0.2);
}
.note.note-primary {
  background-color: #5697d0;
  border-color: #3085a9;
  color: #D8E3F2;
}
.note.note-primary.note-bordered {
  background-color: #3e89c9;
  border-color: #2a7696;
}
.note.note-primary.note-shadow {
  background-color: #428bca;
  border-color: #2a7696;
  box-shadow: 5px 5px rgba(43, 121, 154, 0.2);
}
.note.note-success {
  background-color: #eef7ea;
  border-color: #c9e2b3;
  color: #3c763d;
}
.note.note-success.note-bordered {
  background-color: #dcefd4;
  border-color: #bbdba1;
}
.note.note-success.note-shadow {
  background-color: #dff0d8;
  border-color: #bbdba1;
  box-shadow: 5px 5px rgba(190, 220, 164, 0.2);
}
.note.note-info {
  background-color: #eef7fb;
  border-color: #a6e1ec;
  color: #31708f;
}
.note.note-info.note-bordered {
  background-color: #d5ebf6;
  border-color: #91d9e8;
}
.note.note-info.note-shadow {
  background-color: #d9edf7;
  border-color: #91d9e8;
  box-shadow: 5px 5px rgba(150, 219, 233, 0.2);
}
.note.note-warning {
  background-color: #fcf8e3;
  border-color: #f5d89e;
  color: #8a6d3b;
}
.note.note-warning.note-bordered {
  background-color: #f9f1c7;
  border-color: #f2cf87;
}
.note.note-warning.note-shadow {
  background-color: #faf2cc;
  border-color: #f2cf87;
  box-shadow: 5px 5px rgba(243, 209, 139, 0.2);
}
.note.note-danger {
  background-color: #f9f0f0;
  border-color: #e4b9c0;
  color: #a94442;
}
.note.note-danger.note-bordered {
  background-color: #f1dada;
  border-color: #dca7b0;
}
.note.note-danger.note-shadow {
  background-color: #f2dede;
  border-color: #dca7b0;
  box-shadow: 5px 5px rgba(222, 171, 179, 0.2);
}
.sweet-alert div {
    border-radius: 50% !important;
}
.sweet-alert {
    border-radius: 5px !important;
}
.sweet-alert button {
    border-radius: 5px !important;
}
.spinner {
  width: 50px;
  height: 50px;
  display: inline-block;
  padding: 0px;
  border-radius: 100% !important;
  border: 6px solid;
  border-top-color: #005a9c;
  border-bottom-color: #005a9c;
  border-left-color: rgba(0, 90, 156, 0.15);
  border-right-color: rgba(0, 90, 156, 0.15);
  -webkit-animation: spinner 0.8s ease-in-out infinite alternate;
  animation: spinner 0.8s ease-in-out infinite alternate;
}
@keyframes spinner {
  from {
    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
  }
}
@-webkit-keyframes spinner {
  from {
    -webkit-transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
  }
}
.iti-flag {background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/img/flags.png");}

@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
  .iti-flag {background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/img/flags@2x.png");}
}

.intl-tel-input {
    display: block;
}
.inner-bg {
    padding: 0px 0 80px 0;
}
.form-box {
    margin-top: 30px;
}
i.fa-usahawan  {
    content:"";
    background-image:url('https://www.usahawan.me/assets/global/img/usahawan/logo-100x100-transparent.png');
    width: 100px;
    height: 100px;
    display: inline-block;
    background-position:center;
    background-size:cover;
}

/* start da css for da buttons */
.btn-push {
  border-radius: 5px;
  padding: 10px 20px;
  font-size: 22px;
  text-decoration: none;
  margin: 0px;
  color: #fff;
  position: relative;
  display: inline-block;
}

.btn-push:active {
  transform: translate(0px, 5px);
  -webkit-transform: translate(0px, 5px);
  box-shadow: 0px 1px 0px 0px;
  text-decoration: none;
}

.blue {
  background-color: #55acee;
  box-shadow: 0px 5px 0px 0px #3C93D5;
}

.blue:hover {
  background-color: #6FC6FF;
}

.green {
  background-color: #2ecc71;
  box-shadow: 0px 5px 0px 0px #15B358;
}

.green:hover {
  background-color: #48E68B;
}

.red {
  background-color: #e74c3c;
  box-shadow: 0px 5px 0px 0px #CE3323;
}

.red:hover {
  background-color: #FF6656;
}

.purple {
  background-color: #9b59b6;
  box-shadow: 0px 5px 0px 0px #82409D;
}

.purple:hover {
  background-color: #B573D0;
}

.orange {
  background-color: #e67e22;
  box-shadow: 0px 5px 0px 0px #CD6509;
}

.orange:hover {
  background-color: #FF983C;
}

.yellow {
  background-color: #f1c40f;
  box-shadow: 0px 5px 0px 0px #D8AB00;
}

.yellow:hover {
  background-color: #FFDE29;
}
</style>
<!-- Favicon and touch icons -->
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/global/img/favicons3/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/global/img/favicons3/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/global/img/favicons3/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/global/img/favicons3/manifest.json">
<link rel="mask-icon" href="<?php echo base_url(); ?>assets/global/img/favicons3/safari-pinned-tab.svg" color="#035d8f">
<meta name="theme-color" content="#035d8f">
</head>
<body>
<!-- Top content -->
<div class="top-content">
	<div class="inner-bg">
		<div class="container">
			
			<div class="row">
				
				<div class="col-sm-12">
					<div class="form-box">
						<div class="form-top">
							<div class="form-top-left">
                                <?php if($secure_key == 'invalid'){ ?>
								<h3>Registration Error</h3>
                                <?php }else{ ?>
                                <h3>Registration Success</h3>
                                <?php } ?>
							</div>
							
						</div>
						<div class="form-bottom" style="border-top: 1px solid #ececec; background: #fff;">
							<?php if($secure_key == 'invalid'){ ?>
                            <div class="note note-danger">
								<h4 class="block">Registration error</h4>
								<p>
									 Invalid registration security key. Please back and try register again.
								</p>
							</div>
                            <?php }else{ ?>
                            <div class="alert alert-success text-center">
                                <p>
                                    Hi <?php echo ucwords(strtolower($user_profile['fullname'])); ?>, you have been successfully registered with Community Policing Malaysia
                                </p>
                                <p>
                                    We have sent SMS to <b><?php echo $user_profile['phonemobile']; ?></b> containing your One-Time-Password (OTP).
                                </p>
                                <p>
                                    Please make sure to keep your account secure all the time.
                                </p>
							</div>
                            
                            
                            
                            
                            <?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer -->

<!-- Javascript -->
<script src="<?php echo base_url(); ?>assets/register/assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/register/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/register/assets/js/jquery.backstretch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/register/assets/js/scripts.js?as=a"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/js/intlTelInput.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.1/js/utils.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/sweetalert/sweetalert.min.js"></script>

<script type="text/javascript">
$.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    return this;
  }
</script>
<script type="text/javascript">
$(function(){
    /*
        Fullscreen background
    */
    $.backstretch("<?php echo base_url(); ?>assets/register/assets/img/backgrounds/1.jpg"); 
});
</script>


<script type="text/javascript">
function blockUI() {
    $.blockUI({
        css: {
            backgroundColor: 'transparent',
            border: 'none'
        },
        message: '<div class="spinner"></div>',
        baseZ: 99999,
        overlayCSS: {
            backgroundColor: '#FFFFFF',
            opacity: 0.7,
            cursor: 'wait'
        }
    });
    $('.blockUI.blockMsg').center();
}//end Blockui
</script>
<!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
</body>
</html>