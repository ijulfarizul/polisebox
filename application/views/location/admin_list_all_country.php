<?php $this->load->view('includes/header'); ?>


<div class="table-container">
    <table id="example" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Country Code</th>
                <th>Country Name</th>
                <th>Actions<sup>(s)</sup></th>
            </tr>
        </thead>                             
    </table>
</div>



<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>
<script type="text/javascript">
var datatable_el;
$(function(){
    
    datatable_el = $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>settings/ajax_admin_list_all_country",
            "type": "POST"
        },
        "lengthMenu": [[100, 200, 500, -1], [100, 200, 500, "All"]],
        "columns": [
            { "data": "lc_code" },
            { "data": "lc_name" },
            { "data": "lc_id", "searchable": "false", "render": function ( data, type, row ) {
                redited = '';
                //redited += '<a href="javascript: void(0);" onclick="javascript: quick_edit_section1(\''+row.si_id+'\');" class="btn btn-xs green"><i class="fa fa-pencil"></i></a>';
                //redited += '<a href="javascript: void(0);" onclick="javascript: quick_delete_school(\''+row.sc_id+'\');" class="btn btn-xs red"><i class="fa fa-times"></i></a>'
                return redited
            } },
        ],
        "order": [2, 'asc']
    } );

})
</script>



