<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Qrcodes extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect(base_url());
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function admin_list_all_qrcode(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->view('qrcodes/admin/admin_list_all_qrcode', $this->data);
    }
    
    function datatable_admin_list_all_qrcode(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');
        
        $this->datatables->select('u.uacc_id');
        $this->datatables->select('u.uacc_username');
        $this->datatables->select('p.fullname');
        $this->datatables->select('p.nric');
        $this->datatables->select('qr.qrcode_id');
        $this->datatables->select('qr.serial_number');
        $this->datatables->select('qr.security_number');
        $this->datatables->from('qrcode_detail qr');
        $this->datatables->join('user_accounts u', 'qr.uacc_id=u.uacc_id', 'left');
        $this->datatables->join('user_profiles p', 'u.uacc_id=p.uacc_id', 'left');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function admin_generate_new_qrcode(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->view('qrcodes/admin/admin_generate_new_qrcode', $this->data);
    }
    
    function ajax_admin_generate_qrcode(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('far_qrcode');
        $postdata = $this->input->post('postdata');
        
        $error = array();
        $output = array();
        
        //check quantity
        if(!is_numeric($postdata['quantity'])){
            $error['quantity'] = "Quantity must be digit only";
        }
        if (!preg_match('/^[0-9]+$/', $postdata['quantity'])) {
            $error['quantity'] = "Quantity must be digit only";
        }
        if($postdata['quantity'] == 0){
            $error['quantity'] = "Quantity must be more than 0";
        }
        
        
        if(count($error) == 0){
            
            $this->far_qrcode->generate_qrcode($postdata['quantity']);
        
            $output['status'] = "success";
        }else{
            
            $output['status'] = "error";
            $output['errors'] = $error;
            $output['error_message_single'] = current($error);
        }
        
        echo json_encode($output);
    }
    
    function admin_transfer_qrcode(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        
        $this->load->view('qrcodes/admin/admin_transfer_qrcode', $this->data);
    }
    
    function ajax_dt_admin_list_available_to_transfer_qrcodes(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->library('datatables');
        $uacc_created_by = $this->user['uacc_id'];
        
        $this->datatables->select('qr.uacc_id');
        $this->datatables->select('qr.qrcode_id');
        $this->datatables->select('qr.serial_number');
        $this->datatables->select('qr.security_number');
        
        $this->datatables->from('qrcode_detail qr');
        
        $this->datatables->where('qr.uacc_id IS NULL');
        $this->datatables->where('qr.agent_uacc_id IS NULL');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    
    function ajax_admin_delete_qrcode(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $postdata = $this->input->post('postdata');
        $this->load->library('far_qrcode');
        $error = array();
        $output = array();
        
        if(count($error) == 0){
            
            //delete from database
            $this->far_qrcode->delete_qrcode($postdata['qrcode_id']);
            
            $output['status'] = 'success';
        }else{
            
            $output['status'] = 'error';
        }
        
        echo json_encode($output);
    }
    

    /**
     * Agent
     */
    function agent_list_all_qrcode(){
        $this->far_auth->allowed_group('5', $this->user['ugrp_id']);
        $this->load->view('qrcodes/agent/agent_list_all_qrcode', $this->data);
    }
    
    function datatable_agent_list_all_qrcode(){
        $this->far_auth->allowed_group('5', $this->user['ugrp_id']);
        $this->load->library('datatables');
        $uacc_id = $this->user['uacc_id'];
        
        $this->datatables->select('u.uacc_id');
        $this->datatables->select('u.uacc_username');
        $this->datatables->select('p.fullname');
        $this->datatables->select('p.nric');
        $this->datatables->select('qr.qrcode_id');
        $this->datatables->select('qr.serial_number');
        $this->datatables->select('qr.security_number');
        $this->datatables->from('qrcode_detail qr');
        $this->datatables->join('user_accounts u', 'qr.uacc_id=u.uacc_id', 'left');
        $this->datatables->join('user_profiles p', 'u.uacc_id=p.uacc_id', 'left');
        $this->datatables->where('qr.agent_uacc_id', $uacc_id);
        
        $output = $this->datatables->generate();
        echo $output;
    }
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */