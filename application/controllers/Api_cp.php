<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_cp extends MY_Controller {
    private $post;
    function __construct() 
    {
        parent::__construct();
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		$this->load->library('far_helper');
        

		$postdata_json = json_decode(file_get_contents('php://input'),true);
        if(count($postdata_json) != 0){
            $this->post = $postdata_json;
        }else{
            $this->post = $this->input->post();
        }
		
        header('Content-Type: application/json');
	}
    
    
    function submit_log(){
        $this->load->library('far_cp');
        $this->load->library('far_qrcode');
        $this->load->library('upload');
        $error = array();
        $output = array();
        
        $cp_uacc_id = $this->post['cp_uacc_id'];
        $qr_id = $this->post['qr_id'];
        
        //check if cp exists
        $cp_user_detail = $this->far_users->get_user('uacc_id', $cp_uacc_id);
        if(count($cp_user_detail) == 0){
            $error['cp_uacc_id'] = "CP not exists";
        }
        
        //check qrcode
        if(strlen($qr_id) < 10){
            //$error['qr_id'] = "Qr Code length error";
        }
        
        //check if qr exists
        $qrcode_detail = $this->far_qrcode->get_qrcode('serial_number', $qr_id);
        if(count($qrcode_detail) == 0){
            $error['qr_id'] = "Sorry this Qr Code doesnt exists";
        }
        
        //try to upload photos
        $config['upload_path'] = './assets/log_reports/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = date("Y-m-d").'--'.$cp_user_detail['uacc_id'].'-'.$qrcode_detail['serial_number'];
        //$config['max_size'] = '100';
        //$config['max_width'] = '1024';
        //$config['max_height'] = '768';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('photo')) {
            $error['photo'] = $this->upload->display_errors();
        }else{
            $photo_upload_data = $this->upload->data();
            $photo_insert_data = array(
                'photo_name' => $config['file_name'],
                'photo_url' => base_url().'assets/log_reports/'.$config['file_name']
            );
            $photo_id = $this->far_cp->insert_log_photo($photo_insert_data);
        }
        
        if(count($error) == 0){
            
            $insert_data = array(
                'cp_uacc_id' => $cp_user_detail['uacc_id'],
                'serial_number' => $qrcode_detail['serial_number']
            );
            $log_id = $this->far_cp->insert_log_report($insert_data);
            
            //process photo
            $this->far_cp->update_log_photo('photo_id', $photo_id, 'log_id', $log_id);
            
            $output = array(
                'status'    => 1,
                'error_no'  => '1000',
                'error_msg' => 'SUCCESS.',
                'object'    => $this->_get_log_report($log_id)
            );
            
        }else{
            $output = array(
                'status'    => 0,
                'error_no'  => 9997,
                'error_msg' => current($error),
                'object'    => $error
            );
        }
        
        echo json_encode($output);
    }
    
    private function _get_log_report($log_id){
        $this->load->library('far_cp');
        
        $log_detail = $this->far_cp->get_log_report('log_id', $log_id);
        
        return $log_detail;
    }
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */