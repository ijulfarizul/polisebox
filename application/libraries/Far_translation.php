<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Far_translation {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    /**
     * Get User Account
     */
    /**
    function get_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    */
    /**
    function list_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->result_array();
    }
    */
    
    /**
     * Insert / Create profile
     * @param array $data 
     */
    /**
    function insert_profile($data){
        $this->CI->db->insert('user_profiles', $data);
        return $this->CI->db->insert_id();
    }
    */
    /**
     * Update
     * @param string $key Database column name
     * @param string $key_value Database column value(s)
     * @param string $column Column name to update
     * @param string $value New value for specified column
     */
    /**
    function update_user($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('user_accounts', $data); 
    }
    */
    function list_all_meta(){

        $query = $this->CI->db->query("SELECT * FROM far_translation");

        return $query->result_array();

    }

    

    function list_all_meta_primary_secondary($primary, $secondary){

        $lang_primary = 'lang_'.$primary;

        $lang_secondary = 'lang_'.$secondary;

        

        $query = $this->CI->db->query("SELECT trans_id, trans_meta, $lang_primary, $lang_secondary FROM far_translation ORDER BY trans_create_dttm DESC");

        $rows = $query->result_array();

        return $rows;

    }

    

    function create_meta($trans_meta){

        $data = array(

            'trans_meta' => $trans_meta,

            'trans_create_dttm' => date('Y-m-d H:i:s')

        );

        $this->CI->db->insert('far_translation', $data);

        $last_insertid = $this->CI->db->insert_id();

        return $last_insertid;

    }

    

    function delete_meta($trans_id){

        $this->CI->db->where('trans_id', $trans_id);

        $this->CI->db->delete('far_translation'); 

    }

    

    function update_meta($key, $key_value, $column, $value){
        $data = array(
            $column => $value
            );
        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('far_translation', $data); 
    }

    function is_meta_exists($trans_meta){
        $query = $this->CI->db->query("SELECT * FROM far_translation WHERE trans_meta='".$trans_meta."' LIMIT 1");
        if($query->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function is_meta_exists_by_id($trans_id){
        $query = $this->CI->db->query("SELECT * FROM far_translation WHERE trans_id='".$trans_id."' LIMIT 1");
        if($query->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function list_all_language(){
        $lang = array();
        $query = $this->CI->db->query('SHOW columns FROM far_translation');
        $rows = $query->result_array();
        foreach($rows as $a => $b){
            if(substr($b['Field'], 0, 5) == 'lang_'){
                $languane_x = explode('_', $b['Field']);
                $language_code = $languane_x[1];
                $lang[] = $language_code;
            }
        }
        return $lang;
    }

    

    function get_translation_for_iso_code($language_code){
        $lang_primary = 'lang_'.$language_code;
        $query = $this->CI->db->query("SELECT trans_id, trans_meta, $lang_primary FROM far_translation ORDER BY trans_create_dttm DESC");
        $rows = $query->result_array();
        return $rows;

    }
    
    function list_idiom($language_code){
        $lang_primary = 'lang_'.$language_code;
        $query = $this->CI->db->query("SELECT trans_id, trans_meta, $lang_primary FROM far_translation ORDER BY trans_create_dttm DESC");
        $rows = $query->result_array();
        $list_lang = array();
        foreach($rows as $a => $b){
            $list_lang[$b['trans_meta']] = $b[$lang_primary];
        }
        
        return $list_lang;
    }
    
}


?>