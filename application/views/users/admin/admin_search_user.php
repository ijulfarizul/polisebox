<?php $this->load->view('includes/header'); ?> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
        	<div class="portlet-title">
        		<div class="caption">
        			<i class="fa fa-cogs"></i>Search user<sup>(s)</sup>
        		</div>
        	</div>
        	<div class="portlet-body form">
                <form action="<?php echo base_url('users/admin_search_user'); ?>" method="post" class="horizontal-form" id="search_form">
        			<div class="form-body">
        				<div class="row">
        					<div class="col-md-4">
        						<div class="form-group">
        							<label class="control-label">Fullname</label>
        							<input type="text" id="fullname" name="search_fullname" class="form-control" value="<?php echo set_value('search_fullname');?>" />
        						</div>
        					</div>
        					<!--/span-->
        					<!--
                            <div class="col-md-4">
        						<div class="form-group">
        							<label class="control-label">Serial Number</label>
        							<input type="text" id="search_serial_number" name="search_serial_number" class="form-control" value="<?php echo set_value('search_serial_number');?>" />
        						</div>
        					</div>
                            -->
        					
        					<!--/span-->
                            
                            
                            
                            
        				</div>
        				<!--/row-->
        				
        				
        			</div>
        			<div class="form-actions">
        				<button type="submit" value="search_user" name="search_user" class="btn blue pull-right"><i class="fa fa-check"></i> Search</button>
        			</div>
        		</form>
        	</div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="portlet box green">
        	<div class="portlet-title">
        		<div class="caption">
        			<i class="fa fa-cogs"></i>User<sup>(s)</sup> Information
        		</div>
        	</div>

        	<div class="portlet-body flip-scroll">
        		<?php if(is_array($search_return)){ ?>
                    <table class="table table-striped table-bordered table-hover" id="sample_1a">
                        <thead>
							<tr>
                                <th>Fullname</th>
								<th>NRIC / Passport</th>
								<th>Email</th>
                                <th>Phone number</th>

                                <th>Actions</th>

							</tr>

							</thead>

							<tbody>

                            <?php foreach($search_return as $key => $val){ ?>
                            

							<tr>
                                <td><?php echo $val['fullname'] ?></td>
								<td><?php echo $val['nric'] ?></td>

								<td><?php echo $val['email'] ?></td>
                                <td><?php echo $val['phonemobile'] ?></td>

								

                                <td>
                                    <a href="<?php echo base_url('users/admin_view_user_detail').'/?uacc_id='.$val['uacc_id']; ?>" class="btn btn-sm green margin-bottom"><i class="fa fa-search"></i> Detail</a>
                                </td>

							</tr>

							<?php } ?>

							

							

							

							</tbody>

							</table>

                    

                <?php }else{ ?>

                <table class="table table-striped table-bordered table-hover">
                        <thead>
							<tr>
                                <th>Fullname</th>
								<th>NRIC / Passport</th>
								<th>Email</th>
                                <th>Phone number</th>

                                <th>Actions</th>

							</tr>

							</thead>

							<tbody>
                                <tr>
                                    <td colspan="5" style="text-align: center;">No data</td>
                                </tr>
                            </tbody>
       </table>

                <?php } ?>
                
        	</div>

        </div>

    </div>

</div>

<?php $this->load->view('includes/footer'); ?>

<script type="text/javascript">
$(function(){
    $("#sample_1a").DataTable();
});
</script>