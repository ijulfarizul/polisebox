<?php $this->load->view('includes/header'); ?>
<?php
$user_profile = $this->far_users->get_profile('uacc_id', $logged_in['uacc_id']);

?>
<link href="<?php echo base_url(); ?>assets/admin/pages/css/profile-old.css" rel="stylesheet" type="text/css"/>

<?php
//$total_amount_far_wallet_cash = $this->far_wallet->total_single_wallet($logged_in['uacc_id'], 'far_wallet_cash');

//$total_amount_all_wallet = $this->far_wallet->total_amount_multiple_wallet($logged_in['uacc_id'], array('far_wallet_cash'));
$list_available_wallet = $this->far_meta->get_values('list_available_wallet', 'single_array');
$total_amount_all_wallet = $this->far_wallet->total_amount_multiple_wallet($logged_in['uacc_id'], $list_available_wallet);
$list_all_available_wallet = $this->far_wallet->list_all_available_wallet();
//echo '<pre>'; print_r($list_all_available_wallet); echo '</pre>';
?>
<div class="row">
	<div class="col-md-6">
		<div class="portlet sale-summary">
			<div class="portlet-title">
				<div class="caption">
					 Wallet Description
				</div>
			</div>
			<div class="portlet-body">
				<ul class="list-unstyled">
                    <?php foreach($list_all_available_wallet as $a => $b){ ?>
                    <?php
                    
                    $total_amount_for_this_wallet = $this->far_wallet->total_single_wallet($logged_in['uacc_id'], $b['wallet_table_name']);

                    ?>
					<li>
    					<span class="sale-info">
    					   <?php echo strtoupper($b['wallet_name']); ?> <i class="fa fa-img-up"></i>
    					</span>
                        <span class="sale-num">
                            <?php echo $total_amount_for_this_wallet; ?> 
                        </span>
					</li>
                    <?php } ?>
					<li>
					<span class="sale-info">
					TOTAL WALLET <i class="fa fa-img-up"></i>
					</span>
					<span class="sale-num">
					<?php echo $this->far_helper->convert_number_to_price_format($total_amount_all_wallet); ?> </span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="portlet light">
			<div class="portlet-title tabbable-line">
				<div class="caption caption-md">
					<i class="icon-globe theme-font hide"></i>
					<span class="caption-subject font-blue-madison bold uppercase"><?php echo $user_profile['firstname'].' '.$user_profile['lastname']; ?>
					</span>
				</div>
			</div>
			<div class="portlet-body">
			</div>
		</div>
	</div>
</div>

<div class="portlet box blue">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-list"></i>Transaction Detail
		</div>
	</div>
	<div class="portlet-body">
		<div class="row hidden-print">
			<div class="col-md-12 form">
				<form class="form-horizontal" role="form">
					<div class="form-body">
						<div class="form-group">
							<div class="col-md-12">
								<div class="col-md-4 col-xs-4">
									<select class="form-control select_form" id="select_month">
										<option selected="" value="00">All Month</option>
										<option value="01">January</option>
										<option value="02">February</option>
										<option value="03">March</option>
										<option value="04">April</option>
										<option value="05">May</option>
										<option value="06">June</option>
										<option value="07">July</option>
										<option value="08">August</option>
										<option value="09">September</option>
										<option value="10">October</option>
										<option value="11">November</option>
										<option value="12">December</option>
									</select>
								</div>
								<div class="col-md-4 col-xs-4">
									<select class="form-control select_form" id="select_year">
										<option selected="" value="0000">All Year</option>
										<option value="2012">2012</option>
										<option value="2013">2013</option>
										<option value="2014">2014</option>
										<option value="2015">2015</option>
										<option value="2016">2016</option>
										<option value="2017">2017</option>
										<option value="2018">2018</option>
										<option value="2019">2019</option>
									</select>
								</div>
								<div class="col-md-4 col-xs-4">
									<select class="form-control select_form" id="select_wallet">
                                        <?php foreach($list_all_available_wallet as $d => $e){ ?>
                                        <option value="<?php echo $e['wallet_table_name']; ?>"><?php echo $e['wallet_name']; ?></option>
                                        <?php } ?>
										
									</select>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div id="statement_frame">
		</div>
	</div>
</div>

<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript">
$(function(){
    
    $("#select_month, #select_year, #select_wallet").on('change', function(){
        onchange_select_date();
    })
    onchange_select_date();
});

function onchange_select_date(){
    var month = $("#select_month").val();
    var year = $("#select_year").val();
    var wallet = $("#select_wallet").val();
    
    var monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    
    
    $("#print_month").text(month);
    $("#print_year").text(year);
    
    Metronic.blockUI({
        target: '#statement_frame',
        iconOnly: true
    });
    
    $( "#statement_frame" ).load( "<?php echo base_url(); ?>statement/member_ajax_monthly_statement", { uacc_id: '<?php echo $logged_in['uacc_id']; ?>', month: month, year: year, wallet: wallet }, function() {
        Metronic.unblockUI('#statement_frame');
    });
    
    console.log(month)
    console.log(year);
}
</script>

