<?php

function apiKeyGuard() {
  $CI = &get_instance();


  $request_headers = array_change_key_case($CI->input->request_headers(), CASE_LOWER);


  if(isset($request_headers['api-key'])) {
    $api_key = $request_headers['api-key'];

    $hash_keys = [
      sha1('cpapp_ios_'.USER_APP_IOS),
      sha1('cpapp_ios_'.CP_APP_IOS),
      sha1('cpapp_android_'.USER_APP_ANDROID),
      sha1('cpapp_android_'.CP_APP_ANDROID),
      sha1('masterkey')
    ];

    if(in_array($api_key, $hash_keys)){
      return true;
    } else {
      api_response('', false, 'Invalid API Key');
    }

  } else {
    api_response('', false, 'Couldnt find api-key params in Request Headers');
  }

}

function apiAuthGuard() {
  $CI = &get_instance();
  $user = [];

  $user['error'] = '';
  $request_headers = array_change_key_case($CI->input->request_headers(), CASE_LOWER);


  if(isset($request_headers['authorization'])) {
    list($jwt) = sscanf( $request_headers['authorization'], 'Bearer %s');

    if($jwt) {
      try {
        $token = JWT::decode($jwt, JWT_KEY);

        if(property_exists($token, 'id')) {
          $user = $CI->user_model->getUserById($token->id);

          if(empty($user)) {
            api_response('', false, 'Invalid login_token', 99998);
          }

        } else {
          api_response('', false, 'Invalid login_token', 99998);
        }

      } catch (\Exception $e) {
        api_response('', false, 'Invalid login_token', 99998);
      }

    } else {
      api_response('', false, 'Couldnt find Authorization params in Request Headers', 99998);
    }
  } else {
    api_response('', false, 'Couldnt find Authorization params in Request Headers', 99998);
  }

  return $user;
}

function post_data_handler()  {
  $CI = &get_instance();

  $request_headers = array_change_key_case($CI->input->request_headers(), CASE_LOWER);

  if(!isset($request_headers['content-type'])) {
    $request_headers['content-type'] = 'application/json';
  }

  $contentType = array_map('trim', explode(';', $request_headers['content-type']));
  $post_data = $CI->post();
  if(!in_array('application/json', $contentType)) {
    $post_data = json_decode($post_data[0], true);
  }

  return $post_data;
}

function dd($var) {
  var_dump($var);
  exit();
}

function validate_api_key( $api_key = null, $timestamp = null )  {

  if( $api_key && $timestamp ) {
    $sha1 = sha1('swc_2017'.$timestamp);

    if($sha1 == $api_key){
      return true;
    }
  }

  if( $api_key == '7047a9f4d3096f435cf464657aceda6941a33b88' ) { // masterkey
    return true;
  }

  return false;
}

function currency_format($value) {
  if(empty($value)) {
    $value = 0;
  }
  return number_format($value, 2, '.', ',');
}



function token($length = 32) {
  // Create random token
  $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  
  $max = strlen($string) - 1;
  
  $token = '';
  
  for ($i = 0; $i < $length; $i++) {
    $token .= $string[mt_rand(0, $max)];
  } 
  
  return $token;
}


