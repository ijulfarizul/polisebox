<?php $this->load->view('includes/header'); ?>

<div class="clearfix" style="margin-bottom: 20px;">
    <a href="javascript: void(0);" onclick="javascript: modal_add_new_state();" class="btn green">
        Add New State <i class="fa fa-plus"></i>
    </a>
</div>

<div class="table-container">
    <table id="example" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Country Name</th>
                <th>State Name</th>
                <th>Actions<sup>(s)</sup></th>
            </tr>
        </thead>                             
    </table>
</div>

<div id="modal_add_new_state" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add new state</h4>
    </div>
    <div class="modal-body" id="modal_body_add_new_state">
        <div class="row">
            <div class="col-md-12 form-horizontal">
                
                
                
                <div class="form-group">
                    <label class="col-md-3 control-label">Country</label>
                    <div class="col-md-9">
                        <select class="form-control input-medium" id="country">
                            <option value="-1">Please select country</option>
                            <?php foreach($this->far_location->list_all_country() as $ca => $cb){ ?>
                            <option value="<?php echo $cb['lc_id']; ?>"><?php echo $cb['lc_name']; ?></option>
                            <?php } ?>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                
                
                
                <div class="form-group">
                    <label class="col-md-3 control-label">State Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter State" id="state">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn red pull-left" data-dismiss="modal">Close</button>
        <button type="button" onclick="javascript: btn_add_new_state();" class="btn green pull-right">Submit <i class="fa fa-arrow-right"></i></button>
    </div>
</div>


<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>




<script type="text/javascript">
function modal_add_new_state(){
    $("#modal_add_new_state").modal("show");
}

function btn_add_new_state(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    
    
    var country = $("#country").val();
    if(country == '-1'){
        $("#country").closest('.form-group').addClass('has-error');
        $("#country").closest('.form-group').find('.error_message').text("Please select country").show(); return false
    }
    
    //check location
    var state = $("#state").val();
    if (state === false){ 
        $("#state").closest('.form-group').addClass('has-error');
        $("#state").closest('.form-group').find('.error_message').text("State name cannot blank").show(); return false
    }
    if (state === "") {
        $("#state").closest('.form-group').addClass('has-error');
        $("#state").closest('.form-group').find('.error_message').text("State name cannot blank").show(); return false
    }
    
    Metronic.blockUI({
        target: "#modal_add_new_state",
        boxed: true,
        message: 'Sending data to server...'
    });
    
    $.ajax({
        url: '<?php echo base_url(); ?>settings/ajax_add_new_state',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                state: state,
                lc_id: country
            } 
        },
        success: function(data){
            Metronic.unblockUI("#modal_add_new_state");
            if(data.status == "success"){
                
                swal({
                    title: "Good job!",
                    text: "New state has been added",
                    type: "success",
                    closeOnConfirm: true
                },
                function(){
                    //$("#modal_add_new_state").modal("hide");
                    Metronic.blockUI({
                        target: "#modal_add_new_state",
                        boxed: true,
                        message: 'Reloading data...'
                    });
                    datatable_el.ajax.reload(function(){
                        Metronic.unblockUI("#modal_add_new_state");
                        $("#modal_add_new_state").modal("hide");
                        $("#state").val("");
                    });
                    //window.location = "<?php echo base_url(); ?>order/view/"+data.invoice_number;
                });
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
            
            
        }
    })
}
</script>

<script type="text/javascript">
var datatable_el;
$(function(){
    
    datatable_el = $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>settings/ajax_admin_list_all_state",
            "type": "POST"
        },
        "lengthMenu": [[100, 200, 500, -1], [100, 200, 500, "All"]],
        "columns": [
            { "data": "lc_name" },
            { "data": "ls_name" },
            { "data": "ls_id", "searchable": "false", "render": function ( data, type, row ) {
                redited = '';
                //redited += '<a href="javascript: void(0);" onclick="javascript: quick_edit_section1(\''+row.si_id+'\');" class="btn btn-xs green"><i class="fa fa-pencil"></i></a>';
                redited += '<a href="javascript: void(0);" onclick="javascript: quick_delete_state(\''+row.ls_id+'\');" class="btn btn-xs red"><i class="fa fa-times"></i></a>'
                return redited
            } },
        ],
        "order": [0, 'asc']
    } );

})
</script>

<script type="text/javascript">
function quick_delete_state(ls_id){
    swal({
        title: "Are you sure?",
        text: "This action cannot be undone",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
    function(){
        
        $.ajax({
            url: "<?php echo base_url(); ?>settings/ajax_admin_delete_state",
            type: "POST",
            dataType: "json",
            data: {
                postdata: {
                    ls_id: ls_id
                }
            },
            success: function(data){
                if(data.status == "success"){
                    datatable_el.ajax.reload(function(){
                        swal("Deleted!", "State has been deleted successfully", "success");
                    });
                } else {
                    var message_single = data.message_single;
                    if(message_single){
                        sweetAlert("Oops...", message_single, "error");
                    }else{
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    
                    
                }
            }
        })
        
        
    });
}
</script>



