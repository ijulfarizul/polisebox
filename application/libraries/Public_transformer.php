<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Public_transformer {
  protected $CI;

  function __construct() {
    $this->CI =& get_instance();
  }

  public function transform($data = null, $transform = null, $single = false ) {
    $res = [];
    if(!empty($data)) {
      if($single) { $data = $this->toSingleArray($data); }

      foreach($data as $k1=>$d) {
        if(is_array($d)) {
          foreach($d as $k2=>$row) {
            if(is_null($row)) {
              $data[$k1][$k2] = '';
            }
          }
        }
      }

      $res = $this->{'transform_'.$transform}($data);

      if($single) { $res = $res[0]; }
    }

    return $res;
  }

  public function toSingleArray($data) {
    $tmp[] = $data;
    return $tmp;
  }
  
  // Transform function 

  public function transform_user($data) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'user_id' => $d['uacc_id'],
        'nric' => $d['profile']['nric'],
        'fullname' => $d['profile']['fullname'],
        'username' => $d['uacc_username'],
        'phone' => $d['profile']['phonemobile'],
        'email' => $d['uacc_email'],
        'user_type_id' => $d['uacc_group_fk'],
        'user_type' => $d['user_group']['ugrp_name'],
        'login_token' => $d['uacc_login_token'],
      ];
    }
    
    return $res;
  }

  public function transform_location_area($data) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'area_id' => $d['area_id'],
        'area_name' => $d['area_name'],
        'city_name' => $d['lct_name'],
        'city_id' => $d['lct_id'],
      ];
    }
    
    return $res;
  }

  public function transform_location_city($data) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'city_id' => $d['lct_id'],
        'city_name' => $d['lct_name'],
        'state_name' => $d['ls_name'],
        'state_id' => $d['ls_id'],
      ];
    }
    
    return $res;
  }

  public function transform_location_state($data) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'state_id' => $d['ls_id'],
        'state_name' => $d['ls_name'],
        'country_name' => $d['lc_name'],
        'country_id' => $d['lc_id'],
      ];
    }
    
    return $res;
  }

  public function transform_location_country($data) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'country_id' => $d['lc_id'],
        'country_name' => $d['lc_name'],
        'country_code' => $d['lc_code'],
      ];
    }
    
    return $res;
  }

  public function transform_police_station($data) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'ps_id' => $d['ps_id'],
        'ps_name' => $d['ps_name'],
        'address' => $d['address'],
        'postcode' => $d['postcode'],
        'area' => $d['area_name'],
        'city' => $d['lct_name'],
        'state' => $d['ls_name'],
        'contact' => $d['tel_landline'],
        'email' => $d['email'],
        'distance' => round($d['distance'], 2),
      ];
    }
    
    return $res;
  }

  public function transform_announcement_list( $data ) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'announcement_id' => $d['announcement_id'],
        'title' => $d['title'],
        'date' => $d['create_dttm'],
      ];
    }
    
    return $res;
  }

  public function transform_announcement( $data ) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'announcement_id' => $d['announcement_id'],
        'title' => $d['title'],
        'message' => $d['message'],
        'date' => $d['create_dttm'],
      ];
    }
    
    return $res;
  }

  public function transform_ebox_logs_list( $data ) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'log_id' => $d['log_id'],
        'cp_fullname' => $d['fullname'],
        'cp_id_number' => $d['cp_id_number'],
        'date' => $d['create_dttm'],
      ];
    }
    
    return $res;
  }

  public function transform_ebox_logs ( $data ) {
    $res = [];

    foreach($data as $d) {
      $res[] = [
        'log_id' => $d['log_id'],
        'cp_fullname' => $d['fullname'],
        'cp_id_number' => $d['cp_id_number'],
        'log_photo' => $d['photo_url'],
        'date' => $d['create_dttm'],
      ];
    }
    
    return $res;
  }


}
