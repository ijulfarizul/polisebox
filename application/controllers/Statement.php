<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statement extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function member_account_statement(){
        $this->far_auth->allowed_group('4', $this->user['ugrp_id']);
        
        $this->load->view('statement/member_account_statement.php', $this->data);
    }
    
    function member_ajax_monthly_statement(){
        $this->far_auth->allowed_group('4', $this->user['ugrp_id']);
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $wallet = $this->input->post('wallet');
        $uacc_id = $this->input->post('uacc_id');
        
        
        
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        $list_statement = $this->far_wallet->monthly_statement_single($user_detail['uacc_id'], $month, $year, $wallet);
        
        $wallet_balance = number_format($this->far_wallet->total_single_wallet($user_detail['uacc_id'], $wallet), 2, ".", "," );
        
        $this->data['list_statement'] = $list_statement;
        $this->data['wallet_balance'] = $wallet_balance;
        $this->data['currency'] = $this->far_meta->get_value('currency_default');
        $this->data['wallet_table_name'] = $wallet;
        $this->data['user_detail'] = $user_detail;
        
        $this->load->view('statement/member_ajax_monthly_statement', $this->data);
    }
    
    function member_ajax_monthly_statement2(){
        $this->far_auth->allowed_group('4', $this->user['ugrp_id']);
        $this->load->library('far_datatable');
        $this->load->library('far_date');
        
        $iDisplayLength = intval($this->input->post('length'));
        $iDisplayStart = intval($this->input->post('start'));
        
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $wallet = $this->input->post('wallet');
        $uacc_id = $this->input->post('uacc_id');
        
        $user_detail = $this->far_users->get_user('uacc_id', $uacc_id);
        $list_statement = $this->far_wallet->monthly_statement_single($user_detail['uacc_id'], $month, $year, $wallet, 'success', $iDisplayStart, $iDisplayLength);
        $list_statement_count = $this->far_wallet->monthly_statement_single($user_detail['uacc_id'], $month, $year, $wallet, 'success');
        
        //count total record
        $iTotalRecords = count($list_statement_count);
        
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
        
        $sEcho = intval($this->input->post('draw'));
          
        $records = array();
        $records["data"] = array(); 
        
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        
        $status_list = array(
            array("success" => "Pending"),
            array("info" => "Closed"),
            array("danger" => "On Hold"),
            array("warning" => "Fraud")
        );
        
        //print_r($list_statement);
        /*
        for($i = $iDisplayStart; $i < $end; $i++) {
            $status = $status_list[rand(0, 2)];
            $id = ($i + 1);
            $records["data"][] = array(
              $id,
              $this->far_date->convert_format($list_statement[$i]['create_dttm'], 'd M Y h:i:s A'),
              $list_statement[$i]['remarks'],
              
              rand(1, 10)
           );
        }
        */
        foreach($list_statement as $a => $b){
            $id = ($a+1);
            $records["data"][] = array(
                $b['trxno'],
                $this->far_date->convert_format($b['create_dttm'], 'd M Y h:i:s A'),
                $b['remarks'],
              
                rand(1, 10)
           );
        }
        
        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }
        
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
          
        echo json_encode($records);
        
    }
    

    
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */