<?php $this->load->view('includes/header'); ?>

<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat blue-madison">
			<div class="visual">
				<i class="fa fa-users"></i>
			</div>
			<div class="details">
				<div class="number">
					 <?php echo $this->far_dashboard->count_total_user(); ?>
				</div>
				<div class="desc">
					 Total User<sup>(s)</sup>
				</div>
			</div>
			<a class="more" href="javascript:;">
			View more <i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat red-intense">
			<div class="visual">
				<i class="fa fa-user-secret"></i>
			</div>
			<div class="details">
				<div class="number">
					 <?php echo $this->far_dashboard->count_total_agent(); ?>
				</div>
				<div class="desc">
					 Total Agent<sup>(s)</sup>
				</div>
			</div>
			<a class="more" href="<?php echo base_url(); ?>agent/admin_list_all_agent">
			View more <i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat green-haze">
			<div class="visual">
				<i class="fa fa-qrcode"></i>
			</div>
			<div class="details">
				<div class="number">
					 <?php echo $this->far_dashboard->count_total_qrcode(); ?>
				</div>
				<div class="desc">
					 Total QR Code<sup>(s)</sup>
				</div>
			</div>
			<a class="more" href="<?php echo base_url(); ?>qrcodes/admin_list_all_qrcode">
			View more <i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
	
</div>

<?php $this->load->view('includes/footer'); ?>