<?php $this->load->view('includes/header'); ?>
<a href="<?php echo base_url(); ?>announcement/admin_create_announcement" class="btn blue">
    <i class="fa fa-plus"></i> Create New Announcement
</a>
<br />
<br />
<div class="table-container">
    <table id="example" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Publish date</th>
                <th>Title</th>
                <th>Message</th>
                <th>Actions<sup>(s)</sup></th>
            </tr>
        </thead>                             
    </table>
</div>
<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>
<script type="text/javascript">
$(function(){
    
    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>announcement/ajax_admin_list_all_announcement",
            "type": "POST"
        },
        "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
        "columns": [
            { "data": "create_dttm" },
            { "data": "title" },
            { "data": "message" },
            { "data": "fullname", "render": function ( data, type, row ) {
                var html = "";
                //html += '<a href="<?php echo base_url(); ?>order/view/'+row.od_invoice_number+'" class="btn btn-xs green"><i class="fa fa-search"></i> View</a>';
                return html;
            } },
        ],
        "order": [0, 'desc']
    } );

})
</script>
