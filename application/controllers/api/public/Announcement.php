<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Announcement extends REST_Controller
{
  public $post_data;

  function __construct(){
    parent::__construct();
    // Load Helper
    $this->load->helper('api_response_helper');
    $this->load->helper('common_helper');
    $this->load->helper('jwt_helper');

    // Load Library
    $this->load->library('public_transformer');
    $this->load->library('form_validation');

    // Load Model
    $this->load->model('api/common_model');
    $this->load->model('api/public/user_model');
    $this->load->model('api/public/announcement_model');

    // Retrieve $_POST data
    $this->post_data = post_data_handler();

    // API KEY guard, check for valid api-key
    apiKeyGuard();
  }

  /**
  * @api {post} /public/announcement/list Get announcement list
  * @apiName AnnouncementList
  * @apiGroup Announcement
  *
  * @apiHeader {String} api-key API Key, sha1 of "cpapp_ios_<app_version>" / "cpapp_android_<app_version>"
  * @apiHeader {String} Authorization login token example. "Bearer <login_token>"
  *
  * @apiParam {Integer} [page_number] Page number. default is 1
  * @apiParam {Integer} [data_per_page] Data per page. default is 10
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <announcement list>
  *   }
  * }
  */
  public function list_post() {
    $res = [];

    $auth_user = apiAuthGuard();

    $this->form_validation->set_data($this->post_data);
    $this->form_validation->set_rules('page_number', 'page_number', 'integer');
    $this->form_validation->set_rules('data_per_page', 'data_per_page', 'integer');

    if ($this->form_validation->run() == FALSE) {
      api_response($error_data = [], false, array_values($this->form_validation->error_array())[0]);
    }

    $page = (isset($this->post_data['page_number'])) ? $this->post_data['page_number'] : 1;
    $data_per_page = (isset($this->post_data['data_per_page'])) ? $this->post_data['data_per_page'] : 10;

    $offset = ($page == 1) ? 0 : ($page-1) * $data_per_page;
    $nextpage = $page+1;

    $conf = [
      'offset' => $offset,
      'limit' => $data_per_page
    ];
    $announcement_data = $this->announcement_model->getAnnouncement($conf);

    $res_data = $announcement_data;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res['next_page'] = $nextpage;
    $res['list'] = $this->public_transformer->transform($announcement_data, 'announcement_list');

    api_response($res);
  }

  /**
  * @api {get} /public/announcement/:announcement_id Announcement Details 
  * @apiName AnnouncementDetails
  * @apiGroup Announcement
  *
  * @apiHeader {String} api-key API Key, sha1 of "cpapp_ios_<app_version>" / "cpapp_android_<app_version>"
  * @apiHeader {String} Authorization login token example. "Bearer <login_token>"
  *
  * @apiParam {Integer} :announcement_id Announcement ID for details
  *
  * @apiSuccess {Integer} status Status of the api called.
  * @apiSuccess {Integer} error_no Error Number.
  * @apiSuccess {String} error_msg Error Message.
  * @apiSuccess {Array} object Contain result data.
  * @apiSuccessExample {json} Success-Response:
  * {
  *   "status": 1,
  *   "error_no": 1000,
  *   "error_msg": "SUCCESS",
  *   "object": { 
  *      <announcement object>
  *   }
  * }
  */
  public function index_get( $announcement_id = null ) {
    $res = [];

    $auth_user = apiAuthGuard();

    if(empty($announcement_id)) {
      api_response($error_data = [], false, 'announcement_id is required');
    }

    $conf = [
      'single' => true,
      'where' => ['announcement_id' => $announcement_id]
    ];
    $announcement_data = $this->announcement_model->getAnnouncement($conf);

    $res_data = $announcement_data;
    if(isset($res_data['error'])) {
      api_response($res_data['error']['msg'], false, $res_data['error']['msg']);
    }

    $res = $this->public_transformer->transform($announcement_data, 'announcement', true);

    api_response($res);
  }


}

