<?php $this->load->view('includes/header'); ?>
<div class="row">
    <div class="col-md-12 form-horizontal">
        <div class="form-body">
            
            
            <h3 class="block">App Version</h3>
            <div class="form-group">
                <label class="col-md-3 control-label">Android</label>
                <div class="col-md-9">
                    <div class="input-group">
				        <span class="input-group-addon">
                            <i class="fa fa-android"></i>
				        </span>
				        <input type="number" class="form-control  input-small" min="1" placeholder="Enter Version" value="<?php echo $version_android ?>" step="0.1" id="version_android" />
				    </div>
                    
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-3 control-label">IOS</label>
                <div class="col-md-9">
                    <div class="input-group">
				        <span class="input-group-addon">
                            <i class="fa fa-apple"></i>
				        </span>
				        <input type="number" class="form-control  input-small" min="1" placeholder="Enter Version" value="<?php echo $version_ios ?>" step="0.1" id="version_ios" />
				    </div>
                    
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            
            <h3 class="block">Publish Status</h3>
            <div class="form-group">
                <label class="col-md-3 control-label">Playstore</label>
                <div class="col-md-9">
                    <div class="input-group">
				        <span class="input-group-addon">
                            <i class="fa fa-android"></i>
				        </span>
				        <select class="form-control input-large" id="publish_store_status_android">
                            <option <?php if($publish_store_status_android == 'development'){ echo 'selected="selected"'; } ?> value="development">Development</option>
                            <option <?php if($publish_store_status_android == 'inreview'){ echo 'selected="selected"'; } ?> value="inreview">In Review</option>
                            <option <?php if($publish_store_status_android == 'published'){ echo 'selected="selected"'; } ?> value="published">Published</option>
                        </select>
				    </div>
                    
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-3 control-label">App Store</label>
                <div class="col-md-9">
                    <div class="input-group">
				        <span class="input-group-addon">
                            <i class="fa fa-apple"></i>
				        </span>
				        <select class="form-control input-large" id="publish_store_status_ios">
                            <option <?php if($publish_store_status_ios == 'development'){ echo 'selected="selected"'; } ?> value="development">Development</option>
                            <option <?php if($publish_store_status_ios == 'inreview'){ echo 'selected="selected"'; } ?> value="inreview">In Review</option>
                            <option <?php if($publish_store_status_ios == 'published'){ echo 'selected="selected"'; } ?> value="published">Published</option>
                        </select>
				    </div>
                    
                    <span class="help-block error_message" style="display: none;"></span>
                </div>
            </div>
            
        </div>
        <div class="form-actions">
            <div class="row">
				<div class="col-md-offset-3 col-md-9">
				    <button type="button" onclick="javascript: btn_click_submit();" class="btn green">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript">
function btn_click_submit(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    //check version_android
    var version_android = $("#version_android").val();
    if (version_android === false){ 
        $("#version_android").closest('.form-group').addClass('has-error'); 
        $("#version_android").closest('.form-group').find('.error_message').text("Version for Android cannot blank").show(); return false
    }
    if (version_android === "") {
        $("#version_android").closest('.form-group').addClass('has-error'); 
        $("#version_android").closest('.form-group').find('.error_message').text("Version for Android cannot blank").show(); return false
    }
    
    //check version_ios
    var version_ios = $("#version_ios").val();
    if (version_ios === false){ 
        $("#version_ios").closest('.form-group').addClass('has-error'); 
        $("#version_ios").closest('.form-group').find('.error_message').text("Version for IOS cannot blank").show(); return false
    }
    if (version_ios === "") {
        $("#version_ios").closest('.form-group').addClass('has-error'); 
        $("#version_ios").closest('.form-group').find('.error_message').text("Version for IOS cannot blank").show(); return false
    }
    
    var publish_store_status_android = $("#publish_store_status_android").val();
    var publish_store_status_ios = $("#publish_store_status_ios").val();
    
    Metronic.blockUI({
        boxed: true,
        message: 'Sending data to server...'
    });

    $.ajax({
        url: '<?php echo base_url(); ?>settings/admin_update_app_version',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                version_android: version_android, 
                version_ios: version_ios,
                publish_store_status_android : publish_store_status_android,
                publish_store_status_ios: publish_store_status_ios
            } 
        },
        success: function(data){
            Metronic.unblockUI();
            if(data.status == "success"){
                
                swal({
                    title: "Good job!",
                    text: "App Version has been updated",
                    type: "success",
                    closeOnConfirm: true
                })
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
            
            
        }
    })

    

}
</script>