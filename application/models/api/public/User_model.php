<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
  public $flexi_auth_config;

  function __construct(){
    parent::__construct();
    

    $this->config->load('flexi_auth', TRUE);
    $this->flexi_auth_config = $this->config->item('flexi_auth');
  }

  public function login($nric, $password) {
    $ares = [];

    if($nric && $password) {
      $this->db->select("*");
      $this->db->from('user_accounts');
      $this->db->where('uacc_username', $nric);

      $query = $this->db->get();

      if($query->num_rows()) {
        $user = $query->result_array()[0];

        $database_password = $user['uacc_password'];

        $match=false;
        if( password_verify($password, $database_password)) {
          $match=true;
        }

        if($match) {

          $token = [
            'id' => $user['uacc_id'],
            'timestamp' => strtotime('now')
          ];
          $login_token = JWT::encode($token, JWT_KEY);

          $this->updateLoginToken($login_token, $user['uacc_id']);

          $user['uacc_login_token'] = $login_token;
          $ares = $user;

          $ares['profile'] = $this->getUserProfile($user['uacc_id']);
          $ares['user_group'] = $this->getUserGroup($ares['uacc_group_fk']);

        } else {
          $ares['error']['msg'] = 'Nric and password did not match.';
          return $ares;
        }
      } else {
        $ares['error']['msg'] = 'User not found.';
        return $ares;
      }
    }

    return $ares;
  }

  public function register($post_data = []) {
    $ares = [];

    if($post_data) {

      // CP group id
      $user_group_id = USER_GROUP_ID;

      $salt = token(10);
      $static_salt = $this->flexi_auth_config['security']['static_salt'];
      $hash_password =  password_hash($post_data['password'], 1, ['salt' => $static_salt.$salt]);


      // create account
      $aval = [
        'uacc_group_fk' => $user_group_id,
        'uacc_email' => $post_data['email'],
        'uacc_username' => $post_data['nric'],
        'uacc_password' => $hash_password,
        'uacc_raw_password' => $post_data['password'],
        'uacc_ip_address' => $_SERVER['REMOTE_ADDR'],
        'uacc_salt' => $salt,
        'uacc_date_added' => date('Y-m-d H:i:s'),
        'uacc_login_token' => $user_group_id,
      ];

      $user_id = $this->common_model->add('user_accounts', $aval);

      // create profile
      $aval = [
        'uacc_id' => $user_id,
        'fullname' => $post_data['fullname'],
        'nric' => $post_data['nric'],
        'email' => $post_data['email'],
        'phonemobile' => $post_data['mobile'],
        'address_1' => $post_data['address_line_1'],
        'address_2' => $post_data['address_line_2'],
        'postcode' => $post_data['address_postcode'],
        'city' => $post_data['address_city'],
        'ls_id' => $post_data['address_state_id'],
        'lc_id' => $post_data['address_country_id'],
      ];

      $profile_id = $this->common_model->add('user_profiles', $aval);

      // create token
      $token = [
        'id' => $user_id,
        'timestamp' => strtotime('now')
      ];
      $login_token = JWT::encode($token, JWT_KEY);

      $this->updateLoginToken($login_token, $user_id);

      $ares = $this->getUserById($user_id);
    }

    return $ares;
  }

  public function updateLoginToken($login_token, $user_id) {

    if($login_token && $user_id) {
      $aset = ['uacc_login_token' => $login_token];

      $this->common_model->update('user_accounts', $aset, $user_id, 'uacc_id');
    }
  }

  public function changePassword($post_data, $user_id) {
    $ares = [];

    if($this->verifyPassword($post_data, $user_id)) {
      $salt = token(10);
      $static_salt = $this->flexi_auth_config['security']['static_salt'];
      $hash_password =  password_hash($post_data['new_password'], 1, ['salt' => $static_salt.$salt]);

      $aset = [
        'uacc_password' => $hash_password,
        'uacc_raw_password' => $post_data['new_password'],
        'uacc_salt' => $salt,
      ];

      $this->common_model->update('user_accounts', $aset, $user_id, 'uacc_id');
    } else {
      $ares['error']['msg'] = 'Wrong current password';
    }


    return $ares;
  }

  public function verifyPassword($post_data, $user_id) {
    $res = false;

    $user = $this->getUserById($user_id);

    if($user) {
      $db_password = $user['uacc_password'];
      $db_salt = $user['uacc_salt'];

      $static_salt = $this->flexi_auth_config['security']['static_salt'];

      $data["salt"]=$static_salt.$db_salt;
      if( password_hash($post_data['current_password'], 1, $data) == $db_password) {
        return true;
      } 
    }

    return false;

  }

  public function getUserById( $user_id = null ) {
    $ares = [];

    if($user_id) {
      $this->db->select('*');
      $this->db->from('user_accounts');
      $this->db->where('uacc_id', $user_id);
      $query = $this->db->get();

      if($query->num_rows()) {
        $ares = $query->result_array()[0];

        $ares['profile'] = $this->getUserProfile($user_id);
        $ares['user_group'] = $this->getUserGroup($ares['uacc_group_fk']);
        $ares['ebox'] = $this->getEbox($ares['uacc_id']);
      }
    }

    return $ares;
  }

  public function getUserProfile( $user_id = null ) {
    $ares = [];

    if($user_id) {
      $this->db->select('*');
      $this->db->from('user_profiles');
      $this->db->where('uacc_id', $user_id);
      $query = $this->db->get();

      if($query->num_rows()) {
        $ares = $query->result_array()[0];
      }
    }

    return $ares;
  }

  public function getUserGroup( $user_group_id = null ) {
    $ares = [];

    if($user_group_id) {
      $this->db->select('*');
      $this->db->from('user_groups');
      $this->db->where('ugrp_id', $user_group_id);
      $query = $this->db->get();

      if($query->num_rows()) {
        $ares = $query->result_array()[0];
      }
    }

    return $ares;
  }

  public function getEbox( $user_id = null ) {
    $ares = [];

    if($user_id) {
      $this->db->select('*');
      $this->db->from('qrcode_detail');
      $this->db->where('uacc_id', $user_id);
      $query = $this->db->get();

      if($query->num_rows()) {
        $ares = $query->result_array()[0];
      }
    }

    return $ares;
  }
  

}


