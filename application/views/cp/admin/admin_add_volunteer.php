<?php $this->load->view('includes/header'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/global/plugins/multi.js/multi.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/css/intlTelInput.css">
<style>
.iti-flag {background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/img/flags.png");}

        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
          .iti-flag {background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/img/flags@2x.png");}
        }
        
        .intl-tel-input {
            width: 100% !important;
        }

</style>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12 form-horizontal">
        <div class="form-group">
            <label class="col-md-3 control-label">Fullname</label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Fullname" id="fullname">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">IC Number</label>
            <div class="col-md-9">
                <input type="text" class="form-control input-large" placeholder="IC Number" id="nric">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">CP ID Number</label>
            <div class="col-md-9">
                <input type="text" class="form-control input-large" placeholder="CP ID Number" id="cp_id_number">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Mobile Number</label>
            <div class="col-md-9">
                <input type="text" class="form-control input-large" placeholder="Mobile Number" id="mobile_number">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-3 control-label">Country</label>
            <div class="col-md-9">
            
                <select class="form-control input-medium" id="country">
                            <option value="-1">Please select country</option>
                            <?php foreach($this->far_location->list_all_country() as $ca => $cb){ ?>
                            <option value="<?php echo $cb['lc_id']; ?>"><?php echo $cb['lc_name']; ?></option>
                            <?php } ?>
                        </select>
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        <div class="form-group">
                    <label class="col-md-3 control-label">State</label>
                    <div class="col-md-9">
                        <select class="form-control input-medium" id="state">
                            <option value="-1">Select State</option>
                            
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
        <div class="form-group">
                    <label class="col-md-3 control-label">City</label>
                    <div class="col-md-9">
                        <select class="form-control input-medium" id="city">
                            <option value="-1">Select City</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
        <div class="form-group">
                    <label class="col-md-3 control-label">Area</label>
                    <div class="col-md-9">
                        <select multiple="multiple" class="form-control input-medium" id="area">
                            <option value="-1">Select Area</option>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
        <div class="form-actions">
            <div class="row">
				<div class="col-md-offset-3 col-md-9">
				    <button type="button" onclick="javascript: btn_click_submit();" class="btn green">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->

<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/global/plugins/multi.js/multi.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/js/intlTelInput.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.0/js/utils.js"></script>
        
<script src="//cdnjs.cloudflare.com/ajax/libs/chance/1.0.11/chance.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/keypress/2.1.4/keypress.min.js"></script>
<script type="text/javascript">
var listener = new window.keypress.Listener();
$(function(){
    listener.sequence_combo("a s d", function() {
        var fullname = chance.name();
        var nric = chance.integer({min: 601000000000, max: 991000000000});
        $("#fullname").val(fullname);
        $("#nric").val(nric);
        $("#mobile_number").intlTelInput("setNumber", '+601'+chance.integer({min: 11111111, max: 99999999}));
        $("#cp_id_number").val(chance.string({length: 3, pool: 'AXGVTHKPM'})+chance.integer({min: 111111, max: 999999}))
        
    }, true);
});
</script>

<script type="text/javascript">
$(function(){
    
    $("#mobile_number").intlTelInput({
        onlyCountries: [
            "my",
            "sg",
            "id",
            "th",
            "bn",
            "hk",
            "tw",
            "mo"
        ],
        preferredCountries: [
            "my",
            "id"
        ],
        separateDialCode: true
    });
    
    //autoset country to malaysia
    $("#country").val(163);
    onchange_country();
    
    $("#country").on("change", function(){
        onchange_country();
    });
    
    //onchange_state();
    $("#state").on("change", function(){
        onchange_state();
    });
    
    //onchange_city();
    $("#city").on("change", function(){
        onchange_city();
    })
})
</script>
<script type="text/javascript">
function btn_click_submit(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    var fullname = $("#fullname").val();
    var nric = $("#nric").val();
    var areas = $("#area").val();
    var cp_id_number = $("#cp_id_number").val();
    var phonemobile = $("#mobile_number").intlTelInput("getNumber");
    
    Metronic.blockUI({
        boxed: true,
        message: 'Sending data to server...'
    });

    $.ajax({
        url: '<?php echo base_url(); ?>cp/ajax_admin_add_volunteer',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                fullname: fullname,
                nric: nric,
                phonemobile: phonemobile,
                areas: areas,
                cp_id_number: cp_id_number
            } 
        },
        success: function(data){
            Metronic.unblockUI();
            if(data.status == "success"){
                
                setTimeout(function(){ 
                    window.top.location.href = data.redirect_url;
                }, 3000);
                swal({
                    title: "Good job!",
                    text: "Volunteer has been updated",
                    type: "success",
                    closeOnConfirm: true
                }, function(){
                    window.top.location.href = data.redirect_url;
                })

                
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        if(j){
                            sweetAlert("Oops...", j, "error");
                        } else {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                        }
                        
                    }
                });
            }
            
            
        }
    })

    

}
</script>

<script type="text/javascript">
function onchange_country(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    var country = $("#country").val();
    if(country == "-1"){
        $("#country").closest('.form-group').addClass('has-error'); 
        $("#country").closest('.form-group').find('.error_message').text("Please select country").show(); return false
    }
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/list_state_by_country",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                lc_id: country
            }
        },
        success: function(data){
            if(data.status == "success"){
                var list_state = data.list_state;
                var htmlOption = "<option value='-1'>Please select state</option>"
                $.each(list_state, function(i,j){
                    htmlOption += "<option value='"+j.ls_id+"'>"+j.ls_name+"</option>"
                });
                $("#state").html(htmlOption);
            }else{
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        }
    })
}

function onchange_state(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    var state = $("#state").val();
    if(state == "-1"){
        $("#state").closest('.form-group').addClass('has-error'); 
        $("#state").closest('.form-group').find('.error_message').text("Please select state").show(); return false
    }
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/list_city_by_state",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                ls_id: state
            }
        },
        success: function(data){
            if(data.status == "success"){
                var list_city = data.list_city;
                var htmlOption = "<option value='-1'>Please select city</option>"
                $.each(list_city, function(i,j){
                    htmlOption += "<option value='"+j.lct_id+"'>"+j.lct_name+"</option>"
                });
                $("#city").html(htmlOption);
            }else{
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        }
    })
}

function onchange_city(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    var city = $("#city").val();
    if(city == "-1"){
        $("#city").closest('.form-group').addClass('has-error'); 
        $("#city").closest('.form-group').find('.error_message').text("Please select city").show(); return false
    }
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/list_area_by_city",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                lct_id: city
            }
        },
        success: function(data){
            if(data.status == "success"){
                var list_area = data.list_area;
                var htmlOption = ""
                $.each(list_area, function(i,j){
                    htmlOption += "<option value='"+j.area_id+"'>"+j.area_name+"</option>"
                });
                $("#area").html(htmlOption);
                
                var select = document.getElementById('area');
                multi( select, {
                    non_selected_header: 'Choose area',
                    selected_header: 'Selected area'
                });
                
                
            }else{
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        }
    })
}
</script>