<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model {

  function __construct(){
    parent::__construct();
    
  }

  public function add( $table = null, $aval = [] ) {
    $new_id = null;

    if($table && $aval ) {
      //$this->db->set('created_at', 'NOW()', false);
      $binsert = $this->db->insert($table, $aval);
      if($binsert) {
        $new_id = $this->db->insert_id();
      }
    }

    return $new_id;
  }

  public function update($table = null, $aset = [], $where_id = null, $where_key = null) {
    $aresult = null;

    if($table && $aset && $where_id) {
      $this->db->set($aset);

      if(empty($where_key)) {
        $where = ['id' => $where_id];
      } else {
        $where = [$where_key => $where_id];
      }

      $this->db->where($where);
      $bupdated = $this->db->update($table);

      $aresult = $bupdated;
    }

    return $aresult;
  }


}