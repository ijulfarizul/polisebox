<?php $this->load->view('includes/header'); ?>

<div class="clearfix" style="margin-bottom: 20px;">
    <a href="javascript: void(0);" onclick="javascript: modal_add_new_city();" class="btn green">
        Add New City <i class="fa fa-plus"></i>
    </a>
</div>

<div class="table-container">
    <table id="example" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Country Name</th>
                <th>State Name</th>
                <th>City Name</th>
                <th>Actions<sup>(s)</sup></th>
            </tr>
        </thead>                             
    </table>
</div>

<div id="modal_add_new_city" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add new city</h4>
    </div>
    <div class="modal-body" id="modal_body_add_new_state">
        <div class="row">
            <div class="col-md-12 form-horizontal">
                
                
                
                <div class="form-group">
                    <label class="col-md-3 control-label">Country</label>
                    <div class="col-md-9">
                        <select class="form-control input-medium" id="country">
                            <option value="-1">Please select country</option>
                            <?php foreach($this->far_location->list_all_country() as $ca => $cb){ ?>
                            <option value="<?php echo $cb['lc_id']; ?>"><?php echo $cb['lc_name']; ?></option>
                            <?php } ?>
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label">State</label>
                    <div class="col-md-9">
                        <select class="form-control input-medium" id="state">
                            <option value="-1">Select State</option>
                            
                        </select>
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label">City Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control input-medium" placeholder="Enter City" id="city">
                        <span class="help-block error_message" style="display: none;"></span>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn red pull-left" data-dismiss="modal">Close</button>
        <button type="button" onclick="javascript: btn_add_new_city();" class="btn green pull-right">Submit <i class="fa fa-arrow-right"></i></button>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/pages/scripts/table-ajax.js"></script>

<script type="text/javascript">
    $("#country").on("change", function(){
        onchange_country();
    });
function onchange_country(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    var country = $("#country").val();
    if(country == "-1"){
        $("#country").closest('.form-group').addClass('has-error'); 
        $("#country").closest('.form-group').find('.error_message').text("Please select country").show(); return false
    }
    
    $.ajax({
        url: "<?php echo base_url(); ?>settings/list_state_by_country",
        type: "POST",
        dataType: "json",
        data: {
            postdata: {
                lc_id: country
            }
        },
        success: function(data){
            if(data.status == "success"){
                var list_state = data.list_state;
                var htmlOption = "<option value='-1'>Please select state</option>"
                $.each(list_state, function(i,j){
                    htmlOption += "<option value='"+j.ls_id+"'>"+j.ls_name+"</option>"
                });
                $("#state").html(htmlOption);
            }else{
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        }
    })
}
</script>

<script type="text/javascript">
function modal_add_new_city(){
    $("#modal_add_new_city").modal("show");
}

function btn_add_new_city(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    
    
    var country = $("#country").val();
    if(country == '-1'){
        $("#country").closest('.form-group').addClass('has-error');
        $("#country").closest('.form-group').find('.error_message').text("Please select country").show(); return false
    }
    
    var state = $("#state").val();
    if(state == '-1'){
        $("#state").closest('.form-group').addClass('has-error');
        $("#state").closest('.form-group').find('.error_message').text("Please select city").show(); return false
    }
    
    //check location
    var city = $("#city").val();
    if (city === false){ 
        $("#city").closest('.form-group').addClass('has-error');
        $("#city").closest('.form-group').find('.error_message').text("City name cannot blank").show(); return false
    }
    if (city === "") {
        $("#city").closest('.form-group').addClass('has-error');
        $("#city").closest('.form-group').find('.error_message').text("City name cannot blank").show(); return false
    }
    
    Metronic.blockUI({
        target: "#modal_add_new_city",
        boxed: true,
        message: 'Sending data to server...'
    });
    
    $.ajax({
        url: '<?php echo base_url(); ?>settings/ajax_add_new_city',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                city: city,
                lc_id: country,
                ls_id: state
            } 
        },
        success: function(data){
            Metronic.unblockUI("#modal_add_new_city");
            if(data.status == "success"){
                
                swal({
                    title: "Good job!",
                    text: "New city has been added",
                    type: "success",
                    closeOnConfirm: true
                },
                function(){
                    //$("#modal_add_new_city").modal("hide");
                    Metronic.blockUI({
                        target: "#modal_add_new_city",
                        boxed: true,
                        message: 'Reloading data...'
                    });
                    datatable_el.ajax.reload(function(){
                        Metronic.unblockUI("#modal_add_new_city");
                        $("#modal_add_new_city").modal("hide");
                        $("#city").val("");
                    });
                    //window.location = "<?php echo base_url(); ?>order/view/"+data.invoice_number;
                });
                
                
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    console.log(i);
                    console.log(j)
                })
            }
            
            
        }
    })
}
</script>
<script type="text/javascript">
var datatable_el;
$(function(){
    
    datatable_el = $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "<?php echo base_url(); ?>settings/ajax_admin_list_all_city",
            "type": "POST"
        },
        "lengthMenu": [[100, 200, 500, -1], [100, 200, 500, "All"]],
        "columns": [
            { "data": "lc_name" },
            { "data": "ls_name" },
            { "data": "lct_name" },
            { "data": "lct_id", "searchable": "false", "render": function ( data, type, row ) {
                redited = '';
                //redited += '<a href="javascript: void(0);" onclick="javascript: quick_edit_section1(\''+row.si_id+'\');" class="btn btn-xs green"><i class="fa fa-pencil"></i></a>';
                redited += '<a href="javascript: void(0);" onclick="javascript: quick_delete_city(\''+row.lct_id+'\');" class="btn btn-xs red"><i class="fa fa-times"></i></a>'
                return redited
            } },
        ],
        "order": [[0,'asc'], [1,'asc'], [2,'asc']]
    } );

})
</script>

<script type="text/javascript">
function quick_delete_city(lct_id){
    swal({
        title: "Are you sure?",
        text: "This action cannot be undone",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
    function(){
        
        $.ajax({
            url: "<?php echo base_url(); ?>settings/ajax_admin_delete_city",
            type: "POST",
            dataType: "json",
            data: {
                postdata: {
                    lct_id: lct_id
                }
            },
            success: function(data){
                if(data.status == "success"){
                    datatable_el.ajax.reload(function(){
                        swal("Deleted!", "City has been deleted successfully", "success");
                    });
                } else {
                    var message_single = data.message_single;
                    if(message_single){
                        sweetAlert("Oops...", message_single, "error");
                    }else{
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    
                    
                }
            }
        })
        
        
    });
}
</script>



