<?php $this->load->view('includes/header'); ?>
<div class="row">
    <div class="col-md-12 form-horizontal">
        <div class="form-group">
            <label class="col-md-3 control-label">Title</label>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Title" id="title">
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Message</label>
            <div class="col-md-9">
                <textarea class="form-control" rows="3" id="message" placeholder="Message"></textarea>
                <span class="help-block error_message" style="display: none;"></span>
            </div>
        </div>
        
        <div class="form-actions">
            <div class="row">
				<div class="col-md-offset-3 col-md-9">
				    <button type="button" onclick="javascript: btn_click_submit();" class="btn green">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/autosize.js/4.0.0/autosize.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/chance/1.0.11/chance.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/keypress/2.1.4/keypress.min.js"></script>
<script type="text/javascript">
var listener = new window.keypress.Listener();
$(function(){
    listener.sequence_combo("a s d", function() {
        $("#title").val(chance.sentence({words: 5}));
        $("#message").val(chance.sentence({words: 50}));
        
    }, true);
});
</script>

<script type="text/javascript">
function btn_click_submit(){
    var error_el;
    $(".has-error").removeClass('has-error');
    $(".error_message").hide();
    
    var title = $("#title").val();
    var message = $("#message").val();
    
    Metronic.blockUI({
        boxed: true,
        message: 'Sending data to server...'
    });

    $.ajax({
        url: '<?php echo base_url(); ?>announcement/ajax_admin_create_announcement',
        type: 'POST',
        dataType: 'json',
        data: { 
            postdata: { 
                title: title,
                message: message
            } 
        },
        success: function(data){
            Metronic.unblockUI();
            if(data.status == "success"){
                var redirect_url = "<?php echo base_url(); ?>announcement/admin_list_all_announcement"
                setTimeout(function(){ 
                    window.top.location.href = redirect_url;
                }, 3000);
                swal({
                    title: "Good job!",
                    text: "Announcement has been added",
                    type: "success",
                    closeOnConfirm: true
                }, function(){
                    window.top.location.href = redirect_url;
                });
            }else{
                var eell = data.errors;
                $.each(eell, function(i,j){
                    var el_on_page = $("#"+i).length;
                    if (el_on_page){
                        $("#"+i).closest('.form-group').addClass('has-error');
                        $("#"+i).closest('.form-group').find('.error_message').text(j).show();
                    } else {
                        if(j){
                            sweetAlert("Oops...", j, "error");
                        } else {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                        }
                        
                    }
                });
            }
            
            
        }
    })

    

}
</script>

<script type="text/javascript">
$(function(){
    autosize($('#message')); 
});
</script>