<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Far_sms {
    private $CI;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->helper('url');
        $this->CI->load->library('session');
        $this->CI->load->database();
    }
    
    /**
     * Get User Account
     */
    /**
    function get_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    */
    /**
    function list_account($column, $value){
        $query = $this->CI->db->query("SELECT * FROM user_accounts WHERE ".$column."='".$value."'");
        return $query->result_array();
    }
    */
    
    /**
     * Insert / Create profile
     * @param array $data 
     */
    /**
    function insert_profile($data){
        $this->CI->db->insert('user_profiles', $data); 
    }
    */
    /**
     * Update
     * @param string $key Database column name
     * @param string $key_value Database column value(s)
     * @param string $column Column name to update
     * @param string $value New value for specified column
     */
    /**
    function update_user($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('user_accounts', $data); 
    }
    */
    
    function get_smsout($column, $value){
        $query = $this->CI->db->query("SELECT * FROM far_smsout WHERE ".$column."='".$value."'");
        return $query->row_array();
    }
    
    function insert_smsout($data){
        $data['sent_dttm'] = date("Y-m-d H:i:s");
        $this->CI->db->insert('far_smsout', $data); 
        $insert_id = $this->CI->db->insert_id();
        return $insert_id;
    }
    
    function update_smsout($key, $key_value, $column, $value){
        $data = array(
            $column => $value
        );

        $this->CI->db->where($key, $key_value);
        $this->CI->db->update('far_smsout', $data); 
    }
    
    function send_sms($msisdn, $message, $sms_type = 'bulksms', $uacc_id = NULL){
        $this->CI->load->library('curl');
        $this->CI->load->library('far_meta');
        
        $msisdn = preg_replace('/\D/', '', $msisdn);
        
        //check if msisdn exists
        $sent_status = 'rejected';
        if((is_numeric($msisdn)) && (strlen($msisdn) > 5)){
            $sent_status = 'sent';
        }
        
        //insert smsout
        $smsid = $this->insert_smsout();
        $this->update_smsout('fs_id', $smsid, 'uacc_id', $uacc_id);
        $this->update_smsout('fs_id', $smsid, 'msisdn', $msisdn);
        $this->update_smsout('fs_id', $smsid, 'message', $message);
        $this->update_smsout('fs_id', $smsid, 'sms_type', $sms_type);
        
        
        if($sent_status == 'sent'){
            
            $gosms_url = 'http://api.gosms.com.my/eapi/sms.aspx?company='.$this->CI->far_meta->get_value('gosms_company').'&user='.$this->CI->far_meta->get_value('gosms_username').'&password='.$this->CI->far_meta->get_value('gosms_password').'&gateway=L&mode=BUK&type=TX&hp='.$msisdn.'&mesg='.urlencode($message).'&charge=0&maskid=digi&convert=0';
            
            $return_html = $this->CI->curl->simple_get($gosms_url);
            $this->update_smsout('fs_id', $smsid, 'onewaysms_mtid', $return_html);
            
        }
        
        $this->update_smsout('fs_id', $smsid, 'sent_status', $sent_status);
        
    }
    
    function send_sms_queue($msisdn, $message, $sms_type = 'bulksms', $uacc_id = NULL){
        //check if msisdn exists
        $sent_status = 'rejected';
        if((is_numeric($msisdn)) && (strlen($msisdn) > 5)){
            $sent_status = 'queue';
        }
        
        $smsid = $this->insert_smsout();
        $this->update_smsout('fs_id', $smsid, 'uacc_id', $uacc_id);
        $this->update_smsout('fs_id', $smsid, 'msisdn', $msisdn);
        $this->update_smsout('fs_id', $smsid, 'message', $message);
        $this->update_smsout('fs_id', $smsid, 'sms_type', $sms_type);
        $this->update_smsout('fs_id', $smsid, 'sent_dttm', '0000-00-00 00:00:00');
        $this->update_smsout('fs_id', $smsid, 'sent_status', $sent_status);
    }
    
    function check_onewaysms_balance(){
        $this->CI->load->library('curl');
        $user = 'APIPSNY6HTHZE';
        $pass = 'APIPSNY6HTHZEPSNY6';
        $sms_from = 'onewaysms';
        $query_string = "bulkcredit.aspx?apiusername=".$user."&apipassword=".$pass;
        $url = "http://gateway.onewaysms.com.my:10001/".$query_string;
        
        $return_html = $this->CI->curl->simple_get($url);
        
        return $return_html;
    }
    
}


?>