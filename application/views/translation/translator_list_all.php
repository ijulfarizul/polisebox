<?php $this->load->view('includes/header'); ?>


<form action="<?php echo base_url(); ?>reports/admin_list_app_contact2" method="post" class="horizontal-form" id="searchform">

    <input type="hidden" name="search_user" value="search_user" />

    <div class="form-body">

        

        <div class="row">

            <div class="col-md-12">

                <div class="form-group pull-left">

                    <label class="control-label">Primary Language</label>

                    <select class="form-control input-large" id="filer_primary_lang">

                        <option value="nothing_selected">Please select language</option>

                    <?php foreach($list_all_language as $a => $b){ ?>

                        <option value="<?php echo $b; ?>" class="option_filter_lang"><?php echo $b; ?></option>

                    <?php } ?>

                    </select>

                </div>

                <div class="form-group pull-left">

                    <label class="control-label">Secondary Language</label>

                    <select class="form-control input-large" id="filer_secondary_lang">

                        <option value="nothing_selected">Please select language</option>

                    <?php foreach($list_all_language as $a => $b){ ?>

                        <option value="<?php echo $b; ?>" class="option_filter_lang"><?php echo $b; ?></option>

                    <?php } ?>

                    </select>

                </div>

                <div class="form-group pull-left">

                    <label class="control-label">&nbsp;</label>

                    <div>

                        <button type="button" onclick="javascript: onclick_filter_language_button();" value="filter_lang" name="filter_lang" class="btn blue"><i class="fa fa-check"></i> Filter</button>

                    </div>

                </div>

            </div>

        </div>

    </div>

    

</form>



<div class="row">

    <div class="col-md-12">

        <a href="javascript: void(0);" onclick="javascript: open_modal_add_meta();" class="btn blue"> <i class="fa fa-plus"></i> Add new meta </a>

    </div>

</div>

<hr />



<div id="frame_all_meta"></div>



<div id="modal_add_new_meta" class="modal fade modal-scroll container" tabindex="-1">

    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

        <h4 class="modal-title">Add New Meta</h4>

    </div>

    <div class="modal-body" id="modal_body_add_new_meta">

        <div class="alert alert-danger" style="display: none;" id="add_meta_error_frame">

            <strong>Error!</strong> <span id="add_meta_error_text"></span>

        </div>

        <div class="alert alert-success" style="display: none;" id="add_meta_success_frame">

            <strong>Success!</strong> Meta added.

        </div>

        <div class="row">

            <div class="col-md-12">

                <div class="form-group">

                    <label>Meta</label>

                    <input type="text" id="save_meta" class="form-control input-xlarge" placeholder="Alphabet only">

                </div>

            </div>

        </div>

        <div class="row">

            <div class="col-md-6">

                <div class="form-group">

                    <label><input id="save_lang_primary_name" type="hidden" value="en" /><span id="modal_lang_primary_text"></span></label>

                    <textarea class="form-control" rows="3" id="save_lang_primary_text"></textarea>

                </div>

            </div>

            <div class="col-md-6">

                <div class="form-group">

                    <label><input id="save_lang_secondary_name" type="hidden" value="jp" /><span id="modal_lang_secondary_text"></span></label>

                    <textarea class="form-control" rows="3" id="save_lang_secondary_text"></textarea>

                </div>

            </div>

        </div>

    </div>

    <div class="modal-footer">

        <button type="button" class="btn red" onclick="javascript: confirm_save();" id="confirm_save_btn">Save</button>

        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>

    </div>

</div>



<div id="modal_delete_meta" class="modal fade modal-scroll" tabindex="-1">

    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

        <h4 class="modal-title">Confirm Delete Meta</h4>

    </div>

    <div class="modal-body" id="modal_body_delete_meta">

        Confirm delete this meta?

        

    </div>

    <div class="modal-footer">

        <input type="hidden" id="input_hidden_trans_id" />

        <button type="button" class="btn red" onclick="javascript: confirm_delete();" id="confirm_delete_btn">Confirm</button>

        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>

    </div>

</div>



<?php $this->load->view('includes/footer'); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>

<script type="text/javascript">

$(function(){

    

    $("#save_meta").alphanumeric();

    

    //cookie for language

    var cookie_primary_lang = $.cookie('primary_lang');

    if(typeof cookie_primary_lang == 'string'){

        $("#filer_primary_lang").val(cookie_primary_lang);

    }

    var cookie_secondary_lang = $.cookie('secondary_lang');

    if(typeof cookie_secondary_lang == 'string'){

        console.log(cookie_secondary_lang)

        $("#filer_secondary_lang").val(cookie_secondary_lang);

    }

    

    onchange_filter_primary_lang();

    $("#filer_primary_lang").on('change', function(){

        onchange_filter_primary_lang();

    });

    

    var cookie_secondary_lang = $.cookie('secondary_lang');

    if(typeof cookie_secondary_lang == 'string'){

        $("#filer_secondary_lang").val(cookie_secondary_lang);

    }

    

    $('#save_meta').keyup(function() {

        this.value = this.value.toUpperCase();

    });

    

});



function open_modal_add_meta(){

    var primary = $("#filer_primary_lang").val();

    var secondary = $("#filer_secondary_lang").val();

    

    $("#filer_primary_lang").closest(".col-md-12").removeClass('has-error');

    if(primary == 'nothing_selected' || secondary == 'nothing_selected'){

        $("#filer_primary_lang").closest(".col-md-12").addClass('has-error');

        return false;

    }

    

    //set primary and secondary on modal

    $("#save_lang_primary_name").val(primary);

    $("#save_lang_secondary_name").val(secondary);

    

    $("#modal_lang_primary_text").text(primary.toUpperCase());

    $("#modal_lang_secondary_text").text(secondary.toUpperCase());

    

    

    $('#modal_add_new_meta').modal('show');

}



function confirm_save(){

    var meta = $("#save_meta").val();

    var lang_primary_name = $("#save_lang_primary_name").val();

    var lang_primary_text = $("#save_lang_primary_text").val();

    

    var lang_secondary_name = $("#save_lang_secondary_name").val();

    var lang_secondary_text = $("#save_lang_secondary_text").val();

    

    var data_to_post = {

        meta: meta,

        lang_primary_name: lang_primary_name,

        lang_primary_text: lang_primary_text,

        lang_secondary_name: lang_secondary_name,

        lang_secondary_text: lang_secondary_text

    }

    

    $("#add_meta_error_frame").hide();

    $("#add_meta_success_frame").hide();

    $.ajax({

        url: '<?php echo base_url(); ?>translation/ajax_translator_add_meta_lang',

        type: 'POST',

        dataType: 'json',

        data: { post: data_to_post },

        success: function(data){

            if(data.status == 'error'){

                $("#add_meta_error_text").text(data.single_msg);

                $("#add_meta_error_frame").show();

            }else if(data.status == 'success'){

                $("#add_meta_success_frame").show();

                

                

                $("#add_meta_success_frame").hide();

                $("#add_meta_error_frame").hide();

                

                //clear all text

                $("#save_meta").val('');

                $("#save_lang_primary_text").val('');

                $("#save_lang_secondary_text").val('');

                

                load_meta_frame(lang_primary_name, lang_secondary_name);

                $('#modal_add_new_meta').modal('hide');

                

            }

            console.log(data);

        }

    });

    

    

    

}



function onclick_filter_language_button(){

    var primary = $("#filer_primary_lang").val();

    var secondary = $("#filer_secondary_lang").val();

    

    $("#filer_primary_lang").closest(".col-md-12").removeClass('has-error');

    if(primary == 'nothing_selected' || secondary == 'nothing_selected'){

        $("#filer_primary_lang").closest(".col-md-12").addClass('has-error');

        return false;

    }

    if(primary == secondary){

        console.log('asd');

    }else{

        console.log(primary)

        console.log(secondary)

    }

    

    //return false;

    load_meta_frame(primary, secondary);

}



function load_meta_frame(primary, secondary){

    

    

    

    $('#frame_all_meta').load('<?php echo base_url(); ?>translation/ajax_translator_list_all_meta_primary_secondary', { primary: primary, secondary: secondary }, function(){

        $.cookie('primary_lang', primary);

        $.cookie('secondary_lang', secondary);

        console.log('meta loaded');

    })

}



function onchange_filter_primary_lang(){

    var selected = $("#filer_primary_lang").val();

    

    //enable all language on seconday lang

    $("#filer_secondary_lang option.option_filter_lang").removeAttr('disabled');

    

    if(selected == 'nothing_selected'){

        

    }else{

        

        //disable all

        $("#filer_secondary_lang option.option_filter_lang[value="+selected+"]").attr('disabled', 'disabled');

        

        

        

        //disable same language on secondary lang

        $("#filer_secondary_lang").val('nothing_selected')

        

    }

    

    

    

    

}





</script>