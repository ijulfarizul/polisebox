<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('api_response'))
{
    function api_response( $data = array(), $success = true, $error_msg = '', $error_no = null){
      $CI = &get_instance();

      if(!isset($error_no)) {
        $error_no = ($success) ? 1000 : 9999;
      }

      $status = ($success) ? 1 : 0; 
      $error_msg = ($success) ? "SUCCESS" : $error_msg;

      $res = [
        'status' => $status,
        'error_no' => $error_no,
        'error_msg' => $error_msg,
        'object' => $data
      ];

      if($status == 0) unset($res['object']);

      $CI->response($res, 200);
    }
}
