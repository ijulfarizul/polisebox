<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Default_controller extends MY_Controller {
    private $user;
    function __construct() 
    {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	
		
        if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
        $this->global_pass_to_view();
        $this->user = $this->flexi_auth->get_user_by_identity_row_array();
	}
    
    public function global_pass_to_view(){
        $this->data['logged_in'] = $this->flexi_auth->get_user_by_identity_row_array();
        if($this->flexi_auth->is_admin()){
            $this->data['logged_in']['is_admin'] = true;
        }
    }
    
    function datatables(){
        $this->far_auth->allowed_group('3', $this->user['ugrp_id']);
        $this->load->view('order/user_purchase_report', $this->data);
    }
    
    function ajax_datatables(){
        $this->far_auth->allowed_group('4,5', $this->user['ugrp_id']);
        $this->load->library('datatables');
        $uacc_created_by = $this->user['uacc_id'];
        
        $this->datatables->select('u.uacc_id');
        $this->datatables->select('u.uacc_username');
        $this->datatables->select('p.fullname');
        $this->datatables->select('od.od_invoice_number');
        $this->datatables->select('od.od_quantity');
        $this->datatables->select('od.od_status');
        $this->datatables->select('od.od_create_dttm');
        $this->datatables->select('pd.pd_name');
        $this->datatables->from('far_order od');
        $this->datatables->join('far_product pd', 'od.pd_id=pd.pd_id', 'left');
        $this->datatables->join('user_accounts u', 'od.uacc_id=u.uacc_id', 'left');
        $this->datatables->join('user_profiles p', 'u.uacc_id=p.uacc_id', 'left');
        
        $this->datatables->where('od.uacc_id="'.$uacc_created_by.'"');
        
        $output = $this->datatables->generate();
        echo $output;
    }
    

    
		
	
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */