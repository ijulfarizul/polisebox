<?php $this->load->view('includes/header'); ?>

<div class="table-responsive">
    <table class="table table-striped table-hover table-bordered">
        <thead>
        <tr>
            <th>
                Filename
            </th>
            <th>Size</th>
            <th>
                Create Date
            </th>
            <th>
                &nbsp;
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($files as $a => $b){ ?>
        <tr>
            <td>
                <?php echo $b['path_info']['basename']; ?>
            </td>
            <td>
                <?php echo $b['filesize']; ?>
            </td>
            <td>
                <?php echo $b['create_dttm']; ?>
            </td>
            <td>

            </td>
        </tr>
        <?php } ?>

        </tbody>
    </table>
</div>

<?php $this->load->view('includes/footer'); ?>